const agg = require('./lib/aggregator');
const filter = require('./lib/filter');
const geohelper = require('./lib/geohelper');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

let dir = ARGS[0];

let allCells = agg.getJson(dir);
// allCells = filter.filterArray({"location": {latitude: ["<41.795796", ">41.785557"]}}, allCells);

let numBssid = 0;
const threshold = 500 | ARGS[1];
let output = {
    minLat: 99.9,
    maxLat: -99.9,
    minLng: 99.9,
    maxLng: -99.9,
    data: []
};

let iter = 1;

while (allCells.length > 0) {
    console.log("Iter", iter);
    let exceedBin = [];
    let bssid = {};

    for (let i = allCells.length - 1; i >= 0; i--) {

        let tempExceed = {
            datetime: allCells[i].datetime,
            location: allCells[i].location,
            wifi_info: []
        };

        for (let j = allCells[i].wifi_info.length - 1; j >= 0; j--) {
            let idx = allCells[i].wifi_info[j].bssid;

            if (bssid[idx] === undefined) {
                bssid[idx] = {
                    datetime: [ allCells[i].datetime ],
                    location: {
                        latitude: allCells[i].location.latitude,
                        longitude: allCells[i].location.longitude
                    },
                    cell_info: [],
                    nr_info: [],
                    wifi_info: [ {
                        bssid: idx,
                        ssid: allCells[i].wifi_info[j].ssid,
                        primaryFreq: allCells[i].wifi_info[j].primaryFreq,
                        channelNum: allCells[i].wifi_info[j].channelNum,
                        width: allCells[i].wifi_info[j].width,
                        standard: allCells[i].wifi_info[j].standard
                    } ],
                    k: 1
                };
                ++numBssid;
            } else if (geohelper.measure(bssid[idx].location.latitude, bssid[idx].location.longitude, allCells[i].location.latitude, allCells[i].location.longitude) <= threshold) {
                bssid[idx].datetime.push(allCells[i].datetime);
                bssid[idx].location.latitude = bssid[idx].location.latitude + (allCells[i].location.latitude - bssid[idx].location.latitude) / bssid[idx].k;
                bssid[idx].location.longitude = bssid[idx].location.longitude + (allCells[i].location.longitude - bssid[idx].location.longitude) / bssid[idx].k;
                ++bssid[idx].k;
                let cell = {
                    bssid: idx,
                    ssid: allCells[i].wifi_info[j].ssid,
                    primaryFreq: allCells[i].wifi_info[j].primaryFreq,
                    channelNum: allCells[i].wifi_info[j].channelNum,
                    width: allCells[i].wifi_info[j].width,
                    standard: allCells[i].wifi_info[j].standard
                }
                if (filter.filterArray(cell, bssid[idx].wifi_info).length === 0) {
                    bssid[idx].wifi_info.push(cell);
                }
            } else {
                tempExceed.wifi_info.push(allCells[i].wifi_info[j]);
            }
        }

        if (tempExceed.wifi_info.length > 0) {
            exceedBin.push(tempExceed);
        }
    }

    for (let idx in bssid) {
        let temp = {
            datetime: bssid[idx].datetime,
            location: bssid[idx].location,
            cell_info: [],
            nr_info: [],
            wifi_info: bssid[idx].wifi_info
        };
        output.data.push(temp);

        output.minLat = (output.minLat > temp.location.latitude) ? temp.location.latitude : output.minLat;
        output.minLng = (output.minLng > temp.location.longitude) ? temp.location.longitude : output.minLng;
        output.maxLat = (output.maxLat < temp.location.latitude) ? temp.location.latitude : output.maxLat;
        output.maxLng = (output.maxLng < temp.location.longitude) ? temp.location.longitude : output.maxLng;
    }

    allCells = exceedBin;
    iter++;
    console.log("Num data left: ", allCells.length)
}

console.log("# of unique BSSID: " + numBssid);
fs.writeFileSync(path.join("html", "outputs", "bssid.json"), JSON.stringify(output));
