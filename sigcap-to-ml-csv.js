const agg = require('./lib/aggregator');
const filter = require('./lib/filter');
const logger = require('./lib/logger');
const mlCsv = require('./lib/ml-csv');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output CSV file] [--cell-limit <num>] [--wifi-limit <num>] [--force-label <label>] [--filter <JSON filter>]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let inputFilter;
ARGS.splice(0, 1);
let outputPath = "output-sigcap-ml.csv";
while (ARGS.length > 0) {
    switch (ARGS[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(ARGS[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            ARGS.splice(0, 2);
            break;
        }
        case "--cell-limit": {
            let limit = parseInt(ARGS[1]);
            if (!isNaN(limit)) {
                mlCsv.setLimit({ lte: limit, nr: limit });
            } else {
                console.log(`Number parse error: ${ARGS[1]}`);
                process.exit(1);
            }
            ARGS.splice(0, 2);
            break;
        }
        case "--wifi-limit": {
            let limit = parseInt(ARGS[1]);
            if (!isNaN(limit)) {
                mlCsv.setLimit({ wifi2: limit, wifi5: limit });
            } else {
                console.log(`Number parse error: ${ARGS[1]}`);
                process.exit(1);
            }
            ARGS.splice(0, 2);
            break;
        }
        case "--force-label": {
            if (!mlCsv.setForceLabel(ARGS[1])) {
                console.log(`Invalid label: ${ARGS[1]}`);
                process.exit(1);
            }
            ARGS.splice(0, 2);
            break;
        }
        default: {
            outputPath = ARGS[0];
            ARGS.splice(0, 1);
            break;
        }
    }
}

hashBin = [];

agg.callbackJsonRecursive(inputFolder, data => {
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    logger.i(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${sigcap.length}`);
    if (data.type !== "sigcap") {
        logger.e(`Current path contains non-SigCap file!`);
    }

    for (let entry of sigcap) {
        mlCsv.insert("", entry, data.path);
    }
});

mlCsv.writeAll(outputPath);
logger.i(`Done!`);
