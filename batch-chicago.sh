#!/bin/bash

for i in bronzeville downtown-chicago grant-park hyde-park south-chicago uchicago-campus ; do
    echo $i
    node storage/main.js download input/chicago/${i}/metadata-sigcap.json
    node storage/main.js download input/chicago/${i}/metadata-fcc.json
    node nr-heatmap.js input/chicago/${i}/sigcap --output html/outputs/nr-heatmaps-${i}.geojson
    node fcc-mapping.js input/chicago/${i}/fcc html/outputs/fcc-data-${i}.json
    node nr-pci.js input/chicago/${i}/sigcap --output html/outputs/pci-${i}.geojson
done

./do-upload.sh muhiqbalcr@linux.cs.uchicago.edu "~/html/chicago/" --file-list file-list-chicago.txt