const agg = require('./lib/aggregator');
const filter = require('./lib/filter');
const geohelper = require('./lib/geohelper');
const powerUtils = require('./lib/power-utils');
const dataUtils = require('./lib/data-utils');
const csvUtils = require('./lib/csv-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

const METHOD_LIST = [ "all", "centroid", "weighted", "weighted-sqrt", "weighted-freq" ];

const OPTIONS = {
    inputDir: null, // SigCap input dir
    filter: null,
    method: "weighted-freq", // Method selection, valid inputs: "all", "centroid", "weighted", "weighted-sqrt", "weighted-freq"
    distThreshold: 2000, // Distance threshold for normal centroid, default 2000 m
    kThreshold: 0,
    avgRsrpWThreshold: powerUtils.dbmToW(-200),
    avgRsrpDbmThreshold: -200,
    maxRsrpDbmThreshold: -200,
    maxRsrpDbmThreshold2: -200,
    outputPath: path.join("html", "outputs", "pci.geojson") // output path
};

if (ARGS.length < 1) {
    console.log(`
Usage: node ${path.basename(__filename)} <SigCap input dir>
            [--filter filter input string or path (default: unset)]
            [--method the methods to calculate centroid, valid inputs:
                "all": generate centroid using all methods and output as CSV instead
                "centroid": normal average
                "weighted": RSRP weighted average
                "weighted-sqrt": square root of RSRP weighted average
                "weighted-freq" (default): RSRP and operating frequency weighted average ]
            [--dist-threshold distance threshold for "centroid" method in meter (default: 2000 m)]
            [--num-threshold number of data threshold (default: 0)]
            [--avg-rsrp-threshold average RSRP threshold in dBm (default: -200 dBm)]
            [--max-rsrp-threshold maximum RSRP threshold in dBm (default: -200 dBm)]
            [--output output path with output filename (default: "html/outputs/pci.geojson")]`);
    process.exit(0);
}

while (ARGS.length > 0) {
    switch (ARGS[0]) {
        case "--method":
            if (!METHOD_LIST.includes(ARGS[1])) {
                console.log(`Method ${ARGS[1]} is invalid!`);
                process.exit(0);
            }
            OPTIONS.method = ARGS[1];
            ARGS.splice(0, 2);
            break;
        case "--dist-threshold":
            OPTIONS.distThreshold = parseFloat(ARGS[1]);
            ARGS.splice(0, 2);
            break;
        case "--num-threshold":
            OPTIONS.kThreshold = parseFloat(ARGS[1]);
            ARGS.splice(0, 2);
            break;
        case "--avg-rsrp-threshold":
            OPTIONS.avgRsrpDbmThreshold = parseFloat(ARGS[1]);
            OPTIONS.avgRsrpWThreshold = powerUtils.dbmToW(OPTIONS.avgRsrpDbmThreshold);
            ARGS.splice(0, 2);
            break;
        case "--max-rsrp-threshold":
            OPTIONS.maxRsrpDbmThreshold = parseFloat(ARGS[1]);
            OPTIONS.maxRsrpDbmThreshold2 = OPTIONS.maxRsrpDbmThreshold - 20;
            ARGS.splice(0, 2);
            break;
        case "--output":
            OPTIONS.outputPath = ARGS[1];
            ARGS.splice(0, 2);
            break;
        case "--filter":
            let jsonString = ARGS[1];
            if (jsonString.match(/\.json$/)) jsonString = fs.readFileSync(jsonString);
            OPTIONS.filter = JSON.parse(jsonString);
            ARGS.splice(0, 2);
            break;

        default:
            if (OPTIONS.inputDir !== null) {
                console.log("multiple input dir!");
                process.exit(0);
            }
            OPTIONS.inputDir = ARGS[0];
            ARGS.splice(0, 1);
            break;
    }
}

let numLte = 0;
let numNr = 0;

let output = {
    OPTIONS: {
        minLat: 99.9,
        maxLat: -99.9,
        minLng: 99.9,
        maxLng: -99.9,
        bandList: { "4g": [ "all" ], "5g": [ "all" ] }
    },
    type: "FeatureCollection",
    features: []
};

let pciTable = {};

let updateLocationObj = function(method, datapoint, input, weight) {
    switch (method) {
        case "centroid":
            datapoint.latitude += (input.latitude - datapoint.latitude) / weight;
            datapoint.longitude += (input.longitude - datapoint.longitude) / weight;
            return datapoint;
        case "weighted":
        case "weighted-sqrt":
        case "weighted-freq":
            datapoint.latitude += weight * input.latitude;
            datapoint.longitude += weight * input.longitude;
            datapoint.sumWeight += weight;
            return datapoint;
        default:
            console.log("Should not be here");
            process.exit(0);
    }
}

let updateCentroid = function(datapoint, input, j) {
    let curRsrp = input.cell_info[j].rsrp;
    let curRsrpW = powerUtils.dbmToW(curRsrp);

    // Update centroid
    ++datapoint.k;
    datapoint.datetimeIso.push(input.datetimeIso);
    datapoint.avgRsrpW += (curRsrpW - datapoint.avgRsrpW) / datapoint.k;
    datapoint.avgRsrpDbm += (curRsrp - datapoint.avgRsrpDbm) / datapoint.k;
    if (datapoint.maxRsrpDbm < curRsrp) datapoint.maxRsrpDbm = curRsrp;
    let weight, weightSqrt, weightFreq;
    switch (OPTIONS.method) {
        case "all":
            datapoint.location.centroid = updateLocationObj("centroid", datapoint.location.centroid, input.location, datapoint.k);
            weight = curRsrpW;
            datapoint.location.weighted = updateLocationObj("weighted", datapoint.location.weighted, input.location, weight);
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location.weightedSqrt = updateLocationObj("weighted-sqrt", datapoint.location.weightedSqrt, input.location, weightSqrt);
            weightFreq = curRsrpW * (input.cell_info[j].freq / 1000) * (input.cell_info[j].freq / 1000);
            datapoint.location.weightedFreq = updateLocationObj("weighted-sqrt", datapoint.location.weightedFreq, input.location, weightFreq);
            break;
        case "centroid":
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, datapoint.k);
            break;
        case "weighted":
            weight = curRsrpW;
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weight);
            break;
        case "weighted-sqrt":
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightSqrt);
            break;
        case "weighted-freq":
            weightFreq = curRsrpW * (input.cell_info[j].freq / 1000) * (input.cell_info[j].freq / 1000);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightFreq);
            break;
        default:
            console.log("Should not be here");
            process.exit(0);
    }

    // Put location into areaCal according to RSRP
    datapoint.areaCalc.all.push(input.location);
    if (curRsrp >= -70) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
        datapoint.areaCalc.excellent.push(input.location);
    } else if (curRsrp >= -80) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
    } else if (curRsrp >= -90) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
    } else if (curRsrp >= -100) {
        datapoint.areaCalc.edge.push(input.location);
    }

    // Check whether to put cell_info entry if similar entry is not already in the array
    let cell = {
        pci: input.cell_info[j].pci,
        earfcn: input.cell_info[j].earfcn,
        band: input.cell_info[j].band,
        freq: input.cell_info[j].freq,
        width: input.cell_info[j].width,
        status: (input.cell_info[j].status === "none" && input.cell_info[j].width !== 0) ? "primary" : input.cell_info[j].status ? input.cell_info[j].status : "none"
    }
    if (filter.filterArray(cell, datapoint.cell_info).length === 0) {
        datapoint.cell_info.push(cell);
    }
}

let updateCentroidNr = function(datapoint, input, j) {
    let curRsrp = input.nr_info[j].ssRsrp;
    let curRsrpW = powerUtils.dbmToW(curRsrp);

    // Update centroid
    ++datapoint.k;
    datapoint.datetimeIso.push(input.datetimeIso);
    datapoint.avgRsrpW += (curRsrpW - datapoint.avgRsrpW) / datapoint.k;
    datapoint.avgRsrpDbm += (curRsrp - datapoint.avgRsrpDbm) / datapoint.k;
    if (datapoint.maxRsrpDbm < curRsrp) datapoint.maxRsrpDbm = curRsrp;
    let weight, weightSqrt, weightFreq;
    switch (OPTIONS.method) {
        case "all":
            datapoint.location.centroid = updateLocationObj("centroid", datapoint.location.centroid, input.location, datapoint.k);
            weight = curRsrpW;
            datapoint.location.weighted = updateLocationObj("weighted", datapoint.location.weighted, input.location, weight);
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location.weightedSqrt = updateLocationObj("weighted-sqrt", datapoint.location.weightedSqrt, input.location, weightSqrt);
            weightFreq = curRsrpW * (input.nr_info[j].freq / 1000) * (input.nr_info[j].freq / 1000);
            datapoint.location.weightedFreq = updateLocationObj("weighted-sqrt", datapoint.location.weightedFreq, input.location, weightFreq);
            break;
        case "centroid":
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, datapoint.k);
            break;
        case "weighted":
            weight = curRsrpW;
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weight);
            break;
        case "weighted-sqrt":
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightSqrt);
            break;
        case "weighted-freq":
            weightFreq = curRsrpW * (input.nr_info[j].freq / 1000) * (input.nr_info[j].freq / 1000);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightFreq);
            break;
        default:
            console.log("Should not be here");
            process.exit(0);
    }

    // Put location into areaCal according to RSRP
    datapoint.areaCalc.all.push(input.location);
    if (curRsrp >= -70) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
        datapoint.areaCalc.excellent.push(input.location);
    } else if (curRsrp >= -80) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
    } else if (curRsrp >= -90) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
    } else if (curRsrp >= -100) {
        datapoint.areaCalc.edge.push(input.location);
    }

    // Check whether to put nr_info entry if similar entry is not already in the array
    let cell = {
        nrPci: input.nr_info[j].nrPci,
        nrarfcn: input.nr_info[j].nrarfcn,
        band: input.nr_info[j].band,
        freq: input.nr_info[j].freq,
        width: input.nr_info[j].width,
        status: input.nr_info[j].status
    }
    if (filter.filterArray(cell, datapoint.nr_info).length === 0) {
        datapoint.nr_info.push(cell);
    }
}

let createCentroid = function(input, j) {
    let curRsrp = input.cell_info[j].rsrp;
    let curRsrpW = powerUtils.dbmToW(curRsrp);
    let datapoint = {
        datetimeIso: [ input.datetimeIso ],
        location: {
            latitude: 0,
            longitude: 0,
            sumWeight: 0
        },
        areaCalc: {
            all: [ input.location ], //all points
            edge: [], //RSRP >= -100
            mid: [], //RSRP >= -90
            good: [], //RSRP >= -80
            excellent: [], //RSRP >= -70
        },
        cell_info: [ {
            pci: input.cell_info[j].pci,
            earfcn: input.cell_info[j].earfcn,
            band: input.cell_info[j].band,
            freq: input.cell_info[j].freq,
            width: input.cell_info[j].width,
            status: (input.cell_info[j].status === "none" && input.cell_info[j].width !== 0) ? "primary" : input.cell_info[j].status
        } ],
        nr_info: [],
        avgRsrpW: curRsrpW,
        avgRsrpDbm: curRsrp,
        maxRsrpDbm: curRsrp,
        k: 1
    };

    // Put location into areaCal according to RSRP
    if (curRsrp >= -70) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
        datapoint.areaCalc.excellent.push(input.location);
    } else if (curRsrp >= -80) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
    } else if (curRsrp >= -90) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
    } else if (curRsrp >= -100) {
        datapoint.areaCalc.edge.push(input.location);
    }

    // Update location centroid based on method
    let weight, weightSqrt, weightFreq;
    switch (OPTIONS.method) {
        case "all":
            datapoint.location = {
                centroid: {
                    latitude: 0,
                    longitude: 0
                },
                weighted: {
                    latitude: 0,
                    longitude: 0,
                    sumWeight: 0
                },
                weightedSqrt: {
                    latitude: 0,
                    longitude: 0,
                    sumWeight: 0
                },
                weightedFreq: {
                    latitude: 0,
                    longitude: 0,
                    sumWeight: 0
                }
            };
            datapoint.location.centroid = updateLocationObj("centroid", datapoint.location.centroid, input.location, datapoint.k);
            weight = curRsrpW;
            datapoint.location.weighted = updateLocationObj("weighted", datapoint.location.weighted, input.location, weight);
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location.weightedSqrt = updateLocationObj("weighted-sqrt", datapoint.location.weightedSqrt, input.location, weightSqrt);
            weightFreq = curRsrpW * (input.cell_info[j].freq / 1000) * (input.cell_info[j].freq / 1000);
            datapoint.location.weightedFreq = updateLocationObj("weighted-sqrt", datapoint.location.weightedFreq, input.location, weightFreq);
            break;
        case "centroid":
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, datapoint.k);
            break;
        case "weighted":
            weight = curRsrpW;
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weight);
            break;
        case "weighted-sqrt":
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightSqrt);
            break;
        case "weighted-freq":
            weightFreq = curRsrpW * (input.cell_info[j].freq / 1000) * (input.cell_info[j].freq / 1000);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightFreq);
            break;
        default:
            console.log("Should not be here");
            process.exit(0);
    }
    return datapoint;
}

let createCentroidNr = function(input, j) {
    let curRsrp = input.nr_info[j].ssRsrp;
    let curRsrpW = powerUtils.dbmToW(curRsrp);
    let datapoint = {
        datetimeIso: [ input.datetimeIso ],
        location: {
            latitude: 0,
            longitude: 0,
            sumWeight: 0
        },
        areaCalc: {
            all: [ input.location ], //all points
            edge: [], //RSRP >= -100
            mid: [], //RSRP >= -90
            good: [], //RSRP >= -80
            excellent: [], //RSRP >= -70
        },
        cell_info: [],
        nr_info: [ {
            nrPci: input.nr_info[j].nrPci,
            nrarfcn: input.nr_info[j].nrarfcn,
            band: input.nr_info[j].band,
            freq: input.nr_info[j].freq,
            width: input.nr_info[j].width,
            status: input.nr_info[j].status
        } ],
        avgRsrpW: curRsrpW,
        avgRsrpDbm: curRsrp,
        maxRsrpDbm: curRsrp,
        k: 1
    };

    // Put location into areaCal according to RSRP
    if (curRsrp >= -70) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
        datapoint.areaCalc.excellent.push(input.location);
    } else if (curRsrp >= -80) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
        datapoint.areaCalc.good.push(input.location);
    } else if (curRsrp >= -90) {
        datapoint.areaCalc.edge.push(input.location);
        datapoint.areaCalc.mid.push(input.location);
    } else if (curRsrp >= -100) {
        datapoint.areaCalc.edge.push(input.location);
    }

    // Update location centroid based on method
    let weight, weightSqrt, weightFreq;
    switch (OPTIONS.method) {
        case "all":
            datapoint.location = {
                centroid: {
                    latitude: 0,
                    longitude: 0
                },
                weighted: {
                    latitude: 0,
                    longitude: 0,
                    sumWeight: 0
                },
                weightedSqrt: {
                    latitude: 0,
                    longitude: 0,
                    sumWeight: 0
                },
                weightedFreq: {
                    latitude: 0,
                    longitude: 0,
                    sumWeight: 0
                }
            };
            datapoint.location.centroid = updateLocationObj("centroid", datapoint.location.centroid, input.location, datapoint.k);
            weight = curRsrpW;
            datapoint.location.weighted = updateLocationObj("weighted", datapoint.location.weighted, input.location, weight);
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location.weightedSqrt = updateLocationObj("weighted-sqrt", datapoint.location.weightedSqrt, input.location, weightSqrt);
            weightFreq = curRsrpW * (input.nr_info[j].freq / 1000) * (input.nr_info[j].freq / 1000);
            datapoint.location.weightedFreq = updateLocationObj("weighted-sqrt", datapoint.location.weightedFreq, input.location, weightFreq);
            break;
        case "centroid":
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, datapoint.k);
            break;
        case "weighted":
            weight = curRsrpW;
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weight);
            break;
        case "weighted-sqrt":
            weightSqrt = Math.sqrt(curRsrpW);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightSqrt);
            break;
        case "weighted-freq":
            weightFreq = curRsrpW * (input.nr_info[j].freq / 1000) * (input.nr_info[j].freq / 1000);
            datapoint.location = updateLocationObj(OPTIONS.method, datapoint.location, input.location, weightFreq);
            break;
        default:
            console.log("Should not be here");
            process.exit(0);
    }
    return datapoint;
}

let exceedBin = [];
let pci = {}

let processData = function (allCells) {
    for (let i = 0, length1 = allCells.length; i < length1; i++) {

        let op = dataUtils.getCleanOp(allCells[i]);
        if (pci[op] === undefined) {
            pci[op] = { "4g": {}, "5g": {} }
        }

        let tempExceed = {
            opName: op,
            datetimeIso: allCells[i].datetimeIso,
            location: allCells[i].location,
            cell_info: [],
            nr_info: []
        };

        for (let j = 0, length2 = allCells[i].cell_info.length; j < length2; j++) {
            let idx = allCells[i].cell_info[j].pci;
            let band = allCells[i].cell_info[j].band;
            // If pci === 0, purge
            if (idx === 0
                || allCells[i].cell_info[j].rsrp === 2147483647
                || allCells[i].cell_info[j].rsrp === 0
                || Array.isArray(allCells[i].cell_info[j])) {
                continue;
            }

            // If new PCI, create it and the "all" bands category
            if (pci[op]["4g"][idx] === undefined) {
                pci[op]["4g"][idx] = {}
                pci[op]["4g"][idx].all = createCentroid(allCells[i], j);
                ++numLte;
            } else if (OPTIONS.method !== "centroid"
                    || geohelper.measure(pci[op]["4g"][idx].all.location.latitude, pci[op]["4g"][idx].all.location.longitude, allCells[i].location.latitude, allCells[i].location.longitude) <= distThreshold) {
                // else, update the "all" bands category
                updateCentroid(pci[op]["4g"][idx].all, allCells[i], j);
            }

            // If new band, create the band category
            if (!output.OPTIONS.bandList["4g"].includes(band)) output.OPTIONS.bandList["4g"].push(band);
            if (pci[op]["4g"][idx][band] === undefined) {
                pci[op]["4g"][idx][band] = createCentroid(allCells[i], j);
            } else if (OPTIONS.method !== "centroid"
                    || geohelper.measure(pci[op]["4g"][idx][band].location.latitude, pci[op]["4g"][idx][band].location.longitude, allCells[i].location.latitude, allCells[i].location.longitude) <= distThreshold) {
                // else, update it
                updateCentroid(pci[op]["4g"][idx][band], allCells[i], j);
            } else {
                tempExceed.cell_info.push(allCells[i].cell_info[j]);
            }
        }

        for (let j = 0, length2 = allCells[i].nr_info.length; j < length2; j++) {
            let idx = allCells[i].nr_info[j].nrPci;
            let band = JSON.parse(allCells[i].nr_info[j].band);
            // If pci === 0, purge
            if (idx === 0 || idx === 2147483647 || band.length === 0
                || allCells[i].nr_info[j].ssRsrp === 2147483647
                || allCells[i].nr_info[j].ssRsrp === 0
                || Array.isArray(allCells[i].nr_info[j])) {
                continue;
            }

            // If new PCI, create it and the "all" bands category
            if (pci[op]["5g"][idx] === undefined) {
                pci[op]["5g"][idx] = {}
                pci[op]["5g"][idx].all = createCentroidNr(allCells[i], j);
                ++numNr;
            } else if (OPTIONS.method !== "centroid"
                    || geohelper.measure(pci[op]["5g"][idx].all.location.latitude, pci[op]["5g"][idx].all.location.longitude, allCells[i].location.latitude, allCells[i].location.longitude) <= distThreshold) {
                // else, update the "all" bands category
                updateCentroidNr(pci[op]["5g"][idx].all, allCells[i], j);
            }

            // If new band, create the band category
            for (let b of band) {
                if (!output.OPTIONS.bandList["5g"].includes(b)) output.OPTIONS.bandList["5g"].push(b);
                if (pci[op]["5g"][idx][b] === undefined) {
                    pci[op]["5g"][idx][b] = createCentroidNr(allCells[i], j);
                } else if (OPTIONS.method !== "centroid"
                        || geohelper.measure(pci[op]["5g"][idx][b].location.latitude, pci[op]["5g"][idx][b].location.longitude, allCells[i].location.latitude, allCells[i].location.longitude) <= distThreshold) {
                    // else, update it
                    updateCentroidNr(pci[op]["5g"][idx][b], allCells[i], j);
                } else {
                    tempExceed.nr_info.push(allCells[i].nr_info[j]);
                }
            }
        }

        if (tempExceed.cell_info.length > 0 || tempExceed.nr_info.length > 0) {
            exceedBin.push(tempExceed);
        }
    }
}

let savePciTable = function() {
    for (let op in pci) {
        if (pciTable[op] === undefined) pciTable[op] = {};
        for (let type in pci[op]) {
            if (pciTable[op][type] === undefined) pciTable[op][type] = {};
            for (let idx in pci[op][type]) {
                for (let band in pci[op][type][idx]) {
                    // Populate pciTable only on "all"
                    if (pciTable[op][type][band] === undefined) pciTable[op][type][band] = [];
                    if (OPTIONS.method === "all") {
                        let entry = {
                            pci: idx,
                            k: pci[op][type][idx][band].k,
                            avgRsrp: powerUtils.wToDbm(pci[op][type][idx][band].avgRsrpW),
                            avgRsrpDbm: pci[op][type][idx][band].avgRsrpDbm,
                            maxRsrp: pci[op][type][idx][band].maxRsrpDbm,
                            location: {}
                        };
                        for (let idxMethod of Object.keys(pci[op][type][idx][band].location)) {
                            entry.location[idxMethod] = {
                                latitude: pci[op][type][idx][band].location[idxMethod].latitude,
                                longitude: pci[op][type][idx][band].location[idxMethod].longitude
                            };
                            if (idxMethod.match(/^weighted/)) {
                                entry.location[idxMethod].latitude = entry.location[idxMethod].latitude / pci[op][type][idx][band].location[idxMethod].sumWeight;
                                entry.location[idxMethod].longitude = entry.location[idxMethod].longitude / pci[op][type][idx][band].location[idxMethod].sumWeight;
                            }
                        }
                        pciTable[op][type][band].push(entry);
                    } else if (pci[op][type][idx][band].maxRsrpDbm > OPTIONS.maxRsrpDbmThreshold
                            || (pci[op][type][idx][band].k > OPTIONS.kThreshold
                                && pci[op][type][idx][band].avgRsrpW > OPTIONS.avgRsrpWThreshold
                                && pci[op][type][idx][band].maxRsrpDbm > OPTIONS.maxRsrpDbmThreshold2)) {

                        // Write to output if any of the threshold pass
                        let lat = pci[op][type][idx][band].location.latitude;
                        let lng = pci[op][type][idx][band].location.longitude;
                        if (OPTIONS.method.match(/^weighted/)) {
                            lat = lat / pci[op][type][idx][band].location.sumWeight;
                            lng = lng / pci[op][type][idx][band].location.sumWeight;
                        }
                        // pci[op][type][idx][band].datetimeIso.sort();
                        let datetimeArr = [
                            pci[op][type][idx][band].datetimeIso[0],
                            pci[op][type][idx][band].datetimeIso[pci[op][type][idx][band].datetimeIso.length - 1]
                        ];
                        let featurePoint = {
                            type: "Feature",
                            properties: {
                                IDX: `${op}-${idx}-${band}`,
                                OP: op,
                                PCI: idx,
                                BAND: band,
                                TYPE: type,
                                AVG_RSRP: powerUtils.wToDbm(pci[op][type][idx][band].avgRsrpW),
                                AVG_RSRP_DBM: pci[op][type][idx][band].avgRsrpDbm,
                                MAX_RSRP: pci[op][type][idx][band].maxRsrpDbm,
                                DATETIME: datetimeArr,
                                CELL_INFO: pci[op][type][idx][band].cell_info,
                                NR_INFO: pci[op][type][idx][band].nr_info,
                                K: pci[op][type][idx][band].k
                            },
                            geometry: {
                                type: "Point",
                                coordinates: [
                                    lng,
                                    lat
                                ]
                            }
                        }
                        output.features.push(featurePoint);

                        // Find convex polygon for each area using Graham Scan
                        let polys = {
                            all: geohelper.grahamScan(pci[op][type][idx][band].areaCalc.all),
                            edge: geohelper.grahamScan(pci[op][type][idx][band].areaCalc.edge),
                            mid: geohelper.grahamScan(pci[op][type][idx][band].areaCalc.mid),
                            good: geohelper.grahamScan(pci[op][type][idx][band].areaCalc.good),
                            excellent: geohelper.grahamScan(pci[op][type][idx][band].areaCalc.excellent)
                        }
                        for (let cat in polys) {
                            let featurePoly = {
                                type: "Feature",
                                properties: {
                                    IDX: `${op}-${idx}-${band}-${cat}`,
                                    OP: op,
                                    PCI: idx,
                                    BAND: band,
                                    TYPE: "4g",
                                    CAT: cat
                                },
                                geometry: {
                                    type: "Polygon",
                                    coordinates: [[]]
                                }
                            }
                            if (polys[cat].length > 0) {
                                let loc0 = polys[cat][0];
                                for (let loc of polys[cat]) {
                                    featurePoly.geometry.coordinates[0].push([
                                        loc.longitude,
                                        loc.latitude
                                    ]);
                                }
                                featurePoly.geometry.coordinates[0].push([
                                    loc0.longitude,
                                    loc0.latitude
                                ]);

                                output.features.push(featurePoly);
                            }
                        }

                        if (output.OPTIONS.minLat > lat) output.OPTIONS.minLat = lat;
                        if (output.OPTIONS.minLng > lng) output.OPTIONS.minLng = lng;
                        if (output.OPTIONS.maxLat < lat) output.OPTIONS.maxLat = lat;
                        if (output.OPTIONS.maxLng < lng) output.OPTIONS.maxLng = lng;
                    }
                }
            }
        }
    }
}

let iter = 0;
console.log(`Iter ${iter++}`);

agg.callbackJsonRecursive(OPTIONS.inputDir, data => {
    let allCells = data.json;

    if (OPTIONS.filter) {
        allCells = filter.filterArray(OPTIONS.filter, allCells);
    }
    processData(allCells);
});

savePciTable();

while (exceedBin > 0) {
    console.log(`Item left: ${exceedBin.length}`);
    console.log(`Iter ${iter++}`);

    let allCells = exceedBin;
    exceedBin = [];
    pci = {};

    processData(allCells);
}

output.OPTIONS.bandList["4g"].sort((a,b) => {return a - b});
output.OPTIONS.bandList["5g"].sort((a,b) => {return a - b});

console.log("# of unique LTE PCI: " + numLte);
console.log("# of unique NR PCI: " + numNr);
console.log('LTE band list: ' + output.OPTIONS.bandList["4g"].join(", "));
console.log('NR band list: ' + output.OPTIONS.bandList["5g"].join(", "));
if (OPTIONS.method === "all") {
    let tempPath = OPTIONS.outputPath.replace(/\.geojson$/, ".csv");
    console.log('Writing output to ' + tempPath);
    let outString = `operator,type,band,pci,num_data,avgRsrp,avgRsrpDbm,maxRsrpDbm,`;
    let first = true;
    for (let op in pciTable) {
        for (let type in pciTable[op]) {
            for (let band in pciTable[op][type]) {
                for (let temp of pciTable[op][type][band]) {
                    if (first) {
                        first = false;
                        outString += csvUtils.unpackKeys(temp.location, "location") + "\n";
                    }
                    outString += `${op},${type},${band},${temp.pci},${temp.k},${temp.avgRsrp},${temp.avgRsrpDbm},${temp.maxRsrp},${csvUtils.unpackVals(temp.location)}\n`;
                }
            }
        }
    }
    fs.writeFileSync(tempPath, outString);
} else {
    console.log('Writing output to ' + OPTIONS.outputPath);
    fs.writeFileSync(OPTIONS.outputPath, JSON.stringify(output));
}
