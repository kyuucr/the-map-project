const agg = require('./lib/aggregator');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> <output file.csv>`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputFile = ARGS[1];


let header = `timestamp,datapointTimestamp,latitude,longitude,altitude,hor_acc,ver_acc,uuid,device_model,`
            + `svid,constellationType,azimuthDegrees,elevationDegrees,carrierFreqHz,`
            + `basebandCn0DbHz,cn0DbHz,hasAlmanacData,hasEphemerisData,usedInFix,deltaTimestampMs\n`;

let outString = "";
let totalEntries = 0;

agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;
    let curPath = data.path;

    let timeData = {};
    for (let entry of data.json) {
        if (entry.gps_info === undefined) {
            continue
        }
        for (let gpsEntry of entry.gps_info) {
            let actualTimestamp = dataUtils.cleanNumeric(gpsEntry.timestampMs);
            if (actualTimestamp) {
                outString += `${dataUtils.printDateTime(new Date(actualTimestamp))},${entry.datetimeIso},`
                        + `${entry.location.latitude},${entry.location.longitude},${entry.location.altitude},`
                        + `${entry.location.hor_acc},${entry.location.ver_acc},${dataUtils.cleanString(entry.uuid)},${dataUtils.cleanString(entry.deviceName)},`
                        + `${dataUtils.cleanNumeric(gpsEntry.svid)},`
                        + `${dataUtils.cleanString(gpsEntry.constellationType)},`
                        + `${dataUtils.cleanNumeric(gpsEntry.azimuthDegrees)},`
                        + `${dataUtils.cleanNumeric(gpsEntry.elevationDegrees)},`
                        + `${dataUtils.cleanNumeric(gpsEntry.carrierFreqHz)},`
                        + `${dataUtils.cleanNumeric(gpsEntry.basebandCn0DbHz)},`
                        + `${dataUtils.cleanNumeric(gpsEntry.cn0DbHz)},`
                        + `${gpsEntry.hasAlmanacData},`
                        + `${gpsEntry.hasEphemerisData},`
                        + `${gpsEntry.usedInFix},`
                        + `${dataUtils.cleanNumeric(gpsEntry.deltaTimestampMs)}\n`;
                totalEntries++;
            }
        }
    }

});

if (outString.length > 0) {
    console.log(`Writing to: ${outputFile}, # of GPS entries: ${totalEntries}`);
    fs.writeFileSync(outputFile, header + outString);
}
console.log(`DONE!`);