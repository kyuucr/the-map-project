const OPERAND_LIST = [ "=", "~", ">", "<" ];

let filter = {
    filterArray: function(filter, arr, isReverse = false) {
        let temp = [];
        // Sanity check
        if (!Array.isArray(filter)) {
          filter = [ filter ];
        }
        // For each filter, add filtered results to temp
        // since this is inclusive, it may outputs 2 or more
        // same datapoints depends on the filters
        for (let i = 0, length1 = filter.length; i < length1; i++) {
            for (let j = 0, length2 = arr.length; j < length2; j++) {
                if (isReverse ? !this.compare(filter[i], arr[j]) : this.compare(filter[i], arr[j])) {
                    temp.push(arr[j]);
                }
            }
        }

        return temp;
    },

    compare: function(filter, obj, isDate = false) {
        let ret;
        if( Object.prototype.toString.call( filter ) === '[object Object]' ) {
            // If object, iterate members
            ret = true;
            for(let param in filter) {
                // If obj is array, iterate over obj's entries
                if (obj[param] !== undefined) {
                    if (Array.isArray(obj[param])) {
                        let tempRet = false;
                        for (let i = obj[param].length - 1; i >= 0; i--) {
                            tempRet = tempRet || this.compare(filter[param], obj[param][i], (param === 'datetimeIso' || param === 'local_datetime'));
                        }
                        ret = ret && tempRet;
                    } else {
                        ret = ret && this.compare(filter[param], obj[param], (param === 'datetimeIso' || param === 'local_datetime'));
                    }
                } else {
                    ret = false;
                }
            }
        } else if (Array.isArray(filter)) {
            // If filter is array, do exclusive filter on its entries
            // (assuming obj is neither array nor object)
            ret = true;
            for (let i = filter.length - 1; i >= 0; i--) {
                ret = ret && this.compare(filter[i], obj, isDate);
            }
        } else {
            // Check first char from filter
            let operand = "";
            if (filter.substring !== undefined) operand = filter.substring(0,1);
            if (OPERAND_LIST.includes(operand)) {
                filter = filter.substring(1, filter.length);
            }
            if (isDate) {
                filter = new Date(filter).getTime();
                obj = new Date(obj).getTime();
            } else if (filter === "undefined") {
                filter = undefined;
            }
            // Special case if obj is string
            if (typeof obj === "string"
                    && typeof filter === "string") {
                switch (operand) {
                    case "=":
                        ret = obj.includes(filter);
                        break;
                    case "~":
                        ret = !obj.includes(filter);
                        break;
                    default:
                        ret = obj.includes(filter);
                        break;
                }
            } else {
                switch (operand) {
                    case "=":
                        ret = (filter == obj);
                        break;
                    case "~":
                        ret = (filter != obj);
                        break;
                    case ">":
                        ret = (filter < obj);
                        break;
                    case "<":
                        ret = (filter > obj);
                        break;
                    default:
                        ret = (filter == obj);
                        break;
                }
            }
        }
        return ret;
    }
}


if (typeof module !== "undefined") module.exports = filter