const parseLine = function(line, sep) {
    // Keep comma inside double quotes as one column
    let lineRegex = new RegExp(`("[^"]+"|[^${sep}]*)${sep}|([^${sep}]+$)`, `g`);
    let whitespaceRegex = new RegExp(`^[" \uFEFF]+|[" \uFEFF${sep}\r]+$`, `g`);

    // let colSeparated = lineRegex.exec(line);
    let colSeparated = line.match(lineRegex);
    if (colSeparated && colSeparated.length > 0) {
        // Remove double quotes, whitespace, etc at both ends
        colSeparated = colSeparated.map(val => {
            return val.replace(whitespaceRegex, "");
        });
    }
    return colSeparated;
}

const csvUtils = {
    unpackKeys: function(obj, prev = "", skipKey = []) {
        if (skipKey && !Array.isArray(skipKey)) {
            skipKey = [ skipKey ];
        }
        let ret = "";
        for (let key of Object.keys(obj)) {
            if (skipKey.includes(key)) continue;
            if (Object.prototype.toString.call( obj[key] ) === '[object Object]' ) {
                ret += this.unpackKeys(obj[key], `${prev}${prev !== "" ? "." : ""}${key}`);
            } else {
                ret += `${prev}${prev !== "" ? "." : ""}${key}` + ",";
            }
        }
        return ret;
    },

    getBlanks: function(obj, prev = "", skipKey = []) {
        if (skipKey && !Array.isArray(skipKey)) {
            skipKey = [ skipKey ];
        }
        let ret = "";
        for (let key of Object.keys(obj)) {
            if (skipKey.includes(key)) continue;
            if (Object.prototype.toString.call( obj[key] ) === '[object Object]' ) {
                ret += this.getBlanks(obj[key], "");
            } else {
                ret += ",";
            }
        }
        return ret;
    },

    unpackVals: function(obj, protect = false, skipKey = []) {
        if (skipKey && !Array.isArray(skipKey)) {
            skipKey = [ skipKey ];
        }
        let ret = "";
        if (Object.prototype.toString.call( obj ) === '[object Object]') {
            for (let key of Object.keys(obj)) {
                if (skipKey.includes(key)) continue;
                ret += this.unpackVals(obj[key], protect || (key == "extra"));
            }
        } else {
            if (protect && typeof obj === "string") {
                obj = obj.replace(/\"/g, "\"\"")
                obj = `\"${obj}\"`;
            }
            ret += obj + ",";
        }
        return ret;
    },

    parse: function(obj, sep = ",") {
        let arr = [];
        if (typeof obj === 'string') {
            arr = obj.split("\n");
        } else {
            let prevI = 0;
            for (let i = 0, length1 = obj.length; i < length1; i++ ) {
                if (obj[i] == 10) {
                    arr.push(obj.toString("utf-8", prevI, i));
                    prevI = ++i;
                }
            }
        }
        for(let i = 0, length1 = arr.length; i < length1; i++){
            arr[i] = parseLine(arr[i], sep);
        }
        return arr;
    },

    parseCallback: function(buffer, inputPath, cb, sep = ",") {
        let prevI = 0;
        let lineNum = 0;
        for (let i = 0, length1 = buffer.length; i < length1; i++ ) {
            if (buffer[i] == 10) {
                let line = buffer.toString("utf-8", prevI, i);
                let colSeparated = parseLine(line, sep);
                if (colSeparated && colSeparated.length > 0) {
                    cb({ inputPath: inputPath, lineNum: ++lineNum, line: colSeparated });
                }
                prevI = ++i;
            }
        }
    }
}

module.exports = csvUtils;