const sumReducer = (accumulator, currentValue) => accumulator + currentValue;

let statsUtils = {
    meanArray: function(arr) {
        if (arr.length === 0) return NaN;
        return (arr.reduce(sumReducer) / arr.length);
    },

    cdfArray: function(arr) {
        if (arr.length === 0) return [];
        return arr.sort((a, b) => (a - b));
    },

    sumArray: function(arr) {
        if (arr.length === 0) return 0;
        return arr.reduce(sumReducer);
    },

    stdDevArray: function(arr) {
        if (arr.length === 0) return NaN;
        let mean = this.meanArray(arr);
        let sumSquared = 0;
        for (let i = 0, length1 = arr.length; i < length1; i++) {
            sumSquared += Math.pow((arr[i] - mean), 2);
        }
        return Math.sqrt(sumSquared / arr.length);
    }
}

module.exports = statsUtils;