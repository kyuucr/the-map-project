function getLevelNum (level) {
    switch (level) {
        case "info": {
            return 4;
        }
        case "error": {
            return 3;
        }
        case "warning": {
            return 2;
        }
        case "debug": {
            return 1;
        }
        case "verbose": {
            return 0;
        }
        default: {
            return -1;
        }
    }
}

function checkBarActive (acc, cur) {
    return acc.isActive || cur.isActive;
}

let logger = {
    level: 3, // Default level is error
    rewrite: false,
    progressBars: [],

    setLevel: function (level) {
        this.level = getLevelNum(level);
        if (this.level === -1) {
            this.level = 3;
            return false;
        }
        return true;
    },

    writeDown: function (level = "info", ...msg) {
        let levelNum = getLevelNum(level);
        if (levelNum === -1) throw `${level} is not a valid log level!`;
        if (levelNum >= this.level) {
            // If any of the tracked progress bar is currently active, then clear line first
            if (this.progressBars.length > 0 && this.progressBars.reduce(checkBarActive)) {
                process.stdout.clearLine();
                process.stdout.cursorTo(0);
            }
            if (getLevelNum(level) < 4) {
                console.log(level.toUpperCase(), ...msg);
            } else {
                console.log(...msg);
            }
        }
    },

    i: function (...msg) {
        this.writeDown("info", ...msg);
    },

    e: function (...msg) {
        this.writeDown("error", ...msg);
    },

    w: function (...msg) {
        this.writeDown("warning", ...msg);
    },

    d: function (...msg) {
        this.writeDown("debug", ...msg);
    },

    v: function (...msg) {
        this.writeDown("verbose", ...msg);
    }
}

module.exports = logger