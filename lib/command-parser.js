const logger = require('./logger');
const path = require('path');
const { type } = require('os');

const commands = {};
let mandatoryCount = 0;
const keywordIds = [];
const nonKeywordIds = [];

const createArguments = function() {
    let args = "";

    Object.keys(commands).forEach(id => {
        const temp = [];
        if (commands[id].isFlag) {
            temp.push(commands[id].keyword);
        } else {
            if (commands[id].keyword) {
                temp.push(commands[id].keyword);
                temp.push(`<${id}>`);
            } else {
                temp.push(id);
            }
        }

        if (args !== "") {
            args += " ";
        }
        args += `${commands[id].isMandatory ? "<" : "[" }`
                + `${temp.join(" ")}`
                + `${commands[id].isMandatory ? ">" :  "]"}`
    });
    args += `${args !== "" ? " " : ""}[-h] [--help]`;

    return args;
}

const showHelp = function() {
    console.log(`Usage: nodejs ${path.basename(module.parent.filename)} ${createArguments()}`);
}

const showFullHelp = function() {
    showHelp();
    // TODO implement description and default values for each argument
}

const setValue = function(value, transform) {
    if (typeof transform === 'function') {
        return transform(value);
    } else {
        return value;
    }
}

const parser = {

    addArgument: function(id, options) {
        commands[id] = {
            keyword: options.keyword || null,
            checker: options.checker || null,
            transform: options.transform || null,
            isFlag: options.isFlag || false,
            isMandatory: options.isMandatory || false,
            value: (options.isFlag && !options.defaultValue) ? false : (options.defaultValue || null),
            set: false // Only used by non-keyword arguments
        };
        if (options.isMandatory) {
            mandatoryCount++;
        }
        if (options.keyword) {
            keywordIds.push(id);
        } else {
            nonKeywordIds.push(id);
        }
    },

    parse: function() {
        const args = process.argv.slice(2);
        if (args.length < mandatoryCount) {
            console.log("Not enough argument!");
            showHelp();
            process.exit(1);
        }

        while (args.length > 0) {
            // Help argument takes precedence
            if (args[0] === "-h") {
                showHelp();
                process.exit(0);
            } else if (args[0] === "--help") {
                showFullHelp();
                process.exit(0);
            }

            // Next, log-level
            if (args[0] === "--log-level") {
                if (!args[1] || !logger.setLevel(args[1])) {
                    logger.e(`Log level invalid: ${args[1]}`);
                    process.exit(0);
                }
                logger.setLevel(args[1]);
                args.splice(0, 2);
                continue;
            }

            let found = keywordIds.some(id => {
                if (commands[id].keyword === args[0]) {
                    if (commands[id].isFlag) {
                        commands[id].value = true;
                        args.splice(0, 1);
                        return true;
                    } else if (commands[id].checker == null
                                || (typeof commands[id].checker === 'function'
                                    && commands[id].checker(args[1]))) {
                        commands[id].value = setValue(args[1], commands[id].transform);
                        args.splice(0, 2);
                        return true;
                    }
                }
                return false;
            }, false);

            if (!found) {
                found = !nonKeywordIds.every(id => {
                    if (commands[id].set) {
                        return true;
                    } else {
                        commands[id].value = setValue(args[0], commands[id].transform);
                        commands[id].set = true;
                        args.splice(0, 1);
                        return false;
                    }
                });
            }

            if (!found) {
                console.log(`Unknown argument: ${args[0]}`);
                process.exit(1);
            }
        }

        const options = {};
        Object.keys(commands).forEach(id => {
            options[id] = commands[id].value;
        });
        return options;
    }
}

module.exports = parser;