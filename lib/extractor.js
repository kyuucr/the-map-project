const dataUtils = require("./data-utils");
const filter = require("./filter");
const logger = require("./logger");
const fs = require("fs/promises");
const path = require("path");
const unzipper = require("unzipper");

const writeBuffer = [];

const options = {
    numChunk: 0,
    maxData: 5000,
    samplingRateMillisec: 0,
    filter: null,
    callbackEntry: null
}

let totalNumData = 0;

const setOptions = function (inputOptions) {
    if (typeof inputOptions === `object`) {
        for (let key in inputOptions) {
            if (options[key] !== undefined) {
                logger.v(`extractor: found option key ${key}, value ${inputOptions[key]}`);
                options[key] = inputOptions[key];
            } else {
                logger.e(`extractor: option key ${key} does not exists!`);
            }
        }
    } else {
        logger.e(`extractor: options is not object!`);
    }
}

const flushBuffer = async function (outputPath) {
    if (writeBuffer.length > 0) {
        let filePath = path.join(outputPath, `chunk${options.numChunk++}.json`);
        logger.v(`extractor: flushing writeBuffer, size ${writeBuffer.length}, to ${filePath}`);

        // Reset buffer since after transferring it to content
        let content = `[${writeBuffer.join(",")}]`;
        writeBuffer.length = 0;

        fs.writeFile(filePath, content).catch(err => {
            logger.e(`extractor: error when writing ${filePath} `, err.message);
        });
    }
}

const writeFile = function (outputPath, buffer) {
    totalNumData++;
    writeBuffer.push(buffer.toString());
    if (writeBuffer.length >= options.maxData) {
        flushBuffer(outputPath);
    }
}

const unzip = async function (zipFilePath, outputPath) {
    logger.d(`Processing ${zipFilePath}`);

    const samplingTable = [];
    logger.d(`Sampling table === 0 ?`, samplingTable.length === 0);

    let fd = await fs.open(zipFilePath);

    // return new Promise((resolve, reject) => {
        return fd.createReadStream(zipFilePath)
            .pipe(unzipper.Parse())
            .on("entry", entry => {
                // We got a zip entry
                let pass = false;

                if (entry.type === "File" && entry.path.match(/\.(json|txt)$/)) {
                    logger.v(`extractor: new zip entry ${entry.path}, type ${entry.type}`);
                    // Set to true before preliminary filtering
                    pass = true;

                    // Filter by datetime using filename
                    if (options.datetimeFilter) {
                        let fn = path.basename(entry.path).replace(/\.(json|txt)$/, "");
                        let datetimeObj = { datetimeIso: dataUtils.getCleanDatetime(fn) };
                        pass = filter.compare(datetimeFilter, datetimeObj);
                        logger.d(`extractor: fn ${fn} datetimeObj`, datetimeObj, `pass? ${pass}`);
                    }

                    // Check if we already sampled it
                    if (options.samplingRateMillisec) {
                        let fn = path.basename(entry.path).replace(/\.(json|txt)$/, "");
                        let sampledIdx = parseInt(new Date(dataUtils.getCleanDatetime(fn)).getTime() / options.samplingRateMillisec);
                        if (samplingTable.includes(sampledIdx)) {
                            logger.d(`extractor: sample fn ${fn} already exist at ${sampleIdx}`);
                            pass = false;
                        } else {
                            logger.d(sampledIdx, samplingTable.includes(sampledIdx));
                            samplingTable.push(sampledIdx);
                        }
                    }
                }

                if (pass) {
                    // File has passed the preliminary filters, process the content buffer
                    entry.buffer().then(content => {
                        logger.v(`extractor: processing file ${entry.path}`);

                        // Test if it is a proper formatted JSON
                        let contentJson = null;
                        try {
                            contentJson = JSON.parse(content);
                        } catch(err) {
                            throw new Error(`JSON parse error at ${entry.path} zip file ${zipFilePath}, ${err.message}`);
                        }

                        // include zip filename into the data
                        contentJson.zipFilename = path.basename(zipFilePath);
                        content = JSON.stringify(contentJson);

                        // Additional filter
                        if (!options.filter || filter.compare(metadata.filter, contentJson)) {
                            writeFile(outputPath, content);
                        }
                    });
                } else {
                    // Ignore entry if not file, or does not pass preliminary filter
                    logger.v(`zipfile entry ignored: ${zipFilePath}`);
                    entry.autodrain();
                }
            // }).on("error", err => {
            //     reject(err);
            // }).on("end", () => {
            //     resolve(zipFilePath);
            // });
        }).promise();
    // });
}

const extractor = {

    /**
     * inputZipFiles: array of zip files or string of one zip file
     * outputPath: string path to output directory
     * options: {
     *      numChunk: start of chunk numbering (default: 0),
     *      maxData: maximum number of data in a chunk (default: 5000),
     *      samplingRateMillisec: sampling data in ms (default: 0),
     *      filter: object as filter (default: null),
     *      callbackEntry: function to call when a zip file has been processed (default: null)
     * }
     * return promise
     */
    extractZipFiles: async function (inputZipFiles, outputPath, options = {}) {
        if (typeof inputZipFiles === 'string') {
            inputZipFiles = [ inputZipFiles ];
        }
        setOptions(options);
        logger.v(`extractor: options`, options);

        // Get datetimeFilter only for sigcap
        let datetimeFilter;
        if (options.filter && options.filter.datetimeIso) {
            options.datetimeFilter = { datetimeIso: options.filter.datetimeIso };
            logger.d(`extractor: using datetimeFilter`, options.datetimeFilter);
        }

        const promises = [];
        const results = [];

        for (let inputZip of inputZipFiles) {
            promises.push(
                unzip(inputZip, outputPath).then(data => {
                    logger.d(`extractor: done processing zipfile ${inputZip}`);

                    // Add success result
                    results.push({ file: inputZip, status: "fulfilled" });

                    // Callback on each entry
                    if (typeof options.callbackEntry === `function`) {
                        options.callbackEntry(inputZip);
                    }
                }).catch(err => {
                    // Report errors
                    logger.w(`extractor: error processing zipfile ${inputZip}`, err);
                    results.push({ file: inputZip, status: "rejected", reason: err });
                })
            );
        }

        // This await will block this async function until all promises settled
        // but it does not block the parent process
        await Promise.allSettled(promises).finally(() => {
            // Flush the remaining buffer
            flushBuffer(outputPath);

            logger.v(`extractor: done processing all files!`);
        });

        // Resolve promise with results and stats
        return {
            results: results,
            numChunk: options.numChunk,
            totalNumData: totalNumData
        };
    },
}

module.exports = extractor;