const fs = require('fs');
const path = require('path');
const csvUtils = require('./csv-utils');
const dataUtils = require('./data-utils');

let aggregator = {
    getJson: function(dir) {
        let jsonOutput = [];

        let filenames = fs.readdirSync(dir);
        filenames.forEach(function(filename) {
            if (filename.match(/\.(json|txt)$/)) {
                try {
                    let json = JSON.parse(fs.readFileSync(path.join(dir, filename)));
                    if (Array.isArray(json)) {
                        jsonOutput = jsonOutput.concat(json);
                    } else {
                        jsonOutput.push(json);
                    }
                } catch(e) {
                    console.log(filename, e);
                }
            }
        });

        return jsonOutput;
    },

    getJsonRecursive: function(dir) {
        let output = [];
        let currOutput = [];

        let filenames = fs.readdirSync(dir);
        let obj = this;
        filenames.forEach(function(filename) {
            // console.log(filename);
            if (fs.statSync(path.join(dir, filename)).isDirectory()) {
                output = output.concat(obj.getJsonRecursive(path.join(dir, filename)));
            } else if (filename.match(/\.(json|txt)$/)) {
                try {
                    let json = JSON.parse(fs.readFileSync(path.join(dir, filename)));
                    if (Array.isArray(json)) {
                        currOutput = currOutput.concat(json);
                    } else {
                        currOutput.push(json);
                    }
                } catch(e) {
                    console.log(filename, e);
                }
            }
        });
        if (currOutput.length > 0) {
            output.push({path: dir, json: currOutput});
        }
        return output;
    },

    getFileListRecursive: function(inputPath, regex = undefined) {
        let files = [];
        if (fs.existsSync(inputPath) && fs.lstatSync(inputPath).isDirectory()) {
            let filenames = fs.readdirSync(inputPath);
            filenames.forEach(filename => {
                files = files.concat(this.getFileListRecursive(path.join(inputPath, filename), regex));
            });
        } else if (!regex || (regex.exec && regex.exec(inputPath))) {
            files.push(inputPath);
        }
        return files;
    },

    callbackJsonRecursive: function(inputPath, callback, insideDir = false) {
        if (fs.existsSync(inputPath) && fs.lstatSync(inputPath).isDirectory()) {
            let filenames = fs.readdirSync(inputPath);
            let currOutput = [];
            let currFilePath = [];
            for(let i = 0, length1 = filenames.length; i < length1; i++){
                let temp = this.callbackJsonRecursive(path.join(inputPath, filenames[i]), callback, true);
                if (temp !== undefined) {
                    currOutput = currOutput.concat(temp.json);
                    currFilePath.push(temp.filePath);
                }
                if (currOutput.length > 4000) {
                    callback({path: inputPath, filePath: currFilePath, type: dataUtils.getJSONType(currOutput[0]), json: currOutput});
                    currOutput = [];
                    currFilePath = [];
                }
            }
            if (currOutput.length > 0) {
                callback({path: inputPath, filePath: currFilePath, type: dataUtils.getJSONType(currOutput[0]), json: currOutput});
            }
        } else if (inputPath.match(/\.(json|txt)$/)) {
            let json = [];
            try {
                json = JSON.parse(fs.readFileSync(inputPath));
                if (!Array.isArray(json)) {
                    json = [ json ];
                }
            } catch(e) {
                console.log(inputPath, e);
            }

            if (json.length > 0) {
                if (!insideDir) {
                    callback({path: path.dirname(inputPath), filePath: inputPath, type: dataUtils.getJSONType(json[0]), json: json});
                } else {
                    return { path: path.dirname(inputPath), filePath: inputPath, type: dataUtils.getJSONType(json[0]), json: json };
                }
            }
        }
        return undefined;
    },

    /**
     * getCsv from inputPath, this function does not check if file is actually CSV!
     * return: {
     *     path: input path,
     *     csv: 2d-array of csv data,
     *     header: 1d-array of csv header
     *     headerObj: object mapping column name to column index (e.g., headerObj["col0"] = 0)
     * }
     **/
    getCsv: function(inputPath, sep) {
        let csv = csvUtils.parse(fs.readFileSync(inputPath), sep);
        let header = csv.shift();
        let headerObj = {};
        header.forEach((col, idx) => {
            headerObj[col] = idx;
        });
        return {path: inputPath, csv: csv, header: header, headerObj: headerObj};
    },

    /**
     * Recursively find .csv and .txt files from inputPath and parse it
     * callback:    fn({ path, csv, header})
     * fullParse:   if true then parse the whole file and callback on it
     *              if false then parse and callback per line (used for large files)
     **/
    callbackCsvRecursive: function(inputPath, callback, fullParse = true, csvSep = ',') {
        if (fs.existsSync(inputPath) && fs.lstatSync(inputPath).isDirectory()) {
            let filenames = fs.readdirSync(inputPath);
            filenames.forEach(filename => {
                this.callbackCsvRecursive(path.join(inputPath, filename), callback, fullParse, csvSep);
            });
        } else if (inputPath.match(/\.(csv|txt|dat)$/)) {
            if (fullParse) {
                let csvObj = this.getCsv(inputPath, csvSep);
                if (csvObj.csv.length > 0) {
                    callback(csvObj);
                }
            } else {
                csvUtils.parseCallback(fs.readFileSync(inputPath), inputPath, callback, csvSep);
            }
        }
    }
}

module.exports = aggregator;