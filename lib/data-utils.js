const crypto = require("crypto");
const fs = require("fs");
const cellHelper = require("./cell-helper");
const statsUtils = require("./stats-utils");
const usualCarriers = [
    "AT&T",
    "Sprint",
    "T-Mobile",
    "Verizon",
];

const serviceStateKeys = {
    nrStatus            : "nrState",
    nrAvailable         : "isNrAvailable",
    dcNrRestricted      : "isDcNrRestricted",
    enDcAvailable       : "isEnDcAvailable",
    nrFrequencyRange    : "mNrFrequencyRange",
    ci                  : "mCi",
    cellBandwidths      : "mCellBandwidths",
    usingCA             : "mIsUsingCarrierAggregation",
}

let getKeyValue = function(obj, key) {
    if (obj && obj[key]) return obj[key];
    return undefined;
}

const nrStatusList = {
    "connected": 4,
    "not restricted" : 3,
    "restricted" : 2,
    "none" : 1,
    "N/A" : 0
}

const bandDesignation = {
    1:  [ 2100, "mid" ],
    2:  [ 1900, "mid" ],
    3:  [ 1800, "mid" ],
    4:  [ 1700, "mid" ],
    5:  [ 850, "low" ],
    7:  [ 2600, "mid" ],
    8:  [ 900, "low" ],
    11: [ 1500, "mid" ],
    12: [ 700, "low" ],
    13: [ 700, "low" ],
    14: [ 700, "low" ],
    17: [ 700, "low" ],
    18: [ 850, "low" ],
    19: [ 850, "low" ],
    20: [ 800, "low" ],
    21: [ 1500, "mid" ],
    24: [ 1600, "mid" ],
    25: [ 1900, "mid" ],
    26: [ 850, "low" ],
    28: [ 700, "low" ],
    29: [ 700, "low" ],
    30: [ 2300, "mid" ],
    31: [ 450, "low" ],
    32: [ 1500, "mid" ],
    34: [ 2000, "mid" ],
    35: [ 1900, "mid" ],
    36: [ 1900, "mid" ],
    37: [ 1900, "mid" ],
    38: [ 2600, "mid" ],
    39: [ 1900, "mid" ],
    40: [ 2300, "mid" ],
    41: [ 2500, "mid" ],
    42: [ 3500, "mid" ],
    43: [ 3700, "mid" ],
    44: [ 700, "low" ],
    45: [ 1500, "mid" ],
    46: [ 5200, "mid" ],
    47: [ 5900, "mid" ],
    48: [ 3500, "mid" ],
    49: [ 3500, "mid" ],
    50: [ 1500, "mid" ],
    51: [ 1500, "mid" ],
    52: [ 3300, "mid" ],
    53: [ 2500, "mid" ],
    65: [ 2100, "mid" ],
    66: [ 1700, "mid" ],
    67: [ 700, "low" ],
    68: [ 700, "low" ],
    69: [ 2600, "mid" ],
    70: [ 1700, "mid" ],
    71: [ 600, "low" ],
    72: [ 450, "low" ],
    73: [ 450, "low" ],
    74: [ 1500, "mid" ],
    75: [ 1500, "mid" ],
    76: [ 1500, "mid" ],
    85: [ 700, "low" ],
    87: [ 410, "low" ],
    88: [ 410, "low" ],
}

const apiToAndroidVer = {
    1: "1",
    2: "1.1",
    3: "1.5",
    4: "1.6",
    5: "2",
    6: "2.0.1",
    7: "2.1",
    8: "2.2.3",
    9: "2.3.2",
    10: "2.3.7",
    11: "3",
    12: "3.1",
    13: "3.2.6",
    14: "4.0.2",
    15: "4.0.4",
    16: "4.1.2",
    17: "4.2.2",
    18: "4.3.1",
    19: "4.4.4",
    20: "4.4W.2",
    21: "5.0.2",
    22: "5.1.1",
    23: "6.0.1",
    24: "7",
    25: "7.1.2",
    26: "8",
    27: "8.1",
    28: "9",
    29: "10",
    30: "11",
    31: "12",
    32: "12L",
    33: "13"

}

let dataUtils = {

    sortNumDsc: (a,b) => b - a,
    sortNumAsc: (a,b) => a - b,

    getAndroidVersion: function(apiVer) {
        if (apiVer !== undefined) {
            return apiToAndroidVer[apiVer] ? apiToAndroidVer[apiVer] : `API${apiVer}`;
        } else {
            return "N/A";
        }
    },

    createCdfY: function(arrLen) {
        let out = new Array(arrLen);
        let len = arrLen - 1;
        for (let i = 0; i < arrLen; i++) {
            out[i] = i / len;
        }
        return out;
    },

    parseJSONStringOrFile: function(input) {
        // Need to be surrounded by try catch
        let retObj;
        if (input.match(/\.json$/) && fs.existsSync(input)) {
            retObj = JSON.parse(fs.readFileSync(input));
        } else {
            retObj = JSON.parse(input);
        }
        return retObj;
    },

    getServiceState: function(sigcap, key) {
        if (sigcap[key]) {
            return sigcap[key];
        } else if (sigcap.serviceState) {
            let regex = RegExp(`${serviceStateKeys[key]}\\s*=\\s*[\\d\\w]+[, ]`, "g");
            let matches = sigcap.serviceState.match(regex);
            if (matches) {
                let retNrStatus = "N/A";
                for(let i = 0, length1 = matches.length; i < length1; i++){
                    let params = matches[i].replace(/[, ]+$/, "").split(/[ =]/);
                    let ret = params[params.length - 1];
                    if (key === "nrFrequencyRange") {
                        switch (ret) {
                            case 1: return "low-band";
                            case 2: return "mid-band";
                            case 3: return "high-band";
                            case 4: return "mmWave";
                            default: return "unknown";
                        }
                    } else if (key === "nrStatus") {
                        ret = ret.toLocaleLowerCase('en-US').replace(/_/, " ");
                        if (ret === "connected") return ret;
                        else if (nrStatusList[ret] > nrStatusList[retNrStatus]) retNrStatus = ret;
                    } else {
                        return ret;
                    }
                }
                if (key === "nrStatus") {
                    return retNrStatus;
                } else {
                    throw "Should not be reached";
                }
            } else {
                return "N/A";
            }
        } else {
            return "N/A";
        }
    },

    getCellBw: function(sigcap) {
        let cellBwStr = this.getServiceState(sigcap, "cellBandwidths");
        if (cellBwStr && cellBwStr.match(/\[.+\]/)) {
            return eval(cellBwStr);
        }
        return [ NaN ];
    },

    sumCellBw: function(sigcap) {
        let sum = statsUtils.sumArray(this.getCellBw(sigcap));
        if (sum >= 2147483647) {
            sum = NaN;
        }
        return sum;
    },

    getJSONType: function(data) {
        let ret = "unknown";
        if (data.version && typeof data.version === 'string') {
            ret = "fcc";
        } else if (data.version && typeof data.version === 'number') {
            ret = "sigcap";
        }
        return ret;
    },

    getCleanOp: function(data) {
        let op;
        // TODO: replace with getJSONType after cleared testing
        if (data.opName === undefined) {
            // FCC data
            for (let testName in data.tests) {
                if (testName === "testId") continue;
                if (data.tests[testName].environment.beginning.wifi || data.tests[testName].environment.end.wifi) return "WiFi";
                op = getKeyValue(data.tests[testName].environment.beginning.telephony, "cellular_operator_name")
                    || getKeyValue(data.tests[testName].environment.end.telephony, "cellular_operator_name")
                    || getKeyValue(data.tests[testName].environment.beginning.telephony, "sim_operator_name")
                    || getKeyValue(data.tests[testName].environment.end.telephony, "sim_operator_name");
                if (op !== undefined) {
                    break;
                }
            }
            // Last fallback, very inaccurate
            if (op === undefined) {
                // Try carrier_name, or even sigcap's fields
                op = data.carrier_name ? data.carrier_name : data.simName ? data.simName : data.carrierName ? data.carrierName : "Unknown";
            }
        } else {
            // SigCap
            op = data.opName;
            if (op === "" || op === "Searching for Service" || op === "Extended Network" || op === "Preferred System") {
                op = data.simName ? data.simName : data.carrierName ? data.carrierName : "Unknown";
            }
        }
        op = op.replace(/ +$/, "");
        op = op.replace(/^ +/, "");
        for(let k = 0, length3 = usualCarriers.length; k < length3; k++){
            if (op !== usualCarriers[k] && op.startsWith(usualCarriers[k])) {
                op = usualCarriers[k];
            }
        }
        return op;
    },

    getNetworkIcon: function(sigcap) {
        let op = this.getCleanOp(sigcap);
        let nrStatus = this.getServiceState(sigcap, "nrStatus");
        let nrAvailable = this.getServiceState(sigcap, "nrAvailable");
        let dcNrRestricted = this.getServiceState(sigcap, "dcNrRestricted");
        let enDcAvailable = this.getServiceState(sigcap, "enDcAvailable");
        let nrFrequencyRange = this.getServiceState(sigcap, "nrFrequencyRange");

        let networkType = this.getNetworkType(sigcap);
        if (networkType === undefined) networkType = sigcap.cell_info && sigcap.cell_info.length > 0 ? "4G" : "Unknown";

        if (nrStatus === "connected" || networkType.startsWith("NR") || (sigcap.nr_info && sigcap.nr_info.length > 0)) {
            if (nrFrequencyRange === "mmWave"
                    || sigcap.nr_info.some(nrCell =>
                        cellHelper.getNrarfcnBandCode(nrCell.nrarfcn) === "mmwave"
                    )
                ) {
                if (op === "Verizon") {
                    return "5G UW";
                } else if (op === "AT&T") {
                    return "5G+";
                } else {
                    return "5G (mmWave)"
                }
            } else if (op === "Verizon"
                    && sigcap.nr_info.some(nrCell =>
                        cellHelper.getNrarfcnBandCode(nrCell.nrarfcn) === "cband"
                    )
                ) {
                return "5G UW"
            } else if (op === "T-Mobile"
                    && sigcap.nr_info.some(nrCell =>
                        cellHelper.nrarfcnToBand(nrCell.nrarfcn).match(/n41/)
                    )
                ) {
                return "5G UC"
            } else {
                return "5G"
            }
        } else if (nrStatus === "not restricted"
                && nrAvailable === "true"
                && enDcAvailable === "true"
                && dcNrRestricted === "false") {
            return "5G (NR not restricted)";
        } else if (op === "AT&T"
                && networkType === "LTE"
                && sigcap.cell_info.length > 1) {
            // TODO check this
            return "5Ge (AT&T)"
        } else {
            return networkType;
        }
    },

    getNetworkType: function(data) {
        let networkType;
        if (this.getJSONType(data) === "sigcap") {
            // We don't trust network type from SigCap
            let hasNr = data.nr_info && data.nr_info.length > 0;
            let hasPrimaryNr = data.nr_info && data.nr_info.some(val => val.status === "primary");
            let hasLte = data.cell_info && data.cell_info.length > 0;
            if (hasNr && hasPrimaryNr && !hasLte) {
                networkType = "NR";
            } else if (hasNr && hasLte) {
                networkType = "NR-NSA";
            } else if (hasLte) {
                networkType = "LTE";
            } else if (data.networkType) {
                networkType = data.networkType;
            } else {
                networkType = "unknown";
            }
        } else {
            // Else FCC ST
            if (data.tests) data = data.tests.download;
            if (data.environment) {
                if (data.environment.beginning.wifi && data.environment.end.wifi) {
                    networkType = "Wi-Fi";
                } else if (data.environment.beginning.network && data.environment.end.network) {
                    let start = data.environment.beginning.network.subtype_name;
                    let end = data.environment.end.network.subtype_name;
                    networkType = (start === end) ? start : "Mixed"
                }
            }
        }
        return networkType ? networkType : "Unknown";
    },

    getCleanDatetime: function(sigcap) {
        let datetimeIso;
        if (typeof sigcap === "object") {
            datetimeIso = sigcap.datetimeIso;
            if (datetimeIso === undefined) {
                datetimeIso = `${this.getDate(sigcap.datetime.date)}T${this.getTime(sigcap.datetime.time, true)}${sigcap.datetime.zone}`;
            }
        } else if (typeof sigcap === "string" && sigcap.length === 24) {
            datetimeIso = `${this.getDate(sigcap.substring(0,8))}T${this.getTime(sigcap.substring(9,18), true)}${sigcap.substring(19,24)}`;
        } else {
            throw new Exception("Cannot get a clean datetime: ", sigcap);
        }
        return datetimeIso;
    },

    getDate: function(str) {
        return `${str.substring(0,4)}-${str.substring(4,6)}-${str.substring(6,8)}`;
    },

    getTime: function(str, printMs = false) {
        return `${str.substring(0,2)}:${str.substring(2,4)}:${str.substring(4,6)}${printMs ? "." + str.substring(6,9) : ""}`;
    },

    printDateTime: function(dateObj) {
        const offset = dateObj.getTimezoneOffset();
        dateObj = new Date(dateObj.getTime() - (offset * 60 * 1000));
        return dateObj.toISOString().replace(`Z`, `${offset > 0 ? "-" : "+"}${('00' + Math.abs(offset / 60)).slice(-2)}${('00' + Math.abs(offset % 60)).slice(-2)}`);
    },

    cleanLegacyValue: function(sigcapVersion, val) {
        val = this.cleanNumeric(val);
        return ((sigcapVersion <= 34 && val === 0) || val === 2147483647 || val === 9223372036854775807) ? NaN : val;
    },

    cleanSignal: function(signal) {
        signal = this.cleanNumeric(signal);
        return (signal === 2147483647 || signal === 9223372036854775807) ? NaN : signal;
    },

    cleanNumeric: function(number) {
        if (number === undefined) return NaN;
        if (typeof number !== 'number') {
            try {
                if (number.includes(".")) {
                    number = parseFloat(number);
                } else {
                    number = parseInt(number);
                }
            } catch (err) {
                return NaN;
            }
        }
        return number;
    },

    cleanString: function(string) {
        return (string === undefined) ? "N/A" : string;
    },

    isLtePrimary: function(sigcapCell) {
        return (sigcapCell.width > 0 || sigcapCell.registered);
    },

    getCleanValue: function(obj, keyString = "") {
        let keyArr = keyString.split(".");
        for (key of keyArr) {
            if (key === "") continue;
            obj = obj[key];
            if (obj === undefined) break;
        }
        return obj;
    },

    hashObj: function(obj, hashType = "md5", digest = "base64") {
        const hash = crypto.createHash(hashType);
        hash.update(typeof obj === 'object' ? JSON.stringify(obj) : obj);
        return hash.digest(digest);
    },

    getUuid: function(zipFn) {
        if (zipFn) {
            let matched = zipFn.match(/[0-9a-f]{8}-[0-9a-f]{4}/);
            if (matched) {
                return matched[0];
            }
        }
        return "Unknown";
    },

    getBandDesignation: function(bandNumber) {
        return bandDesignation[bandNumber][1];
    },

    getBandFreq: function(bandNumber) {
        return bandDesignation[bandNumber][0];
    },

    getBands: function() {
        return Object.keys(bandDesignation);
    },

    getXCalHeader: function(header) {
        let colIds = {};
        for (let i = 0, length1 = header.length; i < length1; i++) {
            switch (header[i]) {
                case "Lon": colIds.Lon = i; break;
                case "Lat": colIds.Lat = i; break;
                case "LTE KPI PCell Serving PCI": colIds.LTE_PCell_ServPCI = i; break;
                case "LTE KPI PCell Serving EARFCN(DL)": colIds.LTE_PCell_ServEARFCNDL = i; break;
                case "LTE KPI PCell Serving RSRP[dBm]": colIds.LTE_PCell_ServRSRP = i; break;
                case "5G KPI PCell RF Serving PCI": colIds.PCell_ServPCI = i; break;
                case "5G KPI PCell RF Serving SSB Idx": colIds.PCell_ServSSB = i; break;
                case "5G KPI PCell RF Band": colIds.PCell_ServBand = i; break;
                case "5G KPI PCell RF BandWidth": colIds.PCell_ServBW = i; break;
                case "5G KPI PCell RF Frequency [MHz]": colIds.PCell_ServFreq = i; break;
                case "5G KPI PCell RF NR-ARFCN": colIds.PCell_NRARFCN = i; break;
                case "5G KPI PCell RF Pathloss [dB]": colIds.PCell_ServPL = i; break;
                case "5G KPI PCell RF Serving SS-RSRP [dBm]": colIds.PCell_ServRSRP = i; break;
                case "5G KPI PCell RF Serving SS-RSRQ [dB]": colIds.PCell_ServRSRQ = i; break;
                case "5G KPI PCell RF Serving SS-SINR [dB]": colIds.PCell_ServSINR = i; break;
                case "5G KPI PCell RF Serving CQI [dB]": colIds.PCell_ServCQI = i; break;
                // case "5G KPI PCell RF Serving CQI [dB]": colIds.PCell_ServRI = i; break;
                case "5G KPI PCell RF Best Beam SSB Idx": colIds.PCell_BestSSB = i; break;
                case "5G KPI PCell RF Best Beam SS-RSRP [dBm]": colIds.PCell_BestRSRP = i; break;
                case "5G KPI PCell RF Best Beam SS-RSRQ [dBm]": colIds.PCell_BestRSRQ = i; break;
                case "5G KPI PCell RF Best Beam State": colIds.PCell_BestState = i; break;
                case "5G KPI PCell RF Neighbor Top1 PCI": colIds.PCell_Neighbor1PCI = i; break;
                case "5G KPI PCell RF Neighbor Top1 SS-RSRP [dBm]": colIds.PCell_Neighbor1RSRP = i; break;
                case "5G KPI PCell RF Neighbor Top1 SS-RSRQ [dB]": colIds.PCell_Neighbor1RSRQ = i; break;
                case "5G KPI PCell RF Neighbor Top2 PCI": colIds.PCell_Neighbor2PCI = i; break;
                case "5G KPI PCell RF Neighbor Top2 SS-RSRP [dBm]": colIds.PCell_Neighbor2RSRP = i; break;
                case "5G KPI PCell RF Neighbor Top2 SS-RSRQ [dB]": colIds.PCell_Neighbor2RSRQ = i; break;
                case "5G KPI PCell RF Neighbor Top3 PCI": colIds.PCell_Neighbor3PCI = i; break;
                case "5G KPI PCell RF Neighbor Top3 SS-RSRP [dBm]": colIds.PCell_Neighbor3RSRP = i; break;
                case "5G KPI PCell RF Neighbor Top3 SS-RSRQ [dB]": colIds.PCell_Neighbor3RSRQ = i; break;
                case "5G KPI PCell Layer1 DL BLER [%]": colIds.PCell_DLBLER = i; break;
                case "5G KPI PCell Layer1 DL Modulation0 Representative Value": colIds.PCell_DLMod = i; break;
                case "5G KPI PCell Layer1 DL RB Num (Mode)": colIds.PCell_RBMode = i; break;
                case "5G KPI PCell Layer1 DL RB Num (Avg)": colIds.PCell_RBAvg = i; break;
                case "5G KPI PCell Layer1 PDSCH Throughput [Mbps]": colIds.PCell_PDSCHTput = i; break;
                case "5G KPI PCell Layer2 MAC DL Throughput [Mbps]": colIds.PCell_MACDLTput = i; break;
                case "5G KPI SCell[1] RF NR-ARFCN": colIds.SCell1_NRARFCN = i; break;
                case "5G KPI SCell[1] RF Serving PCI": colIds.SCell1_ServPCI = i; break;
                case "5G KPI SCell[1] RF Serving SSB Idx": colIds.SCell1_ServSSB = i; break;
                case "5G KPI SCell[1] RF Serving SS-RSRP [dBm]": colIds.SCell1_ServRSRP = i; break;
                case "5G KPI SCell[1] RF Serving SS-RSRQ [dB]": colIds.SCell1_ServRSRQ = i; break;
                case "5G KPI SCell[1] RF Serving SS-SINR [dB]": colIds.SCell1_ServSINR = i; break;
                case "5G KPI SCell[1] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell1_Neighbor1RSRP = i; break;
                case "5G KPI SCell[1] Layer1 DL BLER [%]": colIds.SCell1_DLBLER = i; break;
                case "5G KPI SCell[1] Layer1 DL Modulation0 Representative Value": colIds.SCell1_DLMod = i; break;
                case "5G KPI SCell[1] Layer1 DL RB Num (Mode)": colIds.SCell1_RBMode = i; break;
                case "5G KPI SCell[1] Layer1 DL RB Num (Avg)": colIds.SCell1_RBAvg = i; break;
                case "5G KPI SCell[1] Layer1 PDSCH Throughput [Mbps]": colIds.SCell1_PDSCHTput = i; break;
                case "5G KPI SCell[1] Layer2 MAC DL Throughput [Mbps]": colIds.SCell1_MACDLTput = i; break;
                case "5G KPI SCell[2] RF NR-ARFCN": colIds.SCell2_NRARFCN = i; break;
                case "5G KPI SCell[2] RF Serving PCI": colIds.SCell2_ServPCI = i; break;
                case "5G KPI SCell[2] RF Serving SSB Idx": colIds.SCell2_ServSSB = i; break;
                case "5G KPI SCell[2] RF Serving SS-RSRP [dBm]": colIds.SCell2_ServRSRP = i; break;
                case "5G KPI SCell[2] RF Serving SS-RSRQ [dB]": colIds.SCell2_ServRSRQ = i; break;
                case "5G KPI SCell[2] RF Serving SS-SINR [dB]": colIds.SCell2_ServSINR = i; break;
                case "5G KPI SCell[2] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell2_Neighbor1RSRP = i; break;
                case "5G KPI SCell[2] Layer1 DL BLER [%]": colIds.SCell2_DLBLER = i; break;
                case "5G KPI SCell[2] Layer1 DL Modulation0 Representative Value": colIds.SCell2_DLMod = i; break;
                case "5G KPI SCell[2] Layer1 DL RB Num (Mode)": colIds.SCell2_RBMode = i; break;
                case "5G KPI SCell[2] Layer1 DL RB Num (Avg)": colIds.SCell2_RBAvg = i; break;
                case "5G KPI SCell[2] Layer1 PDSCH Throughput [Mbps]": colIds.SCell2_PDSCHTput = i; break;
                case "5G KPI SCell[2] Layer2 MAC DL Throughput [Mbps]": colIds.SCell2_MACDLTput = i; break;
                case "5G KPI SCell[3] RF NR-ARFCN": colIds.SCell3_NRARFCN = i; break;
                case "5G KPI SCell[3] RF Serving PCI": colIds.SCell3_ServPCI = i; break;
                case "5G KPI SCell[3] RF Serving SSB Idx": colIds.SCell3_ServSSB = i; break;
                case "5G KPI SCell[3] RF Serving SS-RSRP [dBm]": colIds.SCell3_ServRSRP = i; break;
                case "5G KPI SCell[3] RF Serving SS-RSRQ [dB]": colIds.SCell3_ServRSRQ = i; break;
                case "5G KPI SCell[3] RF Serving SS-SINR [dB]": colIds.SCell3_ServSINR = i; break;
                case "5G KPI SCell[3] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell3_Neighbor1RSRP = i; break;
                case "5G KPI SCell[3] Layer1 DL BLER [%]": colIds.SCell3_DLBLER = i; break;
                case "5G KPI SCell[3] Layer1 DL Modulation0 Representative Value": colIds.SCell3_DLMod = i; break;
                case "5G KPI SCell[3] Layer1 DL RB Num (Mode)": colIds.SCell3_RBMode = i; break;
                case "5G KPI SCell[3] Layer1 DL RB Num (Avg)": colIds.SCell3_RBAvg = i; break;
                case "5G KPI SCell[3] Layer1 PDSCH Throughput [Mbps]": colIds.SCell3_PDSCHTput = i; break;
                case "5G KPI SCell[3] Layer2 MAC DL Throughput [Mbps]": colIds.SCell3_MACDLTput = i; break;
                case "5G KPI SCell[4] RF NR-ARFCN": colIds.SCell4_NRARFCN = i; break;
                case "5G KPI SCell[4] RF Serving PCI": colIds.SCell4_ServPCI = i; break;
                case "5G KPI SCell[4] RF Serving SSB Idx": colIds.SCell4_ServSSB = i; break;
                case "5G KPI SCell[4] RF Serving SS-RSRP [dBm]": colIds.SCell4_ServRSRP = i; break;
                case "5G KPI SCell[4] RF Serving SS-RSRQ [dB]": colIds.SCell4_ServRSRQ = i; break;
                case "5G KPI SCell[4] RF Serving SS-SINR [dB]": colIds.SCell4_ServSINR = i; break;
                case "5G KPI SCell[4] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell4_Neighbor1RSRP = i; break;
                case "5G KPI SCell[4] Layer1 DL BLER [%]": colIds.SCell4_DLBLER = i; break;
                case "5G KPI SCell[4] Layer1 DL Modulation0 Representative Value": colIds.SCell4_DLMod = i; break;
                case "5G KPI SCell[4] Layer1 DL RB Num (Mode)": colIds.SCell4_RBMode = i; break;
                case "5G KPI SCell[4] Layer1 DL RB Num (Avg)": colIds.SCell4_RBAvg = i; break;
                case "5G KPI SCell[4] Layer1 PDSCH Throughput [Mbps]": colIds.SCell4_PDSCHTput = i; break;
                case "5G KPI SCell[4] Layer2 MAC DL Throughput [Mbps]": colIds.SCell4_MACDLTput = i; break;
                case "5G KPI SCell[5] RF NR-ARFCN": colIds.SCell5_NRARFCN = i; break;
                case "5G KPI SCell[5] RF Serving PCI": colIds.SCell5_ServPCI = i; break;
                case "5G KPI SCell[5] RF Serving SSB Idx": colIds.SCell5_ServSSB = i; break;
                case "5G KPI SCell[5] RF Serving SS-RSRP [dBm]": colIds.SCell5_ServRSRP = i; break;
                case "5G KPI SCell[5] RF Serving SS-RSRQ [dB]": colIds.SCell5_ServRSRQ = i; break;
                case "5G KPI SCell[5] RF Serving SS-SINR [dB]": colIds.SCell5_ServSINR = i; break;
                case "5G KPI SCell[5] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell5_Neighbor1RSRP = i; break;
                case "5G KPI SCell[5] Layer1 DL BLER [%]": colIds.SCell5_DLBLER = i; break;
                case "5G KPI SCell[5] Layer1 DL Modulation0 Representative Value": colIds.SCell5_DLMod = i; break;
                case "5G KPI SCell[5] Layer1 DL RB Num (Mode)": colIds.SCell5_RBMode = i; break;
                case "5G KPI SCell[5] Layer1 DL RB Num (Avg)": colIds.SCell5_RBAvg = i; break;
                case "5G KPI SCell[5] Layer1 PDSCH Throughput [Mbps]": colIds.SCell5_PDSCHTput = i; break;
                case "5G KPI SCell[5] Layer2 MAC DL Throughput [Mbps]": colIds.SCell5_MACDLTput = i; break;
                case "5G KPI SCell[6] RF NR-ARFCN": colIds.SCell6_NRARFCN = i; break;
                case "5G KPI SCell[6] RF Serving PCI": colIds.SCell6_ServPCI = i; break;
                case "5G KPI SCell[6] RF Serving SSB Idx": colIds.SCell6_ServSSB = i; break;
                case "5G KPI SCell[6] RF Serving SS-RSRP [dBm]": colIds.SCell6_ServRSRP = i; break;
                case "5G KPI SCell[6] RF Serving SS-RSRQ [dB]": colIds.SCell6_ServRSRQ = i; break;
                case "5G KPI SCell[6] RF Serving SS-SINR [dB]": colIds.SCell6_ServSINR = i; break;
                case "5G KPI SCell[6] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell6_Neighbor1RSRP = i; break;
                case "5G KPI SCell[6] Layer1 DL BLER [%]": colIds.SCell6_DLBLER = i; break;
                case "5G KPI SCell[6] Layer1 DL Modulation0 Representative Value": colIds.SCell6_DLMod = i; break;
                case "5G KPI SCell[6] Layer1 DL RB Num (Mode)": colIds.SCell6_RBMode = i; break;
                case "5G KPI SCell[6] Layer1 DL RB Num (Avg)": colIds.SCell6_RBAvg = i; break;
                case "5G KPI SCell[6] Layer1 PDSCH Throughput [Mbps]": colIds.SCell6_PDSCHTput = i; break;
                case "5G KPI SCell[6] Layer2 MAC DL Throughput [Mbps]": colIds.SCell6_MACDLTput = i; break;
                case "5G KPI SCell[7] RF NR-ARFCN": colIds.SCell7_NRARFCN = i; break;
                case "5G KPI SCell[7] RF Serving PCI": colIds.SCell7_ServPCI = i; break;
                case "5G KPI SCell[7] RF Serving SSB Idx": colIds.SCell7_ServSSB = i; break;
                case "5G KPI SCell[7] RF Serving SS-RSRP [dBm]": colIds.SCell7_ServRSRP = i; break;
                case "5G KPI SCell[7] RF Serving SS-RSRQ [dB]": colIds.SCell7_ServRSRQ = i; break;
                case "5G KPI SCell[7] RF Serving SS-SINR [dB]": colIds.SCell7_ServSINR = i; break;
                case "5G KPI SCell[7] RF Neighbor Top1 SS-RSRP [dBm]": colIds.SCell7_Neighbor1RSRP = i; break;
                case "5G KPI SCell[7] Layer1 DL BLER [%]": colIds.SCell7_DLBLER = i; break;
                case "5G KPI SCell[7] Layer1 DL Modulation0 Representative Value": colIds.SCell7_DLMod = i; break;
                case "5G KPI SCell[7] Layer1 DL RB Num (Mode)": colIds.SCell7_RBMode = i; break;
                case "5G KPI SCell[7] Layer1 DL RB Num (Avg)": colIds.SCell7_RBAvg = i; break;
                case "5G KPI SCell[7] Layer1 PDSCH Throughput [Mbps]": colIds.SCell7_PDSCHTput = i; break;
                case "5G KPI SCell[7] Layer2 MAC DL Throughput [Mbps]": colIds.SCell7_MACDLTput = i; break;
                case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(5G-NR PDU) [Mbps]": colIds.Total_5GDLPHYTput = i; break;
                case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(LTE PDU) [Mbps]": colIds.Total_4GDLPHYTput = i; break;
                case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(Total PDU) [Mbps]": colIds.Total_DLPHYTput = i; break;
                case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(5G-NR PDU) [Mbps]": colIds.Total_5GDLMACTput = i; break;
                case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(LTE PDU) [Mbps]": colIds.Total_4GDLMACTput = i; break;
                case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(Total PDU) [Mbps]": colIds.Total_DLMACTput = i; break;
                case "Ping & Trace RTT (ms)": colIds.Total_PingRTT = i; break;
                case "API GPS Info Estimated accuracy": colIds.GPS_Acc = i; break;
                case "Qualcomm 5G-NR EN-DC Tx Power Info NR Tx Power [dBm]": colIds.NR_TXPower = i; break;
                case "PCI Beam Switch Analysis PCell PCI Switch": colIds.PCell_PCISwitch = i; break;
                case "PCI Beam Switch Analysis PCell Beam Switch": colIds.PCell_BeamSwitch = i; break;
                case "PCI Beam Switch Analysis PCell Beam Switch Delay Time [sec]": colIds.PCell_BeamSwitchDelay = i; break;
                case "PCI Beam Switch Analysis PCell Intra-Beam Switch": colIds.PCell_IntraSwitch = i; break;
                case "PCI Beam Switch Analysis PCell Inter-Beam Switch": colIds.PCell_InterSwitch = i; break;
                default: break;
            }
        }
        return colIds;
    }
}

module.exports = dataUtils;