const agg = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const cellHelper = require('./lib/cell-helper');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);
if (args.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output path] [--filter <filter JSON string>]`);
    process.exit(1);
}

let outputPath = "output-sigcap.csv";
let inputFolder = args[0];
let inputFilter;
args.splice(0, 1);
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}
console.log(`Input folder: ${inputFolder}`);

console.log(`Writing to: ${outputPath}`);
let os = fs.createWriteStream(outputPath);

// Write header
const header = `operator,sim_operator,carrier,device_id,device_model,android_ver,sigcap_ver,`
             + `timestamp,latitude,longitude,altitude,hor_acc,ver_acc,`
             + `nrStatus,nrAvailable,dcNrRestricted,enDcAvailable,nrFrequencyRange,`
             + `usingCA,cellBandwidths,sumBw*,lte/nr,`
             + `pci,lte-ci/nr-nci,band*,lte-earfcn/nr-arfcn,freq*,bandwidth,`
             + `rsrp,rsrq,lte-rssi/nr-sinr,primary/other*,\n`;
os.write(header);

hashBin = [];

const getOverviewString = function(sigcap) {
    return `${dataUtils.getCleanOp(sigcap)},`
        + `${sigcap.simName ? sigcap.simName : "N/A"},`
        + `${sigcap.carrierName ? sigcap.carrierName : "N/A"},`
        + `${sigcap.uuid ? sigcap.uuid : "N/A"},`
        + `${sigcap.deviceName ? sigcap.deviceName : "N/A"},`
        + `${dataUtils.getAndroidVersion(sigcap.androidVersion)},`
        + `${sigcap.version}${sigcap.isDebug === true ? "-debug" : ""},`
        + `${dataUtils.getCleanDatetime(sigcap)},`
        + `${sigcap.location.latitude},`
        + `${sigcap.location.longitude},`
        + `${sigcap.location.altitude},`
        + `${sigcap.location.hor_acc},`
        + `${sigcap.location.ver_acc},`
        + `${dataUtils.getServiceState(sigcap, "nrStatus")},`
        + `${dataUtils.getServiceState(sigcap, "nrAvailable")},`
        + `${dataUtils.getServiceState(sigcap, "dcNrRestricted")},`
        + `${dataUtils.getServiceState(sigcap, "enDcAvailable")},`
        + `${dataUtils.getServiceState(sigcap, "nrFrequencyRange")},`
        + `"${dataUtils.getServiceState(sigcap, "usingCA")}",`
        + `"${dataUtils.getServiceState(sigcap, "cellBandwidths")}",`
        + `${dataUtils.sumCellBw(sigcap)},`;
}

// TODO: implement region lookup
const currRegion = cellHelper.REGION.NAR;

agg.callbackJsonRecursive(inputFolder, data => {
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${sigcap.length}`);

    // Write csv
    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        // Sanity checks
        let hash = dataUtils.hashObj(sigcap[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (sigcap[i].opName === undefined && sigcap[i].simName === undefined && sigcap[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(sigcap[i]);
        if (op === "Unknown") continue;

        let overviewString = getOverviewString(sigcap[i]);

        // Get LTE Primary
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            if (dataUtils.isLtePrimary(sigcap[i].cell_info[j])) {
                // Write overview
                let entry = overviewString + `lte,`;

                // Write PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                entry += `${dataUtils.cleanSignal(sigcap[i].cell_info[j].pci)},`
                        + `${dataUtils.cleanLegacyValue(sigcap[i].version, sigcap[i].cell_info[j].ci)},`
                        + `${sigcap[i].cell_info[j].earfcn ? cellHelper.earfcnToBand(sigcap[i].cell_info[j].earfcn) : "N/A"},`
                        + `${sigcap[i].cell_info[j].earfcn ? dataUtils.cleanSignal(sigcap[i].cell_info[j].earfcn) : NaN},`
                        + `${sigcap[i].cell_info[j].earfcn ? cellHelper.earfcnToFreq(sigcap[i].cell_info[j].earfcn) : NaN},`
                        + `${sigcap[i].cell_info[j].width ? sigcap[i].cell_info[j].width : NaN},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrq)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rssi)},`;
                // Write primary/other
                entry += `${dataUtils.isLtePrimary(sigcap[i].cell_info[j]) ? "primary" : "other"},`
                // EOL and flush
                entry += `\n`;
                os.write(entry);

                // Clear this entry
                sigcap[i].cell_info.splice(j, 1);
                break;
            }
        }

        // Get NR
        if (sigcap[i].nr_info) {
            for (let j = 0, length2 = sigcap[i].nr_info.length; j < length2; j++){

                let entry = overviewString + `${sigcap[i].nr_info[j].isSignalStrAPI === true ? "nr-SignalStrAPI" : "nr"},`;
                // Write PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                let nrarfcn =  dataUtils.cleanLegacyValue(sigcap[i].version, sigcap[i].nr_info[j].nrarfcn);
                entry += `${dataUtils.cleanSignal(sigcap[i].nr_info[j].nrPci)},`
                        + `${dataUtils.cleanLegacyValue(sigcap[i].version, sigcap[i].nr_info[j].nci)},`
                        + `${cellHelper.nrarfcnToBand(nrarfcn, currRegion, true)},`
                        + `${nrarfcn},`
                        + `${cellHelper.nrarfcnToFreq(nrarfcn)},`
                        + `${sigcap[i].nr_info[j].width ? sigcap[i].nr_info[j].width : NaN},`
                        + `${dataUtils.cleanSignal(sigcap[i].nr_info[j].ssRsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].nr_info[j].ssRsrq)},`
                        + `${dataUtils.cleanSignal(sigcap[i].nr_info[j].ssSinr)},`;
                // Write primary/other
                entry += `${sigcap[i].nr_info[j].status === "primary" ? "primary" : "other"},`
                // EOL and flush
                entry += `\n`;
                os.write(entry);
            }
        }

        // Get rest of LTE
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            // Write overview
            let entry = overviewString + `lte,`;

            // Write PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                entry += `${dataUtils.cleanSignal(sigcap[i].cell_info[j].pci)},`
                        + `${dataUtils.cleanLegacyValue(sigcap[i].version, sigcap[i].cell_info[j].ci)},`
                        + `${sigcap[i].cell_info[j].earfcn ? cellHelper.earfcnToBand(sigcap[i].cell_info[j].earfcn) : "N/A"},`
                        + `${sigcap[i].cell_info[j].earfcn ? dataUtils.cleanSignal(sigcap[i].cell_info[j].earfcn) : NaN},`
                        + `${sigcap[i].cell_info[j].earfcn ? cellHelper.earfcnToFreq(sigcap[i].cell_info[j].earfcn) : NaN},`
                        + `${sigcap[i].cell_info[j].width ? sigcap[i].cell_info[j].width : NaN},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrp)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rsrq)},`
                        + `${dataUtils.cleanSignal(sigcap[i].cell_info[j].rssi)},`;
            // Write primary/other
            entry += `${dataUtils.isLtePrimary(sigcap[i].cell_info[j]) ? "primary" : "other"},`
            // EOL and flush
            entry += `\n`;
            os.write(entry);
        }

    }
});

os.close();
