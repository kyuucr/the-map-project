let pciData = [];
let pciPin = {};
// let markers = {};
let markerAdded = false;
let geoJson = {}
let visMat = {};

let options = {};

const MAP_TYPE = {
    "4g": "4G only",
    "5g": "5G"
}
const areaColor = {
    all: "#565555",
    edge: "#ff0000",
    mid: "#ffbb00",
    good: "#ffff00",
    excellent: "#00ff00"
}
const zIndexes = {
    all: 1,
    edge: 2,
    mid: 3,
    good: 4,
    excellent: 5
}

const tableHeaders = {
    pci: "PCI",
    nci: "NCI",
    earfcn: "EARFCN number",
    nrarfcn: "NRARFCN number",
    band: "Band",
    width: "Bandwidth (kHz)",
    freq: "Center Frequency (MHz)",
    status: "Status"
};

const colLength = {
    field: "-4",
    value: "-8"
}

// Fill info table
let fillTable = function (className, arr) {
    let parent = $(`.${className}`);
    parent.empty();
    if (className !== "overview") {
        let row = $("<div class='row row-heading'></div>");
        for (const key in arr[0]) {
            let col = $(`<div class='col-table col-heading col'>${tableHeaders[key]}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
    for (let i = 0, length1 = arr.length; i < length1; i++){
        var row = $("<div class='row row-table'></div>");
        for (const key in arr[i]) {
                let value = arr[i][key];
                var col = $(`<div class='col-table col${colLength[key] ? colLength[key] : ""}'>${value}</div>`);
                row.append(col);
        }
        parent.append(row);
    }
};

let updatePolyVisibility = function(featureId) {
    map.data.forEach((feature) => {
        if (feature.getGeometry().getType() === "Polygon") {
            let visible = false;
            if (featureId) {
                visible = feature.getId().startsWith(featureId);
            }
            feature.setProperty("isVisible", visible);
        }
    });
    map.data.revertStyle();
};

let initGeoJson = function() {
    if (markerAdded || typeof map === "undefined" || typeof geoJson.features === "undefined") return;
    markerAdded = true;

    let status = updateMarkerOpacity();
    console.log(`Total data: ${geoJson.features.length}`);
    let pciSelect = $("#pci-select").val();
    let kSelect = $("#k-select").val();
    let avgRsrpSelect = $("#avg-rsrp-select").val();
    let maxRsrpSelect = $("#max-rsrp-select").val();
    let bounds = new google.maps.LatLngBounds();

    for(let i = 0, length1 = geoJson.features.length; i < length1; i++){
        let op = geoJson.features[i].properties.OP;
        if (!Object.keys(status).includes(op)) continue;
        let type = geoJson.features[i].properties.TYPE;
        let band = geoJson.features[i].properties.BAND;
        if (status[op][type][band] !== undefined) {
            let visible = false;
            if (geoJson.features[i].geometry.type === "Point") {
                visible = status[op][type][band];
                if (pciSelect) {
                    visible = visible && (geoJson.features[i].properties.PCI == pciSelect);
                }
                let maxRsrpPass1 = true, maxRsrpPass2 = true;
                if (maxRsrpSelect) {
                    maxRsrpPass1 = (geoJson.features[i].properties.MAX_RSRP > maxRsrpSelect);
                    maxRsrpPass2 = ((geoJson.features[i].properties.MAX_RSRP + 20) > maxRsrpSelect);
                }
                let kPass = true;
                if (kSelect) {
                    kPass = (geoJson.features[i].properties.K > kSelect);
                }
                let avgRsrpPass = true;
                if (avgRsrpSelect) {
                    avgRsrpPass = (geoJson.features[i].properties.AVG_RSRP_DBM > avgRsrpSelect);
                }
                visible = visible && (maxRsrpPass1 || (kPass && avgRsrpPass && maxRsrpPass2));
                if (visible) {
                    bounds.extend({lat: geoJson.features[i].geometry.coordinates[1],
                        lng: geoJson.features[i].geometry.coordinates[0]});
                }
            }
            geoJson.features[i].properties.isVisible = visible;
        }
    }

    map.data.setStyle(function(feature) {
        let op = feature.getProperty("OP");
        let visible = feature.getProperty("isVisible");
        let cat = feature.getProperty("CAT");

        if (cat) {
            return {
                strokeOpacity: 1,
                strokeWeight: 2,
                strokeColor: areaColor[cat],
                fillColor: areaColor[cat],
                fillOpacity: 0.2,
                visible: visible,
                zIndex: zIndexes[cat]
            }
        } else {
            return {
                icon: pciPin[op],
                visible: visible
            }
        }
    })
    map.data.addGeoJson(geoJson, {idPropertyName: "IDX"});
    map.data.addListener("click", (event) => {
        let overview = [];
        let latitude = 0, longitude = 0;
        overview.push({field: `Operator`, value: event.feature.getProperty("OP")});
        let cellInfo = event.feature.getProperty("CELL_INFO");
        let nrInfo = event.feature.getProperty("NR_INFO");
        let pci = 0;
        if (cellInfo.length > 0) {
            pci = cellInfo[0].pci;
        } else if (nrInfo.length > 0) {
            pci = nrInfo[0].pci;
        }
        overview.push({field: `PCI`, value: pci});
        overview.push({field: 'Type', value: event.feature.getProperty("TYPE")});
        overview.push({field: 'Localized using band', value: event.feature.getProperty("BAND")});
        overview.push({field: `# of data`, value: event.feature.getProperty("K")});
        overview.push({field: `Average RSRP`, value: `${event.feature.getProperty("AVG_RSRP").toFixed(2)} dBm`});
        overview.push({field: `Average RSRP in dBm scale`, value: `${event.feature.getProperty("AVG_RSRP_DBM").toFixed(2)} dBm`});
        overview.push({field: `Max RSRP`, value: `${event.feature.getProperty("MAX_RSRP")} dBm`});
        event.feature.getGeometry().forEachLatLng((latLng) => {
            latitude = latLng.lat();
            longitude = latLng.lng();
        })
        overview.push({field: 'Location', value: `${latitude}, ${longitude}`});
        let dates = event.feature.getProperty("DATETIME");
        let dateFirst = dates[0] > dates[dates.length - 1] ? dates[dates.length - 1] : dates[0];
        let dateLast = dates[0] > dates[dates.length - 1] ? dates[0] : dates[dates.length - 1];
        overview.push({field: `First time observed`, value: new Date(dateFirst).toString()});
        overview.push({field: `Latest time observed`, value: new Date(dateLast).toString()});
        updatePolyVisibility(event.feature.getId());

        fillTable('overview', overview);
        fillTable('lte-rows', cellInfo);
        fillTable('nr-rows', nrInfo);
    });

    setMapZoomAndCenter(bounds);
}

let createBandCheckboxes = function(opt) {
    // Init visibility matrix
    $('.checkbox-operator').each(function() {
        visMat[$(this).attr("name")] = {};
    });
    $('.checkbox-marker').each(function() {
        for (let op in visMat) {
            visMat[op][$(this).attr("name")] = {};
        }
    });

    // Add PCI to HTML and visMat
    let parent = $(`#band-select`);
    let temp = 0;
    for(let band of opt.bandList) {
        let div = $(`<div class="${temp === 0 ? "mx-sm-2 " : ""}form-check form-check-inline"></div>`);
        let cb = $(`<input class="form-check-input checkbox-band" type="checkbox" id="band-${band}" name="${band}" ${temp++ === 0 ? "checked" : ""} />`);
        // let cb = $(`<input class="form-check-input checkbox-band" type="checkbox" id="band-${band}" name="${band}" checked />`);
        let label = $(`<label class="form-check-label" for="band-${band}">${band}</label>`);
        div.append(cb);
        div.append(label);
        parent.append(div);

        for (let op in visMat) {
            for (let type in visMat[op]) {
                visMat[op][type][band] = true;
            }
        }
    }
    let button = $(`<div class="mx-sm-1 form-check form-check-inline"><button class="btn btn-primary" id="select-bands" type="button" onclick="selectBands()">Select all/none</button></div>`);
    parent.append(button);
};

let selectBands = function() {
    let selectAll = true;
    $(".checkbox-band").each(function() {
        selectAll = selectAll && $(this).prop("checked");
    });
    console.log(selectAll);
    selectNow = !selectAll;
    $(".checkbox-band").each(function() {
        $(this).prop("checked", selectNow);
        if (selectNow) {
            $(this).attr("checked", selectNow);
        } else {
            $(this).removeAttr("checked");
        }
    });
}

let pciTextbox = function() {
    if (event.keyCode == 13) {
        event.preventDefault();
        setMarkerVisible();
        return false;
    }
}

let setMarkerVisible = function() {
    updateMarkerOpacity();
    updatePolyVisibility();

    let pciSelect = $("#pci-select").val();
    let kSelect = $("#k-select").val();
    let avgRsrpSelect = $("#avg-rsrp-select").val();
    let maxRsrpSelect = $("#max-rsrp-select").val();
    let bounds = new google.maps.LatLngBounds();

    map.data.forEach((feature) => {
        if (feature.getGeometry().getType() === "Point") {
            let visible = visMat[feature.getProperty("OP")][feature.getProperty("TYPE")][feature.getProperty("BAND")];
            if (pciSelect) {
                let pci = feature.getProperty("PCI");
                visible = visible && (pci == pciSelect);
            }
            let maxRsrpPass1 = true, maxRsrpPass2 = true;
            if (maxRsrpSelect) {
                let maxRsrp = feature.getProperty("MAX_RSRP");
                maxRsrpPass1 = (maxRsrp > maxRsrpSelect);
                maxRsrpPass2 = ((maxRsrp + 20) > maxRsrpSelect);
            }
            let kPass = true;
            if (kSelect) {
                let k = feature.getProperty("K");
                kPass = (k > kSelect);
            }
            let avgRsrpPass = true;
            if (avgRsrpSelect) {
                let avgRsrp = feature.getProperty("AVG_RSRP_DBM");
                avgRsrpPass = (avgRsrp > avgRsrpSelect);
            }
            visible = visible && (maxRsrpPass1 && kPass && avgRsrpPass);
            feature.setProperty("isVisible", visible);
            if (visible) {
                feature.getGeometry().forEachLatLng((latLng) => {
                    bounds.extend(latLng);
                });
            }
        }
    });
    map.data.revertStyle();
    setMapZoomAndCenter(bounds);
};

let updateMarkerOpacity = function() {
    $('.checkbox-operator').each(function() {
        let op = $(this).attr("name");
        for (let type in visMat[op]) {
            for (let band in visMat[op][type]) {
                visMat[op][type][band] = $(this).prop("checked");
            }
        }
    });
    $('.checkbox-marker').each(function() {
        let type = $(this).attr("name");
        for (let op in visMat) {
            for (let band in visMat[op][type]) {
                visMat[op][type][band] = visMat[op][type][band] && $(this).prop("checked");
            }
        }
    });
    $('.checkbox-band').each(function() {
        let band = $(this).attr("name");
        for (let op in visMat) {
            for (let type in visMat[op]) {
                    visMat[op][type][band] = visMat[op][type][band] && $(this).prop("checked");
            }
        }
    });
    return visMat;
};

let pciCallback = function(json, filename) {
    for (let key in json.OPTIONS) {
        options[key] = json.OPTIONS[key];
    }
    delete json.OPTIONS;
    geoJson = json;

    // updateMapCenter(options);  // options dependent
    createBandCheckboxes(options);
    initGeoJson();
}

let getRegion = function() {
    let region = getHash();
    if (region) {
        region = `-${region}`;
    } else {
        region = "";
    }
    return region;
};

let reload = function() {
    let hash = getHash();
    $("#region").val(hash);
    $("#region").change(function() {
        setHash($(this).val());
    });
    let region = getRegion();
    loadJSON(`outputs/pci${region}.geojson`, pciCallback);

    $("#submit-marker").click(function() {
        setMarkerVisible();
    });

    for (let area in areaColor) {
        $(`#area-${area}`).css("background-color", `${areaColor[area]}33`);
        $(`#area-${area}`).css("border", `2px solid ${areaColor[area]}ff`);
    }
}

// IMPORTANT
initMapCallback = function() {
    // Pin definition
    pciPin["AT&T"] = pins.pinBlue;
    pciPin["T-Mobile"] = pins.pinGreen;
    pciPin["Sprint"] = pins.pinYellow;
    pciPin["Verizon"] = pins.pinRed;
    pciPin["Lycamobile"] = pins.pinPurple;
    pciPin["Simple Mobile"] = pins.pinPurple;
    pciPin["Unknown"] = pins.pinPurple;
    // Init markers
    $(document).ready(reload);
};