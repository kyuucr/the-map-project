var map;
var pins;
var stylePlain = true;
var localStorage = window.localStorage;

var defaultPaths = {
    heatmap: `outputs/nr-heatmaps.geojson`,
    fcc: `outputs/fcc-data.json`,
    force: false
};

var operators = {
    att: "AT&T",
    pci_att: "AT&T",
    tmobile: "T-Mobile",
    pci_tmobile: "T-Mobile",
    verizon: "Verizon",
    pci_verizon: "Verizon",
    bssid_wifi: "Wi-Fi"
};
var wifiChannels = {
    36: [36, 38, 42, 50],
    40: [40, 38, 42, 50],
    44: [44, 46, 42, 50],
    48: [48, 46, 42, 50],
    149: [149, 151, 155],
    153: [153, 151, 155],
    157: [157, 159, 155],
    161: [161, 159, 155],
    165: [165]
};
var infoWindow;

var loadJSON = function(path, callback) {
    console.log(`Loading ${path}`);
    $.getJSON(path, function(json) {
        callback(json, path);
    });
};

$(document).ready(function() {
    // TODO settings here
});

var initMapCallback = function() {
    console.log("initMapCallback unimplemented");
}

var plainMap = [
    {
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#f5f5f5"
        }
        ]
    },
    {
        "elementType": "labels",
        "stylers": [
        {
            "visibility": "on"
        }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
        {
            "visibility": "off"
        }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#616161"
        }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
        {
            "color": "#f5f5f5"
        }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#bdbdbd"
        }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "stylers": [
        {
            "visibility": "off"
        }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#eeeeee"
        }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#757575"
        }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#e5e5e5"
        }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#9e9e9e"
        }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#ffffff"
        }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#757575"
        }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#dadada"
        }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#616161"
        }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#9e9e9e"
        }
        ]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#e5e5e5"
        }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#eeeeee"
        }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
        {
            "color": "#c9c9c9"
        }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
        {
            "color": "#9e9e9e"
        }
        ]
    }
];

var styleToggle = function() {
    // Map style
    if (!stylePlain) {
        stylePlain = true;
        map.set('styles', plainMap);
    } else {
        stylePlain = false;
        map.set('styles', [])
    }
};

var getBoundsZoomLevel = function(bounds, mapDim) {
    var WORLD_DIM = { height: 256, width: 256 };
    // var ZOOM_MAX = 21;
    var ZOOM_MAX = 16;

    function latRad(lat) {
        var sin = Math.sin(lat * Math.PI / 180);
        var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    function zoom(mapPx, worldPx, fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
    }

    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

    var lngDiff = ne.lng() - sw.lng();
    var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

    var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
    var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
};

var setMapZoomAndCenter = function(bounds, zoom = null) {
    let mapDiv = $('#map');
    let mapDim = { height: mapDiv.height(), width: mapDiv.width() };
    if (zoom === null) {
        zoom = getBoundsZoomLevel(bounds, mapDim);
    }
    console.log(`Set center: ${bounds.getCenter().toString()}, zoom: ${zoom}`);
    map.setZoom(zoom);
    map.setCenter(bounds.getCenter());
};

// Initialize map onload
var initMap = function() {

    // Marker colors
    let pinBlue = {
        path: 'M 12, 11.5 A 2.5, 2.5 0 0, 1 9.5, 9 A 2.5, 2.5 0 0,1 12, 6.5 A 2.5, 2.5 0 0, 1 14.5, 9 A 2.5, 2.5 0 0, 1 12, 11.5 M 12, 2 A 7, 7 0 0, 0 5, 9 C 5, 14.25 12, 22 12, 22 C 12,22 19, 14.25 19, 9 A 7, 7 0 0, 0 12, 2 z',
        fillColor: 'blue',
        fillOpacity: 0.8,
        scale: 1,
        size: new google.maps.Size(24, 24),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(12, 24)
    };
    let pinGreen = JSON.parse(JSON.stringify(pinBlue));
    pinGreen.fillColor = 'green';
    let pinRed = JSON.parse(JSON.stringify(pinBlue));
    pinRed.fillColor = 'red';
    let pinPurple = JSON.parse(JSON.stringify(pinBlue));
    pinPurple.fillColor = 'purple';
    let pinYellow = JSON.parse(JSON.stringify(pinBlue));
    pinYellow.fillColor = 'yellow';
    let pinGrey = JSON.parse(JSON.stringify(pinBlue));
    pinGrey.fillColor = 'grey';
    let pinBlueFilled = {
        path: 'M 12,2 C 8.1340068,2 5,5.1340068 5,9 c 0,5.25 7,13 7,13 0,0 7,-7.75 7,-13 0,-3.8659932 -3.134007,-7 -7,-7 z',
        fillColor: 'blue',
        fillOpacity: 0.8,
        scale: 1,
        size: new google.maps.Size(24, 24),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(12, 24)
    };
    let pinGreenFilled = JSON.parse(JSON.stringify(pinBlueFilled));
    pinGreenFilled.fillColor = 'green';
    let pinRedFilled = JSON.parse(JSON.stringify(pinBlueFilled));
    pinRedFilled.fillColor = 'red';
    let pinPurpleFilled = JSON.parse(JSON.stringify(pinBlueFilled));
    pinPurpleFilled.fillColor = 'purple';
    let pinYellowFilled = JSON.parse(JSON.stringify(pinBlueFilled));
    pinYellowFilled.fillColor = 'yellow';
    let pinGreyFilled = JSON.parse(JSON.stringify(pinBlueFilled));
    pinGreyFilled.fillColor = 'grey';
    let dotBlue = {
        path: 'M 12, 12 m -6, 0 a 6, 6 0 1, 0 12, 0 a 6, 6 0 1, 0 -12, 0',
        fillColor: 'blue',
        fillOpacity: 0.8,
        scale: 0.5,
        size: new google.maps.Size(24, 24),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(12, 12),
        strokeWeight: 0
    };
    let dotGreen = JSON.parse(JSON.stringify(dotBlue));
    dotGreen.fillColor = 'green';
    let dotRed = JSON.parse(JSON.stringify(dotBlue));
    dotRed.fillColor = 'red';
    let dotBlack = JSON.parse(JSON.stringify(dotBlue));
    dotBlack.fillColor = 'black';
    pins = {
        dotBlue: dotBlue,
        pinBlue: pinBlue,
        dotGreen: dotGreen,
        pinGreen: pinGreen,
        dotRed: dotRed,
        pinRed: pinRed,
        pinPurple: pinPurple,
        pinYellow: pinYellow,
        pinGrey: pinGrey,
        pinBlueFilled: pinBlueFilled,
        pinGreenFilled: pinGreenFilled,
        pinRedFilled: pinRedFilled,
        pinPurpleFilled: pinPurpleFilled,
        pinYellowFilled: pinYellowFilled,
        pinGreyFilled: pinGreyFilled
    };

    // Center 41.877504, -87.631403
    let chicago = {lat: 41.877504, lng: -87.631403};
    // let chicago = {lat: 41.86861225, lng: -87.6709268};
    map = new google.maps.Map(
            document.getElementById('map'), {zoom: 12, center: chicago});
    let transitLayer = new google.maps.TransitLayer();
    transitLayer.setMap(map);
    map.set('styles', plainMap);

    infoWindow = new google.maps.InfoWindow();
    initMapCallback();
};

var getHash = function() {
    if (window.location.hash) {
        return window.location.hash.substring(1);
    }
    return "";
}

var setHash = function(value) {
    window.location.hash = value;
    window.location.reload(true);
}
