let heatmapLayer = {};
let geoJson = {};
let fccData = [];
let fccPin = {};
let markers = {};
let visMat = {};
let heatmapAdded = false;
let markersAdded = false;

let options = {
    MAX_HEATMAP_OPACITY: 0.85,
    MIN_HEATMAP_OPACITY: 0.5,
};
let heatmapOpacity = options.MAX_HEATMAP_OPACITY; // (0,0.6]

let gradientBase = [
    [163,  0,  0,1],
    [255, 255,  0,1],
    [ 56, 200, 56,1],
];
let gradientBaseAlt = [
    [128, 128, 0, 1],
    [0, 255, 255, 1],
    [0, 0, 255, 1],
    [255, 0, 0, 1]
];
let gradientHeatmap = [];
let gradientHeatmapAlt = [];
for (let i = 0; i < gradientBase.length; ++i) {
        gradientHeatmap.push(`rgba(${gradientBase[i][0]},${gradientBase[i][1]},${gradientBase[i][2]},${gradientBase[i][3]})`);
}
for (let i = 0; i < gradientBaseAlt.length; ++i) {
        gradientHeatmapAlt.push(`rgba(${gradientBaseAlt[i][0]},${gradientBaseAlt[i][1]},${gradientBaseAlt[i][2]},${gradientBaseAlt[i][3]})`);
}
let gradientCss = '(left';
let gradientCssAlt = '(left';
for (let i = 0; i < gradientHeatmap.length; ++i) {
        gradientCss += ', ' + gradientHeatmap[i];
}
for (let i = 0; i < gradientHeatmapAlt.length; ++i) {
        gradientCssAlt += ', ' + gradientHeatmapAlt[i];
}
gradientCss += ')';
gradientCssAlt += ')';
let gradientType = {
    lte: gradientBase,
    laa: gradientBase,
    nr: gradientBase
};

let tableHeading = [ "Metrics", "Value at Beginning", "Value at End" ];

let formatValue = function (obj, key) {
    switch(key) {
        case "location":
        case "avg_location":
            return `${obj.lat}, ${obj.lon}`;
        case "throughput":
        case "download":
        case "download_bytes_per_s":
        case "upload":
        case "upload_bytes_per_s":
            return `${(obj * 8 / 1000000).toFixed(3)} Mbit/s`;
        case "bytes_transferred":
            return `${(obj / 1000000).toFixed(3)} Mbytes`;
        case "latency":
        case "jitter":
        case "round_trip_time":
            return `${(obj / 1000).toFixed(3)} ms`;
        case "packet_loss":
            return `${(obj * 100).toFixed(2)} %`;
        case "time":
            return `${new Date(obj).toString()}`;
        case "duration":
            return `${(obj / 1000000).toFixed(3)} s`;
        case "packet_size":
            return `${obj} bytes`;
        case "stream_bits_per_sec":
            return `${obj} bits/s`;
        case "average_ss_rsrp":
        case "average_rsrp":
        case "rsrp":
            return `${obj} dBm`;
        default:
            return `${obj}`;
    }
}

let formatString = function (str) {
    switch (str) {
        case "download_bytes_per_s":
            return "Download throughput"
        case "upload_bytes_per_s":
            return "Upload throughput"
        case "avg_location":
            return "Average location"
        case "average_ss_rsrp":
            return "Average SS-RSRP (5G)";
        case "average_rsrp":
            return "Average RSRP (4G)";
        case "rsrp":
            return "RSRP (or SS-RSRP if 5G)";
        case "pci":
            return "PCI";
        default:
            return str.charAt(0).toUpperCase() + str.slice(1).replace(/_/g, " ");
    }
}

// Fill info table
let fillTable = function (className, obj) {
    let parent = $(`.${className}`);
    parent.empty();
    if (className !== "overview") {
        let row = $("<div class='row row-heading'></div>");
        for (const head of tableHeading) {
            let col = $(`<div class='col-table col-heading col-4'>${head}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
    for (let key in obj) {
        let row = $("<div class='row row-table'></div>");
        let colMetric = $(`<div class='col-table col-4'>${formatString(key)}</div>`);
        row.append(colMetric);
        if (obj[key].beginning) {
            let colB = $(`<div class='col-table col-4'>${formatValue(obj[key].beginning.value ? obj[key].beginning.value : obj[key].beginning, key)}</div>`);
            row.append(colB);
            let colE = $(`<div class='col-table col-4'>${formatValue(obj[key].end.value ? obj[key].end.value : obj[key].end, key)}</div>`);
            row.append(colE);
        } else {
            let col = $(`<div class='col-table col-8'>${formatValue(obj[key], key)}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
};

let changeNumData = function(newData) {
    $("#num-data").text(newData);
}

let getRgbaFromWeight = function (weight, gradientArr) {
    let gradientIndex = Math.floor(weight * (gradientArr.length - 1));
    // sanity check
    if (gradientIndex >= gradientArr.length - 1) gradientIndex = gradientArr.length - 2;
    let weightRel = weight * (gradientArr.length - 1) - gradientIndex;
    let red = (gradientArr[gradientIndex + 1][0] - gradientArr[gradientIndex][0]) * weightRel + gradientArr[gradientIndex][0];
    let grn = (gradientArr[gradientIndex + 1][1] - gradientArr[gradientIndex][1]) * weightRel + gradientArr[gradientIndex][1];
    let blu = (gradientArr[gradientIndex + 1][2] - gradientArr[gradientIndex][2]) * weightRel + gradientArr[gradientIndex][2];

    return `rgba(${red},${grn},${blu},1)`;
}


let initGeoJson = function() {
    if (heatmapAdded || typeof map === "undefined" || typeof geoJson.features === "undefined") return;
    heatmapAdded = true;

    let status = updateHeatmapOpacity();
    console.log(`Total data: ${geoJson.features.length}`);

    let num = 0;
    for(let i = 0, length1 = geoJson.features.length; i < length1; i++){
        let op = geoJson.features[i].properties.OP;
        if (!Object.keys(status).includes(op)) continue;
        let type = geoJson.features[i].properties.TYPE;
        let pci = geoJson.features[i].properties.PCI;
        let earfcn = geoJson.features[i].properties.EARFCN;
        if (status[op][type][pci] !== undefined && status[op][type][pci][earfcn] !== undefined) {
            geoJson.features[i].properties.isVisible = status[op][type][pci][earfcn];
            num += status[op][type][pci][earfcn];
        }
    }
    console.log(`Num displayed data: ${num}`);
    changeNumData(num);

    map.data.setStyle(function(feature) {
        let weight = feature.getProperty("WEIGHT");
        let op = feature.getProperty("OP");
        let type = feature.getProperty("TYPE");
        let visible = feature.getProperty("isVisible");

        return {
            icon: {
                path: 'M 12, 12 m -6, 0 a 6, 6 0 1, 0 12, 0 a 6, 6 0 1, 0 -12, 0',
                fillColor: getRgbaFromWeight(weight, gradientType[type]),
                fillOpacity: heatmapOpacity,
                scale: 0.4,
                size: new google.maps.Size(24, 24),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(12, 12),
                strokeWeight: 0
            },
            visible: visible
        }
    })
    map.data.addGeoJson(geoJson, {idPropertyName: "IDX"});
    map.data.addListener("click", (event) => {
        infoWindow.setContent(`PCI: ${event.feature.getProperty("PCI")}<br>`
            + `Band: ${event.feature.getProperty("BAND")}<br>`
            + `EARFCN: ${event.feature.getProperty("EARFCN")}<br>`
            + `RSRP: ${event.feature.getProperty("RSRP")} dBm<br>`
            + `RSRQ: ${event.feature.getProperty("RSRQ")} dBm<br>`
            + `Timestamp: ${new Date(event.feature.getProperty("TIMESTAMP"))}<br>`);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(map);
    });
}

let initMarkers = function() {
    if (markersAdded || Object.keys(fccData).length === 0 || typeof map === "undefined") return;
    markersAdded = true;

    let markerStates = getMarkerStates();

    for (let i = fccData.length - 1; i >= 0; i--) {
        if (!Object.keys(markerStates).includes(fccData[i].overview.operator)) continue;

        let loc = {lat: fccData[i].overview.avg_location.lat, lng: fccData[i].overview.avg_location.lon}
        let marker = new google.maps.Marker({position: loc, map: map, icon: fccPin[fccData[i].overview.connection]});
        marker.set('data', fccData[i]);
        marker.setVisible(markerStates[fccData[i].overview.operator][fccData[i].overview.connection]);
        marker.addListener('click', function() {
            let data = this.get('data');
            console.log(data);
            // let overview = [];
            // overview.push({field: 'Location', value: `${data.location.latitude}, ${data.location.longitude}`});
            // overview.push({field: 'Operator', value: operators[filename]});
            fillTable('overview', data.overview);
            fillTable('download', data.download);
            fillTable('upload', data.upload);
            fillTable('latency', data.latency);
        });

        if (markers[fccData[i].overview.operator] === undefined) markers[fccData[i].overview.operator] = {};
        if (markers[fccData[i].overview.operator][fccData[i].overview.connection] === undefined) markers[fccData[i].overview.operator][fccData[i].overview.connection] = [];
        markers[fccData[i].overview.operator][fccData[i].overview.connection].push(marker);
    }
}

let setMarkerVisible = function(op, type, val) {
    if (markers[op] && markers[op][type]) {
        for (let marker of markers[op][type]) {
            marker.setVisible(val);
        }
    }
}

let setHeatmapVisible = function() {
    updateHeatmapOpacity();

    let num = 0;
    map.data.forEach((feature) => {
        let visible = visMat[feature.getProperty("OP")][feature.getProperty("TYPE")][feature.getProperty("PCI")][feature.getProperty("EARFCN")];
        feature.setProperty("isVisible", visible);
        num += visible;
    });
    map.data.revertStyle();
    console.log(`Num displayed data: ${num}`);
    changeNumData(num);
}

let updateHeatmapOpacity = function() {
    $('.checkbox-operator').each(function() {
        let op = $(this).attr("name");
        for (let type in visMat[op]) {
            for (let pci in visMat[op][type]) {
                for (let earfcn in visMat[op][type][pci]) {
                    visMat[op][type][pci][earfcn] = $(this).prop("checked");
                }
            }
        }
    });
    $('.checkbox-heatmap').each(function() {
        let type = $(this).attr("name");
        for (let op in visMat) {
            for (let pci in visMat[op][type]) {
                for (let earfcn in visMat[op][type][pci]) {
                    visMat[op][type][pci][earfcn] = visMat[op][type][pci][earfcn] && $(this).prop("checked");
                }
            }
        }
    });
    $('.checkbox-pci').each(function() {
        let pci = $(this).attr("name");
        for (let op in visMat) {
            for (let type in visMat[op]) {
                for (let earfcn in visMat[op][type][pci]) {
                    visMat[op][type][pci][earfcn] = visMat[op][type][pci][earfcn] && $(this).prop("checked");
                }
            }
        }
    });
    $('.checkbox-earfcn').each(function() {
        let earfcn = $(this).attr("name");
        for (let op in visMat) {
            for (let type in visMat[op]) {
                for (let pci in visMat[op][type]) {
                    visMat[op][type][pci][earfcn] = visMat[op][type][pci][earfcn] && $(this).prop("checked");
                }
            }
        }
    });
    // heatmapOpacity = (options.MAX_HEATMAP_OPACITY - options.MIN_HEATMAP_OPACITY) / num + options.MIN_HEATMAP_OPACITY; // 1 / number of true items * MAX
    heatmapOpacity = options.MAX_HEATMAP_OPACITY;
    return visMat;
};

let getMarkerStates = function() {
    let temp = {};
    let temp2 = {};
    $('.checkbox-operator').each(function() {
        temp[$(this).attr("id")] = $(this).prop("checked");
        temp2[$(this).attr("id")] = {};
    });
    $('.checkbox-marker').each(function() {
        for (let op in temp2) {
            temp2[op][$(this).attr("id")] = temp[op] && $(this).prop("checked");
        }
    });
    return temp2;
};

let createPciBand = function(opt) {
    // Init visibility matrix
    $('.checkbox-operator').each(function() {
        visMat[$(this).attr("name")] = {};
    });
    $('.checkbox-heatmap').each(function() {
        for (let op in visMat) {
            visMat[op][$(this).attr("name")] = {};
        }
    });

    // Add PCI to HTML and visMat
    let parent = $(`#pci-select`);
    let temp = 0;
    for(let pci of opt.pciList) {
        // if (pci === 0) continue;
        let div = $(`<div class="form-check form-check-inline"></div>`);
        let cb = $(`<input class="form-check-input checkbox-pci" type="checkbox" id="pci-${pci}" name="${pci}" ${temp++ === 0 ? "checked" : ""} />`);
        // cb.change(function() {
        //     setHeatmapVisible();
        // });
        let label = $(`<label class="form-check-label" for="pci-${pci}">${pci}</label>`);
        div.append(cb);
        div.append(label);
        parent.append(div);

        for (let op in visMat) {
            for (let type in visMat[op]) {
                visMat[op][type][pci] = {};
            }
        }
    }

    // Add EARFCN to HTML and visMat
    parent = $(`#earfcn-select`);
    temp = 0;
    for (let earfcnBand of opt.earfcnList) {
        let div = $(`<div class="form-check form-check-inline"></div>`);
        let cb = $(`<input class="form-check-input checkbox-earfcn" type="checkbox" id="earfcn-${earfcnBand.earfcn}" name="${earfcnBand.earfcn}" ${temp++ === 0 ? "checked" : ""} />`);
        // cb.change(function() {
        //     setHeatmapVisible();
        // });
        let label = $(`<label class="form-check-label" for="earfcn-${earfcnBand.earfcn}">${earfcnBand.band}/${earfcnBand.earfcn}</label>`);
        div.append(cb);
        div.append(label);
        parent.append(div);

        for (let op in visMat) {
            for (let type in visMat[op]) {
                for (let pci in visMat[op][type]) {
                    visMat[op][type][pci][earfcnBand.earfcn] = true;
                }
            }
        }
    }
}

let updateMapCenter = function(opt) {
    opt.latCenter = (opt.maxLat - opt.minLat) / 2 + opt.minLat;
    opt.lngCenter = (opt.maxLng - opt.minLng) / 2 + opt.minLng;
    map.setCenter({lat: opt.latCenter, lng: opt.lngCenter});
}

let createLegendBox = function(opt) {
    // Create legend gradient box
    $('#gradient-laa').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', 'linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', 'linear-gradient' + gradientCss);
    let legendContainerWidth = $('.map-legend').outerWidth();
    let legendWidth = $('#legend').outerWidth();
    let legendGradientWidth = $('#legend-gradient').outerWidth();
    let legendHeight = $('#legend').outerHeight(true);
    let legendGradientHeight = $('#legend-gradient').outerHeight();

    let maxIntensity = 4;
    let minOffset = Math.floor((legendContainerWidth - legendWidth) / 2);
    let yOffset = Math.floor((legendHeight - legendGradientHeight) / 2);

    for (let i = 0; i <= maxIntensity; ++i) {
        let offset = minOffset + i * legendGradientWidth / maxIntensity;
        if (i > 0 && i < maxIntensity) {
            offset -= 0.5;
        } else if (i == maxIntensity) {
            offset -= 1;
        }
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset - 5) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset + legendGradientHeight) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset - 24) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html((i * opt.rangeSsrsrp / maxIntensity + opt.minSsrsrp).toFixed(2)));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset + legendGradientHeight + 4) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html((i * opt.rangeRsrp / maxIntensity + opt.minRsrp).toFixed(2)));
    }
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 30) + 'px',
        'top': (yOffset - 24) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 30) + 'px',
        'top': (yOffset + legendGradientHeight + 4) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
};

$(document).ready(function() {
    $("#submit-heatmap").click(function() {
        setHeatmapVisible();
    });
    // $('.checkbox-marker').change(function() {
    //     let status = getMarkerStates();
    //     for (let op in status) {
    //         for (let type in status[op]) {
    //             setMarkerVisible(op, type, status[op][type]);
    //         }
    //     }
    // });
    // $('.checkbox-operator').change(function() {
    //     setHeatmapVisible();
    //     status = getMarkerStates();
    //     for (let op in status) {
    //         for (let type in status[op]) {
    //             setMarkerVisible(op, type, status[op][type]);
    //         }
    //     }
    // });
});

let initData = function() {
    console.log("MUST BE OVERRIDDEN");
}

// IMPORTANT
initMapCallback = function() {
    // Pin definition
    fccPin["5g"] = pins.pinRed;
    fccPin["4g"] = pins.pinBlue;
    fccPin["mixed"] = pins.pinPurple;
    fccPin["3g"] = pins.pinGreen;
    fccPin["2g"] = pins.pinYellow;

    initData();
};