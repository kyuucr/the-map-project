let geoJsonCallback = function(json, filename) {
    for (let key in json.OPTIONS) {
        options[key] = json.OPTIONS[key];
    }
    delete json.OPTIONS;
    geoJson = json;

    updateMapCenter(options);  // options dependent
    createLegendBox(options);  // options dependent
    createPciBand(options);
    initGeoJson();
    map.setZoom(16);
}

let fccCallback = function(json, filename) {
    fccData = json;

    initMarkers();
}

initData = function() {
    loadJSON("outputs/nr-dots.geojson", geoJsonCallback);

    // loadJSON("outputs/fcc-data.json", fccCallback);
};
