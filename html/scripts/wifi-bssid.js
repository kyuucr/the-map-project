let wifiData = [];
let pciPin = {};
let markers = {};
let markerAdded = false;

let options = {};

const tableHeaders = {
    ssid: "SSID",
    bssid: "BSSID",
    channelNum: "Channel number",
    width: "Bandwidth (MHz)",
    primaryFreq: "Primary Frequency (MHz)",
    standard: "Standard"
};

let printValue = function (val, key) {
    switch (key) {
        case "bssid":
            return val.substring(0, 6) + "...";
        case "ssid":
            return val ? val : "&lt;hidden&gt;";
        default:
            return val;
    }
}

// Fill info table
let fillTable = function (className, arr) {
    let parent = $(`.${className}`);
    parent.empty();
    if (className !== "overview") {
        let row = $("<div class='row row-heading'></div>");
        for (const key in arr[0]) {
            let col = $(`<div class='col-table col-heading col'>${tableHeaders[key]}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
    for (var i = arr.length - 1; i >= 0; i--) {
        var row = $("<div class='row row-table'></div>");
        for (const key in arr[i]) {
                let value = arr[i][key];
                var col = $(`<div class='col-table col'>${printValue(value, key)}</div>`);
                row.append(col);
        }
        parent.append(row);
    }
};

let getChannelCategory = function (num) {
    if (num < 36) {
        return "2_4G";
    } else if (num < 50) {
        return "U-NII-1";
    } else if (num < 100) {
        return "U-NII-2A";
    } else if (num < 149) {
        return "U-NII-2C";
    } else if (num < 166) {
        return "U-NII-3";
    } else {
        return "unknown";
    }
}

let initMarkers = function() {
    if (markerAdded || wifiData.length === 0) return;
    markerAdded = true;

    for (let i = wifiData.length - 1; i >= 0; i--) {
        // let channel = getChannelCategory(wifiData[i].channelNum);
        let loc = {lat: wifiData[i].location.latitude, lng: wifiData[i].location.longitude};
        let marker = new google.maps.Marker({position: loc, map: map, icon: pciPin["wifi"]});
        marker.set('data', wifiData[i]);
        // marker.setVisible(markerStates[channel]);
        marker.addListener('click', function() {
            let data = this.get('data');
            console.log(data);
            let overview = [];
            if (data.datetime) {
                let datetime = data.datetime;
                if (!Array.isArray(datetime)) datetime = [ datetime ];
                let limit = datetime.length;
                // if (parseInt(localStorage.getItem("dateSetting"))) {
                    limit = 1;
                // }
                // let datetime = data.datetime.length ? data.datetime[data.datetime.length - 1] : data.datetime; // Get latest datetime
                for (let j = 0; j < limit; j++) {
                    let datetimeString = `${datetime[j].date.substring(0,4)}-${datetime[j].date.substring(4,6)}-${datetime[j].date.substring(6,8)}T${datetime[j].time.substring(0,2)}:${datetime[j].time.substring(2,4)}:${datetime[j].time.substring(4,6)}.${datetime[j].time.substring(6,9)}${datetime[j].zone}`;
                    overview.push({field: `${j === limit - 1 ? "Latest t" : "T"}ime observed`, value: new Date(datetimeString).toString()});
                }
            }
            overview.push({field: 'Location', value: `${data.location.latitude}, ${data.location.longitude}`});
            overview.push({field: 'Type', value: "Wi-Fi"});
            fillTable('overview', overview);
            fillTable('wifi-rows', data.wifi_info);
        });

        let chList = [];
        for(let j = 0, length2 = wifiData[i].wifi_info.length; j < length2; j++){
            let channel = getChannelCategory(wifiData[i].wifi_info[j].channelNum);
            chList.push(channel);
        }
        for (let ch of chList) {
            if (markers[ch] === undefined) markers[ch] = [];
            markers[ch].push(marker);
        }
    }

    // Trigger marker visibility
    markerVisibility();
}

let markerVisibility = function () {
    let status = getMarkerStates();
    let statusTrue = [];
    for (let channel in status) {
        if (status[channel] === false) {
            setMarkerVisible(channel, false);
        } else {
            statusTrue.push(channel);
        }
    }
    for (let channel of statusTrue) {
        setMarkerVisible(channel, true);
    }
}

let setMarkerVisible = function(channel, val) {
    if (markers[channel]) {
        for (let marker of markers[channel]) {
            marker.setVisible(val);
        }
    }
}

let getMarkerStates = function() {
    let temp = {};
    $('.checkbox-channel').each(function() {
        temp[$(this).attr("id")] = $(this).prop("checked");
    });
    return temp;
};

let bssidCallback = function(json, filename) {
    for (let key in json) {
        if (key === "data") {
            wifiData = json[key];
        } else {
            options[key] = json[key];
        }
    }

    updateMapCenter();  // options dependent
    // init if not yet initiated
    initMarkers();
}

$(document).ready(function() {
    loadJSON("outputs/bssid.json", bssidCallback);

    // Set legend checkboxes
    $('.checkbox-channel').change(function() {
        markerVisibility();
    });
});

let updateMapCenter = function() {
    options.latCenter = (options.maxLat - options.minLat) / 2 + options.minLat;
    options.lngCenter = (options.maxLng - options.minLng) / 2 + options.minLng;
    map.setCenter({lat: options.latCenter, lng: options.lngCenter});
}

// IMPORTANT
initMapCallback = function() {
    // Pin definition
    pciPin["wifi"] = pins.pinBlue;
    // Init heatmap
    // initHeatmap();
    map.setZoom(16);
    // if (options.latCenter) map.setCenter({lat: options.latCenter, lng: options.lngCenter});
};