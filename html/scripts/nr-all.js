let pciData = [];
let pciPin;
let markers = [];
let markerAdded = false;

let options = {};

const tableHeaders = {
    pci: "PCI",
    nci: "NCI",
    earfcn: "EARFCN number",
    nrarfcn: "NRARFCN number",
    band: "Band",
    width: "Bandwidth (kHz)",
    freq: "Center Frequency (MHz)",
    status: "Status"
};

// Fill info table
let fillTable = function (className, arr) {
    let parent = $(`.${className}`);
    parent.empty();
    if (className !== "overview") {
        let row = $("<div class='row row-heading'></div>");
        for (const key in arr[0]) {
            let col = $(`<div class='col-table col-heading col'>${tableHeaders[key]}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
    for (var i = arr.length - 1; i >= 0; i--) {
        var row = $("<div class='row row-table'></div>");
        for (const key in arr[i]) {
                let value = arr[i][key];
                var col = $(`<div class='col-table col'>${value}</div>`);
                row.append(col);
        }
        parent.append(row);
    }
};

let initMarkers = function(arr) {
    if (arr.length === 0) return;

    // let markerStates = getMarkerStates();

    for (let i = arr.length - 1; i >= 0; i--) {
        let loc = {lat: arr[i].location.latitude, lng: arr[i].location.longitude};
        let marker = new google.maps.Marker({position: loc, map: map, icon: arr[i].cell_info.length > 0 ? pciPin : pciNoSigPin});
        marker.set('data', arr[i]);
        // marker.setVisible(markerStates[arr[i].type]);
        marker.addListener('click', function() {
            let data = this.get('data');
            console.log(data);
            let overview = [];
            if (data.datetime) {
                let datetime = data.datetime;
                if (!Array.isArray(datetime)) datetime = [ datetime ];
                let limit = datetime.length;
                // if (parseInt(localStorage.getItem("dateSetting"))) {
                    limit = 1;
                // }
                // let datetime = data.datetime.length ? data.datetime[data.datetime.length - 1] : data.datetime; // Get latest datetime
                for (let j = 0; j < limit; j++) {
                    let datetimeString = `${datetime[j].date.substring(0,4)}-${datetime[j].date.substring(4,6)}-${datetime[j].date.substring(6,8)}T${datetime[j].time.substring(0,2)}:${datetime[j].time.substring(2,4)}:${datetime[j].time.substring(4,6)}.${datetime[j].time.substring(6,9)}${datetime[j].zone}`;
                    overview.push({field: `${j === limit - 1 ? "Latest t" : "T"}ime observed`, value: new Date(datetimeString).toString()});
                }
            }
            overview.push({field: 'Location', value: `${data.location.latitude}, ${data.location.longitude}`});
            // overview.push({field: 'Type', value: MAP_TYPE[data.type]});
            fillTable('overview', overview);
            fillTable('lte-rows', data.cell_info);
            fillTable('nr-rows', data.nr_info);
        });
        if (markers === undefined) markers = [];
        markers.push(marker);
    }
}

// let setMarkerVisible = function(type, val) {
//     for (let marker of markers[type]) {
//         marker.setVisible(val);
//     }
// }

let removeMarkers = function() {
    for(let i = 0, length1 = markers.length; i < length1; i++){
        markers[i].setMap(null);
    }
    markers = [];
}

let doFilter = function(filterObj) {
    if (Object.prototype.toString.call(filterObj) !== '[object Object]') {
        // Try JSON parse
        filterObj = JSON.parse(filterObj);
    }
    console.log(filterObj)
    let tempArr = filter.filterArray(filterObj, pciData);
    console.log("filtered", tempArr);
    removeMarkers();
    initMarkers(tempArr);
}

// let getMarkerStates = function() {
//     let temp = {};
//     $('.checkbox-marker').each(function() {
//         temp[$(this).attr("id")] = $(this).prop("checked");
//     });
//     return temp;
// };

let pciCallback = function(json, filename) {
    for (let key in json) {
        if (key === "data") {
            pciData = json[key];
        } else {
            options[key] = json[key];
        }
    }

    updateMapCenter();  // options dependent
    // init if not yet initiated
    initMarkers(pciData);
}

$(document).ready(function() {
    loadJSON("outputs/all.json", pciCallback);

    // Set legend checkboxes
    // $('.checkbox-marker').change(function() {
    //     setMarkerVisible($(this).attr("id"), $(this).prop("checked"));
    // });
});

let updateMapCenter = function() {
    options.latCenter = (options.maxLat - options.minLat) / 2 + options.minLat;
    options.lngCenter = (options.maxLng - options.minLng) / 2 + options.minLng;
    map.setCenter({lat: options.latCenter, lng: options.lngCenter});
}

// IMPORTANT
initMapCallback = function() {
    // Pin definition
    pciPin = pins.pinBlue;
    pciNoSigPin = pins.pinRed;
    // Init heatmap
    // initHeatmap();
    map.setZoom(16);
    // if (options.latCenter) map.setCenter({lat: options.latCenter, lng: options.lngCenter});
};