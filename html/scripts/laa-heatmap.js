let heatmaps = {};
let heatmapLayer = {};
let heatmapAdded = false;

let options = {
    MAX_HEATMAP_OPACITY: 0.85,
    MIN_HEATMAP_OPACITY: 0.5,
};
let heatmapOpacity = options.MAX_HEATMAP_OPACITY; // (0,0.6]

let gradient = [
    "rgba(000,255,255,0)",
    "rgba(000,255,000,1)",
    "rgba(255,255,000,1)",
    "rgba(255,000,000,1)",
    "rgba(163,000,000,1)"
];
let gradientAlt = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(255, 0, 0, 1)'
];
let gradientCss = '(left';
let gradientCssAlt = '(left';
for (let i = 0; i < gradient.length; ++i) {
        gradientCss += ', ' + gradient[i];
}
for (let i = 0; i < gradientAlt.length; ++i) {
        gradientCssAlt += ', ' + gradientAlt[i];
}
gradientCss += ')';
gradientCssAlt += ')';

let initHeatmap = function() {
    if (heatmapAdded || Object.keys(heatmaps).length === 0 || typeof map === "undefined") return;
    heatmapAdded = true;

    let status = updateHeatmapOpacity();
    let grd = {
        "AT&T": gradient,
        wifi: gradientAlt
    };
    let chList = [];
    for (let op in heatmaps) {
        if (!Object.keys(status).includes(op)) continue;

        heatmapLayer[op] = {};
        for (let ch in heatmaps[op]) {
            if (!chList.includes(ch)) chList.push(ch);
            let opts = {
                data: [],
                map: map,
                opacity: status[op][ch] ? heatmapOpacity : 0,
                gradient: grd[op],
                dissipating: false,
                radius: 0.00004,
                maxIntensity: 1
            };
            let infoIndex = op === "wifi" ? "wifi_info" : "cell_info";
            for (let i = heatmaps[op][ch].length - 1; i >= 0; i--) {
                opts.data.push({
                    location: new google.maps.LatLng(heatmaps[op][ch][i].location.latitude, heatmaps[op][ch][i].location.longitude),
                    weight: heatmaps[op][ch][i][infoIndex][0].weight
                });
            }
            heatmapLayer[op][ch] = new google.maps.visualization.HeatmapLayer(opts);
        }
    }

    $('.checkbox-channel').each(function() {
        if (!chList.includes($(this).attr("id"))) {
            $(this).prop("disabled", true);
            $(this).prop("checked", false);
        }
    });
};

let updateHeatmapOpacity = function() {
    let num = 0;
    let temp = {};
    let temp2 = {};
    $('.checkbox-heatmap').each(function() {
        temp[$(this).attr("id")] = $(this).prop("checked");
        temp2[$(this).attr("id")] = {};
    });
    $('.checkbox-channel').each(function() {
        for (let op in temp2) {
            temp2[op][$(this).attr("id")] = temp[op] && $(this).prop("checked");
            num += temp2[op][$(this).attr("id")];
        }
    });
    heatmapOpacity = (options.MAX_HEATMAP_OPACITY - options.MIN_HEATMAP_OPACITY) / num + options.MIN_HEATMAP_OPACITY; // 1 / number of true items * MAX
    return temp2;
};

let updateMapCenter = function(opt) {
    opt.latCenter = (opt.maxLat - opt.minLat) / 2 + opt.minLat;
    opt.lngCenter = (opt.maxLng - opt.minLng) / 2 + opt.minLng;
    map.setCenter({lat: opt.latCenter, lng: opt.lngCenter});
}

let createLegendBox = function(opt) {
    // Create legend gradient box
    $('#gradient-laa').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', 'linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-webkit-linear-gradient' + gradientCssAlt);
    $('#gradient-wifi').css('background', '-moz-linear-gradient' + gradientCssAlt);
    $('#gradient-wifi').css('background', '-o-linear-gradient' + gradientCssAlt);
    $('#gradient-wifi').css('background', 'linear-gradient' + gradientCssAlt);
    let legendContainerWidth = $('.map-legend').outerWidth();
    let legendWidth = $('#legend').outerWidth();
    let legendGradientWidth = $('#legend-gradient').outerWidth();
    let legendContainerHeight = $('#form-heatmap-2').outerHeight();
    let legendHeight = $('#legend').outerHeight(true);
    let legendGradientHeight = $('#legend-gradient').outerHeight();

    let maxIntensity = 4;
    let minOffset = Math.floor((legendContainerWidth - legendWidth) / 2);
    let yOffset = Math.floor(legendContainerHeight - ((legendHeight + legendGradientHeight) / 2));

    for (let i = 0; i <= maxIntensity; ++i) {
        let offset = minOffset + i * legendGradientWidth / maxIntensity;
        if (i > 0 && i < maxIntensity) {
            offset -= 0.5;
        } else if (i == maxIntensity) {
            offset -= 1;
        }
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset - 6) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset + legendGradientHeight) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset - 24) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html(i * opt.rangeRssi / maxIntensity + opt.minRssi));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset + legendGradientHeight + 4) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html(i * opt.rangeRssi / maxIntensity + opt.minRssi));
    }
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 10) + 'px',
        'top': (yOffset - 24) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 10) + 'px',
        'top': (yOffset + legendGradientHeight + 4) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
};

let doUpdateOpacity = function () {
    let status = updateHeatmapOpacity();
    for (let op in status) {
        for (let channel in status[op]) {
            if (heatmapLayer[op] && heatmapLayer[op][channel]) {
                heatmapLayer[op][channel].setOptions({
                    opacity: status[op][channel] ? heatmapOpacity : 0
                });
            }
        }
    }
}

$(document).ready(function() {
    // Set legend checkboxes
    $('.checkbox-heatmap').change(function() {
        doUpdateOpacity();
    });
    $('.checkbox-channel').change(function() {
        doUpdateOpacity();
    });
});

// IMPORTANT
initMapCallback = function() {
    // Init heatmap
    initHeatmap();
};

let heatmapCallback = function(json, filename) {
    for (let key in json) {
        if (key === "data") {
            for (let op in json[key]) {
                heatmaps[op] = {};
                for (let type in json[key][op]) {
                    heatmaps[op][type] = json[key][op][type];
                }
            }
        } else {
            options[key] = json[key];
        }
    }
    updateMapCenter(options);  // options dependent
    createLegendBox(options);  // options dependent
    // init if not yet initiated
    initHeatmap();
    map.setZoom(18);
};

$(document).ready(function() {
    loadJSON("outputs/laa-heatmaps.json", heatmapCallback);
});
