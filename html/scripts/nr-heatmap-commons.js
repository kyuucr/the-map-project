// Heatmap & GeoJSON
let addedHeatmap = [];
let geoJsonPath = "";
let tempGeoJson = null;

// FCC & JSON
let fccData = [];
let fccPin = {};
let markers = {};
let heatmapAdded = false;
let markersAdded = false;

// Visibility
let heatmapVisibility = {};
let numHeatmapVisibility = 0;
let markerVisibility = {};

let prevZoom = -1;

let options = {
    MAX_HEATMAP_OPACITY: 0.85,
    MIN_HEATMAP_OPACITY: 0.5,
};
let heatmapOpacity = options.MAX_HEATMAP_OPACITY; // (0,0.6]

let gradientBase = [
    [163,  0,  0,1],
    [255, 255,  0,1],
    [ 56, 200, 56,1],
];
let gradientBaseAlt = [
    [0, 255, 255, 1],
    [0, 0, 255, 1],
    [255, 0, 0, 1]
];
let gradientHeatmap = [];
let gradientHeatmapAlt = [];
for (let i = 0; i < gradientBase.length; ++i) {
        gradientHeatmap.push(`rgba(${gradientBase[i][0]},${gradientBase[i][1]},${gradientBase[i][2]},${gradientBase[i][3]})`);
}
for (let i = 0; i < gradientBaseAlt.length; ++i) {
        gradientHeatmapAlt.push(`rgba(${gradientBaseAlt[i][0]},${gradientBaseAlt[i][1]},${gradientBaseAlt[i][2]},${gradientBaseAlt[i][3]})`);
}
let gradientCss = '(left';
let gradientCssAlt = '(left';
for (let i = 0; i < gradientHeatmap.length; ++i) {
        gradientCss += ', ' + gradientHeatmap[i];
}
for (let i = 0; i < gradientHeatmapAlt.length; ++i) {
        gradientCssAlt += ', ' + gradientHeatmapAlt[i];
}
gradientCss += ')';
gradientCssAlt += ')';
let gradientType = {
    lte: gradientBase,
    laa: gradientBase,
    primary: gradientBase,
    cbrs: gradientBase,
    nr: gradientBase,
    mmWave: gradientBase
};

let tableHeading = [ "Metrics", "Value at Beginning", "Value at End" ];

let formatValue = function (obj, key) {
    switch(key) {
        case "location":
        case "avg_location":
            return `${obj.lat}, ${obj.lon}`;
        case "throughput":
        case "download":
        case "download_bytes_per_s":
        case "upload":
        case "upload_bytes_per_s":
            return `${(obj * 8 / 1000000).toFixed(3)} Mbit/s`;
        case "bytes_transferred":
            return `${(obj / 1000000).toFixed(3)} Mbytes`;
        case "latency":
        case "jitter":
        case "round_trip_time":
            return `${(obj / 1000).toFixed(3)} ms`;
        case "packet_loss":
            return `${(obj * 100).toFixed(2)} %`;
        case "time":
            return `${new Date(obj).toString()}`;
        case "duration":
            return `${(obj / 1000000).toFixed(3)} s`;
        case "packet_size":
            return `${obj} bytes`;
        case "stream_bits_per_sec":
            return `${obj} bits/s`;
        case "average_ss_rsrp":
        case "average_rsrp":
        case "rsrp":
            return `${obj} dBm`;
        default:
            return `${obj}`;
    }
}

let formatString = function (str) {
    switch (str) {
        case "download_bytes_per_s":
            return "Download throughput"
        case "upload_bytes_per_s":
            return "Upload throughput"
        case "avg_location":
            return "Average location"
        case "average_ss_rsrp":
            return "Average SS-RSRP (5G)";
        case "average_rsrp":
            return "Average RSRP (4G)";
        case "rsrp":
            return "RSRP (or SS-RSRP if 5G)";
        case "pci":
            return "PCI";
        default:
            return str.charAt(0).toUpperCase() + str.slice(1).replace(/_/g, " ");
    }
}

// Fill info table
let fillTable = function (className, obj) {
    let parent = $(`.${className}`);
    parent.empty();
    if (className !== "overview") {
        let row = $("<div class='row row-heading'></div>");
        for (const head of tableHeading) {
            let col = $(`<div class='col-table col-heading col-4'>${head}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
    for (let key in obj) {
        let row = $("<div class='row row-table'></div>");
        let colMetric = $(`<div class='col-table col-4'>${formatString(key)}</div>`);
        row.append(colMetric);
        if (obj[key].beginning) {
            let colB = $(`<div class='col-table col-4'>${formatValue(obj[key].beginning.value ? obj[key].beginning.value : obj[key].beginning, key)}</div>`);
            row.append(colB);
            let colE = $(`<div class='col-table col-4'>${formatValue(obj[key].end.value ? obj[key].end.value : obj[key].end, key)}</div>`);
            row.append(colE);
        } else {
            let col = $(`<div class='col-table col-8'>${formatValue(obj[key], key)}</div>`);
            row.append(col);
        }
        parent.append(row);
    }
};

let getRgbaFromWeight = function (weight, gradientArr) {
    let gradientIndex = Math.floor(weight * (gradientArr.length - 1));
    // sanity check
    if (gradientIndex >= gradientArr.length - 1) gradientIndex = gradientArr.length - 2;
    let weightRel = weight * (gradientArr.length - 1) - gradientIndex;
    let red = (gradientArr[gradientIndex + 1][0] - gradientArr[gradientIndex][0]) * weightRel + gradientArr[gradientIndex][0];
    let grn = (gradientArr[gradientIndex + 1][1] - gradientArr[gradientIndex][1]) * weightRel + gradientArr[gradientIndex][1];
    let blu = (gradientArr[gradientIndex + 1][2] - gradientArr[gradientIndex][2]) * weightRel + gradientArr[gradientIndex][2];

    return `rgba(${red},${grn},${blu},1)`;
}

let getZoom = function () {
    let zoomLevel = map.getZoom() - 1;
    if (zoomLevel < options.minZoomLevel) {
        zoomLevel = options.minZoomLevel;
    } else if (zoomLevel > options.maxZoomLevel) {
        zoomLevel = options.maxZoomLevel;
    }
    return zoomLevel;
}

let addGeoJson = function (currGeoJson, path) {
    // let status = updateHeatmapOpacity();

    if (currGeoJson.OPTIONS) {
        delete currGeoJson.OPTIONS;
    }

    // for(let i = 0, length1 = currGeoJson.features.length; i < length1; i++){
    //     // let op = currGeoJson.features[i].properties.OP;
    //     // if (!Object.keys(status).includes(op)) continue;
    //     // let type = `${currGeoJson.features[i].properties.TYPE}-${currGeoJson.features[i].properties.BAND}`;
    //     currGeoJson.features[i].properties.isVisible = false;
    // }

    map.data.addGeoJson(currGeoJson, {idPropertyName: "IDX"});
    console.log(`Done adding ${path}`)
    updateHeatmapVisibility();
};

let partialLoadGeoJson = function () {
    let zoomLevel = getZoom();
    let pathToLoad = geoJsonPath;
    if (zoomLevel !== options.minZoomLevel) {
        pathToLoad = geoJsonPath.replace(/(-z\d+)?\.geojson$/, `-z${zoomLevel}.geojson`);
    }

    // Check before load
    if (addedHeatmap.includes(pathToLoad)) {
        console.log(`GeoJSON has already been added: ${pathToLoad}`);
        updateHeatmapVisibility();
        return;
    }
    addedHeatmap.push(pathToLoad);

    loadJSON(pathToLoad, addGeoJson);
};

let initGeoJson = function (initialGeoJson = null, initialPath = null) {
    console.log(`Init GeoJSON`, initialGeoJson, initialPath);

    if (initialPath !== null) {
        geoJsonPath = initialPath;
    }
    if (initialGeoJson !== null && map === undefined) {
        tempGeoJson = initialGeoJson;
        return;
    } else if (initialGeoJson === null && map !== undefined) {
        if (tempGeoJson !== null) {
            initialGeoJson = tempGeoJson;
            tempGeoJson = null;
        } else {
            return;
        }
    }

    for (let key in initialGeoJson.OPTIONS) {
        options[key] = initialGeoJson.OPTIONS[key];
    }
    delete initialGeoJson.OPTIONS;

    updateMapCenter(options);  // options dependent
    createLegendBox(options);  // options dependent

    addGeoJson(initialGeoJson, geoJsonPath);

    map.data.setStyle(function(feature) {
        let weight = feature.getProperty("WEIGHT");
        let visible = feature.getProperty("isVisible") || false;
        // let op = feature.getProperty("OP");
        // let type = feature.getProperty("TYPE");
        // let band = feature.getProperty("BAND");
        // let zoom = feature.getProperty("ZOOM");
        // let zoomLevel = getZoom();

        // let visible = false;
        // if (heatmapVisibility[op]) {
        //     visible = heatmapVisibility[op][`${type}-${band}`] && (zoom === zoomLevel);
        // }

        return {
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: getRgbaFromWeight(weight, gradientType[feature.getProperty("TYPE")]),
            fillOpacity: visible ? heatmapOpacity : 0,
            visible: visible
        }
    });
    map.data.addListener("click", (event) => {
        infoWindow.setContent(`RSRP: ${event.feature.getProperty("RSRP")}, # of data: ${event.feature.getProperty("NUM")}`);
        infoWindow.setPosition(event.latLng);
        infoWindow.open(map);
    });
}

let initMarkers = function() {
    if (markersAdded || Object.keys(fccData).length === 0 || typeof map === "undefined") return;
    markersAdded = true;

    let markerStates = getMarkerStates();

    for (let i = fccData.length - 1; i >= 0; i--) {
        if (!Object.keys(markerStates).includes(fccData[i].overview.operator)) continue;

        let loc = {lat: fccData[i].overview.avg_location.lat, lng: fccData[i].overview.avg_location.lon}
        let marker = new google.maps.Marker({position: loc, map: map, icon: fccPin[fccData[i].overview.connection]});
        marker.set('data', fccData[i]);
        marker.setVisible(markerStates[fccData[i].overview.operator][fccData[i].overview.connection]);
        marker.addListener('click', function() {
            let data = this.get('data');
            console.log(data);
            // let overview = [];
            // overview.push({field: 'Location', value: `${data.location.latitude}, ${data.location.longitude}`});
            // overview.push({field: 'Operator', value: operators[filename]});
            fillTable('overview', data.overview);
            fillTable('download', data.download);
            fillTable('upload', data.upload);
            fillTable('latency', data.latency);
        });

        if (markers[fccData[i].overview.operator] === undefined) markers[fccData[i].overview.operator] = {};
        if (markers[fccData[i].overview.operator][fccData[i].overview.connection] === undefined) markers[fccData[i].overview.operator][fccData[i].overview.connection] = [];
        markers[fccData[i].overview.operator][fccData[i].overview.connection].push(marker);
    }
}

let setMarkerVisible = function(op, type, val) {
    if (markers[op] && markers[op][type]) {
        for (let marker of markers[op][type]) {
            marker.setVisible(val);
        }
    }
}

let setHeatmapVisible = function(op, type, val) {
    let typeSplit = type.split("-");
    let zoomLevel = getZoom();
    map.data.forEach((feature) => {
        if (feature.getProperty("OP") == op
                && feature.getProperty("TYPE") == typeSplit[0]
                && feature.getProperty("BAND") == typeSplit[1]) {
            feature.setProperty("isVisible", (val && feature.getProperty("ZOOM") == zoomLevel));
        }
    });
};

let updateHeatmapOpacity = function() {
    let temp = {};
    heatmapVisibility = {};
    numHeatmapVisibility = 0;
    $('.checkbox-operator').each(function() {
        temp[$(this).attr("id")] = $(this).prop("checked");
        heatmapVisibility[$(this).attr("id")] = {};
    });
    $('.checkbox-heatmap').each(function() {
        for (let op in heatmapVisibility) {
            heatmapVisibility[op][$(this).attr("id")] = temp[op] && $(this).prop("checked");
            numHeatmapVisibility += heatmapVisibility[op][$(this).attr("id")];
        }
    });
    heatmapOpacity = (options.MAX_HEATMAP_OPACITY - options.MIN_HEATMAP_OPACITY) / numHeatmapVisibility + options.MIN_HEATMAP_OPACITY; // 1 / number of true items * MAX
};

let getMarkerStates = function() {
    let temp = {};
    let temp2 = {};
    $('.checkbox-operator').each(function() {
        temp[$(this).attr("id")] = $(this).prop("checked");
        temp2[$(this).attr("id")] = {};
    });
    $('.checkbox-marker').each(function() {
        for (let op in temp2) {
            temp2[op][$(this).attr("id")] = temp[op] && $(this).prop("checked");
        }
    });
    return temp2;
};

let updateMapCenter = function(opt) {
    prevZoom = opt.minZoomLevel;
    let bounds = new google.maps.LatLngBounds();
    bounds.extend({ lat: opt.minLat, lng: opt.minLng });
    bounds.extend({ lat: opt.maxLat, lng: opt.maxLng });
    setMapZoomAndCenter(bounds, opt.minZoomLevel + 1);
};

let createLegendBox = function(opt) {
    // Create legend gradient box
    $('#gradient-laa').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', 'linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', 'linear-gradient' + gradientCss);
    let legendContainerWidth = $('.map-legend').outerWidth();
    let legendWidth = $('#legend').outerWidth();
    let legendGradientWidth = $('#legend-gradient').outerWidth();
    let legendHeight = $('#legend').outerHeight(true);
    let legendGradientHeight = $('#legend-gradient').outerHeight();

    let maxIntensity = 4;
    let minOffset = Math.floor((legendContainerWidth - legendWidth) / 2);
    let yOffset = Math.floor((legendHeight - legendGradientHeight) / 2);

    for (let i = 0; i <= maxIntensity; ++i) {
        let offset = minOffset + i * legendGradientWidth / maxIntensity;
        if (i > 0 && i < maxIntensity) {
            offset -= 0.5;
        } else if (i == maxIntensity) {
            offset -= 1;
        }
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset - 6) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset + legendGradientHeight) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset - 24) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html((i * opt.rangeSsrsrp / maxIntensity + opt.minSsrsrp).toFixed(2)));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset + legendGradientHeight + 4) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html((i * opt.rangeRsrp / maxIntensity + opt.minRsrp).toFixed(2)));
    }
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 30) + 'px',
        'top': (yOffset - 24) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 30) + 'px',
        'top': (yOffset + legendGradientHeight + 4) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
};

const getDatasetPaths = function() {
    return getHash().split("_");
};

const updateHeatmapVisibility = function() {
    console.log(`Updating heatmap visibility`);
    updateHeatmapOpacity();
    for (let op in heatmapVisibility) {
        for (let type in heatmapVisibility[op]) {
            setHeatmapVisible(op, type, heatmapVisibility[op][type]);
        }
    }
    map.data.revertStyle();
    console.log(`Heatmap visibility updated`);
};

const updateMarkerVisibility = function() {
    let status = getMarkerStates();
    for (let op in status) {
        for (let type in status[op]) {
            setMarkerVisible(op, type, status[op][type]);
        }
    }
};

$(document).ready(function() {
    // Set legend checkboxes
    $('#submit-filters').click(function() {
        updateHeatmapVisibility();
        updateMarkerVisibility();
    });
});

const onZoomChanged = function(_) {
    let zoomLevel = getZoom();
    console.log(`Zoom changed from ${prevZoom} to ${zoomLevel}`);
    if (prevZoom !== zoomLevel) {
        prevZoom = zoomLevel;
        partialLoadGeoJson();
    }
};

// IMPORTANT
initMapCallback = function() {
    // Pin definition
    fccPin["5g"] = pins.pinRed;
    fccPin["4g"] = pins.pinBlue;
    fccPin["mixed"] = pins.pinPurple;
    fccPin["3g"] = pins.pinGreen;
    fccPin["2g"] = pins.pinYellow;
    fccPin["wi-fi"] = pins.pinGrey;

    // Init heatmap
    initGeoJson();
    initMarkers();

    prevZoom = map.zoom;

    // Map zoom callback
    map.addListener("zoom_changed", onZoomChanged);
};