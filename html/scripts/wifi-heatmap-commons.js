// Heatmap & GeoJSON
let addedHeatmap = [];
let geoJsonPath = "";
let tempGeoJson = null;

// FCC & JSON
let fccData = [];
let fccPin = {};
let markers = {};
let heatmapAdded = false;
let markersAdded = false;

// Visibility
let heatmapVisibility = {};
let numHeatmapVisibility = 0;
let markerVisibility = {};

let prevZoom = -1;

let options = {
    MAX_HEATMAP_OPACITY: 0.85,
    MIN_HEATMAP_OPACITY: 0.5,
};
let heatmapOpacity = options.MAX_HEATMAP_OPACITY; // (0,0.6]

let gradientBase = [
    [163,  0,  0,1],
    [255, 255,  0,1],
    [ 56, 200, 56,1],
];
let gradientBaseAlt = [
    [0, 255, 255, 1],
    [0, 0, 255, 1],
    [255, 0, 0, 1]
];
let gradientHeatmap = [];
let gradientHeatmapAlt = [];
for (let i = 0; i < gradientBase.length; ++i) {
        gradientHeatmap.push(`rgba(${gradientBase[i][0]},${gradientBase[i][1]},${gradientBase[i][2]},${gradientBase[i][3]})`);
}
for (let i = 0; i < gradientBaseAlt.length; ++i) {
        gradientHeatmapAlt.push(`rgba(${gradientBaseAlt[i][0]},${gradientBaseAlt[i][1]},${gradientBaseAlt[i][2]},${gradientBaseAlt[i][3]})`);
}
let gradientCss = '(left';
let gradientCssAlt = '(left';
for (let i = 0; i < gradientHeatmap.length; ++i) {
        gradientCss += ', ' + gradientHeatmap[i];
}
for (let i = 0; i < gradientHeatmapAlt.length; ++i) {
        gradientCssAlt += ', ' + gradientHeatmapAlt[i];
}
gradientCss += ')';
gradientCssAlt += ')';
let gradientType = {
    "2.4 GHz": gradientBase,
    "5 GHz": gradientBase,
    "6 GHz": gradientBase
};

let tableHeading = [ "Metrics", "Value at Beginning", "Value at End" ];

let getRgbaFromWeight = function (weight, gradientArr) {
    // For empty heatmap cell
    if (weight === -1) {
        return `rgba(0,0,0,0.3)`;
    }

    let gradientIndex = Math.floor(weight * (gradientArr.length - 1));
    // sanity check
    if (gradientIndex >= gradientArr.length - 1) gradientIndex = gradientArr.length - 2;
    let weightRel = weight * (gradientArr.length - 1) - gradientIndex;
    let red = (gradientArr[gradientIndex + 1][0] - gradientArr[gradientIndex][0]) * weightRel + gradientArr[gradientIndex][0];
    let grn = (gradientArr[gradientIndex + 1][1] - gradientArr[gradientIndex][1]) * weightRel + gradientArr[gradientIndex][1];
    let blu = (gradientArr[gradientIndex + 1][2] - gradientArr[gradientIndex][2]) * weightRel + gradientArr[gradientIndex][2];

    return `rgba(${red},${grn},${blu},1)`;
}

let getZoom = function () {
    let zoomLevel = map.getZoom() - 1;
    if (zoomLevel < options.minZoomLevel) {
        zoomLevel = options.minZoomLevel;
    } else if (zoomLevel > options.maxZoomLevel) {
        zoomLevel = options.maxZoomLevel;
    }
    return zoomLevel;
}

let addGeoJson = function (currGeoJson, path) {
    // let status = updateHeatmapOpacity();

    if (currGeoJson.OPTIONS) {
        delete currGeoJson.OPTIONS;
    }

    // for(let i = 0, length1 = currGeoJson.features.length; i < length1; i++){
    //     // let op = currGeoJson.features[i].properties.OP;
    //     // if (!Object.keys(status).includes(op)) continue;
    //     // let type = `${currGeoJson.features[i].properties.TYPE}-${currGeoJson.features[i].properties.BAND}`;
    //     currGeoJson.features[i].properties.isVisible = false;
    // }

    map.data.addGeoJson(currGeoJson, {idPropertyName: "IDX"});
    console.log(`Done adding ${path}`)
    updateHeatmapVisibility();
};

let partialLoadGeoJson = function () {
    let zoomLevel = getZoom();
    let pathToLoad = geoJsonPath;
    if (zoomLevel !== options.minZoomLevel) {
        pathToLoad = geoJsonPath.replace(/(-z\d+)?\.geojson$/, `-z${zoomLevel}.geojson`);
    }

    // Check before load
    if (addedHeatmap.includes(pathToLoad)) {
        console.log(`GeoJSON has already been added: ${pathToLoad}`);
        updateHeatmapVisibility();
        return;
    }
    addedHeatmap.push(pathToLoad);

    loadJSON(pathToLoad, addGeoJson);
};

let initGeoJson = function (initialGeoJson = null, initialPath = null) {
    console.log(`Init GeoJSON`, initialGeoJson, initialPath);

    if (initialPath !== null) {
        geoJsonPath = initialPath;
    }
    if (initialGeoJson !== null && map === undefined) {
        tempGeoJson = initialGeoJson;
        return;
    } else if (initialGeoJson === null && map !== undefined) {
        if (tempGeoJson !== null) {
            initialGeoJson = tempGeoJson;
            tempGeoJson = null;
        } else {
            return;
        }
    }

    for (let key in initialGeoJson.OPTIONS) {
        options[key] = initialGeoJson.OPTIONS[key];
    }
    delete initialGeoJson.OPTIONS;

    updateMapCenter(options);  // options dependent
    createLegendBox(options);  // options dependent
    createFreqOptions(options.freqList) // options dependent

    addGeoJson(initialGeoJson, geoJsonPath);

    map.data.setStyle(function(feature) {
        let weight = feature.getProperty("WEIGHT");
        let visible = feature.getProperty("isVisible") || false;
        // let op = feature.getProperty("OP");
        // let type = feature.getProperty("TYPE");
        // let band = feature.getProperty("BAND");
        // let zoom = feature.getProperty("ZOOM");
        // let zoomLevel = getZoom();

        // let visible = false;
        // if (heatmapVisibility[op]) {
        //     visible = heatmapVisibility[op][`${type}-${band}`] && (zoom === zoomLevel);
        // }

        return {
            strokeOpacity: 0,
            strokeWeight: 0,
            fillColor: getRgbaFromWeight(weight, gradientType[feature.getProperty("TYPE")]),
            fillOpacity: visible ? heatmapOpacity : 0,
            visible: visible
        }
    });
    map.data.addListener("click", (event) => {
        let rssi = event.feature.getProperty("RSSI");
        let num = event.feature.getProperty("NUM");
        if (rssi && num) {
            infoWindow.setContent(`RSSI: ${rssi}, # of data: ${num}`);
            infoWindow.setPosition(event.latLng);
            infoWindow.open(map);
        }
    });
}

let setHeatmapVisible = function(type, freq, val) {
    let zoomLevel = getZoom();
    map.data.forEach((feature) => {
        if (feature.getProperty("TYPE") == type
                && feature.getProperty("FREQ") == freq) {
            feature.setProperty("isVisible", (val && feature.getProperty("ZOOM") == zoomLevel));
        }
    });
};

let updateHeatmapOpacity = function() {
    heatmapVisibility = {};
    numHeatmapVisibility = 0;
    $('.checkbox-freq').each(function() {
        let freq = $(this).attr("id");
        let type = $(this).attr("name");
        if (heatmapVisibility[type] === undefined) {
            heatmapVisibility[type] = {};
        }
        heatmapVisibility[type][freq] = $(this).prop("checked");
        numHeatmapVisibility += heatmapVisibility[type][freq];
    });
    heatmapOpacity = (options.MAX_HEATMAP_OPACITY - options.MIN_HEATMAP_OPACITY) / numHeatmapVisibility + options.MIN_HEATMAP_OPACITY; // 1 / number of true items * MAX
};

let updateMapCenter = function(opt) {
    prevZoom = opt.minZoomLevel;
    let bounds = new google.maps.LatLngBounds();
    bounds.extend({ lat: opt.minLat, lng: opt.minLng });
    bounds.extend({ lat: opt.maxLat, lng: opt.maxLng });
    setMapZoomAndCenter(bounds, opt.minZoomLevel + 1);
};

let createFreqOptions = function(freqList) {
    for (let type in freqList) {
        for (let freq of freqList[type]) {
            $(`div[name="${type}"]`).append($(`<div class="form-check form-check-inline">
                    <input class="form-check-input checkbox-freq" type="checkbox" id="${freq}" name="${type}" ${freq === "all" ? "checked " : ""}/>
                    <label class="form-check-label" for="${freq}">${freq}</label>`));
        }
    }
}

let createLegendBox = function(opt) {
    // Create legend gradient box
    $('#gradient-laa').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', 'linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-wifi').css('background', 'linear-gradient' + gradientCss);
    let legendContainerWidth = $('.map-legend').outerWidth();
    let legendWidth = $('#legend').outerWidth();
    let legendGradientWidth = $('#legend-gradient').outerWidth();
    let legendHeight = $('#legend').outerHeight(true);
    let legendGradientHeight = $('#legend-gradient').outerHeight();

    let maxIntensity = 4;
    let minOffset = Math.floor((legendContainerWidth - legendWidth) / 2);
    let yOffset = Math.floor((legendHeight - legendGradientHeight) / 2);

    for (let i = 0; i <= maxIntensity; ++i) {
        let offset = minOffset + i * legendGradientWidth / maxIntensity;
        if (i > 0 && i < maxIntensity) {
            offset -= 0.5;
        } else if (i == maxIntensity) {
            offset -= 1;
        }
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset - 6) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset + legendGradientHeight) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset - 24) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html((i * opt.rangeRssi / maxIntensity + opt.minRssi).toFixed(2)));
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset + legendGradientHeight + 4) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html((i * opt.rangeRssi / maxIntensity + opt.minRssi).toFixed(2)));
    }
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 30) + 'px',
        'top': (yOffset - 24) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 30) + 'px',
        'top': (yOffset + legendGradientHeight + 4) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("dBm"));
};

const getDatasetPaths = function() {
    return getHash().split("_");
};

const updateHeatmapVisibility = function() {
    console.log(`Updating heatmap visibility`);
    updateHeatmapOpacity();
    for (let type in heatmapVisibility) {
        for (let freq in heatmapVisibility[type]) {
            setHeatmapVisible(type, freq, heatmapVisibility[type][freq]);
        }
    }
    map.data.revertStyle();
    console.log(`Heatmap visibility updated`);
};

$(document).ready(function() {
    // Set legend checkboxes
    $('#submit-filters').click(function() {
        updateHeatmapVisibility();
    });
});

const onZoomChanged = function(_) {
    let zoomLevel = getZoom();
    console.log(`Zoom changed from ${prevZoom} to ${zoomLevel}`);
    if (prevZoom !== zoomLevel) {
        prevZoom = zoomLevel;
        partialLoadGeoJson();
    }
};

// IMPORTANT
initMapCallback = function() {
    // Init heatmap
    initGeoJson();

    prevZoom = map.zoom;

    // Map zoom callback
    map.addListener("zoom_changed", onZoomChanged);
};