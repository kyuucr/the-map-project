let heatmaps = {};
let heatmapLayer = {};
let fccData = [];
let fccPin = {};
let markers = {};
let heatmapAdded = false;
let markersAdded = false;

let opList = ["AT&T", "T-Mobile", "Verizon"];
let typeList = ["4g", "5g", "mixed", "3g", "2g"];
let testList = ["download", "upload"];

let options = {
    MAX_HEATMAP_OPACITY: 0.85,
    MIN_HEATMAP_OPACITY: 0.5,
    minLat: 99.9,
    maxLat: -99.9,
    minLng: 99.9,
    maxLng: -99.9,
    minTput: 0,
    maxTput: -999.0,
    zoom: 8
};
let heatmapOpacity = options.MAX_HEATMAP_OPACITY; // (0,0.6]

let gradient = [
    "rgba(000,255,255,0)",
    "rgba(000,255,000,1)",
    "rgba(255,255,000,1)",
    "rgba(255,000,000,1)",
    "rgba(163,000,000,1)"
];
let gradientAlt = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
];
let gradientCss = '(left';
let gradientCssAlt = '(left';
for (let i = 0; i < gradient.length; ++i) {
        gradientCss += ', ' + gradient[i];
}
for (let i = 0; i < gradientAlt.length; ++i) {
        gradientCssAlt += ', ' + gradientAlt[i];
}
gradientCss += ')';
gradientCssAlt += ')';


let initHeatmap = function() {
    if (heatmapAdded || fccData.length === 0 || typeof map === "undefined") return;
    heatmapAdded = true;

    let status = updateHeatmapOpacity();
    let grd = {
        upload: gradient,
        download: gradient,
        latency: gradientAlt
    };
    let heatmapData = {};

    for(let i = 0, length1 = fccData.length; i < length1; i++){
        let op = fccData[i].overview.operator;
        let type = fccData[i].overview.connection;
        let weightDownload = fccData[i].overview.download / options.maxTput;
        let weightUpload = fccData[i].overview.upload / options.maxTput;

        if (!heatmapData[op]) {
            heatmapData[op] = {};
        }
        if (!heatmapData[op][type]) {
            heatmapData[op][type] = { download: [], upload: [] };
        }

        heatmapData[op][type].download.push({
            location: new google.maps.LatLng(fccData[i].overview.location.lat, fccData[i].overview.location.lon),
            weight: weightDownload
        });
        heatmapData[op][type].upload.push({
            location: new google.maps.LatLng(fccData[i].overview.location.lat, fccData[i].overview.location.lon),
            weight: weightUpload
        });
    }
    for (let op in heatmapData) {
        heatmapLayer[op] = {};
        for (let type in heatmapData[op]) {
            heatmapLayer[op][type] = {};
            for (let test in heatmapData[op][type]) {
                let opts = {
                    data: heatmapData[op][type][test],
                    map: map,
                    opacity: status[op][type][test] ? heatmapOpacity : 0,
                    gradient: grd[test],
                    // dissipating: false,
                    // radius: 0.0001,
                    maxIntensity: 1
                };

                heatmapLayer[op][type][test] = new google.maps.visualization.HeatmapLayer(opts);;
            }
        }
    }
}

let updateHeatmapOpacity = function() {
    let num = 0;
    let temp = {};
    let temp2 = {};
    let temp3 = {};
    $('.checkbox-operator').each(function() {
        temp[$(this).attr("id")] = $(this).prop("checked");
        temp2[$(this).attr("id")] = {};
        temp3[$(this).attr("id")] = {};
    });
    $('.checkbox-heatmap').each(function() {
        for (let op in temp2) {
            temp2[op][$(this).attr("id")] = temp[op] && $(this).prop("checked");
            temp3[op][$(this).attr("id")] = {};
        }
    });
    $('.checkbox-test').each(function() {
        for (let op in temp3) {
            for (let type in temp3[op]) {
                temp3[op][type][$(this).attr("id")] = temp2[op][type] && $(this).prop("checked");
                num += temp3[op][type][$(this).attr("id")];
            }
        }
    });
    heatmapOpacity = (options.MAX_HEATMAP_OPACITY - options.MIN_HEATMAP_OPACITY) / num + options.MIN_HEATMAP_OPACITY; // 1 / number of true items * MAX
    return temp3;
};

let updateMapCenter = function(opt) {
    opt.latCenter = (opt.maxLat - opt.minLat) / 2 + opt.minLat;
    opt.lngCenter = (opt.maxLng - opt.minLng) / 2 + opt.minLng;
    map.setCenter({lat: opt.latCenter, lng: opt.lngCenter});
    map.setZoom(opt.zoom);
}

let createLegendBox = function(opt) {
    // Create legend gradient box
    $('#gradient-laa').css('background', '-webkit-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-moz-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', '-o-linear-gradient' + gradientCss);
    $('#gradient-laa').css('background', 'linear-gradient' + gradientCss);
    let legendContainerWidth = $('.map-legend').outerWidth();
    let legendWidth = $('#legend').outerWidth();
    let legendGradientWidth = $('#legend-gradient').outerWidth();
    let legendContainerHeight = $('#form-heatmap-2').outerHeight();
    let legendHeight = $('#legend').outerHeight(true);
    let legendGradientHeight = $('#legend-gradient').outerHeight();

    let maxIntensity = 4;
    let minOffset = Math.floor((legendContainerWidth - legendWidth) / 2);
    let yOffset = Math.floor(legendContainerHeight - ((legendHeight + legendGradientHeight) / 2));
    let tputRange = opt.maxTput - opt.minTput;

    for (let i = 0; i <= maxIntensity; ++i) {
        let offset = minOffset + i * legendGradientWidth / maxIntensity;
        if (i > 0 && i < maxIntensity) {
            offset -= 0.5;
        } else if (i == maxIntensity) {
            offset -= 1;
        }
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': offset + 'px',
            'top': (yOffset - 6) + 'px',
            'width': '1px',
            'height': '6px',
            'background': 'black'
        }));
        // $('#legend').append($('<div>').css({
        //     'position': 'absolute',
        //     'left': offset + 'px',
        //     'top': (yOffset + legendGradientHeight) + 'px',
        //     'width': '1px',
        //     'height': '6px',
        //     'background': 'black'
        // }));
        let text = ((i * tputRange / maxIntensity + opt.minTput) * 8 / 1e6).toFixed(0);
        $('#legend').append($('<div>').css({
            'position': 'absolute',
            'left': (offset - 10) + 'px',
            'top': (yOffset - 24) + 'px',
            'width': '10px',
            'text-align': 'center'
        }).html(text));
        // $('#legend').append($('<div>').css({
        //     'position': 'absolute',
        //     'left': (offset - 10) + 'px',
        //     'top': (yOffset + legendGradientHeight + 4) + 'px',
        //     'width': '10px',
        //     'text-align': 'center'
        // }).html(i * opt.rangeRsrp / maxIntensity + opt.minRsrp));
    }
    $('#legend').append($('<div>').css({
        'position': 'absolute',
        'left': (minOffset + legendGradientWidth + 10) + 'px',
        'top': (yOffset - 24) + 'px',
        'width': '10px',
        'text-align': 'center'
    }).html("Mbps"));
    // $('#legend').append($('<div>').css({
    //     'position': 'absolute',
    //     'left': (minOffset + legendGradientWidth + 10) + 'px',
    //     'top': (yOffset + legendGradientHeight + 4) + 'px',
    //     'width': '10px',
    //     'text-align': 'center'
    // }).html("Mbps"));
};

$(document).ready(function() {
    // Set legend checkboxes
    let checkboxChanged = function() {
        let status = updateHeatmapOpacity();
        for (let op in status) {
            for (let type in status[op]) {
                for (let test in status[op][type]) {
                    if (heatmapLayer[op] && heatmapLayer[op][type] && heatmapLayer[op][type][test]) {
                        heatmapLayer[op][type][test].setOptions({
                            opacity: status[op][type][test] ? heatmapOpacity : 0
                        });
                    }
                }
            }
        }
    };
    $('.checkbox-heatmap').change(checkboxChanged);
    $('.checkbox-test').change(checkboxChanged);
    $('.checkbox-operator').change(checkboxChanged);
});

let heatmapCallback = function(json, filename) {
    fccData = json;
    
    for(let i = 0, length1 = fccData.length; i < length1; i++){
        options.minLat = (options.minLat > fccData[i].overview.location.lat) ? fccData[i].overview.location.lat : options.minLat;
        options.minLng = (options.minLng > fccData[i].overview.location.lon) ? fccData[i].overview.location.lon : options.minLng;
        options.maxLat = (options.maxLat < fccData[i].overview.location.lat) ? fccData[i].overview.location.lat : options.maxLat;
        options.maxLng = (options.maxLng < fccData[i].overview.location.lon) ? fccData[i].overview.location.lon : options.maxLng;
        options.maxTput = (options.maxTput < fccData[i].overview.download) ? fccData[i].overview.download : options.maxTput;
        options.maxTput = (options.maxTput < fccData[i].overview.upload) ? fccData[i].overview.upload : options.maxTput;
    }
    updateMapCenter(options);  // options dependent
    createLegendBox(options);  // options dependent
    // init if not yet initiated
    initHeatmap();
};

$(document).ready(function() {
    loadJSON("outputs/fcc-data.json", heatmapCallback);
});

// IMPORTANT
initMapCallback = function() {
    // Init heatmap
    initHeatmap();
};