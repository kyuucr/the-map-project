
let heatmapCallback = function(json, filename) {
    for (let key in json) {
        if (key === "data") {
            for (let op in json[key]) {
                heatmaps[op] = {};
                for (let type in json[key][op]) {
                    heatmaps[op][type] = json[key][op][type];
                }
            }
        } else {
            options[key] = json[key];
        }
    }
    updateMapCenter(options);  // options dependent
    createLegendBox(options);  // options dependent
    // init if not yet initiated
    initHeatmap();
};

let fccCallback = function(json, filename) {
    fccData = filter.filterArray({overview: {operator: "AT&T"}}, json, true);

    initMarkers();
}

$(document).ready(function() {
    loadJSON("outputs/nr-heatmaps-focused.json", heatmapCallback);
    loadJSON("outputs/fcc-data.json", fccCallback);
});
