let geoJsonCallback = function(json, path) {
    initGeoJson(json, path);
}

let fccCallback = function(json, path) {
    fccData = json;

    initMarkers();
}

let reload = function() {
    let hasHeatmapPath = false, hasFccPath = false;
    if (defaultPaths.force === false) {
        /**
         * Proper dataset path (from location.hash, "#" removed):
         *   - Heatmap path heatmap.geojson and fcc path fcc.json: heatmap.geojson_fcc.json
         *   - Heatmap path only: foo-heatmap.geojson
         *   - Specify directory: 2022-campaign/foo-heatmap.geojson_2022-campaign/foo-fcc.json
         */
        let datasetPaths = getDatasetPaths();
        for (let path of datasetPaths) {
            if (path.includes("heatmap")) {
                hasHeatmapPath = true;
                if (!path.startsWith("outputs/")) {
                    path = "outputs/" + path;
                }
                if (!path.endsWith(".geojson")) {
                    path = path + ".geojson";
                }
                loadJSON(path, geoJsonCallback);
            } else if (path.includes("fcc")) {
                hasFccPath = true;
                if (!path.startsWith("outputs/")) {
                    path = "outputs/" + path;
                }
                if (!path.endsWith(".json")) {
                    path = path + ".json";
                }
                loadJSON(path, fccCallback);
            }
        }
    }

    // Load defaults
    if (!hasHeatmapPath) {
        geoJsonPath = defaultPaths.heatmap;
        loadJSON(defaultPaths.heatmap, geoJsonCallback);
    }
    if (!hasFccPath) {
        loadJSON(defaultPaths.fcc, fccCallback);
    }
};

$(document).ready(reload);
