-----------------
# The Map Project
-----------------

The Map Project is a nodejs-based framework to map and process JSON files outputted by SigCap[1] and FCC Speed Test[2] apps.

## Setup

- Install node.js latest LTS version from https://nodejs.org/.
- Git clone or download from https://bitbucket.org/kyuucr/the-map-project/downloads/, unpack and open folder in terminal.
- _Optional to use Mapping and Storage scripts_: run `npm install`.

## CSV Scripts' Usage

### FCC Export to CSV

To process the exported FCC ST files into CSV:
```
node fcc-export-to-csv.js <FCC speed test dir> [output path] [--filter <filter JSON string>]
```

- <FCC speed test dir>: Mandatory, path to input directory containing JSON files
- [output path]: Optional, defaults to "./output-fcc.csv"
- [--filter <filter JSON string>]: Options to specify JSON filter


### SigCap to CSV

To process the SigCap files into a "wide" CSV that contains all Cellular and Wi-Fi information in one file:
```
node sigcap-to-csv.js <input folder> [output path] [--max-lte <max lte cell limit>] [--max-wifi <max wifi AP limit>] [--filter <filter JSON string>] [--include-invalid-op] [--print-sensor-data]
```

- <input folder>: Mandatory, path to input directory containing JSON files
- [output path]: Optional, defaults to "./output-sigcap.csv"
- [--max-lte <max lte cell limit>]: Set maximum LTE headers to be displayed, default is no limit
- [--max-wifi <max wifi AP limit>]: Set maximum Wi-Fi headers to be displayed, default is no limit
- [--filter <filter JSON string>]: Options to specify JSON filter
- [--include-invalid-op]: By default, unknown or invalid Operator name will be skipped, enable this for allow them
- [--print-sensor-data]: Enable this parameter to print sensor data


### SigCap to Cellular CSV

To process the SigCap files into CSV file that contains only Cellular information (e.g., primary/other cell, frequency, RSRP, RSRQ):
```
node sigcap-to-cellular-csv.js <input folder> [output path] [--filter <filter JSON string>]
```

- <input folder>: Mandatory, path to input directory containing JSON files
- [output path]: Optional, defaults to "./output-sigcap.csv"
- [--filter <filter JSON string>]: Options to specify JSON filter


### SigCap to Wi-Fi CSV

To process the SigCap files into CSV file that contains only Wi-Fi information (e.g., channel number, frequency, bandwidth, RSSI):
```
nodejs sigcap-to-wifi-csv.js <input folder> <output path> [JSON/BSSID filters ...]
```

- <input folder>: Mandatory, path to input directory containing JSON files
- <output path>: Mandatory output path
- [JSON/BSSID filters ...]: Options to specify multiple JSON or BSSID filters


### SigCap to GPS CSV

To process the SigCap files into CSV file that contains only GPS information:
```
nodejs sigcap-to-wifi-csv.js <input folder> <output path>
```

- <input folder>: Mandatory, path to input directory containing JSON files
- <output path>: Mandatory output path


### SigCap to ML CSV

To process the SigCap files into CSV file for ML use:
```
nodejs sigcap-to-ml-csv.js <input folder> [output CSV file] [--cell-limit <num>] [--wifi-limit <num>] [--filter <JSON filter>]
```

- <input folder>: Mandatory, path to input directory containing JSON files. The input folder name or
each subfolder inside it must contains "indoor", "outdoor", "mostly indoor", "mostly outdoor" for the
contained SigCap data to be labelled accordingly.
- [output CSV file]: Optional, defaults to "./output-sigcap-ml.csv"
- [--cell-limit <num>]: Optional limit to the number of LTE and NR cell, defaults to unlimited
- [--wifi-limit <num>]: Optional limit to the number of WiFi AP (2.4 and 5 GHz) , defaults to unlimited
- [--filter <filter JSON string>]: Options to specify JSON filter


## Mapping Scripts' Usage

### Create SigCap Cell Heatmap

To create SigCap heatmap (geojson file) of cellular signal (4G, 5G):
```
node mapping-sigcap-cell-heatmap.js <input SigCap dir> [output path with filename (default: "html/outputs/nr-heatmaps.geojson")]
```
This will create multiple geojson files to view at varying zoom levels. To view the map, first run the http server:
```
npm run server
```
Then open a web browser and open the address http://localhost:8000/nr-heatmap.html#filename.geojson (replace filename.geojson with your output geojson file).

## Storage Scripts' Usage

The Storage scripts requires Firebase admin authentication key to pull SigCap data from Firebase server. Put the key in the `storage/auth` folder and edit the `
const serviceAccount = require("./auth/<newfile.json>");` line in `storage/storage.js` file if necessary.

### Fetch Data from Firebase Server

To fetch SigCap data from Firebase server:
```
node storage/main.js fetch --no-hash-check
```


------------------------
[1] https://muhiqbalcr.page/SigCap
[2] https://play.google.com/store/apps/details?id=com.samknows.fcc
