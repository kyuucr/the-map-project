#!/bin/bash

if [[ -z "$2" ]]; then
    echo "./do-upload.sh <server_name> <server_dir> [--dry-run] [--file-list file-list.txt]"
    exit
fi

server=$1
dir=$2

for arg in "$@"; do
    if [[ -n "$file" ]]; then
        fileList=$arg
        unset file
    else
        case $arg in
            "--dry-run" )
                dry=1
                ;;
            "--file-list" )
                file=1
                ;;
        esac
    fi
done

if [[ -n "$fileList" ]]; then
    echo "Using file list: $fileList"
    files=""
    while IFS="" read -r line
    do
        files="$files $line"
    done < "$fileList"
else
    files=`find ./html/*`
fi
# echo $file

echo "Uploading to ${server}:${dir}"

command="chmod 755 ${dir};"
dirs="mkdir -p ${dir};"
filesOnly=""
for f in $files ; do
    if [[ -d "$f" ]] ; then
        command="${command} chmod 755 ${dir}${f#./html/};"
        dirs="$dirs mkdir -p ${dir}${f#./html/};"
    elif [[ "$f" == *">"* ]]; then
        IFS='>' read -ra split <<< "$f"
        command="${command} chmod 644 ${dir}${split[1]#./html/};"
        filesOnly="$filesOnly $f"
    else
        command="${command} chmod 644 ${dir}${f#./html/};"
        filesOnly="$filesOnly $f"
    fi
done

if [[ -n "$dry" ]] ; then
    echo "Dry run: ssh ${server} $dirs"
    for f in $filesOnly; do
        if [[ "$f" == *">"* ]]; then
            IFS='>' read -ra split <<< "$f"
            echo "Dry run: scp ${split[0]} ${server}:${dir}${split[1]#./html/}"
        else
            echo "Dry run: scp $f ${server}:${dir}${f#./html/}"
        fi
    done
    echo "Dry run: ssh ${server} $command"
else
    ssh ${server} "${dirs}"
    for f in $filesOnly; do
        if [[ "$f" == *">"* ]]; then
            IFS='>' read -ra split <<< "$f"
            scp ${split[0]} ${server}:${dir}${split[1]#./html/}
        else
            scp ${f} ${server}:${dir}${f#./html/}
        fi
    done
    ssh ${server} "${command}"
fi

if [[ ${server} == *"@linux.cs.uchicago.edu" ]]; then
    outDir=${dir#\~/html/}
    outDir=${outDir%/}
    echo "Uploaded to https://people.cs.uchicago.edu/~${server%@linux.cs.uchicago.edu}/${outDir}/"
fi
