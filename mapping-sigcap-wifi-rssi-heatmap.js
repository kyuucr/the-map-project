const agg = require('./lib/aggregator');
const cellHelper = require('./lib/cell-helper');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const geohelper = require('./lib/geohelper');
const powerUtils = require('./lib/power-utils');
const fs = require('fs');
const path = require('path');

////////////////////////////////
// Definitions
////////////////////////////////

const OPTIONS = {
    inputDir: null, // SigCap input dir
    tileSize: 12.5,   // array tile size
    accTh: -1,  // threshold of accuracy
    centroid: false, // output each latLng as centroid?
    filter: null,
    pciSelect: -1,
    noNr: false,
    noLte: false,
    minZoomLevel: 10,
    maxZoomLevel: 15,
    multipleZoomOutput: true,
    skip2_4GHz: false,
    skip5GHz: false,
    skip6GHz: false,
    outputPath: path.join("html", "outputs", "wifi-heatmaps.geojson") // output path
};

let createGeoJson = function (heatmap, options, lengths, zoomFilter = -1) {
    console.log("Creating output geoJSON");
    let output = {
        OPTIONS: options,
        type: "FeatureCollection",
        features: []
    };
    for (let type in heatmap) {
        for (let freq in heatmap[type]) {
            for (let zoomIdx in heatmap[type][freq]) {
                // If filter set, exclude zoomIdx !== zoomFilter
                let zoomIdxInt = parseInt(zoomIdx);
                if (zoomFilter !== -1 && zoomIdxInt !== zoomFilter) {
                    continue;
                }
                for (let idx in heatmap[type][freq][zoomIdx]) {
                    let latLng = idx.split("-");
                    let latIdx = parseFloat(latLng[0]);
                    let lngIdx = parseFloat(latLng[1]);
                    let lat0 = options.minLat + (latIdx * lengths[zoomIdx].lat);
                    let lat1 = options.minLat + ((latIdx + 1) * lengths[zoomIdx].lat);
                    let lng0 = options.minLng + (lngIdx * lengths[zoomIdx].lng);
                    let lng1 = options.minLng + ((lngIdx + 1) * lengths[zoomIdx].lng);
                    let rssi = heatmap[type][freq][zoomIdx][idx].rssi;
                    let weight = -1;
                    if (!isNaN(rssi)) {
                        weight = (rssi - options.minRssi) / options.rangeRssi;
                    }
                    let zoomLevel = options.minZoomLevel + zoomIdxInt;

                    let temp = {
                        type: "Feature",
                        properties: {
                            isVisible: false,
                            IDX: `${type}-${freq}-zoom${zoomLevel}-${idx}`,
                            DATATYPE: `rssi`,
                            TYPE: type,
                            FREQ: freq,
                            WEIGHT: weight,
                            ZOOM: zoomLevel,
                            RSSI: rssi,
                            NUM: heatmap[type][freq][zoomIdx][idx].k
                        },
                        geometry: {
                            type: "Polygon",
                            coordinates: [
                                [
                                    [ lng0, lat0 ],
                                    [ lng1, lat0 ],
                                    [ lng1, lat1 ],
                                    [ lng0, lat1 ],
                                    [ lng0, lat0 ]
                                ]
                            ]
                        }
                    }
                    output.features.push(temp);
                }
            }
        }
    }
    return output;
};

////////////////////////////////
// Command line
////////////////////////////////

let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`
Usage: node ${path.basename(__filename)} <SigCap input dir>
            [output path with filename (default: "html/outputs/wifi-heatmaps.geojson")]
            [--tile-size array tile size in meter at maximum zoom level (default: 12.5)]
            [--min-zoom minimum Google Map zoom level (default: 10)]
            [--max-zoom maximum Google Map zoom level (default: 15)]
            [--multiple-zoom-output]
            [--acc-th GPS accuracy threshold in meter (default: unset)]
            [--filter filter input string or path (default: unset)]
            [--centroid flag to enable centroid for lat-lng output (default: unset)]`);
    process.exit(0);
}

while (args.length > 0) {
    switch (args[0]) {
        case "--tile-size":
            OPTIONS.tileSize = parseFloat(args[1]);
            args.splice(0, 2);
            break;
        case "--acc-th":
            OPTIONS.accTh = parseFloat(args[1]);
            args.splice(0, 2);
            break;
        case "--min-zoom":
            OPTIONS.minZoomLevel = parseInt(args[1]);
            args.splice(0, 2);
            break;
        case "--max-zoom":
            OPTIONS.maxZoomLevel = parseInt(args[1]);
            args.splice(0, 2);
            break;
        case "--multiple-zoom-output":
            OPTIONS.multipleZoomOutput = true;
            args.splice(0, 1);
            break;
        case "--centroid":
            OPTIONS.centroid = true;
            args.splice(0, 1);
            break;
        case "--output":
            OPTIONS.outputPath = args[1];
            args.splice(0, 2);
            break;
        case "--filter":
            let jsonString = args[1];
            if (jsonString.match(/\.json$/)) jsonString = fs.readFileSync(jsonString);
            OPTIONS.filter = JSON.parse(jsonString);
            args.splice(0, 2);
            break;
        case "--skip-2.4ghz":
            OPTIONS.skip2_4GHz = true;
            args.splice(0, 1);
            break;
        case "--skip-5ghz":
            OPTIONS.skip5GHz = true;
            args.splice(0, 1);
            break;
        case "--skip-6ghz":
            OPTIONS.skip6GHz = true;
            args.splice(0, 1);
            break;

        default:
            if (OPTIONS.inputDir !== null) {
                OPTIONS.outputPath = args[0];
            } else {
                OPTIONS.inputDir = args[0];
            }
            args.splice(0, 1);
            break;
    }
}

const getType = function(freq) {
    if (freq < 5000) {
        return "2.4 GHz";
    } else if (freq > 5900) {
        return "6 GHz";
    } else {
        return "5 GHz";
    }
}

if (OPTIONS.accTh > 0) console.log(`Using accuracy threshold: ${OPTIONS.accTh} m`);
console.log(`Using tile size: ${OPTIONS.tileSize} m`);

const zoomRange = OPTIONS.maxZoomLevel - OPTIONS.minZoomLevel;
// zoomLevel: 0 (biggest tile, zoomed out, at max zoom level) ----> zoomRange (smallest tile, zoomed in, at max zoom level)
const tileSizeAtZoom = function(zoomLevel) {
    // Invert zoom level: 2^(zoomRange - zoomLevel)
    return Math.pow(2, zoomRange - zoomLevel) * OPTIONS.tileSize;
}
console.log(`Google Map zoom level ${OPTIONS.minZoomLevel} to ${OPTIONS.maxZoomLevel}`);

////////////////////////////////
// Preprocessing
////////////////////////////////
let minLat = 999.9;
let maxLat = -999.9;
let minLng = 999.9;
let maxLng = -999.9;
let freqList = { "2.4 GHz": [ "all" ], "5 GHz": [ "all" ], "6 GHz": [ "all" ] };
console.log(`Preprocessing lat-lng and freq...`);
agg.callbackJsonRecursive(OPTIONS.inputDir, data => {
    let allCells = data.json;

    if (OPTIONS.filter !== null) {
        allCells = filter.filterArray(OPTIONS.filter, allCells);
    }

    for (let i = allCells.length - 1; i >= 0; i--) {
        // Skip data with uninitialized GPS coordinate
        let lat = allCells[i].location.latitude;
        let lng = allCells[i].location.longitude;
        if (lat === 0 || lng === 0) {
            continue;
        }

        minLat = (minLat > allCells[i].location.latitude) ? allCells[i].location.latitude : minLat;
        minLng = (minLng > allCells[i].location.longitude) ? allCells[i].location.longitude : minLng;
        maxLat = (maxLat < allCells[i].location.latitude) ? allCells[i].location.latitude : maxLat;
        maxLng = (maxLng < allCells[i].location.longitude) ? allCells[i].location.longitude : maxLng;
        if (allCells[i].wifi_info) {
            for (let wifi of allCells[i].wifi_info) {
                let type = getType(wifi.primaryFreq);
                if (!freqList[type].includes(wifi.primaryFreq)) {
                    freqList[type].push(wifi.primaryFreq);
                }
            }
        }
    }
});

console.log(`Lat-Lng area: ${minLat}, ${maxLat}, ${minLng}, ${maxLng}`);
console.log(`Center: ${(maxLat - minLat) / 2 + minLat}, ${(maxLng - minLng) / 2 + minLng}`);

// Convert tile size to lat lng degree, since it is more consistent (length of 1-degree longitude depends on the latitude)
// Now tile size is accurate to the midway between min and max latitude, and min longitude
const latLngLength = {};
for (let i = 0; i <= zoomRange; i++) {
    latLngLength[i] = {
        lat: geohelper.addLatitude(minLat, minLng, tileSizeAtZoom(i)) - minLat,
        lng: geohelper.addLongitude((maxLat - minLat) / 2 + minLat, minLng, tileSizeAtZoom(i)) - minLng,
    };
}

// This is now just informational
let latTotal = geohelper.measure(minLat, minLng, maxLat, minLng);
let lngTotalAtMinLat = geohelper.measure(minLat, minLng, minLat, maxLng);
let lngTotalAtMaxLat = geohelper.measure(maxLat, minLng, maxLat, maxLng);
console.log(`Total latitudinal length: ${latTotal}m`);
console.log(`Total longitudinal length at minimum latitude: ${lngTotalAtMinLat}m`);
console.log(`Total longitudinal length at maximum latitude: ${lngTotalAtMaxLat}m`);

////////////////////////////////
// Heatmap
////////////////////////////////

// This will work since obj is not reassigned
const updateRssi = function(obj, type, freq, idxList, rssi) {
    // Update on all zoom levels
    for (let i = 0; i <= zoomRange; i++) {
        if (obj[type][freq][i] === undefined) obj[type][freq][i] = {};
        if (obj[type][freq][i][idxList[i]] === undefined) {
            obj[type][freq][i][idxList[i]] = {};
            obj[type][freq][i][idxList[i]].k = 1;
            obj[type][freq][i][idxList[i]].rssi = rssi;
        } else {
            ++obj[type][freq][i][idxList[i]].k;
            obj[type][freq][i][idxList[i]].rssi += (rssi - obj[type][freq][i][idxList[i]].rssi) / obj[type][freq][i][idxList[i]].k;
        }
    }

};

// Note: lat = y, lng = x
console.log("Calculating heatmap of average RSSI");

// Generate empty heatmap
let heatmap = {};
for (let type in freqList) {
    heatmap[type] = {};
    freqList[type].sort(dataUtils.sortNumAsc);
    for (let freq of freqList[type]) {
        heatmap[type][freq] = {};
        for (let k = 0; k <= zoomRange; k++) {
            heatmap[type][freq][k] = {};
        }
    }
}

let skipped = 0;
let hashBin = [];

agg.callbackJsonRecursive(OPTIONS.inputDir, data => {
    let allCells = data.json;

    if (OPTIONS.filter !== null) {
        allCells = filter.filterArray(OPTIONS.filter, allCells);
    }

    for (let j = allCells.length - 1; j >= 0; j--) {
        let hash = dataUtils.hashObj(allCells[j]);
        if (allCells[j].location === undefined
            || hashBin.includes(hash)
            || (OPTIONS.accTh > 0 && allCells[j].location.hor_acc > OPTIONS.accTh)) {
            skipped++;
            continue;
        }
        hashBin.push(hash);

        // Skip data with uninitialized GPS coordinate
        let lat = allCells[j].location.latitude;
        let lng = allCells[j].location.longitude;
        if (lat === 0 || lng === 0) {
            continue;
        }

        // Init heatmap if uninitialized, create idxList
        let idxList = {};
        for (let type in freqList) {
            for (let freq of freqList[type]) {
                for (let k = 0; k <= zoomRange; k++) {
                    // Init list of lat-lng idx for each zoom level
                    idxList[k] = Math.floor((lat - minLat) / latLngLength[k].lat)
                            + "-"
                            + Math.floor((lng - minLng) / latLngLength[k].lng);
                    if (heatmap[type][freq][k][idxList[k]] === undefined) {
                        heatmap[type][freq][k][idxList[k]] = { k: 0, rssi: 0 };
                    }
                }
            }
        }

        if (allCells[j].wifi_info) {
            for (let k = allCells[j].wifi_info.length - 1; k >= 0; k--) {
                // Sanity checks
                let rssi = dataUtils.cleanSignal(allCells[j].wifi_info[k].rssi);
                if (isNaN(rssi) || rssi === 0) continue;

                // Convert to Watts
                let curRssi = powerUtils.dbmToW(rssi);

                let freq = allCells[j].wifi_info[k].primaryFreq;
                let type = getType(freq);

                if (OPTIONS.skip2_4GHz && type === "2.4 GHz") continue;
                if (OPTIONS.skip5GHz && type === "5 GHz") continue;
                if (OPTIONS.skip6GHz && type === "6 GHz") continue;

                updateRssi(heatmap, type, freq, idxList, curRssi);
                updateRssi(heatmap, type, "all", idxList, curRssi);
            }
        }
    }
});


////////////////////////////////
// Output
////////////////////////////////

if (OPTIONS.skip2_4GHz) {
    delete freqList["2.4 GHz"];
    delete heatmap["2.4 GHz"];
}
if (OPTIONS.skip5GHz) {
    delete freqList["5 GHz"];
    delete heatmap["5 GHz"];
}
if (OPTIONS.skip6GHz) {
    delete freqList["6 GHz"];
    delete heatmap["6 GHz"];
}

let minRssi = 999.9;
let maxRssi = -999.9;
for (let type in heatmap) {
    for (let freq in heatmap[type]) {
        for (let zoomIdx in heatmap[type][freq]) {
            for (let idx in heatmap[type][freq][zoomIdx]) {
                // Convert back to dBm
                if (heatmap[type][freq][zoomIdx][idx].rssi !== 0) {
                    heatmap[type][freq][zoomIdx][idx].rssi = powerUtils.wToDbm(heatmap[type][freq][zoomIdx][idx].rssi);
                    minRssi = (minRssi > heatmap[type][freq][zoomIdx][idx].rssi) ? heatmap[type][freq][zoomIdx][idx].rssi : minRssi;
                    maxRssi = (maxRssi < heatmap[type][freq][zoomIdx][idx].rssi) ? heatmap[type][freq][zoomIdx][idx].rssi : maxRssi;
                } else {
                    heatmap[type][freq][zoomIdx][idx].rssi = NaN;
                }
            }
        }
    }
}
let rangeRssi = maxRssi - minRssi;
console.log (`minRssi: ${minRssi}, maxRssi: ${maxRssi}`);

console.log(`Number of skipped data: ${skipped}`);

let options = {
    minLat: minLat,
    maxLat: maxLat,
    minLng: minLng,
    maxLng: maxLng,
    minRssi: minRssi,
    maxRssi: maxRssi,
    rangeRssi: rangeRssi,
    minZoomLevel: OPTIONS.minZoomLevel,
    maxZoomLevel: OPTIONS.maxZoomLevel,
    freqList: freqList
};
if (OPTIONS.multipleZoomOutput) {
    for (let i = 0; i <= zoomRange; i++) {
        let newOutputPath = OPTIONS.outputPath.replace(/\.geojson$/, `-z${OPTIONS.minZoomLevel + i}.geojson`);
        console.log(`Writing output at ${newOutputPath}`);
        let output = createGeoJson(heatmap, options, latLngLength, i);
        fs.writeFileSync(newOutputPath, JSON.stringify(output));
    }
} else {
    console.log(`Writing output at ${OPTIONS.outputPath}`);
    let output = createGeoJson(heatmap, options, latLngLength);
    fs.writeFileSync(OPTIONS.outputPath, JSON.stringify(output));
}

console.log(`Done!`);