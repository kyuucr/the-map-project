const agg = require('./lib/aggregator');
const filter = require('./lib/filter');
const dataUtils = require('./lib/data-utils');
const fs = require('fs');
const path = require('path');

const TEST_LIST = [ "download", "upload", "latency" ];
const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> [output path] [filter]`);
    process.exit(0);
}

let dir = ARGS[0] || path.join("input", "fcc");
let outputPath = ARGS [1] || path.join("html", "outputs", "fcc-data.json");
let filterString = ARGS[2] ? JSON.parse(ARGS[2]) : false;

let recs = agg.getJson(dir);

if (filterString) {
    console.log(`Using filter: ${JSON.stringify(filterString)}`);
    let before = recs.length;
    recs = filter.filterArray(filterString, recs);
    console.log(`${(before - recs.length)} data filtered`);
}

console.log("FCC data count:", recs.length);

let getRsrp = function(telephony) {
    if (telephony && telephony.cellular_technology) {
        if (telephony.cellular_technology === "3g" || telephony.cellular_technology === "2g") {
            return getCellSignalKey(telephony, "signal_strength");
        }
        else if (telephony.cellular_technology === "4g") {
            return getCellSignalKey(telephony, "received_signal_power");
        } else if (telephony.cellular_technology === "5g") {
            return getCellSignalKey(telephony, "received_signal_power_ss");
        }
    }
    return "N/A";
}

let getRsrq = function(telephony) {
    if (telephony && telephony.cellular_technology) {
        if (telephony.cellular_technology === "4g") {
            return getCellSignalKey(telephony, "received_signal_quality");
        } else if (telephony.cellular_technology === "5g") {
            return getCellSignalKey(telephony, "received_signal_quality_ss");
        }
    }
    return "N/A";
}

let getKeyValue = function(obj, key) {
    if (obj && obj[key]) return obj[key];
    return "N/A";
}

let getCellSignalKey = function(telephony, key) {
    if (telephony) return getKeyValue(telephony.cell_signal, key);
    return "N/A";
}

let isLaa = function(earfcn) {
    return earfcn >= 46790 && earfcn <= 54539;
}

let getConn = function(telephony, wifi) {
    if (telephony && telephony.connection_type) {
        if (telephony.connection_type === "cell") {
            if (telephony.cellular_technology 
                && telephony.cellular_technology === "4g" 
                && telephony.cell_signal 
                && isLaa(telephony.cell_signal.absolute_rf_channel_number)) {
                return "4g-laa";
            }
            return telephony.cellular_technology;
        }
        return telephony.connection_type;
    } else if (wifi) {
        return "wi-fi";
    }
    return "N/A";
}

let getTarget = function(tests) {
    if (tests.download && tests.download.target) return tests.download.target;
    if (tests.upload && tests.upload.target) return tests.upload.target;
    if (tests.latency && tests.latency.target) return tests.latency.target;
    return "N/A";
}

let output = [];
let testIdList = [];
let headerItems = {
    download: null, upload: null, latency: null
};
for (let i = 0, length1 = recs.length; i < length1; i++) {
    if (recs[i].tests.testId === undefined || testIdList.includes(recs[i].tests.testId)) continue;
    testIdList.push(recs[i].tests.testId);
    let aveLat, aveLon, conn, k, startTime;
    let earliestLat, earliestLon;
    for (let test of TEST_LIST) {
        if (recs[i].tests[test] === undefined) continue;
        if (startTime === undefined) {
            startTime = recs[i].tests[test].local_datetime;
        }
        for (let cond in recs[i].tests[test].environment) {
            let curConn = getConn(recs[i].tests[test].environment[cond].telephony, recs[i].tests[test].environment[cond].wifi);

            // if (recs[i].tests[test].environment[cond].location) {
            //     if (aveLat === undefined) {
            //         k = 1;
            //         aveLat = recs[i].tests[test].environment[cond].location.lat;
            //         aveLon = recs[i].tests[test].environment[cond].location.lon;
            //     } else {
            //         ++k;
            //         aveLat += (recs[i].tests[test].environment[cond].location.lat - aveLat) / k;
            //         aveLon += (recs[i].tests[test].environment[cond].location.lon - aveLon) / k;
            //     }
            // }
            if (conn === undefined) {
                conn = curConn;
            } else if (conn !== "mixed" && conn !== curConn) {
                conn = "mixed";
            }

            if (earliestLat === undefined) {
                earliestLat = recs[i].tests[test].environment[cond].location.lat;
                earliestLon = recs[i].tests[test].environment[cond].location.lon;
            }
        }
    }

    let temp = {
        overview: {
            operator: dataUtils.getCleanOp(recs[i]),
            avg_location: {
                // lat: aveLat,
                // lon: aveLon
                lat: earliestLat,
                lon: earliestLon
            },
            time: (startTime) ? startTime : "FAILURE",
            server: getTarget(recs[i].tests),
            download_bytes_per_s: (recs[i].tests.download) ? recs[i].tests.download.throughput : "N/A",
            upload_bytes_per_s: (recs[i].tests.upload) ? recs[i].tests.upload.throughput : "N/A",
            latency_microsec: (recs[i].tests.latency) ? recs[i].tests.latency.round_trip_time : "N/A",
            jitter_microsec: (recs[i].tests.latency) ? recs[i].tests.latency.jitter : "N/A",
            packet_loss: (recs[i].tests.latency && recs[i].tests.latency.packets_sent !== 0) ?
                            (recs[i].tests.latency.packets_sent - recs[i].tests.latency.packets_received) / recs[i].tests.latency.packets_sent :
                            "N/A",
            connection: conn,
            test_ID: recs[i].tests.testId,
            pci_at_start: recs[i].tests.download ? getCellSignalKey(recs[i].tests.download.environment.beginning.telephony, "physical_cell_identity") : "N/A",
            earfcn_at_start: recs[i].tests.download ? getCellSignalKey(recs[i].tests.download.environment.beginning.telephony, "absolute_rf_channel_number") : "N/A",
            cell_identity_at_start: recs[i].tests.download ? getCellSignalKey(recs[i].tests.download.environment.beginning.telephony, "cell_identity") : "N/A",
        }
    };
    for (let testName of TEST_LIST) {
        if (recs[i].tests[testName]) {
            temp[testName] = {
                location: {
                    beginning: recs[i].tests[testName].environment.beginning.location,
                    end: recs[i].tests[testName].environment.end.location
                },
                connection: {
                    beginning: getConn(recs[i].tests[testName].environment.beginning.telephony, recs[i].tests[testName].environment.beginning.wifi),
                    end: getConn(recs[i].tests[testName].environment.end.telephony, recs[i].tests[testName].environment.end.wifi)
                },
                pci: {
                    beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "physical_cell_identity"),
                    end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "physical_cell_identity")
                },
                earfcn: {
                    beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "absolute_rf_channel_number"),
                    end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "absolute_rf_channel_number")
                },
                cell_identity: {
                    beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "cell_identity"),
                    end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "cell_identity")
                },
                cell_operator: {
                    beginning: getKeyValue(recs[i].tests[testName].environment.beginning.telephony, "cellular_operator_name"),
                    end: getKeyValue(recs[i].tests[testName].environment.end.telephony, "cellular_operator_name")
                },
                sim_operator: {
                    beginning: getKeyValue(recs[i].tests[testName].environment.beginning.telephony, "sim_operator_name"),
                    end: getKeyValue(recs[i].tests[testName].environment.end.telephony, "sim_operator_name")
                },
                rsrp: {
                    beginning: getRsrp(recs[i].tests[testName].environment.beginning.telephony),
                    end: getRsrp(recs[i].tests[testName].environment.end.telephony)
                },
                rsrq: {
                    beginning: getRsrq(recs[i].tests[testName].environment.beginning.telephony),
                    end: getRsrq(recs[i].tests[testName].environment.end.telephony)
                },
                rssnr: {
                    beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "received_signal_noise_ratio"),
                    end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "received_signal_noise_ratio")
                },
                cqi: {
                    beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "channel_quality_indicator"),
                    end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "channel_quality_indicator")
                },
                timing_advance: {
                    beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "timing_advance"),
                    end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "timing_advance")
                },
                server: recs[i].tests[testName].target,
                duration: recs[i].tests[testName].duration,
                time: recs[i].tests[testName].local_datetime
            };
            if (testName === "latency") {
                temp[testName].round_trip_time = recs[i].tests[testName].round_trip_time;
                temp[testName].packets_sent = recs[i].tests[testName].packets_sent;
                temp[testName].packets_received = recs[i].tests[testName].packets_received;
                temp[testName].jitter = recs[i].tests[testName].jitter;
                temp[testName].packet_size = recs[i].tests[testName].packet_size;
                temp[testName].stream_bits_per_sec = recs[i].tests[testName].stream_bits_per_sec;
            } else {
                temp[testName].throughput = recs[i].tests[testName].throughput;
                temp[testName].bytes_transferred = recs[i].tests[testName].bytes_transferred;
            }
            if (!headerItems[testName]) {
                headerItems[testName] = temp[testName];
            }
        }
    }

    output.push(temp);
}

console.log(`Writing fcc speed test data: ${outputPath}`);
fs.writeFileSync(outputPath, JSON.stringify(output));