const agg = require('../lib/aggregator');

test('Test loading ./input/capture dir', () => {
    expect(agg.getJson('./input/capture').length).toBeGreaterThan(0);
});