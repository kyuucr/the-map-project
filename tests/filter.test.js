const filter = require('../lib/filter');

let jsonInput = [
    {
        "name": "foo",
        "num": 20,
        "data": [
            {
                "key": "asd",
                "val": 23
            },
            {
                "key": "dsa",
                "val": 44
            }
        ]
    },
    {
        "name": "bar",
        "num": 32,
        "data": [
            {
                "key": "asd",
                "val": 123
            },
            {
                "key": "dsa",
                "val": 13
            }
        ]
    },
    {
        "name": "foobar",
        "num": 11,
        "data": [
            {
                "key": "qwe",
                "val": 94
            },
            {
                "key": "ewq",
                "val": 44
            }
        ]
    },
    {
        "name": "baz",
        "num": 0,
        "data": [
            {
                "key": "qwe",
                "val": 23
            },
            {
                "key": "ewq",
                "val": 67
            }
        ]
    },
    {
        "name": "foobarbaz",
        "num": 50,
        "data": [
            {
                "key": "asd",
                "val": 69
            },
            {
                "key": "asd",
                "val": 96
            }
        ]
    }
];

test('Compare a single item', () => {
    expect(filter.compare({"num": 20}, jsonInput[0])).toBeTruthy();
    expect(filter.compare({"num": "20"}, jsonInput[0])).toBeTruthy();
    expect(filter.compare(">-1", 0)).toBeTruthy();
    expect(filter.compare({"num": 0}, jsonInput[3])).toBeTruthy();
    expect(filter.compare({"name": "foo"}, jsonInput[0])).toBeTruthy();
    expect(filter.compare({"num": 0}, jsonInput[0])).toBeFalsy();
    expect(filter.compare({"data": {"val": 23}}, jsonInput[0])).toBeTruthy();
    expect(filter.compare({"data": {"val": 44}}, jsonInput[0])).toBeTruthy();
});

test('Filter an array', () => {
    expect(filter.filterArray({"num": 20}, jsonInput)).toEqual([jsonInput[0]]);
    expect(filter.filterArray({"num": [">-1", "<33"]}, jsonInput).length).toEqual(4);
    expect(filter.filterArray({"num": [">11", "<33"], "data": {"val": ">90"}}, jsonInput)).toEqual([jsonInput[1]]);
    expect(filter.filterArray([{"num": [">11", "<33"], "data": {"val": ">90"}}, {"name": "foo"}], jsonInput)).toEqual([jsonInput[1], jsonInput[0], jsonInput[2], jsonInput[4]]);
})