const wifiHelper = require('../lib/wifi-helper');

test('freqWidthToChannelNum', () => {
    // 2.4 GHz
    expect(wifiHelper.freqWidthToChannelNum(2417, 20)).toEqual(2);
    expect(wifiHelper.freqWidthToChannelNum(2417, 40)).toEqual(2);
    expect(wifiHelper.freqWidthToChannelNum(2417)).toEqual(2);
    expect(wifiHelper.freqWidthToChannelNum(2441, 20)).toEqual(NaN);
    // 5 GHz
    expect(wifiHelper.freqWidthToChannelNum(5510, 20)).toEqual(100);
    expect(wifiHelper.freqWidthToChannelNum(5510, 40)).toEqual(102);
    expect(wifiHelper.freqWidthToChannelNum(5570, 160)).toEqual(114);
    expect(wifiHelper.freqWidthToChannelNum(5865, 20)).toEqual(173);
    // 6 GHz
    expect(wifiHelper.freqWidthToChannelNum(6045, 20)).toEqual(17);
    expect(wifiHelper.freqWidthToChannelNum(6045, 40)).toEqual(19);
    expect(wifiHelper.freqWidthToChannelNum(6265, 160)).toEqual(47);
    expect(wifiHelper.freqWidthToChannelNum(6595, 40)).toEqual(131);
    // NaN
    expect(wifiHelper.freqWidthToChannelNum(3500, 20)).toEqual(NaN);
    expect(wifiHelper.freqWidthToChannelNum(1000, 20)).toEqual(NaN);
    expect(wifiHelper.freqWidthToChannelNum(5925, 20)).toEqual(NaN);
    expect(wifiHelper.freqWidthToChannelNum(7200, 20)).toEqual(NaN);

});