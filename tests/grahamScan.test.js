const geohelper = require('../lib/geohelper');

test('Test case of graham scan', () => {
    let input = [{
        longitude: 0,
        latitude: 3
    },
    {
        longitude: 1,
        latitude: 1
    },
    {
        longitude: 2,
        latitude: 2
    },
    {
        longitude: 4,
        latitude: 4
    },
    {
        longitude: 0,
        latitude: 0
    },
    {
        longitude: 1,
        latitude: 2
    },
    {
        longitude: 3,
        latitude: 1
    },
    {
        longitude: 3,
        latitude: 3
    }];
    // console.log(geohelper.grahamScan(input));
    expect(geohelper.grahamScan(input).length).toBeGreaterThan(0);
});
