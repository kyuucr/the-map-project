const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> [output path]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];

let pciBand = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (pciBand[op] === undefined) pciBand[op] = {};

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            let band = json[i].cell_info[j].band;
            let pci = json[i].cell_info[j].pci;

            if (pciBand[op][pci] === undefined) pciBand[op][pci] = [];
            if (!pciBand[op][pci].includes(band)) pciBand[op][pci].push(band);
        }

    }
});

let pciOp = {};
let string = "";
for (let op in pciBand) {
    pciOp[op] = [];
    string += `${op}\n`;
    string += `"LAA PCI"{sep}Band\n`;
    for (let pci in pciBand[op]) {
        pciBand[op][pci].sort(dataUtils.sortNumAsc);
        string += `${pci}{sep}"${pciBand[op][pci].join(",")}"\n`;
        for (let band of pciBand[op][pci]) {
            if (!pciOp[op].includes(band)) {
                pciOp[op].push(band);
            }
        }
    }
}
string += `\n"Operator"{sep}Band\n`;
for (let op in pciOp) {
    pciOp[op].sort(dataUtils.sortNumAsc);
    string += `${op} "${pciOp[op].join(",")}"\n`;
}

if (outputPath) {
    fs.writeFileSync(outputPath, string.replace(/\{sep\}/g, ","));
} else {
    console.log(string.replace(/\{sep\}/g, "\t"));
}