const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> <output path> [--remove-zero] [--client-ids <client ID array>`);
    process.exit(0);
}

let clientIds = [
    "unknown",
];
let inputFolder = args[0];
let outputPath = args[1];
let removeZero = false;
args.splice(0, 2);
while (args.length > 0) {
    switch (args[0]) {
        case "--remove-zero": {
            removeZero = true;
            args.splice(0, 1);
            break;
        }
        case "--client-ids": {
            try {
                let temp = JSON.parse(args[1]);
                if (temp[0] !== "unknown") {
                    temp.unshift("unknown");
                }
                clientIds = temp;
            } catch (err) {
                console.log("JSON parse error!", err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            console.log("Unknown argument!", args[0]);
            process.exit(1);
        }
    }
}

let sortNumAsc = (a, b) => a - b;
let sortTimeAsc = (a, b) => (a.time < b.time) ? -1 : (a.time > b.time) ? 1 : 0;
let avgReduce = (avg, value, _, { length }) => avg + value / length;

let clientStats = {};

agg.callbackJsonRecursive(inputFolder, data => {
    if (!data.path.match(/\/fcc-/)) return;
    if (clientStats[data.path] === undefined) {
        clientStats[data.path] = {};
    }
    let recs = data.json;
    for (let i = 0, length1 = recs.length; i < length1; i++) {
        let dlTput = (recs[i].tests && recs[i].tests.download) ? (recs[i].tests.download.throughput * 8 / 1e6) : 0;
        if (dlTput === 0 && removeZero) continue; // Skip if tput is zero and flag is on

        let time = recs[i].tests.download.local_datetime;
        let clientId = recs[i].zipFilename || "unknown";
        if (!clientIds.includes(clientId)) clientIds.push(clientId);

        if (clientStats[data.path][clientId] === undefined) clientStats[data.path][clientId] = [];
        clientStats[data.path][clientId].push({dlTput: dlTput, time: time});
    }
});

for (const currPath in clientStats) {

    let metadataPath = currPath.replace("/data/", "/metadata/metadata-") + ".json";
    let metadataDesc = JSON.parse(fs.readFileSync(metadataPath)).description;

    let clientIndices = Object.keys(clientStats[currPath]).map(val => clientIds.indexOf(val)).sort().map(val => clientIds[val]);

    for (let client of clientIndices) {
        let outputDir = path.join(outputPath, currPath.replace(/^.*\/data\//, ""));
        if (!fs.existsSync(outputDir)) {
            fs.mkdirSync(outputDir, { recursive: true });
        }
        const os = fs.createWriteStream(path.join(outputDir, `${client}.dat`));
        let output = `Time Client${clientIds.findIndex((element) => element === client)}\n`;

        clientStats[currPath][client].sort(sortTimeAsc);
        for (let i = 0, length1 = clientStats[currPath][client].length; i < length1; i++) {
            let data = clientStats[currPath][client][i];
            // output += `"${data.time.match(/\d\d:\d\d:\d\d/)[0]}" ${data.dlTput}\n`;
            // output += `${data.time} ${data.dlTput}\n`;
            output += `${data.time.match(/\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d/)[0]} ${data.dlTput}\n`;
        }
        os.write(output);
        os.close();
    }
}


