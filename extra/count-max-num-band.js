const agg = require('../lib/aggregator');
const path = require('path');
const fs = require('fs');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <input SigCap dir> [output path]`);
    process.exit(0);
}

let dir = ARGS[0];
let outputPath = ARGS[1] || false;

let os;
if (outputPath) {
    os = fs.createWriteStream(outputPath);
    console.log(`Write to: ${outputPath}`);
}

let output = function(string) {
    if (outputPath) {
        os.write(`${string}\n`);
    } else {
        console.log(string);
    }
}

output(`Input path,max band count,max cell count`);

let countMaxBand = function(input) {
    let sigcap = input.json;
    let maxBand = 0;
    let maxCell = 0;
    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        let bandCount = [];
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            if (!bandCount.includes(sigcap[i].cell_info[j].band)) {
                bandCount.push(sigcap[i].cell_info[j].band)
            }
        }
        if (maxBand < bandCount.length) {
            maxBand = bandCount.length
        }
        if (maxCell < sigcap[i].cell_info.length) {
            maxCell = sigcap[i].cell_info.length;
        }
    }
    output(`${input.path},${maxBand},${maxCell}`);
}

agg.callbackJsonRecursive(dir, countMaxBand);