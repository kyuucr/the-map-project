const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 3) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <XCAL CSV file> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const xcalInput = ARGS[1];
const outputPath = ARGS[2];
const usePlotlib = ARGS[2] === "plotlib";
const prefix = ARGS[3] || "";

let xcalData = [];
agg.callbackCsvRecursive(xcalInput, data => {
    let colIds = dataUtils.getXCalHeader(data.header);

    for (let row of data.csv) {
        if (row[0] === "Count") break;
        let temp = {
            timestampDate: new Date(row[0]),
            timestamp: new Date(row[0]).getTime(),
            ltePCellPCI: parseInt(row[colIds.LTE_PCell_ServPCI]),
            ltePCellEARFCN: parseInt(row[colIds.LTE_PCell_ServEARFCNDL]),
            ltePCellRSRP: parseFloat(row[colIds.LTE_PCell_ServRSRP]),
            nrPCellRSRP: parseFloat(row[colIds.PCell_ServRSRP]),
        }
        xcalData.push(temp);
        // console.log("xcal", temp);
    }
});
let xcalStart = xcalData[0].timestamp;
let xcalStop = xcalData[xcalData.length - 1].timestamp;

let output = { ltePCellRSRPDiff: [], ltePCellTimeDiff: [], nrPCellRSRPDiff: [], nrPCellTimeDiff: [] };
let outputTime = { ltePCell: [], nrPCell: [] };
let hashBin = [];
agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        // if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        // let op = dataUtils.getCleanOp(json[i]);
        // if (dataUtils.getNetworkType(json[i]) === "NR-NSA"
        //     && (json[i].cell_info === undefined
        //         || json[i].cell_info.length === 0)) {
        //     continue;
        // }
        let timestampDate = new Date (json[i].datetimeIso);
        let timestamp = timestampDate.getTime();
        if (timestamp < xcalStart || xcalStop < timestamp) continue;

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            let currCell = json[i].cell_info[j];
            if (!currCell.registered && currCell.ci === 2147483647) continue;
            let rsrp = dataUtils.cleanSignal(currCell.rsrp);
            if (isNaN(rsrp)) continue;

            let minDiff = 9999999999;
            let thisTimestamp;
            let rsrpDiff;
            let xcalRsrp;
            for (let xcal of xcalData) {
                let diff = Math.abs(xcal.timestamp - timestamp);
                if (minDiff > diff && !isNaN(xcal.ltePCellRSRP) && xcal.ltePCellPCI === currCell.pci && xcal.ltePCellEARFCN === currCell.earfcn) {
                    minDiff = diff;
                    thisTimestamp = xcal.timestampDate;
                    rsrpDiff = Math.abs(xcal.ltePCellRSRP - rsrp);
                    xcalRsrp = xcal.ltePCellRSRP;
                }
            }
            // console.log("diff", thisTimestamp, timestampDate, minDiff, rsrpDiff);
            if (minDiff < 1000) {
                output.ltePCellTimeDiff.push(minDiff);
                output.ltePCellRSRPDiff.push(rsrpDiff);
                outputTime.ltePCell.push({ timestamp: json[i].datetimeIso, timeDiff: minDiff, xcalRsrp: xcalRsrp, sigcapRsrp: rsrp, rsrpDiff: rsrpDiff });
            }
        }

        if (json[i].nr_info) {
            let minRsrpDiff = 9999;
            let minTimestamp;
            let timeDiff, xcalRsrp, sigcapRsrp;
            for(let j = 0, length2 = json[i].nr_info.length; j < length2; j++){
                let rsrp = dataUtils.cleanSignal(json[i].nr_info[j].ssRsrp);
                if (isNaN(rsrp)) continue;

                let minDiff = 9999999999;
                let thisTimestamp;
                let rsrpDiff;
                let xcalRsrpTemp;
                for (let xcal of xcalData) {
                    let diff = Math.abs(xcal.timestamp - timestamp);
                    if (minDiff > diff && !isNaN(xcal.nrPCellRSRP)) {
                        minDiff = diff;
                        thisTimestamp = xcal.timestampDate
                        rsrpDiff = Math.abs(xcal.nrPCellRSRP - rsrp);
                        xcalRsrpTemp = xcal.nrPCellRSRP;
                    }
                }
                if (minRsrpDiff > rsrpDiff) {
                    minRsrpDiff = rsrpDiff;
                    xcalRsrp = xcalRsrpTemp;
                    sigcapRsrp = rsrp;
                    minTimestamp = thisTimestamp;
                    timeDiff = minDiff;
                }
            }
            // console.log("diff", minTimestamp, timestampDate, timeDiff, minRsrpDiff);
            if (timeDiff < 1000 && minRsrpDiff !== 9999) {
                output.nrPCellTimeDiff.push(timeDiff);
                output.nrPCellRSRPDiff.push(minRsrpDiff);
                outputTime.nrPCell.push({ timestamp: json[i].datetimeIso, timeDiff: timeDiff, xcalRsrp: xcalRsrp, sigcapRsrp: sigcapRsrp, rsrpDiff: minRsrpDiff });
            }
        }
    }
});

let fig = {}, maxValues = {};
const typeName = {
    ltePCellRSRPDiff: "LTE PCell RSRP Diff (dB)",
    ltePCellTimeDiff: "LTE PCell Time Diff (millisec)",
    nrPCellRSRPDiff: "NR PCell RSRP Diff (dB)",
    nrPCellTimeDiff: "NR PCell Time Diff (millisec)",
}

for (let type in output) {
    output[type].sort(dataUtils.sortNumAsc);

    if (usePlotlib) {
        if (maxValues[type] === undefined || maxValues[type] < output[type][output[type].length - 1]) {
            maxValues[type] = output[type][output[type].length - 1];
        }
        if (fig[type] === undefined) {
            fig[type] = [];
        }
        fig[type].push({ x: output[type], y: dataUtils.createCdfY(output[type].length), type: "scatter" });
    } else {
        let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}cdf-${type}.dat`);
        let string = `"Cumulative distributive function" "${typeName[type]}"\n`;

        for(let i = 0, length1 = output[type].length; i < length1; i++){
            string += `${i / (length1 - 1)} ${output[type][i]}\n`;
        }
    }
}

for (let type in outputTime) {
    if (!usePlotlib) {
        let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}timeline-${type}.dat`);
        let string = `"Timestamp" "Time diff (millisec)" "XCAL RSRP (dBm)" "SigCap RSRP (dBm)" "Absolute RSRP diff. (dB)"\n`;

        for(let i = 0, length1 = outputTime[type].length; i < length1; i++){
            string += `${outputTime[type][i].timestamp} ${outputTime[type][i].timeDiff} ${outputTime[type][i].xcalRsrp} ${outputTime[type][i].sigcapRsrp} ${outputTime[type][i].rsrpDiff}\n`;
        }

        fs.writeFileSync(outputFile, string);
    }
}

if (usePlotlib) {
    const plotlib = require('nodeplotlib');

    for (let type in fig) {
        let layout = {
            xaxis: {
                showgrid: true,
                title: `${typeName[type]}`,
                range: [0, maxValues[type]]
            },
            yaxis: {
                showgrid: true,
                title: 'Cumulative distribution function'
            },
            // title: `${typeName[type]}`,
            showlegend: true
        }
        plotlib.stack(fig[type], layout);
    }
    plotlib.plot();
}