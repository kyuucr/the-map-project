const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const filter = require('../lib/filter');
const fs = require('fs');
const path = require('path');
const plotlib = require('nodeplotlib');

const TEST_LIST = [ "download", "upload", "latency" ];
let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> [output path] [--filter <filter JSON string>]`);
    process.exit(1);
}


let outputPath = "output-fcc.csv";
let inputFolder = args[0];
args.splice(0, 1);
let inputFilter;
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}

let rsrpVals = {};
let primaryRsrpScaledVals = {};
let rsrqVals = {};
let primaryRsrqVals = {};
let primaryBWVals = {};
let rsrp5GVals = {};
let rsrq5GVals = {};
let wifiRssiVals = { "2_4": [], "5": [] };
let wifiBWVals = { "2_4": [], "5": [] };

agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    console.log(`path: ${data.path}, files: ${data.filePath.map(data => path.basename(data)).join(",")}, # of data: ${sigcap.length}`);

    for (let i = 0, length1 = sigcap.length; i < length1; i++) {
        // Sanity checks
        let hash = dataUtils.hashObj(sigcap[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (skipInvalidOp
            && sigcap[i].opName === undefined
            && sigcap[i].simName === undefined
            && sigcap[i].carrierName === undefined) {
            continue;
        }
        let op = dataUtils.getCleanOp(sigcap[i]);
        if (skipInvalidOp && op === "Unknown") continue;
        
        for (let cell of sigcap[i].cell_info) {
            let rsrp = cell.rsrp;
            let rsrq = cell.rsrq;
            let bw = cell.width;
            if (rssi === 2147483647 || rsrq === 2147483647 || bw === 2147483647) continue;
            // let networkType = dataUtils.getNetworkType(sigcap[i]);

            if (cell.width > 0) {
                if (primaryRsrpScaledVals[op] === undefined) primaryRsrpScaledVals[op] = { all: [] };
                primaryRsrpScaledVals[op].all.push()
            }
        }
    }
