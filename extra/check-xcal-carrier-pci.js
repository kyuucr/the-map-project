const agg = require('../lib/aggregator');
const geohelper = require('../lib/geohelper');
const dataUtils = require('../lib/data-utils');
const statsUtils = require('../lib/stats-utils');
const fs = require("fs");
const path = require("path");

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <input XCAP CSV file/folder>`);
    process.exit(0);
}
const inputPath = ARGS[0];

const pciFilter = [ "152","133","134","508","21","23","907","478","479","686","425","6","7","327","322","333" ];
const bsLoc = {
    "152": [ 41.87955479, -87.62717675],
    "133": [ 41.87950112, -87.62924641],
    "134": [ 41.87950112, -87.62924641],
    "508": [ 41.87935633, -87.63068307],
    "21":  [ 41.87908324, -87.63072129],
    "23":  [ 41.87908324, -87.63072129],
    "907": [ 41.8780857,  -87.63167918],
    "478": [ 41.87809419, -87.63114944],
    "479": [ 41.87809419, -87.63114944],
    "686": [ 41.87822574, -87.62924742],
    "425": [ 41.87816234, -87.6274775],
    "6":   [ 41.8781903,  -87.62704231],
    "7":   [ 41.8781903,  -87.62704231],
    "327": [ 41.87301299, -87.62185969],
    "322": [ 41.87153991, -87.62091528],
    "333": [ 41.87157813, -87.62207113],
};

const BBP_MAX_LAT = 41.87302922975753;
const BBP_MIN_LAT = 41.871620648385274;
const BBP_MAX_LON = -87.62075759960607;
const BBP_MIN_LON = -87.62208068247578;

// Header
let gpsAcc = [];
let checkColumns = [ "PCell_ServPCI", "SCell1_ServPCI", "SCell2_ServPCI", "SCell3_ServPCI", "SCell4_ServPCI", "SCell5_ServPCI", "SCell6_ServPCI", "SCell7_ServPCI" ];

agg.callbackCsvRecursive(inputPath, data => {
    let path = data.path;
    console.log(path)
    let isBBP = path.includes("BBP");
    let header = data.header;
    let csv = data.csv;
    let colIds = dataUtils.getXCalHeader(header);
    let num = 0;

    for (let row of csv) {
        // let lon = row[colIds.Lon];
        // let lat = row[colIds.Lat];
        // if (isBBP && (lat > BBP_MAX_LAT || lat < BBP_MIN_LAT || lon > BBP_MAX_LON || lon < BBP_MIN_LON)
        //     || (colIds.PCell_ServPCI >= 0 && !pciFilter.includes(row[colIds.PCell_ServPCI]))
        //     || (colIds.PCell_ServFreq >= 0 && row[colIds.PCell_ServFreq] < 20000)) {
        //     continue;
        // }

        let pcis = [];
        for (let col of checkColumns) {
            if (colIds[col] >= 0 && (row[colIds[col]] || col === "PCell_ServPCI")) {
                pcis.push(row[colIds[col]]);
            }
        }
        // console.log(pcis)
        let prevPci;
        for (let pci of pcis) {
            if (prevPci !== undefined && prevPci !== pci) {
                console.log(pcis.join(", "));
                num++;
                break;
            }
            prevPci = pci;
        }
    }
    if (num > 0)
    console.log(`Num: ${num}, ratio: ${num / csv.length}`);
});
