const agg = require('../lib/aggregator');
const geohelper = require('../lib/geohelper');
const dataUtils = require('../lib/data-utils');
const fs = require("fs");

const ARGS = process.argv.slice(2);
if (ARGS.length < 3) {
    console.log(`Usage: node ${path.basename(__filename)} <input XCAP CSV file/folder> <pathloss input CSV> <output CSV>`);
    process.exit(0);
}
const inputPath = ARGS[0];
const pathlossInputPath = ARGS[1];
const os = fs.createWriteStream(ARGS[2]);

const pciFilter = [ "152","133","134","508","21","23","907","478","479","686","425","6","7","327","322","333" ];
const bsLoc = {
    "152": [ 41.87955479, -87.62717675],
    "133": [ 41.87950112, -87.62924641],
    "134": [ 41.87950112, -87.62924641],
    "508": [ 41.87935633, -87.63068307],
    "21":  [ 41.87908324, -87.63072129],
    "23":  [ 41.87908324, -87.63072129],
    "907": [ 41.8780857,  -87.63167918],
    "478": [ 41.87809419, -87.63114944],
    "479": [ 41.87809419, -87.63114944],
    "686": [ 41.87822574, -87.62924742],
    "425": [ 41.87816234, -87.6274775],
    "6":   [ 41.8781903,  -87.62704231],
    "7":   [ 41.8781903,  -87.62704231],
    "327": [ 41.87301299, -87.62185969],
    "322": [ 41.87153991, -87.62091528],
    "333": [ 41.87157813, -87.62207113],
};
const BBP_MAX_LAT = 41.87302922975753;
const BBP_MIN_LAT = 41.871620648385274;
const BBP_MAX_LON = -87.62075759960607;
const BBP_MIN_LON = -87.62208068247578;

let plValues = [];

agg.callbackCsvRecursive(pathlossInputPath, data => {
    let filename = data.path.split("/").pop().replace(/\.csv$/, "");
    let isBBP = filename.includes("BBP");
    let header = data.header;
    let csv = data.csv;
    let colIds = dataUtils.getXCalHeader(header);

    for (let row of csv) {
        let time = row[0];
        let lon = parseFloat(row[colIds.Lon]);
        let lat = parseFloat(row[colIds.Lat]);
        if (!lon || !lat
            || (isBBP && (lat > BBP_MAX_LAT || lat < BBP_MIN_LAT || lon > BBP_MAX_LON || lon < BBP_MIN_LON))
            || (colIds.PCell_ServPCI >= 0 && !pciFilter.includes(row[colIds.PCell_ServPCI]))
            || (colIds.PCell_ServFreq >= 0 && row[colIds.PCell_ServFreq] < 20000)) {
            continue;
        }
        if (colIds.PCell_ServPL >= 0 && row[colIds.PCell_ServPL]) {
            plValues.push({filename: filename, time: new Date(time), lat: lat, lon: lon, pl: parseFloat(row[colIds.PCell_ServPL])});
        }
    }
});

// Header
// os.write(`Timestamp,Lon,Lat,Distance [meter],NR-ARFCN,PCI,TX Beam Idx,RX Beam Idx0,Instant BRSRP RX Path[0] [dBm],RX Beam Idx1,Instant BRSRP RX Path[1] [dBm],Combined BRSRP [dBm],Status,Primary Serving Pathloss (dB)\n`);
os.write(`Timestamp,Lon,Lat,Distance [meter],NR-ARFCN,PCI,TX Beam Idx,RX Beam Idx0,Instant BRSRP RX Path[0] [dBm],RX Beam Idx1,Instant BRSRP RX Path[1] [dBm],Combined BRSRP [dBm],Status,GPS Accuracy (m),NR TX Power (dBm)\n`);

agg.callbackCsvRecursive(inputPath, data => {
    let filename = data.path.split("/").pop().replace(/\.csv$/, "");
    let isBBP = filename.includes("BBP");
    let header = data.header;
    let csv = data.csv;
    let headerPointer = {
        lat: null,
        lon: null,
        servingPci: null,
        nrarfcn: null,
        servingBeamTx: null,
        beamRx0: {},
        beamRx1: {},
        beamRx0Rsrp: {},
        beamRx1Rsrp: {},
        beamRsrp: {},
    }

    for (let i = 0, length1 = header.length; i < length1; i++) {
        if (header[i].match(/^Lat$/)) {
            headerPointer.lat = i;
        } else if (header[i].match(/^Lon$/)) {
            headerPointer.lon = i;
        } else if (header[i].match(/NR-ARFCN/)) {
            headerPointer.nrarfcn = i;
        } else if (header[i].match(/Serving Cell Serving/)) {
            if (header[i].match(/PCI/)) {
                headerPointer.servingPci = i;
            } else if (header[i].match(/SSB Idx/)) {
                headerPointer.servingBeamTx = i;
            } else if (header[i].match(/Rx Beam Idx0/)) {
                headerPointer.beamRx0.serving = i;
            } else if (header[i].match(/Rx Beam Idx1/)) {
                headerPointer.beamRx1.serving = i;
            } else if (header[i].match(/Instant BRSRP Rx Path\[0\]/)) {
                headerPointer.beamRx0Rsrp.serving = i;
            } else if (header[i].match(/Instant BRSRP Rx Path\[1\]/)) {
                headerPointer.beamRx1Rsrp.serving = i;
            } else if (header[i].match(/Filtered Tx BRSRP/)) {
                headerPointer.beamRsrp.serving = i;
            }
        } else if (header[i].match(/Serving Cell Non-Serving Tx Beams/)) {
            let idx = header[i].match(/Tx SSB Beam Idx\[(\d+)/)[1];
            if (idx === undefined) continue;
            if (header[i].match(/Rx Beam Idx0/)) {
                headerPointer.beamRx0[idx] = i;
            } else if (header[i].match(/Rx Beam Idx1/)) {
                headerPointer.beamRx1[idx] = i;
            } else if (header[i].match(/Instant BRSRP Rx Path\[0\]/)) {
                headerPointer.beamRx0Rsrp[idx] = i;
            } else if (header[i].match(/Instant BRSRP Rx Path\[1\]/)) {
                headerPointer.beamRx1Rsrp[idx] = i;
            } else if (header[i].match(/Filtered Tx BRSRP/)) {
                headerPointer.beamRsrp[idx] = i;
            }
        } else if (header[i].match(/Neighbor Cell Detected Tx Beams/)) {
            let pci = header[i].match(/PCI(\d+)/)[1];
            let beam = header[i].match(/Tx SSB Beam Idx\[(\d+)/)[1];
            if (pci === undefined || beam === undefined) continue;
            let idx = `neighbor-${pci}-${beam}`;
            if (header[i].match(/Rx Beam Idx0/)) {
                headerPointer.beamRx0[idx] = i;
            } else if (header[i].match(/Rx Beam Idx1/)) {
                headerPointer.beamRx1[idx] = i;
            } else if (header[i].match(/Instant BRSRP Rx Path\[0\]/)) {
                headerPointer.beamRx0Rsrp[idx] = i;
            } else if (header[i].match(/Instant BRSRP Rx Path\[1\]/)) {
                headerPointer.beamRx1Rsrp[idx] = i;
            } else if (header[i].match(/Filtered Tx BRSRP/)) {
                headerPointer.beamRsrp[idx] = i;
            }
        }
    }
    let colIds = dataUtils.getXCalHeader(header);
    let lastGPS = { val: null, time: new Date(0) };
    let lastPower = { val: null, time: new Date(0) };

    let output = ``;
    for (let row of csv) {
        let datetime = new Date (row[0]);
        let lon = parseFloat(row[headerPointer.lon]);
        let lat = parseFloat(row[headerPointer.lat]);
        if (!lon || !lat) continue;
        if (isBBP && (lat > BBP_MAX_LAT || lat < BBP_MIN_LAT || lon > BBP_MAX_LON || lon < BBP_MIN_LON)) continue;
        let pci = row[headerPointer.servingPci];
        if (headerPointer.servingPci !== null && !pciFilter.includes(pci)) continue;

        let nrarfcn = row[headerPointer.nrarfcn];
        if (row[colIds.GPS_Acc]) {
            lastGPS.val = row[colIds.GPS_Acc];
            lastGPS.time = new Date(row[0]);
        }
        let gpsAcc = (Math.abs(lastGPS.time.getTime() - datetime.getTime()) < 1000 && lastGPS.val > 0) ? lastGPS.val : NaN;
        if (row[colIds.NR_TXPower]) {
            lastPower.val = row[colIds.NR_TXPower];
            lastPower.time = new Date(row[0]);
        }
        let txPower = (Math.abs(lastPower.time.getTime() - datetime.getTime()) < 1000) ? lastPower.val : NaN;
        // let pl = plValues.find(element => {
        //     return element.lat === lat && element.lon === lon && Math.abs(element.time.getTime() - datetime.getTime()) < 1000;
        // });
        // if (pl) {
        //     pl = pl.pl;
        // } else {
        //     pl = NaN;
        // }
        // let values = [];
        // plValues.forEach((pl) => {
        //     if (pl.lat === lat && pl.lon === lon && Math.abs(pl.time.getTime() - datetime.getTime()) < 1000) {
        //         console.log(filename, pl.filename)
        //         if (filename.includes(pl.filename)) {
        //             values.push(pl);
        //         } else {
        //             // TBD
        //         }
        //     }
        // });
        // if (values.length > 1) {
        //     console.log(values, row, filename);
        // }

        for (let beamIdx in headerPointer.beamRx0) {
            let txBeamIdx, status = "Unknown", pathloss = NaN;
            if (beamIdx === "serving") {
                txBeamIdx = row[headerPointer.servingBeamTx];
                status = "Serving PCI Serving Beam";
                // pathloss = values[0].pl;
            } else if (beamIdx.startsWith("neighbor-")) {
                let split = beamIdx.split("-");
                pci = split[1];
                txBeamIdx = split[2];
                status = "Neighbor PCI Non-Serving Beam";
            } else {
                txBeamIdx = beamIdx;
                status = "Serving PCI Non-Serving Beam";
            }
            let distance = geohelper.measure(lat, lon, bsLoc[pci][0], bsLoc[pci][1]);
            let rxBeamIdx0 = row[headerPointer.beamRx0[beamIdx]];
            let rxBeamRsrp0 = row[headerPointer.beamRx0Rsrp[beamIdx]];
            let rxBeamIdx1 = row[headerPointer.beamRx1[beamIdx]];
            let rxBeamRsrp1 = row[headerPointer.beamRx1Rsrp[beamIdx]];
            let beamRsrp = row[headerPointer.beamRsrp[beamIdx]];

            if (txBeamIdx && rxBeamIdx0 && rxBeamRsrp0 && rxBeamIdx1 && rxBeamRsrp1 && beamRsrp) {
                // output += `${lon},${lat},${distance},${nrarfcn},${pci},${txBeamIdx},${rxBeamIdx0},${rxBeamRsrp0},${rxBeamIdx1},${rxBeamRsrp1},${beamRsrp},${status},${pathloss}\n`;
                output += `${datetime.toISOString()},${lon},${lat},${distance},${nrarfcn},${pci},${txBeamIdx},${rxBeamIdx0},${rxBeamRsrp0},${rxBeamIdx1},${rxBeamRsrp1},${beamRsrp},${status},${gpsAcc},${txPower}\n`;
            }
        }
    }
    os.write(output);
});

os.close();
