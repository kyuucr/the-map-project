const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const fs = require('fs');
const path = require('path');

let files = agg.getJsonRecursive('./input/Body_Measurement');
const os = fs.createWriteStream(path.join("extra", "body-measurement.csv"));
let firstData = true;

for(let i = 0, length1 = files.length; i < length1; i++){
    let pathDir = files[i].path.split("/");
    let person = pathDir[2];
    let experiment = pathDir[3];
    let variable = pathDir[4];
    console.log(person, experiment, variable, files[i].json.length);
    let bssid = {};
    let rssiAll = {};
    for(let j = 0, length2 = files[i].json.length; j < length2; j++){
        for(let k = 0, length3 = files[i].json[j].wifi_info.length; k < length3; k++){
            let idx = files[i].json[j].wifi_info[k].bssid;
            let curRssi = files[i].json[j].wifi_info[k].rssi;
            if (bssid[idx] === undefined) {
                bssid[idx] = {
                    bssid: idx,
                    ssid: files[i].json[j].wifi_info[k].ssid,
                    meanRssi: curRssi,
                    primaryFreq: files[i].json[j].wifi_info[k].primaryFreq,
                    channelNum: files[i].json[j].wifi_info[k].channelNum,
                    width: files[i].json[j].wifi_info[k].width,
                    standard: files[i].json[j].wifi_info[k].standard,
                    numData: 1
                };
                rssiAll[idx] = [ curRssi ];
            } else {
                bssid[idx].meanRssi += (curRssi - bssid[idx].meanRssi) / bssid[idx].numData;
                ++bssid[files[i].json[j].wifi_info[k].bssid].numData;
                rssiAll[idx].push(curRssi);
            }
        }
    }
    for (let idx in bssid) {
        if (firstData) {
            firstData = false;
            os.write(`Person,Experiment,Variable,${csvUtils.unpackKeys(bssid[idx])}\n`);
        }
        os.write(`${person},${experiment},${variable},${csvUtils.unpackVals(bssid[idx])}\n`);
        if (idx === "D121444961664D5789DF47BF3AC5C482AA7A9E289299A36D18A9881AE9B2925F"
                || idx === "FA17DEA480DA3BC06B02B662CD06D63294CE4A2BF64ED21230F3DF65C122AB15") {
            let temp = rssiAll[idx];
            temp.sort();
            let os2 = fs.createWriteStream(path.join("extra", `${person}-${experiment}-${variable}.csv`));
            let i = 0;
            for (let j = temp.length - 1, length2 = temp.length; j >= 0; j--) {
                os2.write(`${i++ / length2},${temp[j]}\n`);
            }
            os2.close();
        }
    }
}