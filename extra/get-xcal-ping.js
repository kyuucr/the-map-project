const agg = require('../lib/aggregator');
const geohelper = require('../lib/geohelper');
const dataUtils = require('../lib/data-utils');
const statsUtils = require('../lib/stats-utils');
const fs = require("fs");

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <input XCAP CSV file/folder> <output CSV>`);
    process.exit(0);
}
const inputPath = ARGS[0];
const outputPath = ARGS[1].replace(/\.csv$/, "");

const pciFilter = [ "152","133","134","508","21","23","907","478","479","686","425","6","7","327","322","333" ];
const bsLoc = {
    "152": [ 41.87955479, -87.62717675],
    "133": [ 41.87950112, -87.62924641],
    "134": [ 41.87950112, -87.62924641],
    "508": [ 41.87935633, -87.63068307],
    "21":  [ 41.87908324, -87.63072129],
    "23":  [ 41.87908324, -87.63072129],
    "907": [ 41.8780857,  -87.63167918],
    "478": [ 41.87809419, -87.63114944],
    "479": [ 41.87809419, -87.63114944],
    "686": [ 41.87822574, -87.62924742],
    "425": [ 41.87816234, -87.6274775],
    "6":   [ 41.8781903,  -87.62704231],
    "7":   [ 41.8781903,  -87.62704231],
    "327": [ 41.87301299, -87.62185969],
    "322": [ 41.87153991, -87.62091528],
    "333": [ 41.87157813, -87.62207113],
};

const BBP_MAX_LAT = 41.87302922975753;
const BBP_MIN_LAT = 41.871620648385274;
const BBP_MAX_LON = -87.62075759960607;
const BBP_MIN_LON = -87.62208068247578;

// Header
let pingRTT = { all: [] };

agg.callbackCsvRecursive(inputPath, data => {
    let path = data.path;
    console.log(path)
    let isBBP = path.includes("BBP");
    let isATT = path.includes("ATT");
    let header = data.header;
    let csv = data.csv;
    let colIds = dataUtils.getXCalHeader(header);
    let lastCell = { pci: null, arfcn: null, time: new Date(0) };

    for (let row of csv) {
        let datetime = new Date (row[0]);
        let lon = row[colIds.Lon];
        let lat = row[colIds.Lat];

        if (colIds.PCell_NRARFCN >= 0 && colIds.PCell_ServPCI >= 0) {
            let pci = row[colIds.PCell_ServPCI];
            let arfcn = row[colIds.PCell_NRARFCN];
            if (pci && arfcn) {
                lastCell.pci = pci;
                lastCell.arfcn = arfcn;
                lastCell.time = datetime;
            }
        }

        if (colIds.Total_PingRTT >= 0) {
            let rtt = parseFloat(row[colIds.Total_PingRTT]);
            if (rtt > 0
                && Math.abs(lastCell.time.getTime() - datetime.getTime()) < 1000
                && pciFilter.includes(lastCell.pci)
                && lastCell.arfcn > 2000000) {
                if (pingRTT[lastCell.arfcn] === undefined) pingRTT[lastCell.arfcn] = [];
                pingRTT[lastCell.arfcn].push(rtt);
                pingRTT.all.push(rtt);
            }
        }
    }
});

for (let arfcn in pingRTT) {
    pingRTT[arfcn].sort(dataUtils.sortNumAsc);
    let mean = statsUtils.meanArray(pingRTT[arfcn]);
    let stdDev = statsUtils.stdDevArray(pingRTT[arfcn]);
    console.log(`${arfcn}; Mean: ${mean}\tStd. dev.: ${stdDev}`);
    let string = `Cumulative distribution function,Ping RTT (ms)\n`;
    for (let i = 0, length1 = pingRTT[arfcn].length; i < length1; i++) {
        string += `${i / (length1 - 1)},${pingRTT[arfcn][i]}\n`;
    }
    fs.writeFileSync(outputPath + `-${arfcn}.csv`, string);
}
