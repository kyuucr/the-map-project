const aggregator = require('../lib/aggregator');
const commandParser = require('../lib/command-parser');
const dataUtils = require('../lib/data-utils');
const logger = require('../lib/logger');
const filter = require('../lib/filter');

// Process arguments
commandParser.addArgument("input files", { isMandatory: true });
// commandParser.addArgument("output metadata file", { isMandatory: true });
commandParser.addArgument("filter", { keyword: "-f", defaultValue: null, transform: (input) =>{
    try {
        return JSON.parse(input);
    } catch (err) {
        logger.e(`Filter parsing error!`, err);
        process.exit(1);
    }
} });
const options = commandParser.parse();
const nrBands = {};
const timeWifiBucket = {};

aggregator.callbackJsonRecursive(options["input files"], (data) => {
    if (data.type !== 'sigcap') {
        return;
    }

    const sigcap = options["filter"] ? filter.filterArray(options["filter"], data.json) : data.json;
    for (const entry of sigcap) {
        const deviceName = entry.deviceName;
        if (nrBands[deviceName] === undefined) {
            nrBands[deviceName] = { "All": 0, "2.4 GHz":0, "5 GHz":0, "6 GHz":0, "N/A": 0 };
        }
        const timestampOrigin = new Date(entry.datetimeIso).getTime();

        if (entry.wifi_info && entry.wifi_info.length > 0) {
            for (const wifiEntry of entry.wifi_info) {
                if (timeWifiBucket[wifiEntry.bssid] === undefined) {
                    timeWifiBucket[wifiEntry.bssid] = [];
                }

                const timeWifiEntry = timestampOrigin - dataUtils.cleanNumeric(wifiEntry.timestampDeltaMs);
                if (!timeWifiBucket[wifiEntry.bssid].includes(timeWifiEntry)) {
                    timeWifiBucket[wifiEntry.bssid].push(timeWifiEntry);

                    if (wifiEntry.primaryFreq >= 5955) {
                        nrBands[deviceName]["6 GHz"]++;
                    } else if (wifiEntry.primaryFreq >= 4915) {
                        nrBands[deviceName]["5 GHz"]++;
                    } else if (wifiEntry.primaryFreq < 3000) {
                        nrBands[deviceName]["2.4 GHz"]++;
                    } else {
                        nrBands[deviceName]["N/A"]++;
                    }
                    nrBands[deviceName]["All"]++;
                }
            }
        }
    }
});

logger.i(["Device Model", "Frequency", "Count", "Percentage"].join(';\t'));
for (const deviceName in nrBands) {
    for (const freqGroup in nrBands[deviceName]) {
        if (freqGroup === "All") {
            continue;
        }
        logger.i([deviceName, freqGroup, nrBands[deviceName][freqGroup], (nrBands[deviceName][freqGroup] / nrBands[deviceName]["All"] * 100).toFixed(2) + '%'].join(';\t'));
    }
}