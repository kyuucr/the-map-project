const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const usePlotlib = ARGS[1] === "plotlib";
const prefix = ARGS[2] || "";

let output = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (output[op] === undefined) output[op] = { all: { rsrp_bw: [], rsrq: [] } };

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            if (json[i].cell_info[j].width > 0) {
                let earfcn = json[i].cell_info[j].earfcn;
                let rsrp_bw = dataUtils.cleanSignal(json[i].cell_info[j].rsrp) + (10 * Math.log10(json[i].cell_info[j].width / 1000));
                let rsrq = dataUtils.cleanSignal(json[i].cell_info[j].rsrq);

                if (output[op][earfcn] === undefined) { output[op][earfcn] = { rsrp_bw: [], rsrq: [] } };
                if (!isNaN(rsrp_bw)) {
                    output[op].all.rsrp_bw.push(rsrp_bw);
                    output[op][earfcn].rsrp_bw.push(rsrp_bw);
                }
                if (!isNaN(rsrq) && rsrq >= -20) {
                    output[op].all.rsrq.push(rsrq);
                    output[op][earfcn].rsrq.push(rsrq);
                }
            }
        }
    }
});

let rsrpFig = {}, rsrqFig = {};
let rsrpAllFig = [], rsrqAllFig = [];
let maxRsrp = -999, maxRsrq = -999;

for (let op in output) {
    if (op === "Unknown") continue;

    for (let freq in output[op]) {
        output[op][freq].rsrp_bw.sort(dataUtils.sortNumAsc);
        output[op][freq].rsrq.sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxRsrp < output[op][freq].rsrp_bw[output[op][freq].rsrp_bw.length - 1]) maxRsrp = output[op][freq].rsrp_bw[output[op][freq].rsrp_bw.length - 1];
            if (maxRsrq < output[op][freq].rsrq[output[op][freq].rsrq.length - 1]) maxRsrq = output[op][freq].rsrq[output[op][freq].rsrq.length - 1];
            if (rsrpFig[op] === undefined) {
                rsrpFig[op] = [];
                rsrqFig[op] = [];
            }
            if (freq === "all") {
                rsrpAllFig.push({ x: output[op][freq].rsrp_bw, y: dataUtils.createCdfY(output[op][freq].rsrp_bw.length), name: `${op}`, type: "scatter" });
                rsrqAllFig.push({ x: output[op][freq].rsrq, y: dataUtils.createCdfY(output[op][freq].rsrq.length), name: `${op}`, type: "scatter" });
            } else {
                rsrpFig[op].push({ x: output[op][freq].rsrp_bw, y: dataUtils.createCdfY(output[op][freq].rsrp_bw.length), name: `${freq}`, type: "scatter" })
                rsrqFig[op].push({ x: output[op][freq].rsrq, y: dataUtils.createCdfY(output[op][freq].rsrq.length), name: `${freq}`, type: "scatter" })
            }
        } else {
            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-primary-rsrp-bw-${freq}.dat`);
            let string = `"Cumulative distributive function" "Primary RSRP scaled by BW"\n`;

            for(let i = 0, length1 = output[op][freq].rsrp_bw.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][freq].rsrp_bw[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);

            outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-primary-rsrq-${freq}.dat`);
            string = `"Cumulative distributive function" "Primary RSRQ"\n`;

            for(let i = 0, length1 = output[op][freq].rsrq.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][freq].rsrq[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);
        }
    }
}

if (usePlotlib) {
    const plotlib = require('nodeplotlib');

    plotlib.stack(rsrpAllFig, {
        xaxis: {
            showgrid: true,
            title: 'BW-scaled Primary RSRP (dB)',
            range: [-140, maxRsrp]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Primary RSRP scaled BW - All`,
        showlegend: true
    });
    plotlib.stack(rsrqAllFig, {
        xaxis: {
            showgrid: true,
            title: 'Primary RSRQ (dB)',
            range: [-20, maxRsrq]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Primary RSRQ - All`,
        showlegend: true
    });
    for (let op in rsrpFig) {
        if (rsrpFig[op].length > 0) {
            let layout = {
                xaxis: {
                    showgrid: true,
                    title: 'BW-scaled Primary RSRP (dB)',
                    range: [-140, maxRsrp]
                },
                yaxis: {
                    showgrid: true,
                    title: 'Cumulative distribution function'
                },
                title: `Primary RSRP scaled BW by EARFCN - ${op}`,
                showlegend: true
            }
            plotlib.stack(rsrpFig[op], layout);
        }
    }
    for (let op in rsrqFig) {
        if (rsrqFig[op].length > 0) {
            let layout = {
                xaxis: {
                    showgrid: true,
                    title: 'RSRQ (dB)',
                    range: [-20, maxRsrq]
                },
                yaxis: {
                    showgrid: true,
                    title: 'Cumulative distribution function'
                },
                title: `Primary RSRQ - ${op}`,
                showlegend: true
            }
            plotlib.stack(rsrqFig[op], layout);
        }
    }
    plotlib.plot();
}