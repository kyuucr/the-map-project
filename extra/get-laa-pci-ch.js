const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> [output path]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];

let pciLaa = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (pciLaa[op] === undefined) pciLaa[op] = {};
        let tempPci = {}

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            if (json[i].cell_info[j].band === 46 && json[i].cell_info[j].channelNum !== 0) {
                let ch = json[i].cell_info[j].channelNum;
                let pci = json[i].cell_info[j].pci;

                if (tempPci[pci] === undefined) tempPci[pci] = [];
                if (!tempPci[pci].includes(ch)) tempPci[pci].push(ch);
            }
        }

        for (let pci in tempPci) {
            tempPci[pci].sort(dataUtils.sortNumAsc);
            let ch = `{${tempPci[pci].join(",")}}`;
            if (pciLaa[op][pci] === undefined) pciLaa[op][pci] = [];
            if (!pciLaa[op][pci].includes(ch)) pciLaa[op][pci].push(ch);
        }

    }
});

let string = "";
for (let op in pciLaa) {
    string += `${op}\n`;
    let earfcnList = [];
    let pciCount = {};
    let pciNrCount = {};
    string += `"LAA PCI"{sep}Channel number\n`;
    for (let pci in pciLaa[op]) {
        string += `${pci}{sep}"${pciLaa[op][pci].join(",")}"\n`;
    }
}

if (outputPath) {
    fs.writeFileSync(outputPath, string.replace(/\{sep\}/g, ","));
} else {
    console.log(string.replace(/\{sep\}/g, "\t"));
}