const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> <output folder>`);
    process.exit(1);
}
let inputFolder = ARGS[0];
let outputFolder = ARGS[1];

let files = agg.getJsonRecursive(inputFolder);

for(let i = 0, length1 = files.length; i < length1; i++){
    let curPath = files[i].path;
    fs.mkdirSync(path.join(outputFolder, "summary"), { recursive: true }, (err) => {
        console.log("ERROR: cannot make output dir");
    });
    fs.mkdirSync(path.join(outputFolder, "cdf"), { recursive: true }, (err) => {
        console.log("ERROR: cannot make output dir");
    });

    let currJson = files[i].json;
    // if (jsonFilter.length > 0) {
    //     currJson = filter.filterArray(jsonFilter, currJson);
    // }

    console.log("Path:", curPath);
    console.log("Number of data:", currJson.length);
    let rssiWifiAll = {};
    let rssiLaaAll = {};
    let rsrpLaaAll = {};
    let rsrqLaaAll = {};
    let pci = {};
    let bssid = {};

    for(let j = 0, length2 = files[i].json.length; j < length2; j++){
        // if (files[i].json[j].datetime.date !== "20201021" || files[i].json[j].datetime.time > 131334527) continue;
        for(let k = 0, length3 = files[i].json[j].cell_info.length; k < length3; k++){
            if (files[i].json[j].cell_info[k].band !== 46) continue;
            let idx = `${files[i].json[j].cell_info[k].pci}-${files[i].json[j].cell_info[k].channelNum}`;
            let curRssi = files[i].json[j].cell_info[k].rssi;
            let curRsrp = files[i].json[j].cell_info[k].rsrp;
            let curRsrq = files[i].json[j].cell_info[k].rsrq;
            if (pci[idx] === undefined) {
                pci[idx] = {
                    pci: idx,
                    meanRssi: curRssi,
                    meanRsrp: curRsrp,
                    meanRsrq: curRsrq,
                    freq: files[i].json[j].cell_info[k].freq,
                    channelNum: files[i].json[j].cell_info[k].channelNum,
                    width: 20,
                    numData: 1
                };
                rssiLaaAll[idx] = [ curRssi ];
                rsrpLaaAll[idx] = [ curRsrp ];
                rsrqLaaAll[idx] = [ curRsrq ];
            } else {
                pci[idx].meanRssi += (curRssi - pci[idx].meanRssi) / pci[idx].numData;
                pci[idx].meanRsrp += (curRsrp - pci[idx].meanRsrp) / pci[idx].numData;
                pci[idx].meanRsrq += (curRsrq - pci[idx].meanRsrq) / pci[idx].numData;
                ++pci[idx].numData;
                rssiLaaAll[idx].push(curRssi);
                rsrpLaaAll[idx].push(curRsrp);
                rsrqLaaAll[idx].push(curRsrq);
            }
        }
        for(let k = 0, length3 = files[i].json[j].wifi_info.length; k < length3; k++){
            let idx = files[i].json[j].wifi_info[k].bssid;
            let curRssi = files[i].json[j].wifi_info[k].rssi;
            if (bssid[idx] === undefined) {
                bssid[idx] = {
                    bssid: idx,
                    ssid: files[i].json[j].wifi_info[k].ssid,
                    meanRssi: curRssi,
                    primaryFreq: files[i].json[j].wifi_info[k].primaryFreq,
                    channelNum: files[i].json[j].wifi_info[k].channelNum,
                    width: files[i].json[j].wifi_info[k].width,
                    standard: files[i].json[j].wifi_info[k].standard,
                    numData: 1
                };
                rssiWifiAll[idx] = [ curRssi ];
            } else {
                bssid[idx].meanRssi += (curRssi - bssid[idx].meanRssi) / bssid[idx].numData;
                ++bssid[idx].numData;
                rssiWifiAll[idx].push(curRssi);
            }
        }
    }
    let os = fs.createWriteStream(path.join(outputFolder, "summary", `${curPath.replace(/\/|\\/g,"-")}-laa-summary.csv`));
    let firstData = true;
    for (let idx in pci) {
        if (firstData) {
            firstData = false;
            os.write(`${csvUtils.unpackKeys(pci[idx])}\n`);
        }
        os.write(`${csvUtils.unpackVals(pci[idx])}\n`);

        let temp = rssiLaaAll[idx];
        temp.sort();
        let os2 = fs.createWriteStream(path.join(outputFolder, "cdf", `${curPath.replace(/\/|\\/g,"-")}-laa-rssi-${idx}.csv`));
        let i = 0;
        for (let j = temp.length - 1, length2 = temp.length; j >= 0; j--) {
            os2.write(`${i++ / length2},${temp[j]}\n`);
        }
        os2.close();

        temp = rsrpLaaAll[idx];
        temp.sort();
        os2 = fs.createWriteStream(path.join(outputFolder, "cdf", `${curPath.replace(/\/|\\/g,"-")}-laa-rsrp-${idx}.csv`));
        i = 0;
        for (let j = temp.length - 1, length2 = temp.length; j >= 0; j--) {
            os2.write(`${i++ / length2},${temp[j]}\n`);
        }
        os2.close();

        temp = rsrqLaaAll[idx];
        temp.sort();
        os2 = fs.createWriteStream(path.join(outputFolder, "cdf", `${curPath.replace(/\/|\\/g,"-")}-laa-rsrq-${idx}.csv`));
        i = 0;
        for (let j = temp.length - 1, length2 = temp.length; j >= 0; j--) {
            os2.write(`${i++ / length2},${temp[j]}\n`);
        }
        os2.close();
    }
    os.close();
    os = fs.createWriteStream(path.join(outputFolder, "summary", `${curPath.replace(/\/|\\/g,"-")}-wifi-summary.csv`));
    firstData = true;
    for (let idx in bssid) {
        if (firstData) {
            firstData = false;
            os.write(`${csvUtils.unpackKeys(bssid[idx])}\n`);
        }
        if (bssid[idx].primaryFreq === 5745
                || bssid[idx].primaryFreq === 5765
                || bssid[idx].primaryFreq === 5785
                || bssid[idx].primaryFreq === 5805) {
            os.write(`${csvUtils.unpackVals(bssid[idx])}\n`);

            let temp = rssiWifiAll[idx];
            temp.sort();
            let os2 = fs.createWriteStream(path.join(outputFolder, "cdf", `${curPath.replace(/\/|\\/g,"-")}-wifi-rssi-${bssid[idx].ssid.replace(/\/|\\/g,"-")}-${idx}.csv`));
            let i = 0;
            for (let j = temp.length - 1, length2 = temp.length; j >= 0; j--) {
                os2.write(`${i++ / length2},${temp[j]}\n`);
            }
            os2.close();
        }
    }
    os.close();
}