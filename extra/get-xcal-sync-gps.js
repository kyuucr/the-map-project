const agg = require('../lib/aggregator');
const geohelper = require('../lib/geohelper');
const dataUtils = require('../lib/data-utils');
const statsUtils = require('../lib/stats-utils');
const path = require("path");
const fs = require("fs");

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <input XCAP CSV file/folder> <output CSV>`);
    process.exit(0);
}
const inputPath = ARGS[0];
// const outputPath = ARGS[1].replace(/\.csv$/, "");

const pciFilter = [ "152","133","134","508","21","23","907","478","479","686","425","6","7","327","322","333" ];
const bsLoc = {
    "152": [ 41.87955479, -87.62717675],
    "133": [ 41.87950112, -87.62924641],
    "134": [ 41.87950112, -87.62924641],
    "508": [ 41.87935633, -87.63068307],
    "21":  [ 41.87908324, -87.63072129],
    "23":  [ 41.87908324, -87.63072129],
    "907": [ 41.8780857,  -87.63167918],
    "478": [ 41.87809419, -87.63114944],
    "479": [ 41.87809419, -87.63114944],
    "686": [ 41.87822574, -87.62924742],
    "425": [ 41.87816234, -87.6274775],
    "6":   [ 41.8781903,  -87.62704231],
    "7":   [ 41.8781903,  -87.62704231],
    "327": [ 41.87301299, -87.62185969],
    "322": [ 41.87153991, -87.62091528],
    "333": [ 41.87157813, -87.62207113],
};

const BBP_MAX_LAT = 41.87302922975753;
const BBP_MIN_LAT = 41.871620648385274;
const BBP_MAX_LON = -87.62075759960607;
const BBP_MIN_LON = -87.62208068247578;

// Header
const CELLS = [ "PCell", "SCell1", "SCell2", "SCell3", "SCell4", "SCell5", "SCell6", "SCell7" ];
let rows = [];
let dupes = {};

agg.callbackCsvRecursive(inputPath, data => {
    let path = data.path;
    console.log(path)
    let isBBP = path.includes("BBP");
    let isATT = path.includes("ATT");
    let header = data.header;
    let csv = data.csv;
    // let colIds = dataUtils.getXCalHeader(header);
    // if (colIds.Lon === undefined || colIds.Lat === undefined
    //     || colIds.PCell_ServPCI === undefined
    //     || colIds.PCell_ServFreq === undefined
    //     || colIds.PCell_ServRSRP === undefined
    //     || colIds.PCell_BestRSRP === undefined
    //     || colIds.PCell_NRARFCN === undefined) {
    //     console.log(`Important columns not found!`);
    //     return;
    // }
    // console.log(colIds)

    for (let i = 0, length1 = csv.length; i < length1; i++) {
        let row = csv[i];
        // let datetime = new Date (row[0]);
        // let lon = row[colIds.Lon];
        // let lat = row[colIds.Lat];
        // if (!lon || !lat
        //     || (isBBP && (lat > BBP_MAX_LAT || lat < BBP_MIN_LAT || lon > BBP_MAX_LON || lon < BBP_MIN_LON))
        //     || (!pciFilter.includes(row[colIds.PCell_ServPCI]))
        //     || (row[colIds.PCell_ServFreq] < 20000)) {
        //     continue;
        // }

        let idx = rows.findIndex((el) => { return row[0] === el[0]; });
        if (idx < 0) {
            rows.push(row);
        } else {
            if (dupes[row[0]] === undefined) dupes[row[0]] = [];
            dupes[row[0]].push(row);
            // console.log(`Duplicate timestamp at row ${i}`);
            // console.log(`source: ${rows[idx]}`);
            // console.log(`target: ${row}`);
        }
    }
});

for (let idx in dupes) {
    let findIdx = rows.findIndex((el) => { return idx === el[0]; });
    console.log(`Duplicate timestamp at row ${findIdx}`);
    console.log(`source: ${rows[findIdx]}`);
    for (let row of dupes[idx]) {
        console.log(`target: ${row}`);
    }
}

// for (let cat in servRSRP) {
//     if (servRSRP[cat].length > 0) {
//         servRSRP[cat].sort(dataUtils.sortNumAsc);
//         let mean = statsUtils.meanArray(servRSRP[cat]);
//         let stdDev = statsUtils.stdDevArray(servRSRP[cat]);
//         // console.log(`${cat}; Mean: ${mean}\tStd. dev.: ${stdDev}`);
//         let string = `Cumulative distribution function,Serving RSRP (dBm)\n`;
//         for (let i = 0, length1 = servRSRP[cat].length; i < length1; i++) {
//             string += `${i / (length1 - 1)},${servRSRP[cat][i]}\n`;
//         }
//         fs.writeFileSync(outputPath + `-serving-${cat}.csv`, string);
//     }
// }