const agg = require('../lib/aggregator');
const filter = require('../lib/filter');
const geohelper = require('../lib/geohelper');
const fs = require('fs');
const path = require('path');

let allCells = agg.getJson(path.join("input", "capture_5g_december", "data"));

console.log("datetimeIso,ss_rsrp,rsrp");

for(let j = 0, length2 = allCells.length; j < length2; j++){
    // Sanity check
    if (allCells[j].nr_info.length > 0 && allCells[j].cell_info.length > 0) {
        if (allCells[j].cell_info[0].width === 0) continue;
        ss_rsrp = allCells[j].nr_info[0].ssRsrp;
        rsrp = allCells[j].cell_info[0].rsrp;
        console.log(`${allCells[j].datetimeIso},${ss_rsrp}, ${rsrp}`);
    }
}
