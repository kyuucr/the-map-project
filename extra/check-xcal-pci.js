const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <input CSVs>`);
    process.exit(0);
}

const dir = ARGS[0];

let colIds = {};
let cdf = {};
let hist = {};

agg.callbackCsvRecursive(dir, data => {
    let header = data.header;
    let csv = data.csv;
    let filename = path.basename(data.path).replace(/\.csv/, "");
    console.log(`${filename}, # of cols: ${data.header.length}, # of rows: ${data.csv.length}`)
    if (cdf[filename] === undefined) cdf[filename] = { allPci: { allBeam: {} } };
    if (hist[filename] === undefined) hist[filename] = {};

    let colIds = dataUtils.getXCalHeader(header);

    let prevDatetime = new Date(0);
    let datetimeSpan = { start: null, end: null, traffic: [] };
    let spans = [];

    for (let row of csv) {
        if (row[0] === "Count") break;
        let PCI = row[colIds.PCell_ServPCI];
        for (let i = 1; i <= 7; i++) {
            let SCell_PCI = row[colIds[`SCell${i}_ServPCI`]];
            if (SCell_PCI && PCI !== SCell_PCI) {
                console.log(`PCI difference for PCell: ${PCI}, SCell${i}: ${SCell_PCI}`);
            }
        }
    }

});
