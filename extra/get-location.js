const agg = require('../lib/aggregator');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap data path> <input NSG csv> [output path]`);
    process.exit(0);
}

let dir = ARGS[0];
let nsgPath = ARGS[1];
let outputPath = ARGS[2] || path.join("html", "outputs", "nr-path.geojson");

let sigcap = agg.getJson(dir);
let nsgCsv = fs.readFileSync(nsgPath, 'utf8').split('\n');
let outputSigcap = [];

let nsgIndex = -1;
let nsgData = [];
let timeData = [];
for(let i = 0, length1 = nsgCsv.length; i < length1; i++){
    let split = nsgCsv[i].split(',');
    if (split[0] == "Type") {
        nsgIndex++;
        nsgData[nsgIndex] = [];
    } else if (split[0] !== "") {
        nsgData[nsgIndex].push(split);
        if (split[10].match(/\d\d\.\d\d\.\d\d\d/)) {
            console.log(split[10])
            timeData.push(split[10].replace(/\./g, ""));
        }
    }
}

let options = {
    minLat: 999.9,
    maxLat: -999.9,
    minLng: 999.9,
    maxLng: -999.9
};

nsgIndex = 0;
for(let i = 0, length1 = sigcap.length; i < length1; i++){
    let timeDiff = sigcap[i].datetime.time - parseInt(timeData[nsgIndex]);
    if (timeDiff > 0) {
        options.minLat = (options.minLat > sigcap[i].location.latitude) ? sigcap[i].location.latitude : options.minLat;
        options.minLng = (options.minLng > sigcap[i].location.longitude) ? sigcap[i].location.longitude : options.minLng;
        options.maxLat = (options.maxLat < sigcap[i].location.latitude) ? sigcap[i].location.latitude : options.maxLat;
        options.maxLng = (options.maxLng < sigcap[i].location.longitude) ? sigcap[i].location.longitude : options.maxLng;

        // console.log(sigcap[i].datetime.time, timeData[nsgIndex], timeDiff);
        outputSigcap.push(sigcap[i]);
        nsgIndex++;
    }
}

console.log(nsgData.length, timeData.length, outputSigcap.length)

let outputGeojson = {
    OPTIONS: options,
    type: "FeatureCollection",
    features: []
}
let lineFeature = {
    type: "Feature",
    properties: {
        IDX: outputSigcap.length
    },
    geometry: {
        type: "LineString",
        coordinates: []
    }
}
for(let i = 0, length1 = outputSigcap.length; i < length1; i++){
    // console.log(outputSigcap[i].location.latitude, outputSigcap[i].location.longitude);
    lineFeature.geometry.coordinates.push([outputSigcap[i].location.longitude, outputSigcap[i].location.latitude]);
    let temp = {
        type: "Feature",
        properties: {
            IDX: i,
            SIGCAP_DATA: outputSigcap[i],
            NSG_DATA: nsgData[i]
        },
        geometry: {
            type: "Point",
            coordinates: [outputSigcap[i].location.longitude, outputSigcap[i].location.latitude]
        }
    };
    outputGeojson.features.push(temp);
}
outputGeojson.features.push(lineFeature);

console.log(`Writing output at ${outputPath}`);
fs.writeFileSync(outputPath, JSON.stringify(outputGeojson));
