const agg = require('../lib/aggregator');
const stats = require('../lib/stats-utils');
let temp = agg.getJson("./input/capture_5g_december/data/");
let acc = temp.map(x => x.location.hor_acc);
let cdf = stats.cdfArray(acc);
for(let i = 0, length1 = cdf.length; i < length1; i++){
    console.log(`${i / length1},${cdf[i]}`);
}