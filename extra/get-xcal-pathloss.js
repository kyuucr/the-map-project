const agg = require('../lib/aggregator');
const geohelper = require('../lib/geohelper');
const dataUtils = require('../lib/data-utils');
const statsUtils = require('../lib/stats-utils');
const fs = require("fs");

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <input XCAP CSV file/folder> <output CSV>`);
    process.exit(0);
}
const inputPath = ARGS[0];
const outputPath = ARGS[1];

const pciFilter = [ "152","133","134","508","21","23","907","478","479","686","425","6","7","327","322","333" ];
const bsLoc = {
    "152": [ 41.87955479, -87.62717675],
    "133": [ 41.87950112, -87.62924641],
    "134": [ 41.87950112, -87.62924641],
    "508": [ 41.87935633, -87.63068307],
    "21":  [ 41.87908324, -87.63072129],
    "23":  [ 41.87908324, -87.63072129],
    "907": [ 41.8780857,  -87.63167918],
    "478": [ 41.87809419, -87.63114944],
    "479": [ 41.87809419, -87.63114944],
    "686": [ 41.87822574, -87.62924742],
    "425": [ 41.87816234, -87.6274775],
    "6":   [ 41.8781903,  -87.62704231],
    "7":   [ 41.8781903,  -87.62704231],
    "327": [ 41.87301299, -87.62185969],
    "322": [ 41.87153991, -87.62091528],
    "333": [ 41.87157813, -87.62207113],
};
const BBP_MAX_LAT = 41.87302922975753;
const BBP_MIN_LAT = 41.871620648385274;
const BBP_MAX_LON = -87.62075759960607;
const BBP_MIN_LON = -87.62208068247578;

// Header
let plValues = [];

agg.callbackCsvRecursive(inputPath, data => {
    let path = data.path;
    let isBBP = path.includes("BBP");
    let header = data.header;
    let csv = data.csv;
    let colIds = dataUtils.getXCalHeader(header);

    let output = ``;
    for (let row of csv) {
        let lon = parseFloat(row[colIds.Lon]);
        let lat = parseFloat(row[colIds.Lat]);
        if ((isBBP && (lat > BBP_MAX_LAT || lat < BBP_MIN_LAT || lon > BBP_MAX_LON || lon < BBP_MIN_LON))
            || (colIds.PCell_ServPCI >= 0 && !pciFilter.includes(row[colIds.PCell_ServPCI]))
            || (colIds.PCell_ServFreq >= 0 && row[colIds.PCell_ServFreq] < 20000)) {
            continue;
        }
        if (colIds.PCell_ServPL >= 0 && row[colIds.PCell_ServPL]) {
            plValues.push(parseFloat(row[colIds.PCell_ServPL]));
        }
    }
});

plValues.sort(dataUtils.sortNumAsc);
let meanPL = statsUtils.meanArray(plValues);
let stdDevPL = statsUtils.stdDevArray(plValues);
let string = `Mean,${meanPL}\nStd. dev.,${stdDevPL}\nCumulative distribution function,Primary serving pathloss (dB)\n`;
for (let i = 0, length1 = plValues.length; i < length1; i++) {
    string += `${i / (length1 - 1)},${plValues[i]}\n`;
}
fs.writeFileSync(outputPath, string);
