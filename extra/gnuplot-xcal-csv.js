const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');
const util = require("util");

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <input CSVs> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const prefix = ARGS[2] || "";

let checkNumericVal = function(val, key) {
    let num;
    try {
        num = parseFloat(val);
    } catch (err) {
        return false;
    }
    if (key.match(/RSRP/)) return num >= -140 && num < 0;
    else if (key.match(/RSRQ/)) return num >= -20 && num < 0;
    else if (key.match(/SINR/)) return num >= -40 && num <= 40;
    else if (key.match(/Tput|Ping/)) return num > 0;
    else return num >= 0;
}

let specialPCI = {
    "JA-VZW-MAPPING-ALL-MERGED-5G-RF": {
        "6+7": [ 6, 7 ],
        "21+23": [ 21, 23 ],
        "133+134": [ 133, 134 ],
        "478+479": [ 478, 479 ],
    }
}

let formatDatetime = function(datetimeStart, datetimeEnd) {
    return `${datetimeStart.getFullYear()}_${datetimeStart.getMonth() + 1}_S${datetimeStart.getDate()}_${datetimeStart.getHours()}_${datetimeStart.getMinutes()}_E${datetimeEnd.getDate()}_${datetimeEnd.getHours()}_${datetimeEnd.getMinutes()}`;
}

const fsWriteFile = util.promisify(fs.writeFile);
const MAX_OPEN_FILES = 1000;
let numWrite = 0;
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function writeFile(outputFile, string) {
    if (numWrite < MAX_OPEN_FILES) {
        numWrite++;
        let promise = await fsWriteFile(outputFile, string);
        numWrite--;
        return promise.catch(err => {
            console.log(`Write error: ${outputFile}`, err);
        });
    } else {
        let timeoutMs = Math.random() * 10000;
        await timeout(timeoutMs);
        return await writeFile(outputFile, string);
    }
}

agg.callbackCsvRecursive(dir, data => {
    let header = data.header;
    let csv = data.csv;
    let filename = path.basename(data.path).replace(/\.csv/, "");
    console.log(`${filename}, # of cols: ${data.header.length}, # of rows: ${data.csv.length}`);
    let currDir = path.join(outputPath, filename);
    let allDir = path.join(currDir, "all");
    if (!fs.existsSync(currDir)) fs.mkdirSync(currDir);
    if (!fs.existsSync(allDir)) fs.mkdirSync(allDir);

    let cdf = { allPci: { allBeam: {} } };
    let hist = {};
    let colIds = {};

    for (let i = 0, length1 = header.length; i < length1; i++) {
        switch (header[i]) {
            case "Lon": colIds.Lon = i; break;
            case "Lat": colIds.Lat = i; break;
            case "5G KPI PCell RF Serving PCI": colIds.PCell_ServPCI = i; break;
            case "5G KPI PCell RF Serving SSB Idx": colIds.PCell_ServSSB = i; break;
            case "5G KPI PCell RF Band": colIds.PCell_ServBand = i; break;
            case "5G KPI PCell RF BandWidth": colIds.PCell_ServBW = i; break;
            case "5G KPI PCell RF Frequency [MHz]": colIds.PCell_ServFreq = i; break;
            case "5G KPI PCell RF Pathloss [dB]": colIds.PCell_ServPL = i; break;
            case "5G KPI PCell RF Serving SS-RSRP [dBm]": colIds.PCell_ServRSRP = i; break;
            case "5G KPI PCell RF Serving SS-RSRQ [dB]": colIds.PCell_ServRSRQ = i; break;
            case "5G KPI PCell RF Serving SS-SINR [dB]": colIds.PCell_ServSINR = i; break;
            case "5G KPI PCell RF Serving CQI [dB]": colIds.PCell_ServCQI = i; break;
            case "5G KPI PCell RF RI": colIds.PCell_ServRI = i; break;
            case "5G KPI PCell RF Rx Beam ID 0": colIds.PCell_RXBeam0 = i; break;
            case "5G KPI PCell RF Rx Beam ID 1": colIds.PCell_RXBeam1 = i; break;
            case "5G KPI PCell RF Best Beam SSB Idx": colIds.PCell_BestSSB = i; break;
            case "5G KPI PCell RF Best Beam SS-RSRP [dBm]": colIds.PCell_BestRSRP = i; break;
            case "5G KPI PCell RF Best Beam SS-RSRQ [dBm]": colIds.PCell_BestRSRQ = i; break;
            case "5G KPI PCell RF Best Beam State": colIds.PCell_BestState = i; break;
            case "5G KPI PCell RF Neighbor Top1 PCI": colIds.PCell_Neighbor1PCI = i; break;
            case "5G KPI PCell RF Neighbor Top1 SS-RSRP [dBm]": colIds.PCell_Neighbor1RSRP = i; break;
            case "5G KPI PCell RF Neighbor Top1 SS-RSRQ [dB]": colIds.PCell_Neighbor1RSRQ = i; break;
            case "5G KPI PCell RF Neighbor Top2 PCI": colIds.PCell_Neighbor2PCI = i; break;
            case "5G KPI PCell RF Neighbor Top2 SS-RSRP [dBm]": colIds.PCell_Neighbor2RSRP = i; break;
            case "5G KPI PCell RF Neighbor Top2 SS-RSRQ [dB]": colIds.PCell_Neighbor2RSRQ = i; break;
            case "5G KPI PCell RF Neighbor Top3 PCI": colIds.PCell_Neighbor3PCI = i; break;
            case "5G KPI PCell RF Neighbor Top3 SS-RSRP [dBm]": colIds.PCell_Neighbor3RSRP = i; break;
            case "5G KPI PCell RF Neighbor Top3 SS-RSRQ [dB]": colIds.PCell_Neighbor3RSRQ = i; break;
            case "5G KPI PCell Layer1 DL BLER [%]": colIds.PCell_DLBLER = i; break;
            case "5G KPI PCell Layer1 DL Modulation0 Representative Value": colIds.PCell_DLMod = i; break;
            case "5G KPI PCell Layer1 DL RB Num (Mode)": colIds.PCell_RBMode = i; break;
            case "5G KPI PCell Layer1 DL RB Num (Avg)": colIds.PCell_RBAvg = i; break;
            case "5G KPI PCell Layer1 PDSCH Throughput [Mbps]": colIds.PCell_PDSCHTput = i; break;
            case "5G KPI PCell Layer2 MAC DL Throughput [Mbps]": colIds.PCell_MACDLTput = i; break;
            case "5G KPI SCell[1] RF Serving PCI": colIds.SCell1_ServPCI = i; break;
            case "5G KPI SCell[1] RF Serving SSB Idx": colIds.SCell1_ServSSB = i; break;
            case "5G KPI SCell[1] RF Frequency [MHz]": colIds.SCell1_ServFreq = i; break;
            case "5G KPI SCell[1] RF Serving SS-RSRP [dBm]": colIds.SCell1_ServRSRP = i; break;
            case "5G KPI SCell[1] RF Serving SS-RSRQ [dB]": colIds.SCell1_ServRSRQ = i; break;
            case "5G KPI SCell[1] RF Serving SS-SINR [dB]": colIds.SCell1_ServSINR = i; break;
            case "5G KPI SCell[1] Layer1 DL BLER [%]": colIds.SCell1_DLBLER = i; break;
            case "5G KPI SCell[1] Layer1 DL Modulation0 Representative Value": colIds.SCell1_DLMod = i; break;
            case "5G KPI SCell[1] Layer1 DL RB Num (Mode)": colIds.SCell1_RBMode = i; break;
            case "5G KPI SCell[1] Layer1 DL RB Num (Avg)": colIds.SCell1_RBAvg = i; break;
            case "5G KPI SCell[1] Layer1 PDSCH Throughput [Mbps]": colIds.SCell1_PDSCHTput = i; break;
            case "5G KPI SCell[1] Layer2 MAC DL Throughput [Mbps]": colIds.SCell1_MACDLTput = i; break;
            case "5G KPI SCell[2] RF Serving PCI": colIds.SCell2_ServPCI = i; break;
            case "5G KPI SCell[2] RF Serving SSB Idx": colIds.SCell2_ServSSB = i; break;
            case "5G KPI SCell[2] RF Frequency [MHz]": colIds.SCell2_ServFreq = i; break;
            case "5G KPI SCell[2] RF Serving SS-RSRP [dBm]": colIds.SCell2_ServRSRP = i; break;
            case "5G KPI SCell[2] RF Serving SS-RSRQ [dB]": colIds.SCell2_ServRSRQ = i; break;
            case "5G KPI SCell[2] RF Serving SS-SINR [dB]": colIds.SCell2_ServSINR = i; break;
            case "5G KPI SCell[2] Layer1 DL BLER [%]": colIds.SCell2_DLBLER = i; break;
            case "5G KPI SCell[2] Layer1 DL Modulation0 Representative Value": colIds.SCell2_DLMod = i; break;
            case "5G KPI SCell[2] Layer1 DL RB Num (Mode)": colIds.SCell2_RBMode = i; break;
            case "5G KPI SCell[2] Layer1 DL RB Num (Avg)": colIds.SCell2_RBAvg = i; break;
            case "5G KPI SCell[2] Layer1 PDSCH Throughput [Mbps]": colIds.SCell2_PDSCHTput = i; break;
            case "5G KPI SCell[2] Layer2 MAC DL Throughput [Mbps]": colIds.SCell2_MACDLTput = i; break;
            case "5G KPI SCell[3] RF Serving PCI": colIds.SCell3_ServPCI = i; break;
            case "5G KPI SCell[3] RF Serving SSB Idx": colIds.SCell3_ServSSB = i; break;
            case "5G KPI SCell[3] RF Frequency [MHz]": colIds.SCell3_ServFreq = i; break;
            case "5G KPI SCell[3] RF Serving SS-RSRP [dBm]": colIds.SCell3_ServRSRP = i; break;
            case "5G KPI SCell[3] RF Serving SS-RSRQ [dB]": colIds.SCell3_ServRSRQ = i; break;
            case "5G KPI SCell[3] RF Serving SS-SINR [dB]": colIds.SCell3_ServSINR = i; break;
            case "5G KPI SCell[3] Layer1 DL BLER [%]": colIds.SCell3_DLBLER = i; break;
            case "5G KPI SCell[3] Layer1 DL Modulation0 Representative Value": colIds.SCell3_DLMod = i; break;
            case "5G KPI SCell[3] Layer1 DL RB Num (Mode)": colIds.SCell3_RBMode = i; break;
            case "5G KPI SCell[3] Layer1 DL RB Num (Avg)": colIds.SCell3_RBAvg = i; break;
            case "5G KPI SCell[3] Layer1 PDSCH Throughput [Mbps]": colIds.SCell3_PDSCHTput = i; break;
            case "5G KPI SCell[3] Layer2 MAC DL Throughput [Mbps]": colIds.SCell3_MACDLTput = i; break;
            case "5G KPI SCell[4] RF Serving PCI": colIds.SCell4_ServPCI = i; break;
            case "5G KPI SCell[4] RF Serving SSB Idx": colIds.SCell4_ServSSB = i; break;
            case "5G KPI SCell[4] RF Frequency [MHz]": colIds.SCell4_ServFreq = i; break;
            case "5G KPI SCell[4] RF Serving SS-RSRP [dBm]": colIds.SCell4_ServRSRP = i; break;
            case "5G KPI SCell[4] RF Serving SS-RSRQ [dB]": colIds.SCell4_ServRSRQ = i; break;
            case "5G KPI SCell[4] RF Serving SS-SINR [dB]": colIds.SCell4_ServSINR = i; break;
            case "5G KPI SCell[4] Layer1 DL BLER [%]": colIds.SCell4_DLBLER = i; break;
            case "5G KPI SCell[4] Layer1 DL Modulation0 Representative Value": colIds.SCell4_DLMod = i; break;
            case "5G KPI SCell[4] Layer1 DL RB Num (Mode)": colIds.SCell4_RBMode = i; break;
            case "5G KPI SCell[4] Layer1 DL RB Num (Avg)": colIds.SCell4_RBAvg = i; break;
            case "5G KPI SCell[4] Layer1 PDSCH Throughput [Mbps]": colIds.SCell4_PDSCHTput = i; break;
            case "5G KPI SCell[4] Layer2 MAC DL Throughput [Mbps]": colIds.SCell4_MACDLTput = i; break;
            case "5G KPI SCell[5] RF Serving PCI": colIds.SCell5_ServPCI = i; break;
            case "5G KPI SCell[5] RF Serving SSB Idx": colIds.SCell5_ServSSB = i; break;
            case "5G KPI SCell[5] RF Frequency [MHz]": colIds.SCell5_ServFreq = i; break;
            case "5G KPI SCell[5] RF Serving SS-RSRP [dBm]": colIds.SCell5_ServRSRP = i; break;
            case "5G KPI SCell[5] RF Serving SS-RSRQ [dB]": colIds.SCell5_ServRSRQ = i; break;
            case "5G KPI SCell[5] RF Serving SS-SINR [dB]": colIds.SCell5_ServSINR = i; break;
            case "5G KPI SCell[5] Layer1 DL BLER [%]": colIds.SCell5_DLBLER = i; break;
            case "5G KPI SCell[5] Layer1 DL Modulation0 Representative Value": colIds.SCell5_DLMod = i; break;
            case "5G KPI SCell[5] Layer1 DL RB Num (Mode)": colIds.SCell5_RBMode = i; break;
            case "5G KPI SCell[5] Layer1 DL RB Num (Avg)": colIds.SCell5_RBAvg = i; break;
            case "5G KPI SCell[5] Layer1 PDSCH Throughput [Mbps]": colIds.SCell5_PDSCHTput = i; break;
            case "5G KPI SCell[5] Layer2 MAC DL Throughput [Mbps]": colIds.SCell5_MACDLTput = i; break;
            case "5G KPI SCell[6] RF Serving PCI": colIds.SCell6_ServPCI = i; break;
            case "5G KPI SCell[6] RF Serving SSB Idx": colIds.SCell6_ServSSB = i; break;
            case "5G KPI SCell[6] RF Frequency [MHz]": colIds.SCell6_ServFreq = i; break;
            case "5G KPI SCell[6] RF Serving SS-RSRP [dBm]": colIds.SCell6_ServRSRP = i; break;
            case "5G KPI SCell[6] RF Serving SS-RSRQ [dB]": colIds.SCell6_ServRSRQ = i; break;
            case "5G KPI SCell[6] RF Serving SS-SINR [dB]": colIds.SCell6_ServSINR = i; break;
            case "5G KPI SCell[6] Layer1 DL BLER [%]": colIds.SCell6_DLBLER = i; break;
            case "5G KPI SCell[6] Layer1 DL Modulation0 Representative Value": colIds.SCell6_DLMod = i; break;
            case "5G KPI SCell[6] Layer1 DL RB Num (Mode)": colIds.SCell6_RBMode = i; break;
            case "5G KPI SCell[6] Layer1 DL RB Num (Avg)": colIds.SCell6_RBAvg = i; break;
            case "5G KPI SCell[6] Layer1 PDSCH Throughput [Mbps]": colIds.SCell6_PDSCHTput = i; break;
            case "5G KPI SCell[6] Layer2 MAC DL Throughput [Mbps]": colIds.SCell6_MACDLTput = i; break;
            case "5G KPI SCell[7] RF Serving PCI": colIds.SCell7_ServPCI = i; break;
            case "5G KPI SCell[7] RF Serving SSB Idx": colIds.SCell7_ServSSB = i; break;
            case "5G KPI SCell[7] RF Frequency [MHz]": colIds.SCell7_ServFreq = i; break;
            case "5G KPI SCell[7] RF Serving SS-RSRP [dBm]": colIds.SCell7_ServRSRP = i; break;
            case "5G KPI SCell[7] RF Serving SS-RSRQ [dB]": colIds.SCell7_ServRSRQ = i; break;
            case "5G KPI SCell[7] RF Serving SS-SINR [dB]": colIds.SCell7_ServSINR = i; break;
            case "5G KPI SCell[7] Layer1 DL BLER [%]": colIds.SCell7_DLBLER = i; break;
            case "5G KPI SCell[7] Layer1 DL Modulation0 Representative Value": colIds.SCell7_DLMod = i; break;
            case "5G KPI SCell[7] Layer1 DL RB Num (Mode)": colIds.SCell7_RBMode = i; break;
            case "5G KPI SCell[7] Layer1 DL RB Num (Avg)": colIds.SCell7_RBAvg = i; break;
            case "5G KPI SCell[7] Layer1 PDSCH Throughput [Mbps]": colIds.SCell7_PDSCHTput = i; break;
            case "5G KPI SCell[7] Layer2 MAC DL Throughput [Mbps]": colIds.SCell7_MACDLTput = i; break;
            case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(5G-NR PDU) [Mbps]": colIds.Total_5GDLPHYTput = i; break;
            case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(LTE PDU) [Mbps]": colIds.Total_4GDLPHYTput = i; break;
            case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(Total PDU) [Mbps]": colIds.Total_DLPHYTput = i; break;
            case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(5G-NR PDU) [Mbps]": colIds.Total_5GDLMACTput = i; break;
            case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(LTE PDU) [Mbps]": colIds.Total_4GDLMACTput = i; break;
            case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(Total PDU) [Mbps]": colIds.Total_DLMACTput = i; break;
            case "Ping & Trace RTT (ms)": colIds.Total_PingRTT = i; break;
            default: break;
        }
    }

    // console.log(`Found columns:`);
    // for (let col in colIds) {
    //     console.log(`${col}: ${colIds[col]}`);
    // }

    let spanSeparatedCdf = [];
    let currCdf = { allPci: { allBeam: {} } };
    let currHist = {};
    let currTraffic = [];
    let prevDatetime = new Date(0);
    let lastServTime = new Date(0);
    let startTime, prevPCell_ServPCI, prevPCell_ServSSB, prevPCell_ServFreq;
    for (let row of csv) {
        // Skip end-part
        if (row[0] === "Count") break;
        // Determine span and traffic type
        let datetime = new Date(row[0]);
        let timespan = datetime.getTime() - prevDatetime.getTime();
        if (timespan < 0 || timespan > 60000) {
            // new datetime span, save previous datetime span
            if (prevDatetime.getTime() !== 0) {
                // console.log(`${startTime}, ${prevDatetime}, traffic: ${currTraffic.join(",")}`);
                spanSeparatedCdf.push({ startTime: startTime, endTime: prevDatetime, cdf: currCdf, hist: currHist, traffic: currTraffic.join(",") });
            }
            currTraffic = [];
            currCdf = { allPci: { allBeam: {} } };
            currHist = {}
            startTime = datetime;
        } else {
            if (row[colIds.Total_PingRTT]) {
                if (!currTraffic.includes('ping')) currTraffic.push('ping');
            }
            if (parseFloat(row[colIds.Total_5GDLPHYTput]) > 10) {
                if (!currTraffic.includes('http')) currTraffic.push('http');
            }
        }
        prevDatetime = datetime;

        // Get previous PCell_ServPCI/SSB
        timespan = datetime.getTime() - lastServTime.getTime();
        if (row[colIds[`PCell_ServPCI`]] && row[colIds[`PCell_ServSSB`]] && row[colIds[`PCell_ServFreq`]]
                && (timespan < 0 || timespan >= 1000)) {
            prevPCell_ServPCI = row[colIds[`PCell_ServPCI`]];
            prevPCell_ServSSB = row[colIds[`PCell_ServSSB`]];
            prevPCell_ServFreq = row[colIds[`PCell_ServFreq`]];
            lastServTime = datetime;
            timespan = 0;
        }
        // Skip non-mmWave bands only if the information is fresh
        if (prevPCell_ServFreq < 24000 && timespan >= 0 && timespan < 1000) continue;

        let sumRB = 0;
        for (let key in colIds) {
            if (key.match(/RSR[PQ]|SINR|PL|CQI|Tput|Ping|DLBLER|RBAvg|PDSCHTput|MACDLTput/)) {
                let PCI, SSB;
                let splitKey = key.split("_");
                let carrier = splitKey[0];
                let val = row[colIds[key]];
                let neighbor = key.match(/Neighbor(\d)/);
                if (neighbor) {
                    let num = neighbor[1];
                    PCI = row[colIds[`${carrier}_Neighbor${num}PCI`]];
                    SSB = 1;
                } else if (carrier === "Total") {
                    // If the prev PCell info is outdated, then we have to skip
                    if (timespan < 0 || timespan >= 1000) continue;
                    PCI = prevPCell_ServPCI;
                    SSB = prevPCell_ServSSB;
                } else {
                    PCI = row[colIds[`${carrier}_ServPCI`]];
                    SSB = row[colIds[`${carrier}_ServSSB`]];
                }

                if (PCI && SSB && checkNumericVal(val, key)) {
                    if (key.match(/RBAvg/)) {
                        sumRB += parseFloat(val);
                    }
                    if (cdf.allPci.allBeam[key] === undefined) cdf.allPci.allBeam[key] = [];
                    cdf.allPci.allBeam[key].push(val);

                    if (cdf[PCI] === undefined) cdf[PCI] = { allBeam: {} };
                    if (cdf[PCI].allBeam[key] === undefined) cdf[PCI].allBeam[key] = [];
                    cdf[PCI].allBeam[key].push(val);
                    if (!neighbor) {
                        if (cdf[PCI][SSB] === undefined) cdf[PCI][SSB] = {};
                        if (cdf[PCI][SSB][key] === undefined) cdf[PCI][SSB][key] = [];
                        cdf[PCI][SSB][key].push(val);
                    } else {
                        let PCell_PCI = `P${row[colIds[`PCell_ServPCI`]]}`;
                        let PCell_SSB = `${row[colIds[`PCell_ServSSB`]]}`;

                        if (cdf[PCell_PCI] === undefined) cdf[PCell_PCI] = { allBeam: {} };
                        if (cdf[PCell_PCI].allBeam[key] === undefined) cdf[PCell_PCI].allBeam[key] = [];
                        cdf[PCell_PCI].allBeam[key].push(val);
                        if (cdf[PCell_PCI][PCell_SSB] === undefined) cdf[PCell_PCI][PCell_SSB] = {};
                        if (cdf[PCell_PCI][PCell_SSB][key] === undefined) cdf[PCell_PCI][PCell_SSB][key] = [];
                        cdf[PCell_PCI][PCell_SSB][key].push(val);
                    }

                    // currCdf
                    if (currCdf.allPci.allBeam[key] === undefined) currCdf.allPci.allBeam[key] = [];
                    currCdf.allPci.allBeam[key].push(val);

                    if (currCdf[PCI] === undefined) currCdf[PCI] = { allBeam: {} };
                    if (currCdf[PCI].allBeam[key] === undefined) currCdf[PCI].allBeam[key] = [];
                    currCdf[PCI].allBeam[key].push(val);
                    if (!neighbor) {
                        if (currCdf[PCI][SSB] === undefined) currCdf[PCI][SSB] = {};
                        if (currCdf[PCI][SSB][key] === undefined) currCdf[PCI][SSB][key] = [];
                        currCdf[PCI][SSB][key].push(val);
                    } else {
                        let PCell_PCI = `P${row[colIds[`PCell_ServPCI`]]}`;
                        let PCell_SSB = `${row[colIds[`PCell_ServSSB`]]}`;

                        if (currCdf[PCell_PCI] === undefined) currCdf[PCell_PCI] = { allBeam: {} };
                        if (currCdf[PCell_PCI].allBeam[key] === undefined) currCdf[PCell_PCI].allBeam[key] = [];
                        currCdf[PCell_PCI].allBeam[key].push(val);
                        if (currCdf[PCell_PCI][PCell_SSB] === undefined) currCdf[PCell_PCI][PCell_SSB] = {};
                        if (currCdf[PCell_PCI][PCell_SSB][key] === undefined) currCdf[PCell_PCI][PCell_SSB][key] = [];
                        currCdf[PCell_PCI][PCell_SSB][key].push(val);
                    }

                    // PerCarrier key
                    if (carrier !== "Total" && key.match(/ServRSR[PQ]|ServSINR|Tput|DLBLER|RBAvg/)) {
                        let perCarrierKey = `PerCarrier_${splitKey[1]}`
                        if (cdf[PCI] === undefined) cdf[PCI] = { allBeam: {} };
                        if (cdf[PCI].allBeam[perCarrierKey] === undefined) cdf[PCI].allBeam[perCarrierKey] = [];
                        cdf[PCI].allBeam[perCarrierKey].push(val);

                        if (cdf[PCI][SSB] === undefined) cdf[PCI][SSB] = {};
                        if (cdf[PCI][SSB][perCarrierKey] === undefined) cdf[PCI][SSB][perCarrierKey] = [];
                        cdf[PCI][SSB][perCarrierKey].push(val);

                        // currCdf
                        if (currCdf[PCI] === undefined) currCdf[PCI] = { allBeam: {} };
                        if (currCdf[PCI].allBeam[perCarrierKey] === undefined) currCdf[PCI].allBeam[perCarrierKey] = [];
                        currCdf[PCI].allBeam[perCarrierKey].push(val);

                        if (currCdf[PCI][SSB] === undefined) currCdf[PCI][SSB] = {};
                        if (currCdf[PCI][SSB][perCarrierKey] === undefined) currCdf[PCI][SSB][perCarrierKey] = [];
                        currCdf[PCI][SSB][perCarrierKey].push(val);
                    }

                    // For special PCI combination
                    for (let spFilename in specialPCI) {
                        if (filename.startsWith(spFilename)) {
                            for (let pciList in specialPCI[spFilename]) {
                                if (specialPCI[spFilename][pciList].includes(parseInt(PCI))) {
                                    if (cdf[pciList] === undefined) cdf[pciList] = { allBeam: {} };
                                    if (cdf[pciList].allBeam[key] === undefined) cdf[pciList].allBeam[key] = [];
                                    cdf[pciList].allBeam[key].push(val);

                                    // currCdf
                                    if (currCdf[pciList] === undefined) currCdf[pciList] = { allBeam: {} };
                                    if (currCdf[pciList].allBeam[key] === undefined) currCdf[pciList].allBeam[key] = [];
                                    currCdf[pciList].allBeam[key].push(val);
                                }
                            }
                        }
                    }
                }
            } else if (key.match(/ServPCI|ServFreq|ServRI|ServBW|ServBand/)) {
                let val = row[colIds[key]];
                if (val) {
                    if (hist[key] === undefined) hist[key] = {};
                    if (hist[key][val] === undefined) hist[key][val] = 0;
                    ++hist[key][val];

                    if (currHist[key] === undefined) currHist[key] = {};
                    if (currHist[key][val] === undefined) currHist[key][val] = 0;
                    ++currHist[key][val];
                }
            } else if (key.match(/ServSSB|RXBeam/)) {
                let carrier = key.split("_")[0];
                let PCI = row[colIds[`${carrier}_ServPCI`]];
                let val = row[colIds[key]];
                if (PCI && val) {
                    if (hist[key] === undefined) hist[key] = {};
                    if (hist[key][PCI] === undefined) hist[key][PCI] = {};
                    if (hist[key][PCI][val] === undefined) hist[key][PCI][val] = 0;
                    ++hist[key][PCI][val];

                    if (currHist[key] === undefined) currHist[key] = {};
                    if (currHist[key][PCI] === undefined) currHist[key][PCI] = {};
                    if (currHist[key][PCI][val] === undefined) currHist[key][PCI][val] = 0;
                    ++currHist[key][PCI][val];
                }
            } else if (key.match(/DLMod/)) {
                let splitKey = key.split("_");
                let carrier = splitKey[0];
                let PCI = row[colIds[`${carrier}_ServPCI`]];
                let SSB = row[colIds[`${carrier}_ServSSB`]];
                let val = row[colIds[key]];
                if (PCI && SSB && val) {
                    if (hist[key] === undefined) hist[key] = {};
                    if (hist[key].allPci === undefined) hist[key].allPci = { allBeam: {} };
                    if (hist[key].allPci.allBeam[val] === undefined) hist[key].allPci.allBeam[val] = 0;
                    ++hist[key].allPci.allBeam[val];

                    if (hist[key][PCI] === undefined) hist[key][PCI] = { allBeam: {} };
                    if (hist[key][PCI].allBeam[val] === undefined) hist[key][PCI].allBeam[val] = 0;
                    ++hist[key][PCI].allBeam[val];

                    if (hist[key][PCI][SSB] === undefined) hist[key][PCI][SSB] = {};
                    if (hist[key][PCI][SSB][val] === undefined) hist[key][PCI][SSB][val] = 0;
                    ++hist[key][PCI][SSB][val];

                    // Per carrier key
                    let perCarrierKey = `PerCarrier_${splitKey[1]}`;
                    if (hist[perCarrierKey] === undefined) hist[perCarrierKey] = {};
                    if (hist[perCarrierKey].allPci === undefined) hist[perCarrierKey].allPci = { allBeam: {} };
                    if (hist[perCarrierKey].allPci.allBeam[val] === undefined) hist[perCarrierKey].allPci.allBeam[val] = 0;
                    ++hist[perCarrierKey].allPci.allBeam[val];

                    if (hist[perCarrierKey][PCI] === undefined) hist[perCarrierKey][PCI] = { allBeam: {} };
                    if (hist[perCarrierKey][PCI].allBeam[val] === undefined) hist[perCarrierKey][PCI].allBeam[val] = 0;
                    ++hist[perCarrierKey][PCI].allBeam[val];

                    if (hist[perCarrierKey][PCI][SSB] === undefined) hist[perCarrierKey][PCI][SSB] = {};
                    if (hist[perCarrierKey][PCI][SSB][val] === undefined) hist[perCarrierKey][PCI][SSB][val] = 0;
                    ++hist[perCarrierKey][PCI][SSB][val];

                    // CurrHist
                    if (currHist[key] === undefined) currHist[key] = {};
                    if (currHist[key].allPci === undefined) currHist[key].allPci = { allBeam: {} };
                    if (currHist[key].allPci.allBeam[val] === undefined) currHist[key].allPci.allBeam[val] = 0;
                    ++currHist[key].allPci.allBeam[val];

                    if (currHist[key][PCI] === undefined) currHist[key][PCI] = { allBeam: {} };
                    if (currHist[key][PCI].allBeam[val] === undefined) currHist[key][PCI].allBeam[val] = 0;
                    ++currHist[key][PCI].allBeam[val];

                    if (currHist[key][PCI][SSB] === undefined) currHist[key][PCI][SSB] = {};
                    if (currHist[key][PCI][SSB][val] === undefined) currHist[key][PCI][SSB][val] = 0;
                    ++currHist[key][PCI][SSB][val];

                    // Per carrier key
                    perCarrierKey = `PerCarrier_${splitKey[1]}`;
                    if (currHist[perCarrierKey] === undefined) currHist[perCarrierKey] = {};
                    if (currHist[perCarrierKey].allPci === undefined) currHist[perCarrierKey].allPci = { allBeam: {} };
                    if (currHist[perCarrierKey].allPci.allBeam[val] === undefined) currHist[perCarrierKey].allPci.allBeam[val] = 0;
                    ++currHist[perCarrierKey].allPci.allBeam[val];

                    if (currHist[perCarrierKey][PCI] === undefined) currHist[perCarrierKey][PCI] = { allBeam: {} };
                    if (currHist[perCarrierKey][PCI].allBeam[val] === undefined) currHist[perCarrierKey][PCI].allBeam[val] = 0;
                    ++currHist[perCarrierKey][PCI].allBeam[val];

                    if (currHist[perCarrierKey][PCI][SSB] === undefined) currHist[perCarrierKey][PCI][SSB] = {};
                    if (currHist[perCarrierKey][PCI][SSB][val] === undefined) currHist[perCarrierKey][PCI][SSB][val] = 0;
                    ++currHist[perCarrierKey][PCI][SSB][val];
                }
            }
        }

        if (sumRB > 0 && timespan >= 0 && timespan <= 1000) {
            let key = "Total_SumRB";
            let PCI = prevPCell_ServPCI;
            if (cdf.allPci.allBeam[key] === undefined) cdf.allPci.allBeam[key] = [];
            cdf.allPci.allBeam[key].push(sumRB);

            if (cdf[PCI] === undefined) cdf[PCI] = { allBeam: {} };
            if (cdf[PCI].allBeam[key] === undefined) cdf[PCI].allBeam[key] = [];
            cdf[PCI].allBeam[key].push(sumRB);

            // currCdf
            if (currCdf.allPci.allBeam[key] === undefined) currCdf.allPci.allBeam[key] = [];
            currCdf.allPci.allBeam[key].push(sumRB);

            if (currCdf[PCI] === undefined) currCdf[PCI] = { allBeam: {} };
            if (currCdf[PCI].allBeam[key] === undefined) currCdf[PCI].allBeam[key] = [];
            currCdf[PCI].allBeam[key].push(sumRB);

            // specialPCI
            for (let spFilename in specialPCI) {
                if (filename.startsWith(spFilename)) {
                    for (let pciList in specialPCI[spFilename]) {
                        if (specialPCI[spFilename][pciList].includes(parseInt(PCI))) {
                            if (cdf[pciList] === undefined) cdf[pciList] = { allBeam: {} };
                            if (cdf[pciList].allBeam[key] === undefined) cdf[pciList].allBeam[key] = [];
                            cdf[pciList].allBeam[key].push(sumRB);

                            // currCdf
                            if (currCdf[pciList] === undefined) currCdf[pciList] = { allBeam: {} };
                            if (currCdf[pciList].allBeam[key] === undefined) currCdf[pciList].allBeam[key] = [];
                            currCdf[pciList].allBeam[key].push(sumRB);
                        }
                    }
                }
            }
        }
    }
    // Save the last span
    spanSeparatedCdf.push({ startTime: startTime, endTime: prevDatetime, cdf: currCdf, hist: currHist, traffic: currTraffic.join(",") });

    console.log(`Writing output files...`);
    // let promises = [];

    // for (let pci in cdf) {
    //     for (let beam in cdf[pci]) {
    //         for (let key in cdf[pci][beam]) {
    //             cdf[pci][beam][key].sort(dataUtils.sortNumAsc);

    //             let outputFile = path.join(allDir, `${prefix}${prefix ? "-" : ""}cdf-${key}-${pci}-${beam}.dat`);
    //             let string = `"Cumulative distributive function" "${pci}-${beam}"\n`;

    //             if (cdf[pci][beam][key].length === 1) cdf[pci][beam][key].push(cdf[pci][beam][key][0]);
    //             for(let i = 0, length1 = cdf[pci][beam][key].length; i < length1; i++){
    //                 string += `${i / (length1 - 1)} ${cdf[pci][beam][key][i]}\n`;
    //             }

    //             fs.writeFileSync(outputFile, string);
    //         }
    //     }
    // }
    cdf = {};

    let spanIdx = 0;
    let cdfTraffic = {};
    let histTraffic = {};
    for (let spanCdf of spanSeparatedCdf) {
        let spanDir = path.join(currDir, `span${++spanIdx}-${spanCdf.traffic}-${formatDatetime(spanCdf.startTime, spanCdf.endTime)}`);
        if (!fs.existsSync(spanDir)) fs.mkdirSync(spanDir);
        if (cdfTraffic[spanCdf.traffic] === undefined) cdfTraffic[spanCdf.traffic] = {};

        for (let pci in spanCdf.cdf) {
            if (cdfTraffic[spanCdf.traffic][pci] === undefined) cdfTraffic[spanCdf.traffic][pci] = {};
            for (let beam in spanCdf.cdf[pci]) {
                if (cdfTraffic[spanCdf.traffic][pci][beam] === undefined) cdfTraffic[spanCdf.traffic][pci][beam] = {};
                for (let key in spanCdf.cdf[pci][beam]) {
                    if (cdfTraffic[spanCdf.traffic][pci][beam][key] === undefined) cdfTraffic[spanCdf.traffic][pci][beam][key] = [];
                    cdfTraffic[spanCdf.traffic][pci][beam][key] = cdfTraffic[spanCdf.traffic][pci][beam][key].concat(spanCdf.cdf[pci][beam][key]);
                    spanCdf.cdf[pci][beam][key].sort(dataUtils.sortNumAsc);

                    let outputFile = path.join(spanDir, `${prefix}${prefix ? "-" : ""}cdf-${key}-${pci}-${beam}.dat`);
                    let string = `"Cumulative distributive function" "${pci}-${beam}"\n`;

                    if (spanCdf.cdf[pci][beam][key].length === 1) spanCdf.cdf[pci][beam][key].push(spanCdf.cdf[pci][beam][key][0]);
                    for(let i = 0, length1 = spanCdf.cdf[pci][beam][key].length; i < length1; i++){
                        string += `${i / (length1 - 1)} ${spanCdf.cdf[pci][beam][key][i]}\n`;
                    }

                    fs.writeFileSync(outputFile, string);
                }
            }
        }

        if (histTraffic[spanCdf.traffic] === undefined) histTraffic[spanCdf.traffic] = {};
        for (let key in spanCdf.hist) {
            if (histTraffic[spanCdf.traffic][key] === undefined) histTraffic[spanCdf.traffic][key] = {};
            let outputFile = path.join(spanDir, `${prefix}${prefix ? "-" : ""}hist-${key}.dat`);
            let string = "";
            if (key.match(/ServSSB|RXBeam/)) {
                string += `"PCI" "${key}" "Count"\n`;
                for (let pci in spanCdf.hist[key]) {
                    if (histTraffic[spanCdf.traffic][key][pci] === undefined) histTraffic[spanCdf.traffic][key][pci] = {};
                    for (let val in spanCdf.hist[key][pci]) {
                        if (histTraffic[spanCdf.traffic][key][pci][val] === undefined) histTraffic[spanCdf.traffic][key][pci][val] = 0;
                        histTraffic[spanCdf.traffic][key][pci][val] += spanCdf.hist[key][pci][val];
                        string += `${pci} ${val} ${spanCdf.hist[key][pci][val]}\n`;
                    }
                }
            } else if (key.match(/DLMod/)) {
                string += `"PCI" "SSB Idx" "Modulation" "Count"\n`;
                for (let pci in spanCdf.hist[key]) {
                    if (histTraffic[spanCdf.traffic][key][pci] === undefined) histTraffic[spanCdf.traffic][key][pci] = {};
                    for (let ssb in spanCdf.hist[key][pci]) {
                        if (histTraffic[spanCdf.traffic][key][pci][ssb] === undefined) histTraffic[spanCdf.traffic][key][pci][ssb] = {};
                        for (let val in spanCdf.hist[key][pci][ssb]) {
                            if (histTraffic[spanCdf.traffic][key][pci][ssb][val] === undefined) histTraffic[spanCdf.traffic][key][pci][ssb][val] = 0;
                            histTraffic[spanCdf.traffic][key][pci][ssb][val] += spanCdf.hist[key][pci][ssb][val];
                            string += `${pci} ${ssb} "${val}" ${spanCdf.hist[key][pci][ssb][val]}\n`;
                        }
                    }
                }
            } else {
                string += `"${key}" "Count"\n`;
                for (let val in spanCdf.hist[key]) {
                    if (histTraffic[spanCdf.traffic][key][val] === undefined) histTraffic[spanCdf.traffic][key][val] = 0;
                    histTraffic[spanCdf.traffic][key][val] += spanCdf.hist[key][val];
                    string += `"${val}" ${spanCdf.hist[key][val]}\n`;
                }
            }
            fs.writeFileSync(outputFile, string);
        }
    }
    spanSeparatedCdf = [];

    for (let traffic in cdfTraffic) {
        let spanDir = path.join(currDir, `traffic-${traffic}`);
        if (!fs.existsSync(spanDir)) fs.mkdirSync(spanDir);

        for (let pci in cdfTraffic[traffic]) {
            for (let beam in cdfTraffic[traffic][pci]) {
                for (let key in cdfTraffic[traffic][pci][beam]) {
                    cdfTraffic[traffic][pci][beam][key].sort(dataUtils.sortNumAsc);

                    let outputFile = path.join(spanDir, `${prefix}${prefix ? "-" : ""}cdf-${key}-${pci}-${beam}.dat`);
                    let string = `"Cumulative distributive function" "${pci}-${beam}"\n`;

                    if (cdfTraffic[traffic][pci][beam][key].length === 1) cdfTraffic[traffic][pci][beam][key].push(cdfTraffic[traffic][pci][beam][key][0]);
                    for(let i = 0, length1 = cdfTraffic[traffic][pci][beam][key].length; i < length1; i++){
                        string += `${i / (length1 - 1)} ${cdfTraffic[traffic][pci][beam][key][i]}\n`;
                    }

                    fs.writeFileSync(outputFile, string);
                }
            }
        }
        for (let key in histTraffic[traffic]) {
            let outputFile = path.join(spanDir, `${prefix}${prefix ? "-" : ""}hist-${key}.dat`);
            let string = "";
            if (key.match(/ServSSB|RXBeam/)) {
                string += `"PCI" "${key}" "Count"\n`;
                for (let pci in histTraffic[traffic][key]) {
                    for (let val in histTraffic[traffic][key][pci]) {
                        string += `${pci} ${val} ${histTraffic[traffic][key][pci][val]}\n`;
                    }
                }
            } else if (key.match(/DLMod/)) {
                string += `"PCI" "SSB Idx" "Modulation" "Count"\n`;
                for (let pci in histTraffic[traffic][key]) {
                    for (let ssb in histTraffic[traffic][key][pci]) {
                        for (let val in histTraffic[traffic][key][pci][ssb]) {
                            string += `${pci} ${ssb} "${val}" ${histTraffic[traffic][key][pci][ssb][val]}\n`;
                        }
                    }
                }
            } else {
                string += `"${key}" "Count"\n`;
                for (let val in histTraffic[traffic][key]) {
                    string += `"${val}" ${histTraffic[traffic][key][val]}\n`;
                }
            }
            fs.writeFileSync(outputFile, string);
        }
    }
    cdfTraffic = {};

    // for (let key in hist) {
    //     let outputFile = path.join(allDir, `${prefix}${prefix ? "-" : ""}hist-${key}.dat`);
    //     let string = "";
    //     if (key.match(/ServSSB|RXBeam/)) {
    //         string += `"PCI" "${key}" "Count"\n`;
    //         for (let pci in hist[key]) {
    //             for (let val in hist[key][pci]) {
    //                 string += `${pci} ${val} ${hist[key][pci][val]}\n`;
    //             }
    //         }
    //     } else if (key.match(/DLMod/)) {
    //         string += `"PCI" "SSB Idx" "Modulation" "Count"\n`;
    //         for (let pci in hist[key]) {
    //             for (let ssb in hist[key][pci]) {
    //                 for (let val in hist[key][pci][ssb]) {
    //                     string += `${pci} ${ssb} "${val}" ${hist[key][pci][ssb][val]}\n`;
    //                 }
    //             }
    //         }
    //     } else {
    //         string += `"${key}" "Count"\n`;
    //         for (let val in hist[key]) {
    //             string += `"${val}" ${hist[key][val]}\n`;
    //         }
    //     }
    //     fs.writeFileSync(outputFile, string);
    // }

    // // Wait for all finished
    // const waitPromise = await Promise.allSettled(promises).then(data => {
    //     // Do nothing since we don't care about the results
    // }).catch(err => {
    //     console.log(`Error writing for ${filename}`, err);
    // }).finally(() => {
    //     console.log(`Finished for ${filename}!`);
    // });

});
