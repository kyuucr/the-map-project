const agg = require('../lib/aggregator');
const powerUtils = require('../lib/power-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <sigcap path> [sigcap path 2] ...`);
    process.exit(0);
}

let avgReduce = (avg, value, _, { length }) => avg + value / length;

let rssiList = {};

ARGS.forEach(dir => {
    agg.callbackJsonRecursive(dir, data => {
        data.json.forEach(sigcap => {
            if (rssiList[sigcap.uuid] === undefined) rssiList[sigcap.uuid] = {};
            sigcap.cell_info.forEach(cell => {
                if (cell.band === 46) {
                    if (rssiList[sigcap.uuid][cell.channelNum] === undefined) rssiList[sigcap.uuid][cell.channelNum] = [];
                    rssiList[sigcap.uuid][cell.channelNum].push(powerUtils.dbmToW(cell.rssi));
                }
            });
        });
    });
});

for (let uuid in rssiList) {
    for (let chNum in rssiList[uuid]) {
        console.log(`${uuid}\t${chNum}\t${powerUtils.wToDbm(rssiList[uuid][chNum].reduce(avgReduce, null, 0))}`)
    }
}
