const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const filter = require('../lib/filter');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> <output folder> [--label-location] [filters ...]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputFolder = ARGS[1];
let jsonFilter = [];
let isLocationLabel = false;
if (ARGS.length > 2) {
    for(let i = 2, length1 = ARGS.length; i < length1; i++){
        if (ARGS[i] === "--label-location") {
            isLocationLabel = true;
        } else {
            // Try to parse to JSON
            try {
                let json = JSON.parse(ARGS[i]);
                jsonFilter.push(json);
            } catch (e) {
                console.log("Filter not recognized:", ARGS[i]);
            }
        }
    }
}

let files = agg.getJsonRecursive(inputFolder);

for(let i = 0, length1 = files.length; i < length1; i++){
    let firstData = true;
    let curPath = files[i].path;
    fs.mkdirSync(outputFolder, { recursive: true }, (err) => {
        console.log("ERROR: cannot make output dir");
    });
    let os = fs.createWriteStream(path.join(outputFolder, `${curPath.replace(/\/|\\/g,"-")}.csv`));
    let currLocationLabel = ""
    if (isLocationLabel) {
        if (curPath.includes("indoor")) {
            currLocationLabel = "indoor";
        } else if (curPath.includes("outdoor")) {
            currLocationLabel = "outdoor"
        } else {
            currLocationLabel = "undefined"
        }
    }

    let currJson = files[i].json;
    if (jsonFilter.length > 0) {
        currJson = filter.filterArray(jsonFilter, currJson);
    }

    console.log("Path:", curPath);
    if (isLocationLabel) console.log("Location label:", currLocationLabel)
    console.log("Number of data:", currJson.length);

    for(let j = 0, length2 = currJson.length; j < length2; j++){
        for(let k = 0, length3 = currJson[j].cell_info.length; k < length3; k++){
            if (firstData) {
                firstData = false;
                os.write(`timestamp,${csvUtils.unpackKeys(currJson[j].location)}${isLocationLabel ? ("location_category,") : ""}${csvUtils.unpackKeys(currJson[j].cell_info[k])}\n`);
            }
            os.write(`${currJson[j].datetimeIso},${csvUtils.unpackVals(currJson[j].location)}${isLocationLabel ? (currLocationLabel + ",") : ""}${csvUtils.unpackVals(currJson[j].cell_info[k])}\n`);
        }
    }
    os.close();
}