const aggregator = require('../lib/aggregator');
const commandParser = require('../lib/command-parser');
const dataUtils = require('../lib/data-utils');
const cellHelper = require('../lib/cell-helper');
const logger = require('../lib/logger');

// Process arguments
commandParser.addArgument("input files", { isMandatory: true });
// commandParser.addArgument("output metadata file", { isMandatory: true });
// commandParser.addArgument("duration", { keyword: "-d", defaultValue: 299000 });
const options = commandParser.parse();
const lteBands = {};

aggregator.callbackJsonRecursive(options["input files"], (data) => {
    if (data.type !== 'sigcap') {
        return;
    }

    const sigcap = data.json;
    for (const entry of sigcap) {
        const operator = dataUtils.getCleanOp(entry);
        const deviceName = entry.deviceName;
        if (lteBands[operator] === undefined) {
            lteBands[operator] = {};
        }
        if (lteBands[operator][deviceName] === undefined) {
            lteBands[operator][deviceName] = { "All": 0, "N/A": 0 };
        }

        if (entry.cell_info && entry.cell_info.length > 0) {
            for (const lteEntry of entry.cell_info) {
                const band = cellHelper.earfcnToBand(lteEntry.earfcn);
                if (lteBands[operator][deviceName][band] === undefined) {
                    lteBands[operator][deviceName][band] = 1;
                } else {
                    lteBands[operator][deviceName][band]++;
                }
                lteBands[operator][deviceName]["All"]++;
            }
        }
    }
});

logger.i(["Device Model", "Operator", "LTE Band", "Count", "Percentage"].join(';\t'));
for (const operator in lteBands) {
    for (const deviceName in lteBands[operator]) {
        for (const band in lteBands[operator][deviceName]) {
            if (band === "All") {
                continue;
            }
            logger.i([deviceName, operator, band, lteBands[operator][deviceName][band], (lteBands[operator][deviceName][band] / lteBands[operator][deviceName]["All"] * 100).toFixed(2) + '%'].join(';\t'));
        }
    }
}