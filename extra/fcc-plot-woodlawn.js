const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const filter = require('../lib/filter');
const fs = require('fs');
const path = require('path');
const plotlib = require('nodeplotlib');

const TEST_LIST = [ "download", "upload", "latency" ];
let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> [--filter <filter JSON string>]`);
    process.exit(1);
}

let inputFolder = args[0];
args.splice(0, 1);
let inputFilter;
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}


let getRsrp = function(telephony) {
    if (telephony && telephony.cell_signal && telephony.cell_signal.network_type) {
        if (telephony.cell_signal.network_type === "LTE") {
            return getKeyValue(telephony.cell_signal, "received_signal_power");
        } else if (telephony.cell_signal.network_type === "NR" || telephony.cell_signal.network_type === "NRNSA") {
            return getKeyValue(telephony.cell_signal, "received_signal_power_ss");
        } else {
            return getKeyValue(telephony.cell_signal, "signal_strength");
        }
    }
    return "N/A";
}

let getRsrq = function(telephony) {
    if (telephony && telephony.cell_signal && telephony.cell_signal.network_type) {
        if (telephony.cell_signal.network_type === "LTE") {
            return getKeyValue(telephony.cell_signal, "received_signal_quality");
        } else if (telephony.cell_signal.network_type === "NR" || telephony.cell_signal.network_type === "NRNSA") {
            return getKeyValue(telephony.cell_signal, "received_signal_quality_ss");
        }
    }
    return "N/A";
}

let getKeyValue = function(obj, key) {
    if (obj && obj[key]) return obj[key];
    return "N/A";
}

let getCellSignalKey = function(telephony, key) {
    if (telephony) return getKeyValue(telephony.cell_signal, key);
    return "N/A";
}

let isLaa = function(earfcn) {
    return earfcn >= 46790 && earfcn <= 54539;
}

let isTelephonyExists = function(recs) {
    let exists = true;
    for (let testName of TEST_LIST) {
        if (recs.tests[testName] === undefined) continue;
        for (let cond in recs.tests[testName].environment) {
            exists = exists && recs.tests[testName].environment[cond].telephony && recs.tests[testName].environment[cond].telephony.cell_signal;
        }
    }
    return exists;
}

let isNetworkExists = function(recs) {
    let exists = true;
    for (let testName of TEST_LIST) {
        if (recs.tests[testName] === undefined) continue;
        for (let cond in recs.tests[testName].environment) {
            exists = exists && recs.tests[testName].environment[cond].network;
        }
    }
    return exists;
}

let getConn = function(telephony, wifi) {
    if (telephony && telephony.connection_type) {
        if (telephony.connection_type === "cell") {
            if (telephony.cellular_technology
                && telephony.cellular_technology === "4g"
                && telephony.cell_signal
                && isLaa(telephony.cell_signal.absolute_rf_channel_number)) {
                return "4g-laa";
            }
            return telephony.cellular_technology;
        }
        return telephony.connection_type;
    } else if (wifi) {
        return "wi-fi";
    }
    return "N/A";
}

let getTarget = function(tests) {
    if (tests.download && tests.download.target) return tests.download.target;
    if (tests.upload && tests.upload.target) return tests.upload.target;
    if (tests.latency && tests.latency.target) return tests.latency.target;
    return "N/A";
}

let normalizeFreq = function(freq) {
    if (freq >= 2400 && freq < 2500) return "2.4 GHz";
    if (freq > 5000 && freq < 5900) return "5 GHz";
    if (freq > 5900 && freq < 7125) return "6 GHz";
    return freq;
}

let nullNetwork = {
    "is_available": "N/A",
    "is_connected": "N/A",
    "is_connected_or_connecting": "N/A",
    "is_failover": "N/A",
    "is_roaming": "N/A",
    "state": "N/A",
    "subtype": "N/A",
    "subtype_name": "N/A",
    "type": "N/A"
};
let nullWifi = {
    "frequency": "N/A",
    "hidden_ssid": "N/A",
    "link_speed": "N/A",
    "network_id": "N/A",
    "rssi": "N/A",
    "supplicant_state": "N/A"
};

let testIdList = [];
let values = {};

let printZipfilename = false;
agg.callbackJsonRecursive(inputFolder, data => {
    let recs = data.json
    if (inputFilter) {
        recs = filter.filterArray(inputFilter, recs);
    }
    let pathDir = path.basename(data.path);
    if (!pathDir.startsWith("fcc-exp")) return;
    console.log(`path: ${data.path}, files: ${data.filePath.map(data => path.basename(data)).join(",")}, # of data: ${recs.length}`);

    for (let i = 0, length1 = recs.length; i < length1; i++) {
        if (recs[i].tests.testId === undefined || testIdList.includes(recs[i].tests.testId)) continue;
        testIdList.push(recs[i].tests.testId);

        let op = dataUtils.getCleanOp(recs[i]);
        let zipFilename = recs[i].zipFilename;
        for (let testName in recs[i].tests) {
            let currValue = (recs[i].tests.download.throughput * 8 / 1e6);
            if (testName === "download" || testName === "upload") {
                currValue = (recs[i].tests[testName].throughput * 8 / 1e6);
            } else if (testName === "latency") {
                currValue = (recs[i].tests[testName].round_trip_time / 1e3);
            } else {
                continue;
            }
            let networkBegin, networkEnd;
            if (op === "WiFi") {
                networkBegin = dataUtils.getCleanValue(recs[i].tests[testName].environment.beginning, "wifi.frequency");
                networkBegin = normalizeFreq(networkBegin);
                networkEnd = dataUtils.getCleanValue(recs[i].tests[testName].environment.end, "wifi.frequency");
                networkEnd = normalizeFreq(networkEnd);
            } else {
                networkBegin = dataUtils.getCleanValue(recs[i].tests[testName].environment.beginning, "network.subtype_name");
                networkEnd = dataUtils.getCleanValue(recs[i].tests[testName].environment.end, "network.subtype_name");
            }
            if (currValue > 0 && networkBegin === networkEnd) {
                if (values[pathDir] === undefined) values[pathDir] = {};
                if (values[pathDir][zipFilename] === undefined) values[pathDir][zipFilename] = { download: {}, upload: {}, latency: {} };
                if (values[pathDir][zipFilename][testName][op] === undefined) values[pathDir][zipFilename][testName][op] = {};
                if (values[pathDir][zipFilename][testName][op][networkBegin] === undefined) values[pathDir][zipFilename][testName][op][networkBegin] = [];
                values[pathDir][zipFilename][testName][op][networkBegin].push(currValue);
                if (op === "WiFi") {
                    if (values[pathDir][zipFilename][testName][op].all === undefined) values[pathDir][zipFilename][testName][op].all = [];
                    values[pathDir][zipFilename][testName][op].all.push(currValue);
                }
            }
        }
    }
});

(function() {
    const scenarios = {
        "los-close": "A",
        "los-mid": "B",
        "los-far": "C",
        "nlos-far": "D",
        "nlos-door": "E"
    };
    const constOp = { "Verizon": [ "NRNSA" ] };

    // Experiment 1-5
    for (let i = 1; i <= 5; i++) {
        let dlFig = [];
        let dlLayout = {
            xaxis: {
                showgrid: true,
                title: 'DL Throughput (Mbps)',
                range: [ 0, 2000 ]
            },
            yaxis: {
                showgrid: true,
                title: 'Cumulative distribution function'
            },
            title: `Room e${i+1}06`,
            showlegend: true
        };
        for (let scenario in scenarios) {
            let dirname = `fcc-exp${i}-room-e${1+i}06-mmwave-${scenario}`;
            if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);


            for (let zipFilename in values[dirname]) {
                if (!zipFilename.includes("1eb4856b")) continue;
                for (let op in constOp) {
                    if (values[dirname][zipFilename].download[op] === undefined) {
                        for (let network of constOp[op]) {
                            dlFig.push({ x: [ 0 ], y: [ 0 ], name: `${scenarios[scenario]}`, type: "scatter" });
                        }
                    } else {
                        for (let network of constOp[op]) {
                            if (values[dirname][zipFilename].download[op][network] === undefined) {
                                dlFig.push({ x: [ 0 ], y: [ 0 ], name: `${scenarios[scenario]}`, type: "scatter" });
                            } else {
                                if (values[dirname][zipFilename].download[op][network].length > 1) {
                                    values[dirname][zipFilename].download[op][network].sort(dataUtils.sortNumAsc);
                                } else {
                                    values[dirname][zipFilename].download[op][network].push(values[dirname][zipFilename].download[op][network][0]);
                                }
                                dlFig.push({ x: values[dirname][zipFilename].download[op][network], y: dataUtils.createCdfY(values[dirname][zipFilename].download[op][network].length), name: `${scenarios[scenario]}`, type: "scatter" });
                            }
                        }
                    }
                }
            }
        }
        plotlib.stack(dlFig, dlLayout);
    }

    for (let i = 1; i <= 5; i++) {
        let ulFig = [];
        let ulLayout = {
            xaxis: {
                showgrid: true,
                title: 'UL Throughput (Mbps)',
                range: [0, 100]
            },
            yaxis: {
                showgrid: true,
                title: 'Cumulative distribution function'
            },
            title: `Room e${i+1}06`,
            showlegend: true
        };
        for (let scenario in scenarios) {
            let dirname = `fcc-exp${i}-room-e${1+i}06-mmwave-${scenario}`;
            if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);

            for (let zipFilename in values[dirname]) {
                if (!zipFilename.includes("1eb4856b")) continue;
                for (let op in constOp) {
                    if (values[dirname][zipFilename].upload[op] === undefined) {
                        for (let network of constOp[op]) {
                            ulFig.push({ x: [ 0 ], y: [ 0 ], name: `${scenarios[scenario]}`, type: "scatter" });
                        }
                    } else {
                        for (let network of constOp[op]) {
                            if (values[dirname][zipFilename].upload[op][network] === undefined) {
                                ulFig.push({ x: [ 0 ], y: [ 0 ], name: `${scenarios[scenario]}`, type: "scatter" });
                            } else {
                                if (values[dirname][zipFilename].upload[op][network].length > 1) {
                                    values[dirname][zipFilename].upload[op][network].sort(dataUtils.sortNumAsc);
                                } else {
                                    values[dirname][zipFilename].upload[op][network].push(values[dirname][zipFilename].upload[op][network][0]);
                                }
                                ulFig.push({ x: values[dirname][zipFilename].upload[op][network], y: dataUtils.createCdfY(values[dirname][zipFilename].upload[op][network].length), name: `${scenarios[scenario]}`, type: "scatter" });
                            }
                        }
                    }
                }
            }
        }
        plotlib.stack(ulFig, ulLayout);
    }

    for (let i = 1; i <= 5; i++) {
        let latFig = [];
        let latLayout = {
            xaxis: {
                showgrid: true,
                title: 'Latency (millisec)',
                range: [0, 65]
            },
            yaxis: {
                showgrid: true,
                title: 'Cumulative distribution function'
            },
            title: `Room e${i+1}06`,
            showlegend: true
        };
        for (let scenario in scenarios) {
            let dirname = `fcc-exp${i}-room-e${1+i}06-mmwave-${scenario}`;
            if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);

            for (let zipFilename in values[dirname]) {
                if (!zipFilename.includes("1eb4856b")) continue;
                for (let op in constOp) {
                    if (values[dirname][zipFilename].latency[op] === undefined) {
                        for (let network of constOp[op]) {
                            latFig.push({ x: [ 0 ], y: [ 0 ], name: `${scenarios[scenario]}`, type: "scatter" });
                        }
                    } else {
                        for (let network of constOp[op]) {
                            if (values[dirname][zipFilename].latency[op][network] === undefined) {
                                latFig.push({ x: [ 0 ], y: [ 0 ], name: `${scenarios[scenario]}`, type: "scatter" });
                            } else {
                                if (values[dirname][zipFilename].latency[op][network].length > 1) {
                                    values[dirname][zipFilename].latency[op][network].sort(dataUtils.sortNumAsc);
                                } else {
                                    values[dirname][zipFilename].latency[op][network].push(values[dirname][zipFilename].latency[op][network][0]);
                                }
                                latFig.push({ x: values[dirname][zipFilename].latency[op][network], y: dataUtils.createCdfY(values[dirname][zipFilename].latency[op][network].length), name: `${scenarios[scenario]}`, type: "scatter" });
                            }
                        }
                    }
                }
            }
        }
        plotlib.stack(latFig, latLayout);
    }
    plotlib.plot();
})();


// // Experiment 6
// (function() {
//     const constOp = { "Verizon": [ "NRNSA" ] };

//     let dlFig = [];
//     let dlLayout = {
//         xaxis: {
//             showgrid: true,
//             title: 'DL Throughput (Mbps)'
//         },
//         yaxis: {
//             showgrid: true,
//             title: 'Cumulative distribution function'
//         },
//         // title: `Room e${i+1}06`,
//         showlegend: true
//     };
//     for (let i = 1; i <= 4; i++) {
//         let dirname = `fcc-exp6-room-e206-mmwave-los-close-gap-${i}`;
//         if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);

//         for (let op in constOp) {
//             if (values[dirname].download[op] === undefined) {
//                 for (let network of constOp[op]) {
//                     dlFig.push({ x: [ 0 ], y: [ 0 ], name: `Gap ${i}`, type: "scatter" });
//                 }
//             } else {
//                 for (let network of constOp[op]) {
//                     if (values[dirname].download[op][network] === undefined) {
//                         dlFig.push({ x: [ 0 ], y: [ 0 ], name: `Gap ${i}`, type: "scatter" });
//                     } else {
//                         if (values[dirname].download[op][network].length > 1) {
//                             values[dirname].download[op][network].sort(dataUtils.sortNumAsc);
//                         } else {
//                             values[dirname].download[op][network].push(values[dirname].download[op][network][0]);
//                         }
//                         dlFig.push({ x: values[dirname].download[op][network], y: dataUtils.createCdfY(values[dirname].download[op][network].length), name: `Gap ${i}`, type: "scatter" });
//                     }
//                 }
//             }
//         }
//     }
//     plotlib.stack(dlFig, dlLayout);

//     let ulFig = [];
//     let ulLayout = {
//         xaxis: {
//             showgrid: true,
//             title: 'UL Throughput (Mbps)'
//         },
//         yaxis: {
//             showgrid: true,
//             title: 'Cumulative distribution function'
//         },
//         // title: `Room e${i+1}06`,
//         showlegend: true
//     };
//     for (let i = 1; i <= 4; i++) {
//         let dirname = `fcc-exp6-room-e206-mmwave-los-close-gap-${i}`;
//         if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);

//         for (let op in constOp) {
//             if (values[dirname].upload[op] === undefined) {
//                 for (let network of constOp[op]) {
//                     ulFig.push({ x: [ 0 ], y: [ 0 ], name: `Gap ${i}`, type: "scatter" });
//                 }
//             } else {
//                 for (let network of constOp[op]) {
//                     if (values[dirname].upload[op][network] === undefined) {
//                         ulFig.push({ x: [ 0 ], y: [ 0 ], name: `Gap ${i}`, type: "scatter" });
//                     } else {
//                         if (values[dirname].upload[op][network].length > 1) {
//                             values[dirname].upload[op][network].sort(dataUtils.sortNumAsc);
//                         } else {
//                             values[dirname].upload[op][network].push(values[dirname].upload[op][network][0]);
//                         }
//                         ulFig.push({ x: values[dirname].upload[op][network], y: dataUtils.createCdfY(values[dirname].upload[op][network].length), name: `Gap ${i}`, type: "scatter" });
//                     }
//                 }
//             }
//         }
//     }
//     plotlib.stack(ulFig, ulLayout);

//     let latFig = [];
//     let latLayout = {
//         xaxis: {
//             showgrid: true,
//             title: 'Latency (millisec)'
//         },
//         yaxis: {
//             showgrid: true,
//             title: 'Cumulative distribution function'
//         },
//         // title: `Room e${i+1}06`,
//         showlegend: true
//     };
//     for (let i = 1; i <= 4; i++) {
//         let dirname = `fcc-exp6-room-e206-mmwave-los-close-gap-${i}`;
//         if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);

//         for (let op in constOp) {
//             if (values[dirname].latency[op] === undefined) {
//                 for (let network of constOp[op]) {
//                     latFig.push({ x: [ 0 ], y: [ 0 ], name: `Gap ${i}`, type: "scatter" });
//                 }
//             } else {
//                 for (let network of constOp[op]) {
//                     if (values[dirname].latency[op][network] === undefined) {
//                         latFig.push({ x: [ 0 ], y: [ 0 ], name: `Gap ${i}`, type: "scatter" });
//                     } else {
//                         if (values[dirname].latency[op][network].length > 1) {
//                             values[dirname].latency[op][network].sort(dataUtils.sortNumAsc);
//                         } else {
//                             values[dirname].latency[op][network].push(values[dirname].latency[op][network][0]);
//                         }
//                         latFig.push({ x: values[dirname].latency[op][network], y: dataUtils.createCdfY(values[dirname].latency[op][network].length), name: `Gap ${i}`, type: "scatter" });
//                     }
//                 }
//             }
//         }
//     }
//     plotlib.stack(latFig, latLayout);
//     plotlib.plot();
// })();

// Experiment 7
// (function() {
//     const scenarios = {
//         "base-1-client": { 
//             name:"1 Client",
//             users: {
//                 "7106a534": "UE1 P5",
//             }
//         },
//         "base-2-client": { 
//             name:"2 Clients",
//             users: {
//                 "7106a534": "UE1 P5",
//                 "c628d52e": "UE2 P5",
//             }
//         },
//         "base-3-client": { 
//             name:"3 Clients",
//             users: {
//                 "7106a534": "UE1 P5",
//                 "c628d52e": "UE2 P5",
//                 "a9d53ace": "UE3 P3",
//             }
//         },
//         "base-4-client": { 
//             name:"4 Clients",
//             users: {
//                 "7106a534": "UE1 P5",
//                 "c628d52e": "UE2 P5",
//                 "a9d53ace": "UE3 P3",
//                 "a5227324": "UE4 P3"
//             }
//         },
//         "base-4-client-separated": { 
//             name:"4 Clients Separated",
//             users: {
//                 "7106a534": "UE1 P5",
//                 "c628d52e": "UE2 P5",
//                 "a9d53ace": "UE3 P3",
//                 "a5227324": "UE4 P3"
//             }
//         },
//         "roaming": { 
//             name:"1 Client Roaming",
//             users: {
//                 "7106a534": "UE1 P5",
//             }
//         }
//     };
//     // const constOp = { "WiFi": [ "2.4 GHz", "5 GHz" ] };
//     const constOp = { "WiFi": [ "5 GHz" ] };
//     // const constOp = { "WiFi": [ "all" ] };

//     for (let scenario in scenarios) {
//         let dirname = `fcc-exp7-room-e206-mmwave-los-close-hotspot-${scenario}`;
//         if (values[dirname] === undefined) throw new Exception(`${dirname} not found!`);
//         let dlFig = [];
//         let dlLayout = {
//             xaxis: {
//                 showgrid: true,
//                 title: 'DL Throughput (Mbps)',
//                 range: [ 0, 140 ]
//             },
//             yaxis: {
//                 showgrid: true,
//                 title: 'Cumulative distribution function'
//             },
//             title: `${scenarios[scenario].name} DL Tput`,
//             showlegend: true
//         };
//         let ulFig = [];
//         let ulLayout = {
//             xaxis: {
//                 showgrid: true,
//                 title: 'UL Throughput (Mbps)',
//                 range: [ 0, 110 ]
//             },
//             yaxis: {
//                 showgrid: true,
//                 title: 'Cumulative distribution function'
//             },
//             title: `${scenarios[scenario].name} UL Tput`,
//             showlegend: true
//         };
//         let latFig = [];
//         let latLayout = {
//             xaxis: {
//                 showgrid: true,
//                 title: 'Latency (millisec)',
//                 range: [ 0, 700 ]
//             },
//             yaxis: {
//                 showgrid: true,
//                 title: 'Cumulative distribution function'
//             },
//             title: `${scenarios[scenario].name} Latency`,
//             showlegend: true
//         };

//         let users = scenarios[scenario].users;
//         for (let user in users) {
//             for (let zipFilename in values[dirname]) {
//                 if (!zipFilename.includes(user)) continue;
//                 for (let op in constOp) {
//                     if (values[dirname][zipFilename].download[op] === undefined) {
//                         for (let network of constOp[op]) {
//                             dlFig.push({ x: [ 0 ], y: [ 0 ], name: `${users[user]}`, type: "scatter" });
//                         }
//                     } else {
//                         for (let network of constOp[op]) {
//                             if (values[dirname][zipFilename].download[op][network] === undefined) {
//                                 dlFig.push({ x: [ 0 ], y: [ 0 ], name: `${users[user]}`, type: "scatter" });
//                             } else {
//                                 if (values[dirname][zipFilename].download[op][network].length > 1) {
//                                     values[dirname][zipFilename].download[op][network].sort(dataUtils.sortNumAsc);
//                                 } else {
//                                     values[dirname][zipFilename].download[op][network].push(values[dirname][zipFilename].download[op][network][0]);
//                                 }
//                                 dlFig.push({ x: values[dirname][zipFilename].download[op][network], y: dataUtils.createCdfY(values[dirname][zipFilename].download[op][network].length), name: `${users[user]}`, type: "scatter" });
//                             }
//                         }
//                     }

//                     if (values[dirname][zipFilename].upload[op] === undefined) {
//                         for (let network of constOp[op]) {
//                             ulFig.push({ x: [ 0 ], y: [ 0 ], name: `${users[user]}`, type: "scatter" });
//                         }
//                     } else {
//                         for (let network of constOp[op]) {
//                             if (values[dirname][zipFilename].upload[op][network] === undefined) {
//                                 ulFig.push({ x: [ 0 ], y: [ 0 ], name: `${users[user]}`, type: "scatter" });
//                             } else {
//                                 if (values[dirname][zipFilename].upload[op][network].length > 1) {
//                                     values[dirname][zipFilename].upload[op][network].sort(dataUtils.sortNumAsc);
//                                 } else {
//                                     values[dirname][zipFilename].upload[op][network].push(values[dirname][zipFilename].upload[op][network][0]);
//                                 }
//                                 ulFig.push({ x: values[dirname][zipFilename].upload[op][network], y: dataUtils.createCdfY(values[dirname][zipFilename].upload[op][network].length), name: `${users[user]}`, type: "scatter" });
//                             }
//                         }
//                     }

//                     if (values[dirname][zipFilename].latency[op] === undefined) {
//                         for (let network of constOp[op]) {
//                             latFig.push({ x: [ 0 ], y: [ 0 ], name: `${users[user]}`, type: "scatter" });
//                         }
//                     } else {
//                         for (let network of constOp[op]) {
//                             if (values[dirname][zipFilename].latency[op][network] === undefined) {
//                                 latFig.push({ x: [ 0 ], y: [ 0 ], name: `${users[user]}`, type: "scatter" });
//                             } else {
//                                 if (values[dirname][zipFilename].latency[op][network].length > 1) {
//                                     values[dirname][zipFilename].latency[op][network].sort(dataUtils.sortNumAsc);
//                                 } else {
//                                     values[dirname][zipFilename].latency[op][network].push(values[dirname][zipFilename].latency[op][network][0]);
//                                 }
//                                 latFig.push({ x: values[dirname][zipFilename].latency[op][network], y: dataUtils.createCdfY(values[dirname][zipFilename].latency[op][network].length), name: `${users[user]}`, type: "scatter" });
//                             }
//                         }
//                     }
//                 }
//             }

//         }
//         plotlib.stack(dlFig, dlLayout);
//         plotlib.stack(ulFig, ulLayout);
//         plotlib.stack(latFig, latLayout);
//     }
//     plotlib.plot();
// })();
