const aggregator = require('../lib/aggregator');
const commandParser = require('../lib/command-parser');
const dataUtils = require('../lib/data-utils');
const cellHelper = require('../lib/cell-helper');
const logger = require('../lib/logger');

// Process arguments
commandParser.addArgument("input files", { isMandatory: true });
// commandParser.addArgument("output metadata file", { isMandatory: true });
// commandParser.addArgument("duration", { keyword: "-d", defaultValue: 299000 });
const options = commandParser.parse();
const nrBands = {};

aggregator.callbackJsonRecursive(options["input files"], (data) => {
    if (data.type !== 'sigcap') {
        return;
    }

    const sigcap = data.json;
    for (const entry of sigcap) {
        const operator = dataUtils.getCleanOp(entry);
        const deviceName = entry.deviceName;
        if (nrBands[operator] === undefined) {
            nrBands[operator] = {};
        }
        if (nrBands[operator][deviceName] === undefined) {
            nrBands[operator][deviceName] = { "All": 0, "N/A": 0 };
        }

        if (entry.nr_info && entry.nr_info.length > 0) {
            let band = "N/A";
            for (const nrEntry of entry.nr_info) {
                band = cellHelper.nrarfcnToBand(nrEntry.nrarfcn, cellHelper.REGION.NAR, true);
                if (band !== "N/A") {
                    break;
                }
            }
            if (nrBands[operator][deviceName][band] === undefined) {
                nrBands[operator][deviceName][band] = 1;
            } else {
                nrBands[operator][deviceName][band]++;
            }
            nrBands[operator][deviceName]["All"]++;
        }
    }
});

logger.i(["Device Model", "Operator", "NR Band", "Count", "Percentage"].join(';\t'));
for (const operator in nrBands) {
    for (const deviceName in nrBands[operator]) {
        for (const band in nrBands[operator][deviceName]) {
            if (band === "All") {
                continue;
            }
            logger.i([deviceName, operator, band, nrBands[operator][deviceName][band], (nrBands[operator][deviceName][band] / nrBands[operator][deviceName]["All"] * 100).toFixed(2) + '%'].join(';\t'));
        }
    }
}