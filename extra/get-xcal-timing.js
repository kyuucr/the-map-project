const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <input CSVs>`);
    process.exit(0);
}

const dir = ARGS[0];

let colIds = {};
let cdf = {};
let hist = {};

agg.callbackCsvRecursive(dir, data => {
    let header = data.header;
    let csv = data.csv;
    let filename = path.basename(data.path).replace(/\.csv/, "");
    console.log(`${filename}, # of cols: ${data.header.length}, # of rows: ${data.csv.length}`)
    if (cdf[filename] === undefined) cdf[filename] = { allPci: { allBeam: {} } };
    if (hist[filename] === undefined) hist[filename] = {};

    for (let i = 0, length1 = header.length; i < length1; i++) {
        switch (header[i]) {
            case "Lon": colIds.Lon = i; break;
            case "Lat": colIds.Lat = i; break;
            case "5G KPI PCell RF Serving PCI": colIds.PCell_ServPCI = i; break;
            case "5G KPI PCell RF Serving SSB Idx": colIds.PCell_ServSSB = i; break;
            case "5G KPI PCell RF Band": colIds.PCell_ServBand = i; break;
            case "5G KPI PCell RF BandWidth": colIds.PCell_ServBW = i; break;
            case "5G KPI PCell RF Frequency [MHz]": colIds.PCell_ServFreq = i; break;
            case "5G KPI PCell RF Pathloss [dB]": colIds.PCell_ServPL = i; break;
            case "5G KPI PCell RF Serving SS-RSRP [dBm]": colIds.PCell_ServRSRP = i; break;
            case "5G KPI PCell RF Serving SS-RSRQ [dB]": colIds.PCell_ServRSRQ = i; break;
            case "5G KPI PCell RF Serving SS-SINR [dB]": colIds.PCell_ServSINR = i; break;
            case "5G KPI PCell RF Serving CQI [dB]": colIds.PCell_ServCQI = i; break;
            case "5G KPI PCell RF Serving CQI [dB]": colIds.PCell_ServRI = i; break;
            case "5G KPI PCell RF Best Beam SSB Idx": colIds.PCell_BestSSB = i; break;
            case "5G KPI PCell RF Best Beam SS-RSRP [dBm]": colIds.PCell_BestRSRP = i; break;
            case "5G KPI PCell RF Best Beam SS-RSRQ [dBm]": colIds.PCell_BestRSRQ = i; break;
            case "5G KPI PCell RF Best Beam State": colIds.PCell_BestState = i; break;
            case "5G KPI PCell RF Neighbor Top1 PCI": colIds.PCell_Neighbor1PCI = i; break;
            case "5G KPI PCell RF Neighbor Top1 SS-RSRP [dBm]": colIds.PCell_Neighbor1RSRP = i; break;
            case "5G KPI PCell RF Neighbor Top1 SS-RSRQ [dB]": colIds.PCell_Neighbor1RSRQ = i; break;
            case "5G KPI PCell RF Neighbor Top2 PCI": colIds.PCell_Neighbor2PCI = i; break;
            case "5G KPI PCell RF Neighbor Top2 SS-RSRP [dBm]": colIds.PCell_Neighbor2RSRP = i; break;
            case "5G KPI PCell RF Neighbor Top2 SS-RSRQ [dB]": colIds.PCell_Neighbor2RSRQ = i; break;
            case "5G KPI PCell RF Neighbor Top3 PCI": colIds.PCell_Neighbor3PCI = i; break;
            case "5G KPI PCell RF Neighbor Top3 SS-RSRP [dBm]": colIds.PCell_Neighbor3RSRP = i; break;
            case "5G KPI PCell RF Neighbor Top3 SS-RSRQ [dB]": colIds.PCell_Neighbor3RSRQ = i; break;
            case "5G KPI PCell Layer1 DL BLER [%]": colIds.PCell_DLBLER = i; break;
            case "5G KPI PCell Layer1 DL Modulation0 Representative Value": colIds.PCell_DLMod = i; break;
            case "5G KPI PCell Layer1 DL RB Num (Mode)": colIds.PCell_RBMode = i; break;
            case "5G KPI PCell Layer1 DL RB Num (Avg)": colIds.PCell_RBAvg = i; break;
            case "5G KPI PCell Layer1 PDSCH Throughput [Mbps]": colIds.PCell_PDSCHTput = i; break;
            case "5G KPI PCell Layer2 MAC DL Throughput [Mbps]": colIds.PCell_MACDLTput = i; break;
            case "5G KPI SCell[1] RF Serving PCI": colIds.SCell1_ServPCI = i; break;
            case "5G KPI SCell[1] RF Serving SSB Idx": colIds.SCell1_ServSSB = i; break;
            case "5G KPI SCell[1] RF Serving SS-RSRP [dBm]": colIds.SCell1_ServRSRP = i; break;
            case "5G KPI SCell[1] RF Serving SS-RSRQ [dB]": colIds.SCell1_ServRSRQ = i; break;
            case "5G KPI SCell[1] RF Serving SS-SINR [dB]": colIds.SCell1_ServSINR = i; break;
            case "5G KPI SCell[1] Layer1 DL BLER [%]": colIds.SCell1_DLBLER = i; break;
            case "5G KPI SCell[1] Layer1 DL Modulation0 Representative Value": colIds.SCell1_DLMod = i; break;
            case "5G KPI SCell[1] Layer1 DL RB Num (Mode)": colIds.SCell1_RBMode = i; break;
            case "5G KPI SCell[1] Layer1 DL RB Num (Avg)": colIds.SCell1_RBAvg = i; break;
            case "5G KPI SCell[1] Layer1 PDSCH Throughput [Mbps]": colIds.SCell1_PDSCHTput = i; break;
            case "5G KPI SCell[1] Layer2 MAC DL Throughput [Mbps]": colIds.SCell1_MACDLTput = i; break;
            case "5G KPI SCell[2] RF Serving PCI": colIds.SCell2_ServPCI = i; break;
            case "5G KPI SCell[2] RF Serving SSB Idx": colIds.SCell2_ServSSB = i; break;
            case "5G KPI SCell[2] RF Serving SS-RSRP [dBm]": colIds.SCell2_ServRSRP = i; break;
            case "5G KPI SCell[2] RF Serving SS-RSRQ [dB]": colIds.SCell2_ServRSRQ = i; break;
            case "5G KPI SCell[2] RF Serving SS-SINR [dB]": colIds.SCell2_ServSINR = i; break;
            case "5G KPI SCell[2] Layer1 DL BLER [%]": colIds.SCell2_DLBLER = i; break;
            case "5G KPI SCell[2] Layer1 DL Modulation0 Representative Value": colIds.SCell2_DLMod = i; break;
            case "5G KPI SCell[2] Layer1 DL RB Num (Mode)": colIds.SCell2_RBMode = i; break;
            case "5G KPI SCell[2] Layer1 DL RB Num (Avg)": colIds.SCell2_RBAvg = i; break;
            case "5G KPI SCell[2] Layer1 PDSCH Throughput [Mbps]": colIds.SCell2_PDSCHTput = i; break;
            case "5G KPI SCell[2] Layer2 MAC DL Throughput [Mbps]": colIds.SCell2_MACDLTput = i; break;
            case "5G KPI SCell[3] RF Serving PCI": colIds.SCell3_ServPCI = i; break;
            case "5G KPI SCell[3] RF Serving SSB Idx": colIds.SCell3_ServSSB = i; break;
            case "5G KPI SCell[3] RF Serving SS-RSRP [dBm]": colIds.SCell3_ServRSRP = i; break;
            case "5G KPI SCell[3] RF Serving SS-RSRQ [dB]": colIds.SCell3_ServRSRQ = i; break;
            case "5G KPI SCell[3] RF Serving SS-SINR [dB]": colIds.SCell3_ServSINR = i; break;
            case "5G KPI SCell[3] Layer1 DL BLER [%]": colIds.SCell3_DLBLER = i; break;
            case "5G KPI SCell[3] Layer1 DL Modulation0 Representative Value": colIds.SCell3_DLMod = i; break;
            case "5G KPI SCell[3] Layer1 DL RB Num (Mode)": colIds.SCell3_RBMode = i; break;
            case "5G KPI SCell[3] Layer1 DL RB Num (Avg)": colIds.SCell3_RBAvg = i; break;
            case "5G KPI SCell[3] Layer1 PDSCH Throughput [Mbps]": colIds.SCell3_PDSCHTput = i; break;
            case "5G KPI SCell[3] Layer2 MAC DL Throughput [Mbps]": colIds.SCell3_MACDLTput = i; break;
            case "5G KPI SCell[4] RF Serving PCI": colIds.SCell4_ServPCI = i; break;
            case "5G KPI SCell[4] RF Serving SSB Idx": colIds.SCell4_ServSSB = i; break;
            case "5G KPI SCell[4] RF Serving SS-RSRP [dBm]": colIds.SCell4_ServRSRP = i; break;
            case "5G KPI SCell[4] RF Serving SS-RSRQ [dB]": colIds.SCell4_ServRSRQ = i; break;
            case "5G KPI SCell[4] RF Serving SS-SINR [dB]": colIds.SCell4_ServSINR = i; break;
            case "5G KPI SCell[4] Layer1 DL BLER [%]": colIds.SCell4_DLBLER = i; break;
            case "5G KPI SCell[4] Layer1 DL Modulation0 Representative Value": colIds.SCell4_DLMod = i; break;
            case "5G KPI SCell[4] Layer1 DL RB Num (Mode)": colIds.SCell4_RBMode = i; break;
            case "5G KPI SCell[4] Layer1 DL RB Num (Avg)": colIds.SCell4_RBAvg = i; break;
            case "5G KPI SCell[4] Layer1 PDSCH Throughput [Mbps]": colIds.SCell4_PDSCHTput = i; break;
            case "5G KPI SCell[4] Layer2 MAC DL Throughput [Mbps]": colIds.SCell4_MACDLTput = i; break;
            case "5G KPI SCell[5] RF Serving PCI": colIds.SCell5_ServPCI = i; break;
            case "5G KPI SCell[5] RF Serving SSB Idx": colIds.SCell5_ServSSB = i; break;
            case "5G KPI SCell[5] RF Serving SS-RSRP [dBm]": colIds.SCell5_ServRSRP = i; break;
            case "5G KPI SCell[5] RF Serving SS-RSRQ [dB]": colIds.SCell5_ServRSRQ = i; break;
            case "5G KPI SCell[5] RF Serving SS-SINR [dB]": colIds.SCell5_ServSINR = i; break;
            case "5G KPI SCell[5] Layer1 DL BLER [%]": colIds.SCell5_DLBLER = i; break;
            case "5G KPI SCell[5] Layer1 DL Modulation0 Representative Value": colIds.SCell5_DLMod = i; break;
            case "5G KPI SCell[5] Layer1 DL RB Num (Mode)": colIds.SCell5_RBMode = i; break;
            case "5G KPI SCell[5] Layer1 DL RB Num (Avg)": colIds.SCell5_RBAvg = i; break;
            case "5G KPI SCell[5] Layer1 PDSCH Throughput [Mbps]": colIds.SCell5_PDSCHTput = i; break;
            case "5G KPI SCell[5] Layer2 MAC DL Throughput [Mbps]": colIds.SCell5_MACDLTput = i; break;
            case "5G KPI SCell[6] RF Serving PCI": colIds.SCell6_ServPCI = i; break;
            case "5G KPI SCell[6] RF Serving SSB Idx": colIds.SCell6_ServSSB = i; break;
            case "5G KPI SCell[6] RF Serving SS-RSRP [dBm]": colIds.SCell6_ServRSRP = i; break;
            case "5G KPI SCell[6] RF Serving SS-RSRQ [dB]": colIds.SCell6_ServRSRQ = i; break;
            case "5G KPI SCell[6] RF Serving SS-SINR [dB]": colIds.SCell6_ServSINR = i; break;
            case "5G KPI SCell[6] Layer1 DL BLER [%]": colIds.SCell6_DLBLER = i; break;
            case "5G KPI SCell[6] Layer1 DL Modulation0 Representative Value": colIds.SCell6_DLMod = i; break;
            case "5G KPI SCell[6] Layer1 DL RB Num (Mode)": colIds.SCell6_RBMode = i; break;
            case "5G KPI SCell[6] Layer1 DL RB Num (Avg)": colIds.SCell6_RBAvg = i; break;
            case "5G KPI SCell[6] Layer1 PDSCH Throughput [Mbps]": colIds.SCell6_PDSCHTput = i; break;
            case "5G KPI SCell[6] Layer2 MAC DL Throughput [Mbps]": colIds.SCell6_MACDLTput = i; break;
            case "5G KPI SCell[7] RF Serving PCI": colIds.SCell7_ServPCI = i; break;
            case "5G KPI SCell[7] RF Serving SSB Idx": colIds.SCell7_ServSSB = i; break;
            case "5G KPI SCell[7] RF Serving SS-RSRP [dBm]": colIds.SCell7_ServRSRP = i; break;
            case "5G KPI SCell[7] RF Serving SS-RSRQ [dB]": colIds.SCell7_ServRSRQ = i; break;
            case "5G KPI SCell[7] RF Serving SS-SINR [dB]": colIds.SCell7_ServSINR = i; break;
            case "5G KPI SCell[7] Layer1 DL BLER [%]": colIds.SCell7_DLBLER = i; break;
            case "5G KPI SCell[7] Layer1 DL Modulation0 Representative Value": colIds.SCell7_DLMod = i; break;
            case "5G KPI SCell[7] Layer1 DL RB Num (Mode)": colIds.SCell7_RBMode = i; break;
            case "5G KPI SCell[7] Layer1 DL RB Num (Avg)": colIds.SCell7_RBAvg = i; break;
            case "5G KPI SCell[7] Layer1 PDSCH Throughput [Mbps]": colIds.SCell7_PDSCHTput = i; break;
            case "5G KPI SCell[7] Layer2 MAC DL Throughput [Mbps]": colIds.SCell7_MACDLTput = i; break;
            case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(5G-NR PDU) [Mbps]": colIds.Total_5GDLPHYTput = i; break;
            case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(LTE PDU) [Mbps]": colIds.Total_4GDLPHYTput = i; break;
            case "Qualcomm 5G-NR EN-DC PHY Throughput PDSCH Throughput(Total PDU) [Mbps]": colIds.Total_DLPHYTput = i; break;
            case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(5G-NR PDU) [Mbps]": colIds.Total_5GDLMACTput = i; break;
            case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(LTE PDU) [Mbps]": colIds.Total_4GDLMACTput = i; break;
            case "Qualcomm 5G-NR EN-DC MAC Throughput DL MAC Throughput(Total PDU) [Mbps]": colIds.Total_DLMACTput = i; break;
            case "Ping & Trace RTT (ms)": colIds.Total_PingRTT = i; break;
            default: break;
        }
    }

    let prevDatetime = new Date(0);
    let datetimeSpan = { start: null, end: null, traffic: [] };
    let spans = [];

    for (let row of csv) {
        if (row[0] === "Count") break;
        let datetime = new Date(row[0]);
        let timespan = datetime.getTime() - prevDatetime.getTime();
        if (timespan < 0 || timespan > 60000) {
            // new datetime span, save previous datetime span
            datetimeSpan.end = prevDatetime;
            if (datetimeSpan.start) spans.push(datetimeSpan);
            datetimeSpan = { start: datetime, end: null, traffic: [] };
        } else {
            if (row[colIds.Total_PingRTT]) {
                if (!datetimeSpan.traffic.includes('ping')) datetimeSpan.traffic.push('ping');
            }
            if (parseFloat(row[colIds.Total_5GDLPHYTput]) > 10) {
                if (!datetimeSpan.traffic.includes('http')) datetimeSpan.traffic.push('http');
            }
        }
        prevDatetime = datetime;
    }
    datetimeSpan.end = prevDatetime;
    spans.push(datetimeSpan);

    console.log('Time spans:');
    for (let span of spans) {
        console.log(`${span.start}, ${span.end}, traffic: ${span.traffic.join(",")}`);
    }

});
