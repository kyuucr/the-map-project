const fs = require('fs');
const path = require('path');
const csvUtils = require('../lib/csv-utils');
const filter = require('../lib/filter');

const OPTIONS = {
    inputFile: path.join("html", "outputs", "fcc-data.json"), // FCC input file
    outputFile: path.join("extra", "fcc-data.csv"), // output path
    isLocationLabel: false, // Is using location label?
    filter: null
};

let args = process.argv.slice(2);

if (args.length === 1 && (args[0] === "-h" || args[0] === "--help")) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} [--input fcc-data.json file] [--output file] [--label-location] [--filters ...] [-h | --help]`);
    process.exit(1);
}

while (args.length > 0) {
    switch (args[0]) {
        case "--input":
            OPTIONS.inputFile = args[1];
            args.splice(0, 2);
            break;
        case "--output":
            OPTIONS.outputFile = args[1];
            args.splice(0, 2);
            break;
        case "--label-location":
            OPTIONS.isLocationLabel = true;
            args.splice(0, 1);
            break;
        case "--filter":
            OPTIONS.filter = JSON.parse(args[1]);
            args.splice(0, 2);
            break;

        default:
            break;
    }
}

const fccData = JSON.parse(fs.readFileSync(OPTIONS.inputFile));
if (OPTIONS.filter !== null) {
    fccData = filter.filterArray(OPTIONS.filter, fccData);
}

const os = fs.createWriteStream(OPTIONS.outputFile);

let currLocationLabel = ""
if (OPTIONS.isLocationLabel) {
    if (OPTIONS.inputFile.includes("indoor")) {
        currLocationLabel = "indoor";
    } else if (OPTIONS.inputFile.includes("outdoor")) {
        currLocationLabel = "outdoor"
    } else {
        currLocationLabel = "undefined"
    }
    console.log("Location label:", currLocationLabel);
}

os.write(csvUtils.unpackKeys(fccData[0].overview)
    + (OPTIONS.isLocationLabel ? "location_cat," : "")
    + csvUtils.unpackKeys(fccData[0].download.rsrp, "download.rsrp")
    + csvUtils.unpackKeys(fccData[0].download.connection, "download.connection")
    + csvUtils.unpackKeys(fccData[0].download.pci, "download.pci")
    + csvUtils.unpackKeys(fccData[0].download.location, "download.location")
    + csvUtils.unpackKeys(fccData[0].upload.rsrp, "upload.rsrp")
    + csvUtils.unpackKeys(fccData[0].upload.connection, "upload.connection")
    + csvUtils.unpackKeys(fccData[0].upload.pci, "upload.pci")
    + csvUtils.unpackKeys(fccData[0].upload.location, "upload.location")
    + csvUtils.unpackKeys(fccData[0].latency.rsrp, "latency.rsrp")
    + csvUtils.unpackKeys(fccData[0].latency.connection, "latency.connection")
    + csvUtils.unpackKeys(fccData[0].latency.pci, "latency.pci")
    + csvUtils.unpackKeys(fccData[0].latency.location, "latency.location")
    + "\n");

for (let i = 0, length1 = fccData.length; i < length1; i++){
    os.write(csvUtils.unpackVals(fccData[i].overview));
    if (OPTIONS.isLocationLabel) os.write(`${currLocationLabel},`);
    os.write(`${fccData[i].download ? csvUtils.unpackVals(fccData[i].download.rsrp) : "FAILURE,FAILURE,FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].download ? csvUtils.unpackVals(fccData[i].download.connection) : "FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].download ? csvUtils.unpackVals(fccData[i].download.pci) : "FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].download ? csvUtils.unpackVals(fccData[i].download.location) : "FAILURE,FAILURE,FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].upload ? csvUtils.unpackVals(fccData[i].upload.rsrp) : "FAILURE,FAILURE,FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].upload ? csvUtils.unpackVals(fccData[i].upload.connection) : "FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].upload ? csvUtils.unpackVals(fccData[i].upload.pci) : "FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].upload ? csvUtils.unpackVals(fccData[i].upload.location) : "FAILURE,FAILURE,FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].latency ? csvUtils.unpackVals(fccData[i].latency.rsrp) : "FAILURE,FAILURE,FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].latency ? csvUtils.unpackVals(fccData[i].latency.connection) : "FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].latency ? csvUtils.unpackVals(fccData[i].latency.pci) : "FAILURE,FAILURE,"}`);
    os.write(`${fccData[i].latency ? csvUtils.unpackVals(fccData[i].latency.location) : "FAILURE,FAILURE,FAILURE,FAILURE,"}`);
    os.write("\n");
}