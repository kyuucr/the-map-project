const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> [output path] [--remove-zero] [--client-ids <JSON array in file or as a string>]`);
    process.exit(0);
}

let clientIds = [
    "unknown",
];
let clientTypes = [];
let testIdList = [];
let outputPath = "fcc-stats-bookstore.csv";
let inputFolder = args[0];
let removeZero = false;
args.splice(0, 1);
while (args.length > 0) {
    switch (args[0]) {
        case "--remove-zero": {
            removeZero = true;
            args.splice(0, 1);
            break;
        }
        case "--client-ids": {
            try {
                let temp = dataUtils.parseJSONStringOrFile(args[1]);
                if (temp[0] !== "unknown") {
                    temp.unshift("unknown");
                }
                clientIds = temp;
            } catch (err) {
                console.log("JSON parse error!", err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        case "--client-types": {
            try {
                let temp = dataUtils.parseJSONStringOrFile(args[1]);
                if (temp[0] !== "unknown") {
                    temp.unshift("unknown");
                }
                clientTypes = temp;
            } catch (err) {
                console.log("JSON parse error!", err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}


const os = fs.createWriteStream(outputPath);

let sortNumAsc = (a, b) => a - b;
let avgReduce = (avg, value, _, { length }) => avg + value / length;

let allDl = {};
let clientStatsDl = {};
let serverStatsDl = {};
let allLat = {};
let clientStatsLat = {};
let serverStatsLat = {};

agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "fcc") return;
    if (allDl[data.path] === undefined) {
        allDl[data.path] = [];
        clientStatsDl[data.path] = {};
        serverStatsDl[data.path] = {};
        allLat[data.path] = [];
        clientStatsLat[data.path] = {};
        serverStatsLat[data.path] = {};
    }
    let recs = data.json;
    for (let i = 0, length1 = recs.length; i < length1; i++) {
        if (recs[i].tests.testId === undefined || testIdList.includes(recs[i].tests.testId)) continue;
        testIdList.push(recs[i].tests.testId);
        let clientId = recs[i].zipFilename.replace(":", "_") || "unknown";
        if (!clientIds.includes(clientId)) clientIds.push(clientId);
        let serverId = (recs[i].tests && recs[i].tests.download) ? recs[i].tests.download.target : "unknown";

        let dlTput = (recs[i].tests && recs[i].tests.download) ? (recs[i].tests.download.throughput * 8 / 1e6) : 0;
        if (!removeZero || dlTput > 0) {
            let clientType = `${clientId}#${dataUtils.getNetworkType(recs[i].tests.download)}`;
            if (!clientTypes.includes(clientType)) clientTypes.push(clientType);
            allDl[data.path].push(dlTput);
            if (clientStatsDl[data.path][clientType] === undefined) clientStatsDl[data.path][clientType] = [];
            clientStatsDl[data.path][clientType].push(dlTput);
            if (serverStatsDl[data.path][serverId] === undefined) serverStatsDl[data.path][serverId] = [];
            serverStatsDl[data.path][serverId].push(dlTput);
        }

        let latency = (recs[i].tests && recs[i].tests.latency) ? (recs[i].tests.latency.round_trip_time / 1e3) : 0;
        if (!removeZero || latency > 0) {
            let clientType = `${clientId}#${dataUtils.getNetworkType(recs[i].tests.latency)}`;
            if (!clientTypes.includes(clientType)) clientTypes.push(clientType);
            allLat[data.path].push(latency);
            if (clientStatsLat[data.path][clientType] === undefined) clientStatsLat[data.path][clientType] = [];
            clientStatsLat[data.path][clientType].push(latency);
            if (serverStatsLat[data.path][serverId] === undefined) serverStatsLat[data.path][serverId] = [];
            serverStatsLat[data.path][serverId].push(latency);
        }
    }
});

for (const path in allDl) {

    let metadataPath = path.replace(/\/data.*\//, "/metadata/metadata-") + ".json";
    let metadataDesc = "";
    if (fs.existsSync(metadataPath)) {
        metadataDesc = JSON.parse(fs.readFileSync(metadataPath)).description;
    }

    let output = `${path},${metadataDesc}\n`;
    let clientIndices = Object.keys(clientStatsDl[path]).map(val => clientTypes.indexOf(val)).sort().map(val => clientTypes[val]);
    let serverIndices = Object.keys(serverStatsDl[path]).sort()

    allDl[path].sort(sortNumAsc);
    output += `stats,allDl,`;
    for (let client of clientIndices) {
        let num = clientIds.indexOf(client.split("#")[0]);
        output += `client${num}_${client.split("#")[1]},`;
        if (clientStatsDl[path][client]) clientStatsDl[path][client].sort(sortNumAsc);
    }
    for (let server of serverIndices) {
        output += `server_${server},`;
        serverStatsDl[path][server].sort(sortNumAsc);
    }
    output += `\n`;
    // MEAN
    output += `mean,${allDl[path].reduce(avgReduce, 0)},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client].reduce(avgReduce, 0)},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server].reduce(avgReduce, 0)},`;
    }
    output += `\n`;
    // MIN
    output += `min,${allDl[path][0]},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client][0]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server][0]},`;
    }
    output += `\n`;
    // 25th-percentile
    output += `25th,${allDl[path][Math.floor(allDl[path].length / 4)]},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client][Math.floor(clientStatsDl[path][client].length / 4)]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server][Math.floor(serverStatsDl[path][server].length / 4)]},`;
    }
    output += `\n`;
    // Median
    output += `median,${allDl[path][Math.floor(allDl[path].length / 2)]},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client][Math.floor(clientStatsDl[path][client].length / 2)]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server][Math.floor(serverStatsDl[path][server].length / 2)]},`;
    }
    output += `\n`;
    // 75th-percentile
    output += `75th,${allDl[path][Math.floor(allDl[path].length / 4 * 3)]},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client][Math.floor(clientStatsDl[path][client].length / 4 * 3)]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server][Math.floor(serverStatsDl[path][server].length / 4 * 3)]},`;
    }
    output += `\n`;
    // MAX
    output += `max,${allDl[path][allDl[path].length - 1]},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client][clientStatsDl[path][client].length - 1]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server][serverStatsDl[path][server].length - 1]},`;
    }
    output += `\n`;
    // COUNT
    output += `count,${allDl[path].length},`;
    for (let client of clientIndices) {
        if (clientStatsDl[path][client]) output += `${clientStatsDl[path][client].length},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsDl[path][server].length},`;
    }
    output += `\n`;
    os.write(output);
}

os.write("\n");

for (const path in allLat) {

    let metadataPath = path.replace(/\/data.*\//, "/metadata/metadata-") + ".json";
    let metadataDesc = "";
    if (fs.existsSync(metadataPath)) {
        metadataDesc = JSON.parse(fs.readFileSync(metadataPath)).description;
    }

    let output = `${path},${metadataDesc}\n`;
    let clientIndices = Object.keys(clientStatsLat[path]).map(val => clientTypes.indexOf(val)).sort().map(val => clientTypes[val]);
    let serverIndices = Object.keys(serverStatsLat[path]).sort()

    allLat[path].sort(sortNumAsc);
    output += `stats,allLat,`;
    for (let client of clientIndices) {
        let num = clientIds.indexOf(client.split("#")[0]);
        output += `client${num}_${client.split("#")[1]},`;
        if (clientStatsLat[path][client]) clientStatsLat[path][client].sort(sortNumAsc);
    }
    for (let server of serverIndices) {
        output += `server_${server},`;
        serverStatsLat[path][server].sort(sortNumAsc);
    }
    output += `\n`;
    // MEAN
    output += `mean,${allLat[path].reduce(avgReduce, 0)},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client].reduce(avgReduce, 0)},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server].reduce(avgReduce, 0)},`;
    }
    output += `\n`;
    // MIN
    output += `min,${allLat[path][0]},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client][0]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server][0]},`;
    }
    output += `\n`;
    // 25th-percentile
    output += `25th,${allLat[path][Math.floor(allLat[path].length / 4)]},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client][Math.floor(clientStatsLat[path][client].length / 4)]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server][Math.floor(serverStatsLat[path][server].length / 4)]},`;
    }
    output += `\n`;
    // Median
    output += `median,${allLat[path][Math.floor(allLat[path].length / 2)]},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client][Math.floor(clientStatsLat[path][client].length / 2)]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server][Math.floor(serverStatsLat[path][server].length / 2)]},`;
    }
    output += `\n`;
    // 75th-percentile
    output += `75th,${allLat[path][Math.floor(allLat[path].length / 4 * 3)]},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client][Math.floor(clientStatsLat[path][client].length / 4 * 3)]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server][Math.floor(serverStatsLat[path][server].length / 4 * 3)]},`;
    }
    output += `\n`;
    // MAX
    output += `max,${allLat[path][allLat[path].length - 1]},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client][clientStatsLat[path][client].length - 1]},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server][serverStatsLat[path][server].length - 1]},`;
    }
    output += `\n`;
    // COUNT
    output += `count,${allLat[path].length},`;
    for (let client of clientIndices) {
        if (clientStatsLat[path][client]) output += `${clientStatsLat[path][client].length},`;
    }
    for (let server of serverIndices) {
        output += `${serverStatsLat[path][server].length},`;
    }
    output += `\n`;
    os.write(output);
}

let addendum = `\nADDENDUM\nclient_id,zip_filename\n`;
clientIds.forEach((val, idx) => {
    addendum += `${idx},${val}\n`
});
os.write(addendum);

os.close();
