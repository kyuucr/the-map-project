const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const statsUtils = require('../lib/stats-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 3) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC/SigCap dir> <output dir> <timeList JSON> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputDir = ARGS[1];
let timeList;
try {
    let string;
    if (fs.existsSync(ARGS[2])) {
        string = fs.readFileSync(ARGS[2]);
    } else {
        string = ARGS[2];
    }
    timeList = JSON.parse(string);
    for (let i = 0, length1 = timeList.length; i < length1; i++) {
        timeList[i].start = new Date(timeList[i].start).getTime();
        timeList[i].end = new Date(timeList[i].end).getTime();
    }
} catch (e) {
    console.log(`JSON parse error: `, e);
    process.exit(0);
}
const prefix = ARGS[3] || "";

let testIdBin = [];
let output = {};
for (let j = 0, length2 = timeList.length; j < length2; j++) {
    output[j] = { dlMbps: { all: [] }, fccRsrp: { all: [] }, sigcapRsrp: { all: [] }, temp: { all: [] }, gpuTemp: { all: [] }, skinTemp: { all: [] } };
}
const valString = {
    dlMbps: "DL Tput (Mbps)",
    fccRsrp : "RSRP (dBm)",
    sigcapRsrp : "RSRP (dBm)",
    temp : "CPU Temp (Celcius)",
    skinTemp : "Skin Temp (Celcius)",
}

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;
    let dataType = dataUtils.getJSONType(json[0]);
    console.log(`Folder: ${path.basename(data.path)}; Data type: ${dataType}`);
    if (dataType === "fcc") {
        for(let i = 0, length1 = json.length; i < length1; i++){
            let testId = json[i].tests.testId;
            if (testId === undefined || testIdBin.includes(testId)) continue;
            testIdBin.push(testId);
            let downloadTest = json[i].tests.download;
            if (downloadTest === undefined || downloadTest.local_datetime === undefined) continue;
            let startTime = new Date(downloadTest.local_datetime).getTime();
            let idx;
            for (let j = 0, length2 = timeList.length; j < length2; j++) {
                if (startTime > timeList[j].start && startTime < timeList[j].end) {
                    idx = j;
                    break;
                }
            }
            if (idx === undefined) continue;

            let startNet = dataUtils.getCleanValue(downloadTest, "environment.beginning.network.subtype_name");
            let endNet = dataUtils.getCleanValue(downloadTest, "environment.end.network.subtype_name");

            let dlMbps = downloadTest.throughput * 8 / 1e6;
            if (dlMbps > 0) {
                if (output[idx].dlMbps[endNet] === undefined) output[idx].dlMbps[endNet] = [];
                output[idx].dlMbps[endNet].push({ value: dlMbps, time: startTime });
                output[idx].dlMbps.all.push({ value: dlMbps, time: startTime });
            }

            let startRsrp = dataUtils.cleanSignal(
                dataUtils.getCleanValue(
                    downloadTest, `environment.beginning.telephony.cell_signal.received_signal_power${(startNet.startsWith("NR")) ? "_ss" : ""}`));
            if (output[idx].fccRsrp[startNet] === undefined) output[idx].fccRsrp[startNet] = [];
            output[idx].fccRsrp[startNet].push({ value: startRsrp, time: startTime });
            output[idx].fccRsrp.all.push({ value: startRsrp, time: startTime });

            let endTime = startTime + (downloadTest.duration / 1e3);
            let endRsrp = dataUtils.cleanSignal(
                dataUtils.getCleanValue(
                    downloadTest, `environment.end.telephony.cell_signal.received_signal_power${(startNet.startsWith("NR")) ? "_ss" : ""}`));
            if (output[idx].fccRsrp[endNet] === undefined) output[idx].fccRsrp[endNet] = [];
            output[idx].fccRsrp[endNet].push({ value: endRsrp, time: endTime });
            output[idx].fccRsrp.all.push({ value: endRsrp, time: endTime });
        }
    } else if (dataType === "sigcap") {
        for (let i = 0, length1 = json.length; i < length1; i++){
            let time = new Date(json[i].datetimeIso).getTime();
            let idx;
            for (let j = 0, length2 = timeList.length; j < length2; j++) {
                if (time > timeList[j].start && time < timeList[j].end) {
                    idx = j;
                    break;
                }
            }
            if (idx === undefined) continue;

            if (json[i].sensor && json[i].sensor.hardwareCpuTempC && json[i].sensor.hardwareCpuTempC.length > 0) {
                output[idx].temp.all.push({ value: statsUtils.meanArray(json[i].sensor.hardwareCpuTempC), time: time });
            }
            if (json[i].sensor && json[i].sensor.hardwareGpuTempC && json[i].sensor.hardwareGpuTempC.length > 0) {
                output[idx].gpuTemp.all.push({ value: statsUtils.meanArray(json[i].sensor.hardwareGpuTempC), time: time });
            }
            if (json[i].sensor && json[i].sensor.hardwareSkinTempC && json[i].sensor.hardwareSkinTempC.length > 0) {
                output[idx].skinTemp.all.push({ value: statsUtils.meanArray(json[i].sensor.hardwareSkinTempC), time: time });
            }
            if (json[i].nr_info && json[i].nr_info[0]) {
                output[idx].sigcapRsrp.all.push({ value: json[i].nr_info[0].ssRsrp, time: time });
            }
        }
    }

});

let sortTimeAsc = (a,b) => { return (a.time < b.time) ? -1 : (a.time > b.time) ? 1 : 0 };

for (let idx in output) {
    for (let valType in output[idx]) {
        for (let net in output[idx][valType]) {
            output[idx][valType][net].sort(sortTimeAsc);
            let outputPath = path.join(outputDir, `${prefix ? prefix + "-" : ""}${idx}-${valType}-${net}.dat`);

            let startTime = timeList[idx].start;
            let string = `"Elapsed Time (s)" "${valString[valType]}"\n`
            for (let row of output[idx][valType][net]) {
                string += `${(row.time - startTime) / 1e3} ${row.value}\n`;
            }
            fs.writeFileSync(outputPath, string);
        }
    }
}
