const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <Input CSV file> [output path] [output folder suffix]`);
    process.exit(0);
}

let inputCsv = ARGS[0];
let outputPath = ARGS[1] || path.dirname(ARGS[0]);
let outputSuff = "";
if (ARGS[2]) {
    outputSuff = `-${ARGS[2]}`;
}

fs.mkdirSync(outputPath, { recursive: true });

agg.callbackCsvRecursive(inputCsv, data => {
    for(let i = 0, length1 = data.csv.length; i < length1; i++){
        // If last row is empty, skip
        if (i === length1 - 1 && data.csv[i].length === 1 && data.csv[i][0] === "") continue;

        let metadata = {
            description: "",
            type: "",
            dataFolder: "",
            filter: {},
            numChunk: 0,
            fileList: []
        }
        let filenameSuff = "";

        for(let j = 0, length2 = data.csv[i].length; j < length2; j++){
            // If last column is empty, skip
            if (j === length2 - 1 && data.header[j] === "") continue;

            switch (data.header[j]) {
                case "output": {
                    metadata.dataFolder = path.relative(outputPath, path.join(path.dirname(inputCsv), `data${outputSuff}`, data.csv[i][j]));
                    filenameSuff = `-${data.csv[i][j]}`;
                    break;
                }
                case "type":
                case "description": {
                    metadata[data.header[j]] = data.csv[i][j];
                    break;
                }
                default: {
                    if (data.header[j].startsWith("filter")) {
                        let filterPath = data.header[j].split(".");
                        let filter = metadata[filterPath[0]];
                        for(let k = 1, length3 = filterPath.length; k < length3; k++){
                            if (k === length3 - 1) {
                                if (filter[filterPath[k]] === undefined) {
                                    filter[filterPath[k]] = data.csv[i][j];
                                } else if (Array.isArray(filter[filterPath[k]])) {
                                    filter[filterPath[k]].push(data.csv[i][j]);
                                } else {
                                    let temp = filter[filterPath[k]];
                                    filter[filterPath[k]] = [ temp, data.csv[i][j] ];
                                }
                            } else {
                                if (filter[filterPath[k]] === undefined) {
                                    filter[filterPath[k]] = {}
                                }
                                filter = filter[filterPath[k]];
                            }
                        }
                    }
                    break;
                }
            }
        }
        let outputFilepath = path.join(outputPath, `metadata${filenameSuff}.json`);
        if (fs.existsSync(outputFilepath)) {
            console.log(`Output file exists: ${outputFilepath}, editing file...`);
            try {
                let json = JSON.parse(fs.readFileSync(outputFilepath));
                for (let key in json) {
                    if (key !== "numChunk" && key !== "fileList" && metadata[key] !== undefined) {
                        json[key] = metadata[key];
                    }
                }
                fs.writeFileSync(outputFilepath, JSON.stringify(json, null, 4));
            } catch (err) {
                console.log(`Parsing/writing error!`, err);
            }
        } else {
            fs.writeFileSync(outputFilepath, JSON.stringify(metadata, null, 4));
        }
    }
});