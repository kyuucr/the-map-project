const aggregator = require("../lib/aggregator");
const commandParser = require('../lib/command-parser');
const extractor = require("../lib/extractor");
const logger = require("../lib/logger");
const cliProgress = require("cli-progress");
const fs = require("fs");

const progressBar = new cliProgress.SingleBar({
    clearOnComplete: true,
    etaBuffer: 100,
    linewrap: true
}, cliProgress.Presets.shades_classic);
logger.progressBars.push(progressBar);

// Process arguments
commandParser.addArgument("input dir", { isMandatory: true });
commandParser.addArgument("output dir", { isMandatory: true });
const options = commandParser.parse();

let zipFiles = aggregator.getFileListRecursive(options["input dir"], /\.zip$/);
progressBar.start(zipFiles.length, 0);
logger.i(`Processing ${zipFiles.length} file(s)...`)

// make sure output dir exists
fs.mkdirSync(options["output dir"], { recursive: true });

extractor.extractZipFiles(
    zipFiles,
    options["output dir"],
    { callbackEntry: zipFile => { progressBar.increment(); } }
).then(data => {
    let errors = data.results.filter((val) => val.status === `rejected`);
    logger.i(`# of chunks generated: ${data.numChunk}`);
    logger.i(`Total # of entry: ${data.totalNumData}`);
    logger.i(`# of error zip file: ${errors.length}`);
    for (let error of errors) {
        logger.d(`error: ${error.file}, reason ${error.reason}`);
    }
}).finally(() => {
    progressBar.stop();
    logger.i(`Done!`);
});
