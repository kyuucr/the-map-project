const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const filter = require('../lib/filter');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [filter]`);
    process.exit(1);
}
let inputFolder = ARGS[0];
let filterString;

if (ARGS[2]) {
    let jsonString = ARGS[2];
    if (jsonString.match(/\.json$/)) jsonString = fs.readFileSync(jsonString);
    filterString = JSON.parse(jsonString);
}

let pci = {}
agg.callbackJsonRecursive(inputFolder, data => {
    let curPath = data.path;
    let currJson = data.json;
    if (filterString) {
        currJson = filter.filterArray(filterString, currJson);
    }

    console.log(`path: ${curPath}, # data: ${currJson.length}`);

    for(let j = 0, length2 = currJson.length; j < length2; j++){
        let opName = dataUtils.getCleanOp(currJson[j]);
        if (pci[opName] === undefined) {
            pci[opName] = {};
        }
        for(let k = 0, length3 = currJson[j].cell_info.length; k < length3; k++){
            let currPci = currJson[j].cell_info[k].pci;
            let currBand = currJson[j].cell_info[k].band;
            if (pci[opName][currPci] === undefined) {
                pci[opName][currPci] = {};
                pci[opName][currPci][currBand] = 1;
            } else if (pci[opName][currPci][currBand] === undefined) {
                pci[opName][currPci][currBand] = 1;
            } else {
                pci[opName][currPci][currBand]++;
            }
        }
    }
});

let avgBandPerPci = {};
for (let opName in pci) {
    for (let currPci in pci[opName]) {
        let bands = Object.keys(pci[opName][currPci]);
        if (avgBandPerPci[opName] === undefined) {
            avgBandPerPci[opName] = {
                avg: bands.length,
                k: 1
            };
        } else {
            ++avgBandPerPci[opName].k;
            avgBandPerPci[opName].avg += (bands.length - avgBandPerPci[opName].avg) / avgBandPerPci[opName].k;
        }
    }
}

for (let opName in avgBandPerPci) {
    console.log(`${opName}: AVG ${avgBandPerPci[opName].avg}, K ${avgBandPerPci[opName].k}`);
}