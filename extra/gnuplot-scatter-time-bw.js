const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const prefix = ARGS[2] || "";

let entries = {};

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let op = dataUtils.getCleanOp(json[i]);
        if (entries[op] === undefined) entries[op] = `Time "Sum BW"\n`;
        entries[op] += `${dataUtils.getTime(json[i])} ${dataUtils.sumCellBw(json[i])}\n`;
    }
});

for (let op in entries) {
    fs.writeFileSync(path.join(outputPath, `${prefix}-scatter-time-bw-${op}.dat`), entries[op]);
}