const agg = require('../lib/aggregator');
const filter = require('../lib/filter');
const geohelper = require('../lib/geohelper');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output path] [max lte cell limit]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputPath = ARGS[1] || path.join("output-stats-sigcap-rsrp.csv");

let numTotalData = { indoor: 0, outdoor: 0, unknown: 0 };
let numOfLowerPrimaryRsrp = { indoor: 0, outdoor: 0, unknown: 0 };
let rsrpDiffBucket = { indoor: {}, outdoor: {}, unknown: {}};
let maxDiff = -9999;
let minDiff = 9999;

let sigcap = agg.callbackJsonRecursive(inputFolder, (dir) => {
    let path = dir.path;
    let sigcap = dir.json;
    console.log(`Processing ${path}; num: ${sigcap.length}`);
    let locationLabel = "unknown";
    if (path.includes("indoor")) {
        locationLabel = "indoor";
    } else if (path.includes("outdoor")) {
        locationLabel = "outdoor";
    }

    let numSkip = 0;
    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        let rsrpPrimary = -999;
        let maxRsrpOther = -999;

        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            if (dataUtils.isLtePrimary(sigcap[i].cell_info[j])) {
                rsrpPrimary = sigcap[i].cell_info[j].rsrp;
            } else if (sigcap[i].cell_info[j].rsrp > maxRsrpOther) {
                maxRsrpOther = sigcap[i].cell_info[j].rsrp;
            }
        }
        if (rsrpPrimary !== -999) {
            if (rsrpPrimary < maxRsrpOther) {
                numOfLowerPrimaryRsrp[locationLabel]++;
                let diff = maxRsrpOther - rsrpPrimary;
                if (diff > maxDiff) maxDiff = diff;
                if (diff < minDiff) minDiff = diff;
                if (rsrpDiffBucket[locationLabel][diff] === undefined) {
                    rsrpDiffBucket[locationLabel][diff] = 1;
                } else {
                    rsrpDiffBucket[locationLabel][diff]++;
                }
            }
        } else {
            numSkip++;
        }
    }

    numTotalData[locationLabel] += sigcap.length;
});

let os = fs.createWriteStream(outputPath);

let lowRsrpStat = `location_label,num_of_lower_primary_rsrp,num_total,ratio\n`;
let totalLower = 0;
let totalNum = 0;
for (let locationLabel of Object.keys(numTotalData)) {
    lowRsrpStat += `${locationLabel},${numOfLowerPrimaryRsrp[locationLabel]},${numTotalData[locationLabel]},${numOfLowerPrimaryRsrp[locationLabel]/numTotalData[locationLabel]}\n`;
    totalLower += numOfLowerPrimaryRsrp[locationLabel];
    totalNum += numTotalData[locationLabel];
}
lowRsrpStat += `total,${totalLower},${totalNum},${totalLower/totalNum}\n\n`;
os.write(lowRsrpStat);

let diffBucketStat = `diff(dB),indoor,outdoor,unknown,total\n`;
for (let i = minDiff; i <= maxDiff; i++) {
    let tempDiff = { indoor: 0, outdoor: 0, unknown: 0 };
    let diffTotal = 0;
    for (let locationLabel of Object.keys(rsrpDiffBucket)) {
        if (rsrpDiffBucket[locationLabel][i] !== undefined) {
            tempDiff[locationLabel] = rsrpDiffBucket[locationLabel][i];
            diffTotal += rsrpDiffBucket[locationLabel][i];
        }
    }
    diffBucketStat += `${i},${tempDiff.indoor},${tempDiff.outdoor},${tempDiff.unknown},${diffTotal}\n`;
}
os.write(diffBucketStat);
