const agg = require('../lib/aggregator');
const commandParser = require('../lib/command-parser');
const filter = require('../lib/filter');
const logger = require('../lib/logger');
const fs = require('fs');
const path = require('path');

// Process arguments
commandParser.addArgument("input dir", { isMandatory: true });
commandParser.addArgument("output dir", { isMandatory: true });
commandParser.addArgument("filter", { isMandatory: true });
const options = commandParser.parse();

const outDir = options["output dir"];
fs.mkdirSync(outDir, { recursive: true });

let filterStr = options["filter"];
let inputFilter = {};

if (fs.existsSync(filterStr)) {
    filterStr = fs.readFileSync(filterStr);
}
try {
    inputFilter = JSON.parse(filterStr);
} catch (err) {
    logger.e(`Filter parsing error!`, err);
    process.exit(1);
}

agg.callbackJsonRecursive(options["input dir"], data => {
    if (data.json.length === 0) {
        logger.w(`Skipping ${data.filePath[0]} since it is empty before filtered`);
        return;
    }
    let filtered = filter.filterArray(inputFilter, data.json);
    if (filtered.length === 0) {
        logger.w(`Skipping ${data.filePath[0]} since it is empty after filtered`);
        return;
    }

    fs.writeFileSync(path.join(outDir, path.basename(data.filePath[0])), JSON.stringify(filtered));
});

logger.i(`Done!`);