const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const usePlotlib = ARGS[1] === "plotlib";
const prefix = ARGS[2] || "";

let output = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (output[op] === undefined) output[op] = { all: [] };

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            if (json[i].cell_info[j].width > 0) {
                let band = json[i].cell_info[j].band;
                let bw = json[i].cell_info[j].width / 1000;

                if (output[op][band] === undefined) output[op][band] = [];
                if (!isNaN(bw)) {
                    output[op].all.push(bw);
                    output[op][band].push(bw);
                }
            }
        }
    }
});

let bwFig = {};
let bwAllFig = [];
let maxBW = -999;

for (let op in output) {
    if (op === "Unknown") continue;
    
    for (let band in output[op]) {
        output[op][band].sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxBW < output[op][band][output[op][band].length - 1]) maxBW = output[op][band][output[op][band].length - 1];
            if (bwFig[op] === undefined) {
                bwFig[op] = [];
            }
            if (band === "all") {
                bwAllFig.push({ x: output[op][band], y: dataUtils.createCdfY(output[op][band].length), name: `${op}`, type: "scatter" });
            } else {
                bwFig[op].push({ x: output[op][band], y: dataUtils.createCdfY(output[op][band].length), name: `B${band}`, type: "scatter" });
            }
        } else {
            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-primary-bw-${band}.dat`);
            let string = `"Cumulative distributive function" "BW MHz"\n`;

            for(let i = 0, length1 = output[op][band].length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][band][i]}\n`;
            }

            fs.writeFileSync(outputFile, string);
        }
    }
}

if (usePlotlib) {
    const plotlib = require('nodeplotlib');

    plotlib.stack(bwAllFig, {
        xaxis: {
            showgrid: true,
            title: 'Channel width (MHz)',
            range: [0, maxBW + 1]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Primary BW - All`,
        showlegend: true
    });
    for (let op in bwFig) {
        if (bwFig[op].length > 0) {
            let layout = {
                xaxis: {
                    showgrid: true,
                    title: 'Channel width (MHz)',
                    range: [0, maxBW + 1]
                },
                yaxis: {
                    showgrid: true,
                    title: 'Cumulative distribution function'
                },
                title: `Primary BW by Band - ${op}`,
                showlegend: true
            }
            plotlib.stack(bwFig[op], layout);
        }
    }
    plotlib.plot();
}