const aggregator = require('../lib/aggregator');
const commandParser = require('../lib/command-parser');
const dataUtils = require('../lib/data-utils');
const cellHelper = require('../lib/cell-helper');
const logger = require('../lib/logger');

// Process arguments
commandParser.addArgument("input files", { isMandatory: true });
// commandParser.addArgument("output metadata file", { isMandatory: true });
// commandParser.addArgument("duration", { keyword: "-d", defaultValue: 299000 });
const options = commandParser.parse();
const cellCount = {};

aggregator.callbackJsonRecursive(options["input files"], (data) => {
    if (data.type !== 'sigcap') {
        return;
    }

    const sigcap = data.json;
    for (const entry of sigcap) {
        const operator = dataUtils.getCleanOp(entry);
        const deviceName = entry.deviceName;
        if (cellCount[operator] === undefined) {
            cellCount[operator] = {};
        }
        if (cellCount[operator][deviceName] === undefined) {
            cellCount[operator][deviceName] = { nr: { "All": 0 }, nrSs: { "All": 0 }, lte: { "All": 0 }};
        }

        if (entry.nr_info && entry.nr_info.length > 0) {
            let nrCount = 0;
            let nrSsCount = 0;
            for (const nrEntry of entry.nr_info) {
                if (nrEntry.isSignalStrAPI) {
                    nrSsCount++;
                } else {
                    nrCount++;
                }
            }
            if (cellCount[operator][deviceName].nr[nrCount] === undefined) {
                cellCount[operator][deviceName].nr[nrCount] = 1;
            } else {
                cellCount[operator][deviceName].nr[nrCount]++;
            }
            cellCount[operator][deviceName].nr["All"]++;
            if (cellCount[operator][deviceName].nrSs[nrSsCount] === undefined) {
                cellCount[operator][deviceName].nrSs[nrSsCount] = 1;
            } else {
                cellCount[operator][deviceName].nrSs[nrSsCount]++;
            }
            cellCount[operator][deviceName].nrSs["All"]++;
        }

        const lteCount = entry.cell_info.length;
        if (cellCount[operator][deviceName].lte[lteCount] === undefined) {
            cellCount[operator][deviceName].lte[lteCount] = 1;
        } else {
            cellCount[operator][deviceName].lte[lteCount]++;
        }
        cellCount[operator][deviceName].lte["All"]++;
    }
});

logger.i(["Device Model", "Operator", "Type", "#", "Count", "Percentage"].join(';\t'));
for (const operator in cellCount) {
    for (const deviceName in cellCount[operator]) {
        for (const type in cellCount[operator][deviceName]) {
            for (const number in cellCount[operator][deviceName][type]) {
                if (number === "All") {
                    continue;
                }
                logger.i([deviceName, operator, type, number, cellCount[operator][deviceName][type][number], (cellCount[operator][deviceName][type][number] / cellCount[operator][deviceName][type]["All"] * 100).toFixed(2) + '%'].join(';\t'));
            }
        }
    }
}