const aggregator = require('../lib/aggregator');
const commandParser = require('../lib/command-parser');
const dataUtils = require('../lib/data-utils');
const logger = require('../lib/logger');
const filter = require('../lib/filter');
const { createHash } = require('crypto');

// Process arguments
commandParser.addArgument("input files", { isMandatory: true });
// commandParser.addArgument("output metadata file", { isMandatory: true });
commandParser.addArgument("filter", { keyword: "-f", defaultValue: null, transform: (input) =>{
    try {
        return JSON.parse(input);
    } catch (err) {
        logger.e(`Filter parsing error!`, err);
        process.exit(1);
    }
} });
const options = commandParser.parse();
const bucket = { "allYear": {
    "allBands": [],
    "2.4": [],
    "U-NII-1": [],
    "U-NII-2A": [],
    "U-NII-2B": [],
    "U-NII-2C": [],
    "U-NII-3": [],
    "U-NII-4": [],
    "U-NII-5": [],
    "U-NII-6": [],
    "U-NII-7": [],
    "U-NII-8": []
}};

const freqToUnii = function(freq) {
    if (freq < 5150) {
        return "2.4";
    } else if (freq >= 5150 && freq < 5250) {
        return "U-NII-1";
    } else if (freq >= 5250 && freq < 5350) {
        return "U-NII-2A";
    } else if (freq >= 5350 && freq < 5470) {
        return "U-NII-2B";
    } else if (freq >= 5470 && freq < 5725) {
        return "U-NII-2C";
    } else if (freq >= 5725 && freq < 5850) {
        return "U-NII-3";
    } else if (freq >= 5850 && freq < 5925) {
        return "U-NII-4";
    } else if (freq >= 5925 && freq < 6425) {
        return "U-NII-5";
    } else if (freq >= 6425 && freq < 6525) {
        return "U-NII-6";
    } else if (freq >= 6525 && freq < 6875) {
        return "U-NII-7";
    } else if (freq >= 6875 && freq < 7125) {
        return "U-NII-8";
    } else return undefined
}

aggregator.callbackJsonRecursive(options["input files"], (data) => {
    if (data.type !== 'sigcap') {
        return;
    }

    const sigcap = options["filter"] ? filter.filterArray(options["filter"], data.json) : data.json;
    for (const entry of sigcap) {
        let year = dataUtils.getCleanDatetime(entry).substring(0, 4);
        if (year === "2020" || year === "2021") {
            continue;
        }

        for (const wifi of entry.wifi_info) {
            let unii = freqToUnii(wifi.primaryFreq);
            logger.d(wifi.primaryFreq, unii)
            let bssid = wifi.bssid.length === 17 ? createHash("sha256").update(wifi.bssid).digest("hex").toUpperCase() : wifi.bssid;

            if (!bucket["allYear"]["allBands"].includes(bssid)) {
                bucket["allYear"]["allBands"].push(bssid);
            }
            if (!bucket["allYear"][unii].includes(bssid)) {
                bucket["allYear"][unii].push(bssid);
            }
            if (bucket[year] === undefined) {
                bucket[year] = {
                    "allBands": [],
                    "2.4": [],
                    "U-NII-1": [],
                    "U-NII-2A": [],
                    "U-NII-2B": [],
                    "U-NII-2C": [],
                    "U-NII-3": [],
                    "U-NII-4": [],
                    "U-NII-5": [],
                    "U-NII-6": [],
                    "U-NII-7": [],
                    "U-NII-8": []
                };
            }
            if (!bucket[year]["allBands"].includes(bssid)) {
                bucket[year]["allBands"].push(bssid);
            }
            if (!bucket[year][unii].includes(bssid)) {
                bucket[year][unii].push(bssid);
            }
        }
    }
});

logger.i(["Year", "Band", "Count", "Percentage"].join(';\t'));
for (const year in bucket) {
    let total = bucket[year]["allBands"].length;
    for (const unii in bucket[year]) {
        if (unii === "allBands") {
            continue;
        }
        let count = bucket[year][unii].length;
        logger.i([year, unii, count, (count / total)].join(";\t"));
    }
}
