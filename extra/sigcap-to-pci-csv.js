const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output path] [appearance filter]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputPath = ARGS[1] || path.join("output-sigcap.csv");
let filterAppear = parseFloat(ARGS[2] || 0);
console.log(`Input folder: ${inputFolder}`);
console.log(`Using appearance filter: ${filterAppear}`);

let sortWifiRssiDsc = (a,b) => {return b.rssi - a.rssi};
let sumWifiRssi = (a, b) => {return {rssi: a.rssi + b.rssi}};

let pciBandPairList = {};
let pciBandPairPrimary = {};
let maxWifi2_4Count = 0;
let maxWifi5Count = 0;

// Get SigCap
let sigcap = agg.getJson(inputFolder);

for(let i = 0, length1 = sigcap.length; i < length1; i++){
    // Get LTE PCIs
    for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
        let idx = `${sigcap[i].cell_info[j].pci}-${sigcap[i].cell_info[j].band}-${sigcap[i].cell_info[j].earfcn}`;
        if (pciBandPairList[idx] === undefined) {
            pciBandPairList[idx] = 1;
        } else {
            pciBandPairList[idx]++;
        }
        if (dataUtils.isLtePrimary(sigcap[i].cell_info[j])) {
            if (pciBandPairPrimary[idx] === undefined) {
                pciBandPairPrimary[idx] = 1;
            } else {
                pciBandPairPrimary[idx]++;
            }
        }
    }

    // Get max # of wifi
    let wifi2_4Count = 0;
    let wifi5Count = 0;
    for (let k = 0, length3 = sigcap[i].wifi_info.length; k < length3; k++){
        if (sigcap[i].wifi_info[k].primaryFreq >= 5000) {
            wifi5Count++;
        } else {
            wifi2_4Count++;
        }
    }
    if (maxWifi2_4Count < wifi2_4Count) {
        maxWifi2_4Count = wifi2_4Count;
    }
    if (maxWifi5Count < wifi5Count) {
        maxWifi5Count = wifi5Count;
    }
}

// Sort PCI list
let keys = Object.keys(pciBandPairList);
keys.sort();
let keysFiltered = [];

// Log statistics
console.log(`PCI-Band-EARFCN statistics`);
for(let i = 0, length1 = keys.length; i < length1; i++){
    let stats = pciBandPairList[keys[i]] / sigcap.length;
    let statsPrimary = (pciBandPairPrimary[keys[i]] ? pciBandPairPrimary[keys[i]] : 0) / sigcap.length;
    if (stats > filterAppear || statsPrimary > 0) {
        console.log(`${keys[i]}: ${stats.toFixed(3)}; primary: ${statsPrimary.toFixed(3)}${stats < 0.2 && statsPrimary > 0 ? " low stats primary" : ""}`);
        // Take the PCI-Band that are substantial
        keysFiltered.push(keys[i]);
    }
}

console.log(`Max number of WiFi 2.4 GHz: ${maxWifi2_4Count}`);
console.log(`Max number of WiFi 5 GHz: ${maxWifi5Count}`);

// Write csv
console.log(`Writing to: ${outputPath}`);
let os = fs.createWriteStream(outputPath);

// Write header
let header = `operator,sim_operator,carrier,timestamp,${csvUtils.unpackKeys(sigcap[0].location)}nrStatus,nrAvailable,dcNrRestricted,enDcAvailable,nrFrequencyRange,ci,cellBandwidths,sumBw,`;
header += `lte_primary_pci,lte_primary_band,lte_primary_earfcn,lte_primary_bandwidth,lte_primary_rsrp,lte_primary_rsrq,lte_primary_rssi,`;
for (pciBand of keysFiltered) {
    let temp = pciBand.split("-");
    header += `lte_pci_${temp[0]}_band_${temp[1]}_earfcn_${temp[2]}_rsrp,lte_pci_${temp[0]}_band_${temp[1]}_earfcn_${temp[2]}_rsrq,lte_pci_${temp[0]}_band_${temp[1]}_earfcn_${temp[2]}_rssi,`;
}
header += `nr_rsrp,nr_rsrq,`;
header += `num_of_wifi_2_4,avg_rssi_of_wifi_2_4,`;
for(let i = 1; i <= maxWifi2_4Count; i++){
    header += `${i}_wifi_2_4_freq,${i}_wifi_2_4_width,${i}_wifi_2_4_rssi,`;
}
header += `num_of_wifi_5,avg_rssi_of_wifi_5,`;
for(let i = 1; i <= maxWifi5Count; i++){
    header += `${i}_wifi_5_freq,${i}_wifi_5_width,${i}_wifi_5_rssi,`;
}
header += `\n`;
os.write(header);

for(let i = 0, length1 = sigcap.length; i < length1; i++){
    // Write overview
    let entry = `${sigcap[i].opName},${sigcap[i].simName},${sigcap[i].carrierName},${sigcap[i].datetimeIso},`
                + `${csvUtils.unpackVals(sigcap[i].location)}${sigcap[i].nrStatus},${sigcap[i].nrAvailable},`
                + `${sigcap[i].dcNrRestricted},${sigcap[i].enDcAvailable},${sigcap[i].nrFrequencyRange},`
                + `${sigcap[i].ci},"${sigcap[i].cellBandwidths}",${dataUtils.sumCellBw(sigcap[i])},`;

    // Get LTE primary
    let found = false;
    if (sigcap[i].cell_info.length > 0) {
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            if (dataUtils.isLtePrimary(sigcap[i].cell_info[j])) {
                found = true;
                // Write primary PCI-Band-EARFCN, bandwidth, rsrp, rsrq, rssi
                entry += `${sigcap[i].cell_info[j].pci},${sigcap[i].cell_info[j].band},${sigcap[i].cell_info[j].earfcn},${sigcap[i].cell_info[j].width},`;
                entry += `${(sigcap[i].cell_info[j].rsrp === 2147483647 ? "" : sigcap[i].cell_info[j].rsrp)},`
                        + `${(sigcap[i].cell_info[j].rsrq === 2147483647 ? "" : sigcap[i].cell_info[j].rsrq)},`
                        + `${(sigcap[i].cell_info[j].rssi === 2147483647 ? "" : sigcap[i].cell_info[j].rssi)},`;
                break;
            }
        }
    }
    if (!found) {
        entry += `0,0,0,0,,,,`;
    }

    // Get LTE PCIs
    for (pciBand of keysFiltered) {
        let found = false;
        for(let j = 0, length2 = sigcap[i].cell_info.length; j < length2; j++){
            let idx = `${sigcap[i].cell_info[j].pci}-${sigcap[i].cell_info[j].band}-${sigcap[i].cell_info[j].earfcn}`;
            if (idx === pciBand) {
                found = true;
                // Write rsrp, rsrq, rssi
                entry += `${(sigcap[i].cell_info[j].rsrp === 2147483647 ? "" : sigcap[i].cell_info[j].rsrp)},`
                        + `${(sigcap[i].cell_info[j].rsrq === 2147483647 ? "" : sigcap[i].cell_info[j].rsrq)},`
                        + `${(sigcap[i].cell_info[j].rssi === 2147483647 ? "" : sigcap[i].cell_info[j].rssi)},`;
                break;
            }
        }
        if (!found) {
            entry += `,,,`;
        }
    }

    // Get NR, assume only one NR cell
    if (sigcap[i].nr_info && sigcap[i].nr_info.length > 0) {
        entry += `${(sigcap[i].nr_info[0].ssRsrp === 2147483647 ? "" : sigcap[i].nr_info[0].ssRsrp)},`
                + `${(sigcap[i].nr_info[0].ssRsrq === 2147483647 ? "" : sigcap[i].nr_info[0].ssRsrq)},`
    } else {
        entry += `,,`;
    }

    // Get 2.4 and 5 Wifi
    let holder2 = [], holder5 = [];
    for (let k = 0, length3 = sigcap[i].wifi_info.length; k < length3; k++){
        if (sigcap[i].wifi_info[k].rssi === 2147483647) continue; // wrong value
        if (sigcap[i].wifi_info[k].primaryFreq < 5000) {
            holder2.push({freq: sigcap[i].wifi_info[k].centerFreq0, width: sigcap[i].wifi_info[k].width, rssi: sigcap[i].wifi_info[k].rssi});
        } else {
            holder5.push({freq: sigcap[i].wifi_info[k].centerFreq0, width: sigcap[i].wifi_info[k].width, rssi: sigcap[i].wifi_info[k].rssi});
        }
    }

    // Write Wifi 2.4
    holder2.sort(sortWifiRssiDsc);
    tempAvg = (holder2.length === 0) ? "" : (holder2.reduce(sumWifiRssi).rssi / holder2.length);
    entry += `${holder2.length},${tempAvg},`;
    let count = 0;
    for (let length3 = holder2.length; count < length3; count++) {
        entry += `${holder2[count].freq},${holder2[count].width},${holder2[count].rssi},`;
    }
    for (; count < maxWifi2_4Count; count++) {
        entry += `,,,`;
    }
    // Write Wifi 5
    holder5.sort(sortWifiRssiDsc);
    tempAvg = (holder5.length === 0) ? "" : (holder5.reduce(sumWifiRssi).rssi / holder5.length);
    entry += `${holder5.length},${tempAvg},`;
    count = 0;
    for (let length3 = holder5.length; count < length3; count++) {
        entry += `${holder5[count].freq},${holder5[count].width},${holder5[count].rssi},`;
    }
    for (; count < maxWifi5Count; count++) {
        entry += `,,,`;
    }

    // EOL and flush
    entry += `\n`;
    os.write(entry);
}

os.close();