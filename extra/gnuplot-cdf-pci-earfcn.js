const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const prefix = ARGS[2] || "";

let output = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (output[op] === undefined) output[op] = {};

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            let earfcn = json[i].cell_info[j].earfcn;
            let pci = json[i].cell_info[j].pci;
            let rsrp = dataUtils.cleanSignal(json[i].cell_info[j].rsrp);
            let rsrq = dataUtils.cleanSignal(json[i].cell_info[j].rsrq);
            let rssi = dataUtils.cleanSignal(json[i].cell_info[j].rssi);

            if (output[op][earfcn] === undefined) output[op][earfcn] = {};
            if (output[op][earfcn][pci] === undefined) { output[op][earfcn][pci] = { rsrp: [], rsrq: [], rssi: [] } };
            if (!isNaN(rsrp)) {
                output[op][earfcn][pci].rsrp.push(rsrp);
            }
            if (!isNaN(rsrq) && rsrq >= -20) {
                output[op][earfcn][pci].rsrq.push(rsrq);
            }
            if (!isNaN(rssi)) {
                output[op][earfcn][pci].rssi.push(rssi);
            }
        }
    }
});

for (let op in output) {
    for (let earfcn in output[op]) {
        for (let pci in output[op][earfcn]) {
            output[op][earfcn][pci].rsrp.sort(dataUtils.sortNumAsc);
            output[op][earfcn][pci].rsrq.sort(dataUtils.sortNumAsc);
            output[op][earfcn][pci].rssi.sort(dataUtils.sortNumAsc);

            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-rsrp-${earfcn}-${pci}.dat`);
            let string = `"Cumulative distributive function" "RSRP"\n`;

            for(let i = 0, length1 = output[op][earfcn][pci].rsrp.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][earfcn][pci].rsrp[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);

            outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-rsrq-${earfcn}-${pci}.dat`);
            string = `"Cumulative distributive function" "RSRQ"\n`;

            for(let i = 0, length1 = output[op][earfcn][pci].rsrq.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][earfcn][pci].rsrq[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);

            outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-rssi-${earfcn}-${pci}.dat`);
            string = `"Cumulative distributive function" "RSSI"\n`;

            for(let i = 0, length1 = output[op][earfcn][pci].rssi.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][earfcn][pci].rssi[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);
        }
    }
}