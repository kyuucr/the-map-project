const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const usePlotlib = ARGS[1] === "plotlib";
const prefix = ARGS[2] || "";

let output = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (output[op] === undefined) {
            output[op] = {
                nrNsaConnectedFr1: {
                    rsrp_bw: []
                },
                nrNsaConnectedFr2: {
                    rsrp_bw: []
                },
                nrSaConnectedFr1: {
                    rsrp_bw: []
                },
                nrSaConnectedFr2: {
                    rsrp_bw: []
                }
            };
        }

        if (json[i].nr_info) {
            for (let j = 0, length2 = json[i].nr_info.length; j < length2; j++) {
                let rsrp = dataUtils.cleanSignal(json[i].nr_info[j].ssRsrp);
                let nrStatus = dataUtils.getServiceState(json[i], "nrStatus");
                let nrSubtype = dataUtils.getNetworkType(json[i]);
                let nrFrequency = dataUtils.getServiceState(json[i], "nrFrequencyRange");
                // Determine bw based on op and FR
                let bw;
                if (nrStatus === "connected") {
                    switch (op) {
                        case "AT&T":
                            if (nrFrequency === "mmWave") {
                                bw = 100;
                            } else {
                                bw = 5;
                            }
                            break;
                        case "T-Mobile":
                            if (nrFrequency === "mmWave") {
                                // should not go here
                                continue;
                            } else {
                                bw = 20;
                            }
                            break;
                        case "Verizon":
                            if (nrFrequency === "mmWave") {
                                bw = 100;
                            } else {
                                bw = 10;
                            }
                            break;
                        default:
                            continue;
                    }

                    let rsrp_bw = rsrp + (10 * Math.log10(bw));

                    if (!isNaN(rsrp_bw)) {
                        if (nrFrequency === "mmWave") {
                            if (nrSubtype === "NR-NSA") {
                                output[op].nrNsaConnectedFr2.rsrp_bw.push(rsrp_bw);
                            } else {
                                output[op].nrSaConnectedFr2.rsrp_bw.push(rsrp_bw);
                            }
                        } else {
                            if (nrSubtype === "NR-NSA") {
                                output[op].nrNsaConnectedFr1.rsrp_bw.push(rsrp_bw);
                            } else {
                                output[op].nrSaConnectedFr1.rsrp_bw.push(rsrp_bw);
                            }
                        }
                    }
                }
            }
        }
    }
});

let rsrpFig = {};
let rsrpAllFig = [];
let maxRsrp = -999;

const typeString = {
    nrNsaConnectedFr1: "NR-NSA FR1",
    nrNsaConnectedFr2: "NR-NSA FR2",
    nrSaConnectedFr1: "NR-SA FR1",
    nrSaConnectedFr2: "NR-SA FR2",
}

for (let op in output) {
    for (let freq in output[op]) {
        output[op][freq].rsrp_bw.sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxRsrp < output[op][freq].rsrp_bw[output[op][freq].rsrp_bw.length - 1]) maxRsrp = output[op][freq].rsrp_bw[output[op][freq].rsrp_bw.length - 1];
            if (freq !== "nrNsaConnectedFr2")
            rsrpAllFig.push({ x: output[op][freq].rsrp_bw, y: dataUtils.createCdfY(output[op][freq].rsrp_bw.length), name: `${op} - ${typeString[freq]}`, type: "scatter" });
        } else {
            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-nr-connected-rsrp-bw-${freq}.dat`);
            let string = `"Cumulative distributive function" "NR RSRP scaled by BW"\n`;

            for(let i = 0, length1 = output[op][freq].rsrp_bw.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][freq].rsrp_bw[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);
        }
    }
}

if (usePlotlib) {
    const plotlib = require('nodeplotlib');

    plotlib.stack(rsrpAllFig, {
        xaxis: {
            showgrid: true,
            title: 'BW-Scaled NR RSRP (dB)',
            range: [-140, maxRsrp]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `BW-Scaled NR RSRP - All`,
        showlegend: true
    });
    plotlib.plot();
}