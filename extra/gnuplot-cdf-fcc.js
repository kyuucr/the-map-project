const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const prefix = ARGS[2] || "";

let output = {};
let testIdBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let testId = json[i].tests.testId;
        if (testId === undefined || testIdBin.includes(testId)) continue;
        testIdBin.push(testId);
        let op = dataUtils.getCleanOp(json[i]);

        // val === tput in dl and ul, or val === rtt in latency
        if (output[op] === undefined) {
            output[op] = {
                allServer: {
                    allTests: {
                        "Wi-Fi": { rsrp: [], rsrq: [], val: [] },
                        "LTE": { rsrp: [], rsrq: [] },
                        "NR": { rsrp: [], rsrq: [] },
                        "NRNSA": { rsrp: [], rsrq: [] }
                    }
                }
            };
        }

        for (let testName in json[i].tests) {
            if (testName === "testId") continue;
            let server = json[i].tests[testName].target;
            if (server === "") continue;
            if (output[op][server] === undefined) {
                output[op][server] = {
                    allTests: {
                        "Wi-Fi": { rsrp: [], rsrq: [], val: [] },
                        "LTE": { rsrp: [], rsrq: [] },
                        "NR": { rsrp: [], rsrq: [] },
                        "NRNSA": { rsrp: [], rsrq: [] }
                    }
                };
            }
            if (output[op][server][testName] === undefined) {
                output[op][server][testName] = {
                    allConn: { val: [] },
                    "Wi-Fi": { rsrp: [], rsrq: [], val: [] },
                    "LTE": { rsrp: [], rsrq: [], val: [] },
                    "NR": { rsrp: [], rsrq: [], val: [] },
                    "NRNSA": { rsrp: [], rsrq: [], val: [] }
                };
            }
            if (output[op].allServer[testName] === undefined) {
                output[op].allServer[testName] = {
                    allConn: { val: [] },
                    "Wi-Fi": { rsrp: [], rsrq: [], val: [] },
                    "LTE": { rsrp: [], rsrq: [], val: [] },
                    "NR": { rsrp: [], rsrq: [], val: [] },
                    "NRNSA": { rsrp: [], rsrq: [], val: [] }
                };
            }

            let val;
            switch (testName) {
                case "download":
                case "upload": {
                    val = json[i].tests[testName].throughput * 8 / 1e6; // Mbps
                    break;
                }
                case "latency": {
                    val = json[i].tests[testName].round_trip_time / 1e3; // millisec
                    break;
                }
                default: {
                    throw `Should no be reached! ${testName}`;
                }
            }

            let currConn;
            let isRsrpNan = false;
            let isRsrqNan = false;
            for (let cond in json[i].tests[testName].environment) {
                let conn = "Unknown";
                if (json[i].tests[testName].environment[cond].wifi) {
                    conn = "Wi-Fi";
                } else {
                    conn = dataUtils.getCleanValue(json[i].tests[testName].environment[cond].telephony, "cell_signal.network_type");
                    if (conn === undefined) {
                        conn = dataUtils.getCleanValue(json[i].tests[testName].environment[cond].network, "subtype_name");
                    }
                }
                let rsrp, rsrq;
                if (conn === "LTE") {
                    rsrp = dataUtils.cleanSignal(dataUtils.getCleanValue(json[i].tests[testName].environment[cond].telephony, "cell_signal.received_signal_power"));
                    rsrq = dataUtils.cleanSignal(dataUtils.getCleanValue(json[i].tests[testName].environment[cond].telephony, "cell_signal.received_signal_quality"));
                } else if (conn === "NR" || conn === "NRNSA") {
                    rsrp = dataUtils.cleanSignal(dataUtils.getCleanValue(json[i].tests[testName].environment[cond].telephony, "cell_signal.received_signal_power_ss"));
                    rsrq = dataUtils.cleanSignal(dataUtils.getCleanValue(json[i].tests[testName].environment[cond].telephony, "cell_signal.received_signal_quality_ss"));
                } else if (conn === "Wi-Fi") {
                    rsrp = dataUtils.cleanSignal(dataUtils.getCleanValue(json[i].tests[testName].environment[cond].wifi, "rssi"));
                    rsrq = NaN;
                } else {
                    continue;
                }
                if (!isNaN(rsrp)) {
                    output[op].allServer.allTests[conn].rsrp.push(rsrp);
                    output[op].allServer[testName][conn].rsrp.push(rsrp);
                    output[op][server].allTests[conn].rsrp.push(rsrp);
                    output[op][server][testName][conn].rsrp.push(rsrp);
                } else {
                    isRsrpNan = true;
                }
                if (!isNaN(rsrq) && rsrq >= -20) {
                    output[op].allServer.allTests[conn].rsrq.push(rsrq);
                    output[op].allServer[testName][conn].rsrq.push(rsrq);
                    output[op][server].allTests[conn].rsrq.push(rsrq);
                    output[op][server][testName][conn].rsrq.push(rsrq);
                } else {
                    isRsrqNan = true;
                }
                if (currConn === undefined) {
                    currConn = conn;
                } else if (currConn !== "mixed" && currConn !== conn) {
                    currConn = "mixed";
                }
            }

            if (currConn === undefined) {
                console.log (`Error: cannot find network type for ${json[i].tests.testId}`);
                continue;
            }

            if (!isNaN(val) && val > 0) {
                output[op].allServer[testName].allConn.val.push(val);
                output[op][server][testName].allConn.val.push(val);
                if ((!isRsrpNan && !isRsrqNan && currConn !== "mixed")
                    || (op === "T-Mobile" && currConn === "NR")
                    || (currConn === "Wi-Fi" && !isRsrpNan)) {
                    output[op].allServer[testName][currConn].val.push(val);
                    output[op][server][testName][currConn].val.push(val);
                }
            }
        }
    }
});

for (let op in output) {
    for (let server in output[op]) {
        for (let testName in output[op][server]) {
            for (let conn in output[op][server][testName]) {
                if (conn !== "allConn") {
                    if (output[op][server][testName][conn].rsrp.length > 0) {
                        output[op][server][testName][conn].rsrp.sort(dataUtils.sortNumAsc);

                        let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}fcc-${op}-cdf-primary-${conn === "Wi-Fi" ? "rssi" : "rsrp"}-${server}-${testName}-${conn}.dat`);
                        let string = `"Cumulative distributive function" "Primary ${conn === "Wi-Fi" ? "RSSI" : "RSRP"}, dBm"\n`;

                        if (output[op][server][testName][conn].rsrp.length === 1) {
                            output[op][server][testName][conn].rsrp.push(output[op][server][testName][conn].rsrp[0]);
                        }
                        for(let i = 0, length1 = output[op][server][testName][conn].rsrp.length; i < length1; i++){
                            string += `${i / (length1 - 1)} ${output[op][server][testName][conn].rsrp[i]}\n`;
                        }

                        fs.writeFileSync(outputFile, string);
                    }

                    if (output[op][server][testName][conn].rsrq.length > 0) {
                        output[op][server][testName][conn].rsrq.sort(dataUtils.sortNumAsc);

                        let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}fcc-${op}-cdf-primary-rsrq-${server}-${testName}-${conn}.dat`);
                        let string = `"Cumulative distributive function" "Primary RSRQ, dB"\n`;

                        if (output[op][server][testName][conn].rsrq.length === 1) {
                            output[op][server][testName][conn].rsrq.push(output[op][server][testName][conn].rsrq[0]);
                        }
                        for(let i = 0, length1 = output[op][server][testName][conn].rsrq.length; i < length1; i++){
                            string += `${i / (length1 - 1)} ${output[op][server][testName][conn].rsrq[i]}\n`;
                        }

                        fs.writeFileSync(outputFile, string);
                    }
                }
                if (testName !== "allTests") {
                    if (output[op][server][testName][conn].val.length > 0) {
                        output[op][server][testName][conn].val.sort(dataUtils.sortNumAsc);

                        outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}fcc-${op}-cdf-val-${server}-${testName}-${conn}.dat`);
                        let valLabel;
                        switch (testName) {
                            case "download":
                                valLabel = "DL Throughput, Mbps";
                                break;
                            case "upload":
                                valLabel = "UL Throughput, Mbps";
                                break;
                            case "latency":
                                valLabel = "Round trip time, millisec";
                                break;
                            default:
                                throw `Should not be reached: ${testName}`;
                        }
                        string = `"Cumulative distributive function" "${valLabel}"\n`;

                        if (output[op][server][testName][conn].val.length === 1) {
                            output[op][server][testName][conn].val.push(output[op][server][testName][conn].val[0]);
                        }
                        for(let i = 0, length1 = output[op][server][testName][conn].val.length; i < length1; i++){
                            string += `${i / (length1 - 1)} ${output[op][server][testName][conn].val[i]}\n`;
                        }

                        fs.writeFileSync(outputFile, string);
                    }
                }
            }
        }
    }
}