const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const statsUtils = require('../lib/stats-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <input CSVs> <output path>`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const pciFilter = [ "152","133","134","508","21","23","907","478","479","686","425","6","7","327","322","333" ];
const pciPair = [ [ "478", "479" ], [ "6", "7" ], [ "133", "134" ], [ "21", "23" ] ];
const pci28 = [ "133", "134", "21", "23" ];
const BBP_MAX_LAT = 41.87312981002079;
const BBP_MIN_LAT = 41.87164430310314;
const BBP_MAX_LON = -87.62077678119164;
const BBP_MIN_LON = -87.62222139742597;

let isPair = function(pair) {
    for (let i = 0, length1 = pciPair.length; i < length1; i++) {
        if (pciPair[i].includes(pair[0]) && pciPair[i].includes(pair[1])) {
            return true;
        }
    }
    return false;
}

let string = `Case,Beam Switch,,,Switch from mmWave BS to non-mmWave BS,,,Switch from non-mmWave BS to mmWave BS,,,Switch from mmWave BS to mmWave (s),,\n`;
string += `,Switch Interval,Mean Delay (s),Std.Dev Delay,Switch Interval,Mean Delay (s),Std.Dev Delay,Switch Interval,Mean Delay (s),Std.Dev Delay,Switch Interval,Mean Delay (s),Std.Dev Delay\n`;

agg.callbackCsvRecursive(dir, data => {
    let spans = [], delaysPciFrom = [], delaysPciTo = [], delaysPciFromTo = [], delaysBeam = [];
    let header = data.header;
    let csv = data.csv;
    let filename = path.basename(data.path).replace(/\.csv/, "");
    console.log(`${filename}, # of cols: ${data.header.length}, # of rows: ${data.csv.length}`);

    let colIds = dataUtils.getXCalHeader(header);
    let prevDatetime = new Date(0);
    let datetimeSpan = { start: null, end: null, pciSwitch: null, numOfBeamSwitch: 0 };

    for (let row of csv) {
        if (row[0] === "Count" || row.length === 1) break;
        let datetime = new Date(row[0]);
        if (row[colIds.PCell_BeamSwitch]) {
            let pcis = row[colIds.PCell_BeamSwitch].match(/pci(\d+)/g).map((el) => el.replace('pci', ''));
            if (pci28.includes(pcis[0]) || pci28.includes(pcis[1])) continue;
        }
        if (row[colIds.PCell_PCISwitch]) {
            let pcis = row[colIds.PCell_PCISwitch].match(/pci(\d+)/g).map((el) => el.replace('pci', ''));
            let type;
            if (pciFilter.includes(pcis[0]) && !pciFilter.includes(pcis[1])) {
                type = "mmToNon";
            } else if (pciFilter.includes(pcis[1]) && !pciFilter.includes(pcis[0])) {
                type = "nonToMm";
            } else if (pciFilter.includes(pcis[0]) && pciFilter.includes(pcis[1]) && !isPair(pcis)) {
                type = "mmToMm";
            }
            if (type !== undefined) {
                // new datetime span, save previous datetime span
                datetimeSpan.end = datetime;
                if (datetimeSpan.start) spans.push(datetimeSpan);
                datetimeSpan = { start: datetime, end: null, from: pcis[0], to: pcis[1], type: type, numOfBeamSwitch: 0 };
            }
        }
        if (row[colIds.PCell_BeamSwitch]) {
            // Check with current timespan
            let pcis = row[colIds.PCell_BeamSwitch].match(/pci(\d+)/g).map((el) => el.replace('pci', ''));
            if (datetimeSpan.to !== pcis[1]) {
                // PCI has changed without our knowledge, start new datetime span
                datetimeSpan.end = prevDatetime;
                if (datetimeSpan.start) spans.push(datetimeSpan);
                datetimeSpan = { start: datetime, end: null, from: null, to: pcis[1], type: "break", numOfBeamSwitch: 0 };
            }
            ++datetimeSpan.numOfBeamSwitch;
            prevDatetime = datetime;
        }
        if (row[colIds.PCell_BeamSwitchDelay]) {
            if (row[colIds.PCell_PCISwitch] && datetimeSpan.from !== datetimeSpan.to) {
                if (datetimeSpan.type === "mmToNon") {
                    delaysPciFrom.push(parseFloat(row[colIds.PCell_BeamSwitchDelay]));
                }
                if (datetimeSpan.type === "nonToMm") {
                    delaysPciTo.push(parseFloat(row[colIds.PCell_BeamSwitchDelay]));
                }
                if (datetimeSpan.type === "mmToMm") {
                    delaysPciFromTo.push(parseFloat(row[colIds.PCell_BeamSwitchDelay]));
                }
            } else if (row[colIds.PCell_BeamSwitch] && pciFilter.includes(datetimeSpan.to)) {
                delaysBeam.push(parseFloat(row[colIds.PCell_BeamSwitchDelay]));
            }
        }
    }
    datetimeSpan.end = prevDatetime;
    spans.push(datetimeSpan);

    let totalBeamSwitch = 0, totalBeamSeconds = 0, totalAllSeconds = 0;
    let numOfBeamSwitch = {}, numBeamSeconds = {};
    let totalPCISwitch = {};
    let secondsPCISwitch = {};
    let prevTo;
    for (let span of spans) {
        let spanSeconds = (span.end.getTime() - span.start.getTime()) / 1e3;
        if (span)
        console.log(`Duration: ${spanSeconds}, # of Beam switch: ${span.numOfBeamSwitch}`);
        if (span.numOfBeamSwitch > 1 && pciFilter.includes(span.to)) {
            totalBeamSwitch += span.numOfBeamSwitch;
            totalBeamSeconds += spanSeconds;
        }
        if (prevTo !== undefined && prevTo !== span.from) {
            console.log(`PCI switch not matching: ${prevTo}, ${span.from}`)
        } else {
            console.log(`PCI match: ${prevTo} => ${span.to}`)
        }
        prevTo = span.to;
        if (totalPCISwitch[span.type] === undefined) totalPCISwitch[span.type] = 0;
        ++totalPCISwitch[span.type];
        if (secondsPCISwitch[span.type] === undefined) secondsPCISwitch[span.type] = 0;
        secondsPCISwitch[span.type] += spanSeconds;
        // totalAllSeconds += spanSeconds;
    }
    string += `${filename},${(totalBeamSeconds / totalBeamSwitch).toFixed(3)} s,`;
    string += `${statsUtils.meanArray(delaysBeam).toFixed(3)},${statsUtils.stdDevArray(delaysBeam).toFixed(3)},`;
    string += `${(secondsPCISwitch.mmToNon / totalPCISwitch.mmToNon).toFixed(3)} s,`;
    string += `${statsUtils.meanArray(delaysPciFrom).toFixed(3)},${statsUtils.stdDevArray(delaysPciFrom).toFixed(3)},`;
    string += `${(secondsPCISwitch.nonToMm / totalPCISwitch.nonToMm).toFixed(3)} s,`;
    string += `${statsUtils.meanArray(delaysPciTo).toFixed(3)},${statsUtils.stdDevArray(delaysPciTo).toFixed(3)},`;
    string += `${(secondsPCISwitch.mmToMm / totalPCISwitch.mmToMm).toFixed(3)} s,`;
    string += `${statsUtils.meanArray(delaysPciFromTo).toFixed(3)},${statsUtils.stdDevArray(delaysPciFromTo).toFixed(3)}\n`;
});

fs.writeFileSync(outputPath, string);
