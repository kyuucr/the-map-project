const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [ssid-filter] [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const usePlotlib = ARGS[1] === "plotlib";
const ssidFilter = (ARGS[2] ? new RegExp(ARGS[2]) : null);
const prefix = ARGS[3] || "";

let outputRssi = { all: { "2.4 GHz": [], "5 GHz": []} };
let outputBW = { all: { "2.4 GHz": [], "5 GHz": []} };
let outputConnRssi = { all: { "2.4 GHz": [], "5 GHz": []} };
let outputConnBW = { all: { "2.4 GHz": [], "5 GHz": []} };
let outputConnTXLink = { all: { "2.4 GHz": [], "5 GHz": []} };
let outputConnRXLink = { all: { "2.4 GHz": [], "5 GHz": []} };
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].wifi_info.length === 0) continue;

        for (let j = 0, length2 = json[i].wifi_info.length; j < length2; j++) {
            let ssid = json[i].wifi_info[j].ssid ? json[i].wifi_info[j].ssid : json[i].wifi_info[j].bssid;
            // Filtering
            if (ssidFilter && !ssid.match(ssidFilter)) continue;

            let rssi = dataUtils.cleanSignal(json[i].wifi_info[j].rssi);
            let width = json[i].wifi_info[j].width;
            let chNum = json[i].wifi_info[j].channelNum;
            let band = (chNum > 30 ? "5 GHz" : "2.4 GHz");

            if (!isNaN(rssi) && width) {
                outputRssi.all[band].push(rssi);
                if (outputRssi[ssid] === undefined) outputRssi[ssid] = { "2.4 GHz": [], "5 GHz": [] };
                outputRssi[ssid][band].push(rssi);
                if (outputRssi.all[chNum] === undefined) outputRssi.all[chNum] = [];
                outputRssi.all[chNum].push(rssi);
                if (outputRssi[ssid][chNum] === undefined) outputRssi[ssid][chNum] = [];
                outputRssi[ssid][chNum].push(rssi);

                outputBW.all[band].push(width);
                if (outputBW[ssid] === undefined) outputBW[ssid] = { "2.4 GHz": [], "5 GHz": [] };
                outputBW[ssid][band].push(width);
                if (outputBW.all[chNum] === undefined) outputBW.all[chNum] = [];
                outputBW.all[chNum].push(width);
                if (outputBW[ssid][chNum] === undefined) outputBW[ssid][chNum] = [];
                outputBW[ssid][chNum].push(width);

                if (json[i].wifi_info[j].connected) {
                    outputConnRssi.all[band].push(rssi);
                    if (outputConnRssi[ssid] === undefined) outputConnRssi[ssid] = { "2.4 GHz": [], "5 GHz": [] };
                    outputConnRssi[ssid][band].push(rssi);
                    if (outputConnRssi.all[chNum] === undefined) outputConnRssi.all[chNum] = [];
                    outputConnRssi.all[chNum].push(rssi);
                    if (outputConnRssi[ssid][chNum] === undefined) outputConnRssi[ssid][chNum] = [];
                    outputConnRssi[ssid][chNum].push(rssi);

                    outputConnBW.all[band].push(width);
                    if (outputConnBW[ssid] === undefined) outputConnBW[ssid] = { "2.4 GHz": [], "5 GHz": [] };
                    outputConnBW[ssid][band].push(width);
                    if (outputConnBW.all[chNum] === undefined) outputConnBW.all[chNum] = [];
                    outputConnBW.all[chNum].push(width);
                    if (outputConnBW[ssid][chNum] === undefined) outputConnBW[ssid][chNum] = [];
                    outputConnBW[ssid][chNum].push(width);

                    let txLink = json[i].wifi_info[j].txLinkSpeed;
                    if (txLink > 0) {
                        outputConnTXLink.all[band].push(txLink);
                        if (outputConnTXLink[ssid] === undefined) outputConnTXLink[ssid] = { "2.4 GHz": [], "5 GHz": [] };
                        outputConnTXLink[ssid][band].push(txLink);
                        if (outputConnTXLink.all[chNum] === undefined) outputConnTXLink.all[chNum] = [];
                        outputConnTXLink.all[chNum].push(txLink);
                        if (outputConnTXLink[ssid][chNum] === undefined) outputConnTXLink[ssid][chNum] = [];
                        outputConnTXLink[ssid][chNum].push(txLink);
                    }

                    let rxLink = json[i].wifi_info[j].rxLinkSpeed;
                    if (rxLink > 0) {
                        outputConnRXLink.all[band].push(rxLink);
                        if (outputConnRXLink[ssid] === undefined) outputConnRXLink[ssid] = { "2.4 GHz": [], "5 GHz": [] };
                        outputConnRXLink[ssid][band].push(rxLink);
                        if (outputConnRXLink.all[chNum] === undefined) outputConnRXLink.all[chNum] = [];
                        outputConnRXLink.all[chNum].push(rxLink);
                        if (outputConnRXLink[ssid][chNum] === undefined) outputConnRXLink[ssid][chNum] = [];
                        outputConnRXLink[ssid][chNum].push(rxLink);
                    }
                }
            }
        }
    }
});

let rssiAllFig = [];
let rssiConnAllFig = [];
let bwAllFig = [];
let bwConnAllFig = [];
let txConnAllFig = [];
let rxConnAllFig = [];
let maxRssi = -999;
let maxBW = -999;
let maxLink = -999;

for (let ssid in outputRssi) {
    for (let chNum in outputRssi[ssid]) {
        outputRssi[ssid][chNum].sort(dataUtils.sortNumAsc);
        outputBW[ssid][chNum].sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxRssi < outputRssi[ssid][chNum][outputRssi[ssid][chNum].length - 1]) maxRssi = outputRssi[ssid][chNum][outputRssi[ssid][chNum].length - 1];
            if (maxBW < outputBW[ssid][chNum][outputBW[ssid][chNum].length - 1]) maxBW = outputBW[ssid][chNum][outputBW[ssid][chNum].length - 1];
            if (ssid === "all" && (chNum === "2.4 GHz" || chNum === "5 GHz")) {
                rssiAllFig.push({ x: outputRssi[ssid][chNum], y: dataUtils.createCdfY(outputRssi[ssid][chNum].length), name: `${chNum}`, type: "scatter" });
                bwAllFig.push({ x: outputBW[ssid][chNum], y: dataUtils.createCdfY(outputBW[ssid][chNum].length), name: `${chNum}`, type: "scatter" });
            }

        } else {
            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-cdf-rssi-${chNum}.dat`);
            let string = `"Cumulative distributive function" "RSSI, dBm"\n`;

            for(let i = 0, length1 = outputRssi[ssid][chNum].length; i < length1; i++){
                string += `${i / (length1 - 1)} ${outputRssi[ssid][chNum][i]}\n`;
            }

            fs.writeFileSync(outputFile, string);
        }
    }
}

for (let ssid in outputConnRssi) {
    for (let chNum in outputConnRssi[ssid]) {
        outputConnRssi[ssid][chNum].sort(dataUtils.sortNumAsc);
        outputConnBW[ssid][chNum].sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxRssi < outputConnRssi[ssid][chNum][outputConnRssi[ssid][chNum].length - 1]) maxRssi = outputConnRssi[ssid][chNum][outputConnRssi[ssid][chNum].length - 1];
            if (maxBW < outputConnBW[ssid][chNum][outputConnBW[ssid][chNum].length - 1]) maxBW = outputConnBW[ssid][chNum][outputConnBW[ssid][chNum].length - 1];
            if (ssid === "all" && (chNum === "2.4 GHz" || chNum === "5 GHz")) {
                rssiConnAllFig.push({ x: outputConnRssi[ssid][chNum], y: dataUtils.createCdfY(outputConnRssi[ssid][chNum].length), name: `${chNum}`, type: "scatter" });
                bwConnAllFig.push({ x: outputConnBW[ssid][chNum], y: dataUtils.createCdfY(outputConnBW[ssid][chNum].length), name: `${chNum}`, type: "scatter" });
            }


        } else {
            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-cdf-rssi-${chNum}.dat`);
            let string = `"Cumulative distributive function" "RSSI, dBm"\n`;

            for(let i = 0, length1 = outputConnRssi[ssid][chNum].length; i < length1; i++){
                string += `${i / (length1 - 1)} ${outputConnRssi[ssid][chNum][i]}\n`;
            }

            fs.writeFileSync(outputFile, string);
        }
    }
}

for (let ssid in outputConnTXLink) {
    for (let chNum in outputConnTXLink[ssid]) {
        outputConnTXLink[ssid][chNum].sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxLink < outputConnTXLink[ssid][chNum][outputConnTXLink[ssid][chNum].length - 1]) maxLink = outputConnTXLink[ssid][chNum][outputConnTXLink[ssid][chNum].length - 1];
            if (ssid === "all" && (chNum === "2.4 GHz" || chNum === "5 GHz")) {
                txConnAllFig.push({ x: outputConnTXLink[ssid][chNum], y: dataUtils.createCdfY(outputConnTXLink[ssid][chNum].length), name: `${chNum}`, type: "scatter" });
            }
        } else {
            // let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-cdf-rssi-${chNum}.dat`);
            // let string = `"Cumulative distributive function" "RSSI, dBm"\n`;

            // for(let i = 0, length1 = outputConnRssi[ssid][chNum].length; i < length1; i++){
            //     string += `${i / (length1 - 1)} ${outputConnRssi[ssid][chNum][i]}\n`;
            // }

            // fs.writeFileSync(outputFile, string);
        }
    }
}

for (let ssid in outputConnRXLink) {
    for (let chNum in outputConnRXLink[ssid]) {
        outputConnRXLink[ssid][chNum].sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxLink < outputConnRXLink[ssid][chNum][outputConnRXLink[ssid][chNum].length - 1]) maxLink = outputConnRXLink[ssid][chNum][outputConnRXLink[ssid][chNum].length - 1];
            if (ssid === "all" && (chNum === "2.4 GHz" || chNum === "5 GHz")) {
                rxConnAllFig.push({ x: outputConnRXLink[ssid][chNum], y: dataUtils.createCdfY(outputConnRXLink[ssid][chNum].length), name: `${chNum}`, type: "scatter" });
            }
        } else {
            // let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-cdf-rssi-${chNum}.dat`);
            // let string = `"Cumulative distributive function" "RSSI, dBm"\n`;

            // for(let i = 0, length1 = outputConnRssi[ssid][chNum].length; i < length1; i++){
            //     string += `${i / (length1 - 1)} ${outputConnRssi[ssid][chNum][i]}\n`;
            // }

            // fs.writeFileSync(outputFile, string);
        }
    }
}

if (usePlotlib) {
    const plotlib = require('nodeplotlib');

    plotlib.stack(rssiAllFig, {
        xaxis: {
            showgrid: true,
            title: 'RSSI (dBm)',
            range: [-140, maxRssi]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Wi-Fi RSSI - All`,
        showlegend: true
    });
    plotlib.stack(rssiConnAllFig, {
        xaxis: {
            showgrid: true,
            title: 'RSSI (dBm)',
            range: [-140, maxRssi]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Wi-Fi RSSI - Connected APs`,
        showlegend: true
    });
    plotlib.stack(bwAllFig, {
        xaxis: {
            showgrid: true,
            title: 'Channel width (MHz)',
            range: [0, maxBW+5]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Wi-Fi BW - All`,
        showlegend: true
    });
    plotlib.stack(bwConnAllFig, {
        xaxis: {
            showgrid: true,
            title: 'Channel width (MHz)',
            range: [0, maxBW+5]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Wi-Fi BW - Connected APs`,
        showlegend: true
    });
    plotlib.stack(txConnAllFig, {
        xaxis: {
            showgrid: true,
            title: 'Link Speed (Mbps)',
            range: [0, maxLink]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Wi-Fi TX Link Speed - Connected APs`,
        showlegend: true
    });
    plotlib.stack(rxConnAllFig, {
        xaxis: {
            showgrid: true,
            title: 'Link Speed (Mbps)',
            range: [0, maxLink]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        title: `Wi-Fi RX Link Speed - Connected APs`,
        showlegend: true
    });

    plotlib.plot();
}