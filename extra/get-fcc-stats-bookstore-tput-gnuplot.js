const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> <output path>`);
    process.exit(0);
}

let dir = ARGS[0];
let outputPath = ARGS [1];

let sortNumAsc = (a, b) => a - b;
let avgReduce = (avg, value, _, { length }) => avg + value / length;

let all = {};
// let clientStats = {};
// let serverStats = {};

agg.callbackJsonRecursive(dir, data => {
    if (!data.path.match(/\/fcc-/)) return;
    if (all[data.path] === undefined) {
        all[data.path] = [];
        // clientStats[data.path] = {};
        // serverStats[data.path] = {};
    }
    let recs = data.json;
    for (let i = 0, length1 = recs.length; i < length1; i++) {
        // let clientId = recs[i].zipFilename || "unknown";
        // let serverId = (recs[i].tests && recs[i].tests.download) ? recs[i].tests.download.target : "unknown";
        let dlTput = (recs[i].tests && recs[i].tests.download) ? (recs[i].tests.download.throughput * 8 / 1e6) : 0;
        // if (!clientIds.includes(clientId)) clientIds.push(clientId);

        all[data.path].push(dlTput);
        // if (clientStats[data.path][clientId] === undefined) clientStats[data.path][clientId] = [];
        // clientStats[data.path][clientId].push(dlTput);
        // if (serverStats[data.path][serverId] === undefined) serverStats[data.path][serverId] = [];
        // serverStats[data.path][serverId].push(dlTput);
    }
});

for (const currPath in all) {
    let dirName = currPath.split("/").pop();
    let os = fs.createWriteStream(path.join(outputPath, `${dirName}.csv`));

    let metadataPath = currPath.replace("/data/", "/metadata/metadata-") + ".json";
    let metadataDesc = JSON.parse(fs.readFileSync(metadataPath)).description.split(" 1-LAA ");
    let label = "Wi-Fi";
    let location = "Unknown";
    if (currPath.includes("closelaa")) {
        location = "Close to LAA BS";
    } else if (currPath.includes("closewifi")) {
        location = "Close to Wi-Fi AP";
    } else if (currPath.includes("center")) {
        location = "Center";
    }
    if (metadataDesc.length > 1) {
        label = metadataDesc.pop();
    }
    // console.log(label);
    all[currPath].sort(sortNumAsc);

    let output = "";
    for (data of all[currPath]) {
        output += `"${location}" ${label} ${data}\n`;
    }
    os.write(output);
    os.close();
}

