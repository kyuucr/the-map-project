const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder>`);
    process.exit(1);
}
const inputFolder = ARGS[0];

const filenames = fs.readdirSync(inputFolder);
filenames.forEach(function(filename) {
    if (filename.match(/\.(json|txt)$/)) {
        let filepath = path.join(inputFolder, filename);
        fs.readFile(filepath, (err, data) => {
            if (err) return;
            try {
                let json = JSON.parse(data.toString());
                if (json.nrFrequencyRange === undefined
                    && json.nr_info
                    && json.nr_info.length > 0) {
                    json.nrFrequencyRange = "mmWave";
                    fs.writeFile(filepath, JSON.stringify(json), (err) =>{
                        if (err) {
                            console.log(filename, err);
                        }
                    });
                }
            } catch(e) {
                console.log(filename, e);
            }
        })
    }
});