const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input json file/folder> [output path CSV]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputPath = ARGS[1] || path.join("output.csv");
console.log(`Input folder: ${inputFolder}`);

console.log(`Writing to: ${outputPath}`);
let os = fs.createWriteStream(outputPath);
let isHeader = true;

// Get SigCap using callback since we can have data chunks of 140 MB file
agg.callbackJsonRecursive(inputFolder, data => {
    let json = data.json;
    console.log(`path: ${data.path}, # of data: ${json.length}`);

    let entry = "";
    // Write header
    if (isHeader) {
        entry += `${csvUtils.unpackKeys(json[0])}\n`;
        isHeader = false;
    }

    // Write csv
    for(let i = 0, length1 = json.length; i < length1; i++){
        entry += `${csvUtils.unpackVals(json[i], true)}\n`;
    }
    os.write(entry);
});

os.close();
