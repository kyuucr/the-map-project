const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> [output csv] [--only-connected]`);
    process.exit(0);
}

let onlyConnected = false;
let outputPath = "wifi-param-stats.csv";
let inputFolder = args[0];
let removeZero = false;
args.splice(0, 1);
while (args.length > 0) {
    switch (args[0]) {
        case "--only-connected": {
            onlyConnected = true;
            args.splice(0, 1);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}

let sortNumAsc = (a, b) => a - b;
let avgReduce = (avg, value, _, { length }) => avg + value / length;

let output = {};
let allValues = { width: [], freq: [], primaryChNum: [] };

const updateSsid = function (ssid, entry) {
    let currFreq = entry.centerFreq0 ? entry.centerFreq0 : entry.primaryFreq;
    let currBssid = entry.bssid;
    if (!allValues.width.includes(entry.width)) {
        allValues.width.push(entry.width);
    }
    if (!allValues.primaryChNum.includes(entry.primaryChNum)) {
        allValues.primaryChNum.push(entry.primaryChNum);
    }
    if (!allValues.freq.includes(currFreq)) {
        allValues.freq.push(currFreq);
    }

    if (output[ssid] === undefined) {
        let currWidth = entry.width;
        let currPrimaryChNum = entry.primaryChNum;
        output[ssid] = {
            bssid: [ currBssid ],
            width: {},
            freq: {},
            primaryChNum: {},
        };
        output[ssid].width[currWidth] = [ currBssid ];
        output[ssid].freq[currFreq] = [ currBssid ];
        output[ssid].primaryChNum[currPrimaryChNum] = [ currBssid ];
    } else {
        for (let key in output[ssid]) {
            let currValue;
            if (key === "freq") {
                currValue = currFreq;
            } else {
                currValue = entry[key];
            }
            if (key === "bssid") {
                if (!output[ssid][key].includes(currBssid)) {
                    output[ssid][key].push(currBssid);
                }
            } else {
                if (output[ssid][key][currValue] === undefined) {
                    output[ssid][key][currValue] = [ currBssid ];
                } else if (!output[ssid][key][currValue].includes(currBssid)) {
                    output[ssid][key][currValue].push(currBssid);
                }
            }
        }
    }
}

agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;

    let sigcap = data.json;
    for (let i = 0, length1 = sigcap.length; i < length1; i++) {
        for (let j = 0, length2 = sigcap[i].wifi_info.length; j < length2; j++) {
            if (!onlyConnected || sigcap[i].wifi_info[j].connected) {
                let ssid = sigcap[i].wifi_info[j].ssid.replace(/(^['"]|['"]$)/g, "");
                if (ssid === "") {
                    ssid = "<empty>";
                }
                updateSsid("<all combined>", sigcap[i].wifi_info[j]);
                updateSsid(ssid, sigcap[i].wifi_info[j]);
            }
        }
    }
});

let outString = "# of unique BSSID\nSSID,All,";
for (let key in allValues) {
    allValues[key].sort(sortNumAsc);
    for (let val of allValues[key]) {
        if (val === 0) {
            // allValues[key].splice(allValues[key].indexOf(val), 1);
            continue;
        } else {
            outString += `${key} ${val},`;
        }
    }
}
outString += `\n`;

for (let ssid in output) {
    outString += `"${ssid}",${output[ssid].bssid.length},`;
    for (let key in allValues) {
        for (let value of allValues[key]) {
            if (value === 0) {
                continue;
            }
            if (output[ssid][key][value] === undefined) {
                outString += `0,`;
            } else {
                outString += `${output[ssid][key][value].length},`;
            }
        }
    }
    outString += "\n";
}

fs.writeFileSync(outputPath, outString);
