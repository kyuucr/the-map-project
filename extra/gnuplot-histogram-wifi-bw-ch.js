const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const prefix = ARGS[2] || "";

let outputBw = { all: {} };
let outputCh = { all: {} };
let outputPrimaryCh = { all: {} };
let hashBin = [];
let ssidInfo = {};

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].wifi_info.length === 0) continue;

        for (let j = 0, length2 = json[i].wifi_info.length; j < length2; j++) {
            let ssid = json[i].wifi_info[j].ssid ? json[i].wifi_info[j].ssid : json[i].wifi_info[j].bssid;
            let bssid = json[i].wifi_info[j].bssid;

            if (ssidInfo[ssid] === undefined) ssidInfo[ssid] = {};
            if (ssidInfo[ssid][bssid] === undefined) ssidInfo[ssid][bssid] = { bw: [], ch: [], primaryCh: [] };

            let bw = json[i].wifi_info[j].width;
            if (bw > 0) {
                if (outputBw[ssid] === undefined) outputBw[ssid] = {};
                if (outputBw[ssid][bw] === undefined) outputBw[ssid][bw] = [];
                if (!outputBw[ssid][bw].includes(bssid)) outputBw[ssid][bw].push(bssid);
                if (outputBw.all[bw] === undefined) outputBw.all[bw] = [];
                if (!outputBw.all[bw].includes(bssid)) outputBw.all[bw].push(bssid);

                if (!ssidInfo[ssid][bssid].bw.includes(bw)) ssidInfo[ssid][bssid].bw.push(bw);
            }

            let chNum = json[i].wifi_info[j].channelNum;
            if (chNum > 0) {
                if (outputCh[ssid] === undefined) outputCh[ssid] = {};
                if (outputCh[ssid][chNum] === undefined) outputCh[ssid][chNum] = [];
                if (!outputCh[ssid][chNum].includes(bssid)) outputCh[ssid][chNum].push(bssid);
                if (outputCh.all[chNum] === undefined) outputCh.all[chNum] = [];
                if (!outputCh.all[chNum].includes(bssid)) outputCh.all[chNum].push(bssid);

                if (!ssidInfo[ssid][bssid].ch.includes(chNum)) ssidInfo[ssid][bssid].ch.push(chNum);
            }

            let primaryChNum = json[i].wifi_info[j].primaryChNum;
            if (primaryChNum > 0) {
                if (outputPrimaryCh[ssid] === undefined) outputPrimaryCh[ssid] = {};
                if (outputPrimaryCh[ssid][primaryChNum] === undefined) outputPrimaryCh[ssid][primaryChNum] = [];
                if (!outputPrimaryCh[ssid][primaryChNum].includes(bssid)) outputPrimaryCh[ssid][primaryChNum].push(bssid);
                if (outputPrimaryCh.all[primaryChNum] === undefined) outputPrimaryCh.all[primaryChNum] = [];
                if (!outputPrimaryCh.all[primaryChNum].includes(bssid)) outputPrimaryCh.all[primaryChNum].push(bssid);

                if (!ssidInfo[ssid][bssid].primaryCh.includes(primaryChNum)) ssidInfo[ssid][bssid].primaryCh.push(primaryChNum);
            }
        }
    }
});

for (let ssid in outputBw) {
    let string = `"Wi-Fi SSID" "BW (MHz)" "# of unique BSSID"\n`;
    for (let bw in outputBw[ssid]) {
        string += `"${ssid}" ${bw} ${outputBw[ssid][bw].length}\n`;
    }
    let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-histogram-wifi-bw.dat`);
    fs.writeFileSync(outputFile, string);
}

for (let ssid in outputCh) {
    string = `"Wi-Fi SSID" "Channel number" "# of unique BSSID"\n`;
    for (let ch in outputCh[ssid]) {
        string += `"${ssid}" ${ch} ${outputCh[ssid][ch].length}\n`;
    }
    outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-histogram-wifi-ch.dat`);
    fs.writeFileSync(outputFile, string);
}

for (let ssid in outputPrimaryCh) {
    string = `"Wi-Fi SSID" "Primary channel number" "# of unique BSSID"\n`;
    for (let ch in outputPrimaryCh[ssid]) {
        string += `"${ssid}" ${ch} ${outputPrimaryCh[ssid][ch].length}\n`;
    }
    outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-${ssid}-histogram-wifi-primary-ch.dat`);
    fs.writeFileSync(outputFile, string);
}

string = `"Wi-Fi SSID","Wi-Fi BSSID","BW","Ch numbers","primary ch nums"\n`;
for (let ssid in ssidInfo) {
    for (let bssid in ssidInfo[ssid]) {
        string += `"${ssid}",${bssid},"${ssidInfo[ssid][bssid].bw.join(",")}","${ssidInfo[ssid][bssid].ch.join(",")}","${ssidInfo[ssid][bssid].primaryCh.join(",")}"\n`
    }
}
outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}ssid-info.csv`);
fs.writeFileSync(outputFile, string);
