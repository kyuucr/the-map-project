const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const filter = require('../lib/filter');
const fs = require('fs');
const path = require('path');
const plotlib = require('nodeplotlib');

const TEST_LIST = [ "download", "upload", "latency" ];
let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> [--zipFn] [--merge] [--filter <filter JSON string>]`);
    process.exit(1);
}


let outputPath = "plotlib";
let inputFolder = args[0];
let zipFn = false;
let merge = false;
args.splice(0, 1);
let inputFilter;
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        case "--zipFn": {
            zipFn = true;
            args.splice(0, 1);
            break;
        }
        case "--merge": {
            merge = true;
            args.splice(0, 1);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}


let getRsrp = function(telephony) {
    if (telephony && telephony.cell_signal && telephony.cell_signal.network_type) {
        if (telephony.cell_signal.network_type === "LTE") {
            return getKeyValue(telephony.cell_signal, "received_signal_power");
        } else if (telephony.cell_signal.network_type === "NR" || telephony.cell_signal.network_type === "NRNSA") {
            return getKeyValue(telephony.cell_signal, "received_signal_power_ss");
        } else {
            return getKeyValue(telephony.cell_signal, "signal_strength");
        }
    }
    return "N/A";
}

let getRsrq = function(telephony) {
    if (telephony && telephony.cell_signal && telephony.cell_signal.network_type) {
        if (telephony.cell_signal.network_type === "LTE") {
            return getKeyValue(telephony.cell_signal, "received_signal_quality");
        } else if (telephony.cell_signal.network_type === "NR" || telephony.cell_signal.network_type === "NRNSA") {
            return getKeyValue(telephony.cell_signal, "received_signal_quality_ss");
        }
    }
    return "N/A";
}

let getKeyValue = function(obj, key) {
    if (obj && obj[key]) return obj[key];
    return "N/A";
}

let getCellSignalKey = function(telephony, key) {
    if (telephony) return getKeyValue(telephony.cell_signal, key);
    return "N/A";
}

let isLaa = function(earfcn) {
    return earfcn >= 46790 && earfcn <= 54539;
}

let isTelephonyExists = function(recs) {
    let exists = true;
    for (let testName of TEST_LIST) {
        if (recs.tests[testName] === undefined) continue;
        for (let cond in recs.tests[testName].environment) {
            exists = exists && recs.tests[testName].environment[cond].telephony && recs.tests[testName].environment[cond].telephony.cell_signal;
        }
    }
    return exists;
}

let isNetworkExists = function(recs) {
    let exists = true;
    for (let testName of TEST_LIST) {
        if (recs.tests[testName] === undefined) continue;
        for (let cond in recs.tests[testName].environment) {
            exists = exists && recs.tests[testName].environment[cond].network;
        }
    }
    return exists;
}

let getConn = function(telephony, wifi) {
    if (telephony && telephony.connection_type) {
        if (telephony.connection_type === "cell") {
            if (telephony.cellular_technology
                && telephony.cellular_technology === "4g"
                && telephony.cell_signal
                && isLaa(telephony.cell_signal.absolute_rf_channel_number)) {
                return "4g-laa";
            }
            return telephony.cellular_technology;
        }
        return telephony.connection_type;
    } else if (wifi) {
        return "wi-fi";
    }
    return "N/A";
}

let getTarget = function(tests) {
    if (tests.download && tests.download.target) return tests.download.target;
    if (tests.upload && tests.upload.target) return tests.upload.target;
    if (tests.latency && tests.latency.target) return tests.latency.target;
    return "N/A";
}

let normalizeFreq = function(freq) {
    if (freq >= 2400 && freq < 2500) return "2.4 GHz";
    if (freq > 5000 && freq < 5900) return "5 GHz";
    if (freq > 5900 && freq < 7125) return "6 GHz";
    return freq;
}

let nullNetwork = {
    "is_available": "N/A",
    "is_connected": "N/A",
    "is_connected_or_connecting": "N/A",
    "is_failover": "N/A",
    "is_roaming": "N/A",
    "state": "N/A",
    "subtype": "N/A",
    "subtype_name": "N/A",
    "type": "N/A"
};
let nullWifi = {
    "frequency": "N/A",
    "hidden_ssid": "N/A",
    "link_speed": "N/A",
    "network_id": "N/A",
    "rssi": "N/A",
    "supplicant_state": "N/A"
};

let testIdList = [];
let values = {};
let zipIds = {};

agg.callbackJsonRecursive(inputFolder, data => {
    let recs = data.json
    if (inputFilter) {
        recs = filter.filterArray(inputFilter, recs);
    }
    console.log(`path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${recs.length}`);
    if (merge) {
        pathId = "all";
    } else {
        pathId = data.path;
    }
    if (values[pathId] === undefined) values[pathId] = { download: {}, upload: {}, latency: {} };

    for (let i = 0, length1 = recs.length; i < length1; i++) {
        if (recs[i].tests.testId === undefined || testIdList.includes(recs[i].tests.testId)) continue;
        testIdList.push(recs[i].tests.testId);

        let op = dataUtils.getCleanOp(recs[i]);
        for (let testName in recs[i].tests) {
            let currValue = (recs[i].tests.download.throughput * 8 / 1e6);
            if (testName === "download" || testName === "upload") {
                currValue = (recs[i].tests[testName].throughput * 8 / 1e6);
            } else if (testName === "latency") {
                currValue = (recs[i].tests[testName].round_trip_time / 1e3);
            } else {
                continue;
            }
            let networkBegin, networkEnd;
            if (zipFn) {
                networkBegin = dataUtils.getUuid(recs[i].zipFilename);
                networkEnd = networkBegin;
                if (zipIds[pathId] === undefined) zipIds[pathId] = {};
                if (zipIds[pathId][op] === undefined) zipIds[pathId][op] = [ "all" ];
                if (!zipIds[pathId][op].includes(networkBegin)) zipIds[pathId][op].push(networkBegin);
            } else if (op === "WiFi") {
                networkBegin = dataUtils.getCleanValue(recs[i].tests[testName].environment.beginning, "wifi.frequency");
                networkBegin = normalizeFreq(networkBegin);
                networkEnd = dataUtils.getCleanValue(recs[i].tests[testName].environment.end, "wifi.frequency");
                networkEnd = normalizeFreq(networkEnd);
            } else {
                networkBegin = dataUtils.getCleanValue(recs[i].tests[testName].environment.beginning, "network.subtype_name");
                networkEnd = dataUtils.getCleanValue(recs[i].tests[testName].environment.end, "network.subtype_name");
            }
            if (currValue > 0 && networkBegin === networkEnd) {
                if (values[pathId][testName] === undefined) values[pathId][testName] = {};
                if (values[pathId][testName][op] === undefined) values[pathId][testName][op] = {};
                if (values[pathId][testName][op][networkBegin] === undefined) values[pathId][testName][op][networkBegin] = [];
                values[pathId][testName][op][networkBegin].push(currValue);
                if (zipFn) {
                    if (values[pathId][testName][op].all === undefined) values[pathId][testName][op].all = [];
                    values[pathId][testName][op].all.push(currValue);
                }
            }
        }
    }
});


for (let pathId in values) {
    let constOp = {
        WiFi: [ "2.4 GHz", "5 GHz" ],
        "AT&T": [ "LTE", "NRNSA" ],
        "T-Mobile": [ "LTE", "NRNSA", "NR" ],
        "Verizon": [ "LTE", "NRNSA" ]
    }
    if (zipFn) {
        constOp = zipIds[pathId];
    }
    console.log(`Processing... ${pathId}`);

    let dlFig = [];
    let dlLayout = {
        xaxis: {
            showgrid: true,
            title: 'DL Throughput (Mbps)'
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        showlegend: true
    };
    if (!merge) {
        dlLayout.title = `${path.basename(pathId)}`;
    }
    let currOutDir;
    if (outputPath !== "plotlib") {
        currOutDir = path.join(outputPath, path.basename(pathId));
        if (!fs.existsSync(currOutDir)){
            fs.mkdirSync(currOutDir, { recursive: true });
        }
    }
    for (let op in constOp) {
        if (values[pathId].download[op] === undefined) {
            if (outputPath === "plotlib") {
                for (let network of constOp[op]) {
                    let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                    dlFig.push({ x: [ 0 ], y: [ 0 ], name: lineName, type: "scatter" });
                }
            }
        } else {
            for (let network of constOp[op]) {
                if (values[pathId].download[op][network] === undefined) {
                    if (outputPath === "plotlib") {
                        let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                        dlFig.push({ x: [ 0 ], y: [ 0 ], name: lineName, type: "scatter" });
                    }
                } else {
                    if (values[pathId].download[op][network].length > 1) {
                        values[pathId].download[op][network].sort(dataUtils.sortNumAsc);
                    } else {
                        values[pathId].download[op][network].push(values[pathId].download[op][network][0]);
                    }
                    let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                    if (outputPath === "plotlib") {
                        dlFig.push({ x: values[pathId].download[op][network], y: dataUtils.createCdfY(values[pathId].download[op][network].length), name: lineName, type: "scatter" });
                    } else {
                        let outFilePath = path.join(currOutDir, `dl-${lineName}.dat`);
                        let string = `"Cumulative distribution function" "DL Tput, Mbps"\n`;
                        for (let i = 0, length1 = values[pathId].download[op][network].length; i < length1; i++) {
                            string += `${i / (length1 - 1)} ${values[pathId].download[op][network][i]}\n`;
                        }
                        fs.writeFileSync(outFilePath, string);
                    }
                }
            }
        }
    }
    if (dlFig.length > 0) {
        plotlib.stack(dlFig, dlLayout);
    }

    let ulFig = [];
    let ulLayout = {
        xaxis: {
            showgrid: true,
            title: 'UL Throughput (Mbps)'
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        showlegend: true
    };
    for (let op in constOp) {
        if (values[pathId].upload[op] === undefined) {
            if (outputPath === "plotlib") {
                for (let network of constOp[op]) {
                    let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                    ulFig.push({ x: [ 0 ], y: [ 0 ], name: lineName, type: "scatter" });
                }
            }
        } else {
            for (let network of constOp[op]) {
                if (values[pathId].upload[op][network] === undefined) {
                    if (outputPath === "plotlib") {
                        let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                        ulFig.push({ x: [ 0 ], y: [ 0 ], name: lineName, type: "scatter" });
                    }
                } else {
                    if (values[pathId].upload[op][network].length > 1) {
                        values[pathId].upload[op][network].sort(dataUtils.sortNumAsc);
                    } else {
                        values[pathId].upload[op][network].push(values[pathId].upload[op][network][0]);
                    }
                    let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                    if (outputPath === "plotlib") {
                        ulFig.push({ x: values[pathId].upload[op][network], y: dataUtils.createCdfY(values[pathId].upload[op][network].length), name: lineName, type: "scatter" });
                    } else {
                        let outFilePath = path.join(currOutDir, `ul-${lineName}.dat`);
                        let string = `"Cumulative distribution function" "UL Tput, Mbps"\n`;
                        for (let i = 0, length1 = values[pathId].upload[op][network].length; i < length1; i++) {
                            string += `${i / (length1 - 1)} ${values[pathId].upload[op][network][i]}\n`;
                        }
                        fs.writeFileSync(outFilePath, string);
                    }
                }
            }
        }
    }
    if (ulFig.length > 0) {
        plotlib.stack(ulFig, ulLayout);
    }

    let latFig = [];
    let latLayout = {
        xaxis: {
            showgrid: true,
            title: 'Latency (millisec)',
            range: [0, 200]
        },
        yaxis: {
            showgrid: true,
            title: 'Cumulative distribution function'
        },
        showlegend: true
    };
    for (let op in constOp) {
        if (values[pathId].latency[op] === undefined) {
            if (outputPath === "plotlib") {
                for (let network of constOp[op]) {
                    let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                    latFig.push({ x: [ 0 ], y: [ 0 ], name: lineName, type: "scatter" });
                }
            }
        } else {
            for (let network of constOp[op]) {
                if (values[pathId].latency[op][network] === undefined) {
                    if (outputPath === "plotlib") {
                        let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                        latFig.push({ x: [ 0 ], y: [ 0 ], name: lineName, type: "scatter" });
                    }
                } else {
                    if (values[pathId].latency[op][network].length > 1) {
                        values[pathId].latency[op][network].sort(dataUtils.sortNumAsc);
                    } else {
                        values[pathId].latency[op][network].push(values[pathId].latency[op][network][0]);
                    }
                    let lineName = (zipFn ? `Client-${constOp[op].indexOf(network)}-${op}-${network}` : `${op}-${network}`);
                    if (outputPath === "plotlib") {
                        latFig.push({ x: values[pathId].latency[op][network], y: dataUtils.createCdfY(values[pathId].latency[op][network].length), name: lineName, type: "scatter" });
                    } else {
                        let outFilePath = path.join(currOutDir, `lat-${lineName}.dat`);
                        let string = `"Cumulative distribution function" "Round trip latency, ms"\n`;
                        for (let i = 0, length1 = values[pathId].latency[op][network].length; i < length1; i++) {
                            string += `${i / (length1 - 1)} ${values[pathId].latency[op][network][i]}\n`;
                        }
                        fs.writeFileSync(outFilePath, string);
                    }
                }
            }
        }
    }
    if (latFig.length > 0) {
        plotlib.stack(latFig, latLayout);
    }
    if (dlFig.length > 0 || ulFig.length > 0 || latFig.length > 0) {
        plotlib.plot();
    }
}
