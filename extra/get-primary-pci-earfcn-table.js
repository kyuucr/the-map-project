const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> [output path]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];

let lteCount = {};
let nrCount = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);

        if (lteCount[op] === undefined) lteCount[op] = {};
        if (nrCount[op] === undefined) nrCount[op] = {};
        let primaryPci, primaryEarfcn;

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            if (json[i].cell_info[j].width > 0) {
                primaryEarfcn = json[i].cell_info[j].earfcn;
                primaryPci = json[i].cell_info[j].pci;

                if (lteCount[op].allPci === undefined) lteCount[op].allPci = {};
                if (lteCount[op].allPci[primaryEarfcn] === undefined) lteCount[op].allPci[primaryEarfcn] = 0;
                ++lteCount[op].allPci[primaryEarfcn];

                if (lteCount[op][primaryPci] === undefined) lteCount[op][primaryPci] = {};
                if (lteCount[op][primaryPci][primaryEarfcn] === undefined) lteCount[op][primaryPci][primaryEarfcn] = 0;
                ++lteCount[op][primaryPci][primaryEarfcn];
            }
        }

        if (json[i].nr_info && json[i].nr_info.length > 0 && primaryPci && primaryEarfcn) {
            if (nrCount[op][primaryPci] === undefined) nrCount[op][primaryPci] = {};
            if (nrCount[op][primaryPci][primaryEarfcn] === undefined) nrCount[op][primaryPci][primaryEarfcn] = 0;
            nrCount[op][primaryPci][primaryEarfcn] += json[i].nr_info.length;

            if (nrCount[op].allPci === undefined) nrCount[op].allPci = {};
            if (nrCount[op].allPci[primaryEarfcn] === undefined) nrCount[op].allPci[primaryEarfcn] = 0;
            nrCount[op].allPci[primaryEarfcn] += json[i].nr_info.length;
        }
    }
});

let string = ""
for (let op in lteCount) {
    string += `${op}\n`;
    let earfcnList = [];
    let pciCount = {};
    let pciNrCount = {};
    for (let pci in lteCount[op]) {
        let tempList = Object.keys(lteCount[op][pci]);
        if (pciCount[pci] === undefined) pciCount[pci] = 0;
        if (pciNrCount[pci] === undefined) pciNrCount[pci] = 0;
        for (let earfcn of tempList) {
            if (!earfcnList.includes(earfcn)) {
                earfcnList.push(earfcn);
            }
            pciCount[pci] += lteCount[op][pci][earfcn];
            if (nrCount[op][pci] && nrCount[op][pci][earfcn]) {
                pciNrCount[pci] += nrCount[op][pci][earfcn];
            }
        }
    }
    earfcnList.sort(dataUtils.sortNumAsc);
    string += `"Primary PCI (count)"{sep}`;
    for (let earfcn of earfcnList) {
        string += `"EARFCN = ${earfcn}"{sep}`;
    }
    string += `Total\n`;
    for (let pci in lteCount[op]) {
        if (pci === "allPci") continue;
        string += `"${pci}"{sep}`;
        for (let earfcn of earfcnList) {
            string += `${lteCount[op][pci][earfcn] ? lteCount[op][pci][earfcn] : 0}${nrCount[op][pci] && nrCount[op][pci][earfcn] ? " (NR " + nrCount[op][pci][earfcn] + ")" : ""}{sep}`
        }
        string += `${pciCount[pci]}${pciNrCount[pci] ? (" (NR " + pciNrCount[pci]) + ")" : ""}\n`;
    }
    string += `All PCI{sep}`;
    for (let earfcn of earfcnList) {
        string += `${lteCount[op].allPci[earfcn] ? lteCount[op].allPci[earfcn] : 0}${nrCount[op].allPci && nrCount[op].allPci[earfcn] ? " (NR " + nrCount[op].allPci[earfcn] + ")" : ""}{sep}`
    }
    string += `${pciCount.allPci}${pciNrCount.allPci ? (" (NR " + pciNrCount.allPci) + ")" : ""}\n`;
}

if (outputPath) {
    fs.writeFileSync(outputPath, string.replace(/\{sep\}/g, ","));
} else {
    console.log(string.replace(/\{sep\}/g, "\t"));
}