const agg = require('../lib/aggregator');
const dataUtils = require('../lib/data-utils');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);

if (ARGS.length < 2) {
    console.log(`Usage: node ${path.basename(__filename)} <SigCap dir> <output path> [prefix]`);
    process.exit(0);
}

const dir = ARGS[0];
const outputPath = ARGS[1];
const usePlotlib = ARGS[1] === "plotlib";
const prefix = ARGS[2] || "";

let output = {};
let hashBin = [];

agg.callbackJsonRecursive(dir, data => {
    let json = data.json;

    for(let i = 0, length1 = json.length; i < length1; i++){
        let hash = dataUtils.hashObj(json[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (json[i].opName === undefined && json[i].simName === undefined && json[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(json[i]);
        if (dataUtils.getNetworkType(json[i]) === "NR-NSA"
            && (json[i].cell_info === undefined
                || json[i].cell_info.length === 0)) {
            continue;
        }

        if (output[op] === undefined) {
            output[op] = {
                all: {
                    rsrp: [],
                    rsrq: [],
                    rssi: []
                },
                primary: {
                    rsrp: [],
                    rsrq: [],
                    rssi: []
                },
                other: {
                    rsrp: [],
                    rsrq: [],
                    rssi: []
                },
                cbrs: {
                    rsrp: [],
                    rsrq: [],
                    rssi: []
                },
                laa: {
                    rsrp: [],
                    rsrq: [],
                    rssi: []
                },
                nrFr1: {
                    rsrp: [],
                    rsrq: []
                },
                nrFr2: {
                    rsrp: [],
                    rsrq: []
                },
                nrNsaConnectedFr1: {
                    rsrp: [],
                    rsrq: []
                },
                nrNsaConnectedFr2: {
                    rsrp: [],
                    rsrq: []
                },
                nrSaConnectedFr1: {
                    rsrp: [],
                    rsrq: []
                },
                nrSaConnectedFr2: {
                    rsrp: [],
                    rsrq: []
                }
            };
        }

        for (let j = 0, length2 = json[i].cell_info.length; j < length2; j++) {
            let rsrq = dataUtils.cleanSignal(json[i].cell_info[j].rsrq);
            let rsrp = dataUtils.cleanSignal(json[i].cell_info[j].rsrp);
            let rssi = dataUtils.cleanSignal(json[i].cell_info[j].rssi);
            let chNum = json[i].cell_info[j].channelNum;

            if (json[i].cell_info[j].band === 46
                && chNum !== 0
                && output[op][`laa-${chNum}`] === undefined) {
                output[op][`laa-${chNum}`] = {
                    rsrp: [],
                    rsrq: [],
                    rssi: []
                };
            }
            if (json[i].cell_info[j].width > 0) {
                if (!isNaN(rsrp)) {
                    output[op].all.rsrp.push(rsrp);
                    output[op].primary.rsrp.push(rsrp);
                }
                if (!isNaN(rsrq) && rsrq >= -20) {
                    output[op].all.rsrq.push(rsrq);
                    output[op].primary.rsrq.push(rsrq);
                }
                if (!isNaN(rssi)) {
                    output[op].all.rssi.push(rssi);
                    output[op].primary.rssi.push(rssi);
                }
            } else {
                if (!isNaN(rsrp)) {
                    output[op].all.rsrp.push(rsrp);
                    output[op].other.rsrp.push(rsrp);
                    if (json[i].cell_info[j].band === 46) {
                        output[op].laa.rsrp.push(rsrp);
                        if (chNum) output[op][`laa-${chNum}`].rsrp.push(rsrp);
                    }
                    if (json[i].cell_info[j].band === 48) output[op].cbrs.rsrp.push(rsrp);
                }
                if (!isNaN(rsrq) && rsrq >= -20) {
                    output[op].all.rsrq.push(rsrq);
                    output[op].other.rsrq.push(rsrq);
                    if (json[i].cell_info[j].band === 46) {
                        output[op].laa.rsrq.push(rsrq);
                        if (chNum) output[op][`laa-${chNum}`].rsrq.push(rsrq);
                    }
                    if (json[i].cell_info[j].band === 48) output[op].cbrs.rsrq.push(rsrq);
                }
                if (!isNaN(rssi)) {
                    output[op].all.rssi.push(rssi);
                    output[op].other.rssi.push(rssi);
                    if (json[i].cell_info[j].band === 46) {
                        output[op].laa.rssi.push(rssi);
                        if (chNum) output[op][`laa-${chNum}`].rssi.push(rssi);
                    }
                    if (json[i].cell_info[j].band === 48) output[op].cbrs.rssi.push(rssi);
                }
            }
        }

        if (json[i].nr_info) {
            for(let j = 0, length2 = json[i].nr_info.length; j < length2; j++){
                let rsrp = dataUtils.cleanSignal(json[i].nr_info[j].ssRsrp);
                let rsrq = dataUtils.cleanSignal(json[i].nr_info[j].ssRsrq);
                let nrStatus = dataUtils.getServiceState(json[i], "nrStatus");
                let nrSubtype = dataUtils.getNetworkType(json[i]);
                let nrFrequency = dataUtils.getServiceState(json[i], "nrFrequencyRange");
                if (nrFrequency === "mmWave") {
                    if (!isNaN(rsrp)) {
                        output[op].nrFr2.rsrp.push(rsrp);
                        if (nrStatus === "connected") {
                            if (nrSubtype === "NR-NSA") {
                                output[op].nrNsaConnectedFr2.rsrp.push(rsrp);
                            } else {
                                output[op].nrSaConnectedFr2.rsrp.push(rsrp);
                            }
                        }
                    }
                    if (!isNaN(rsrq) && rsrq >= -20) {
                        output[op].nrFr2.rsrq.push(rsrq);
                        if (nrStatus === "connected") {
                            if (nrSubtype === "NR-NSA") {
                                output[op].nrNsaConnectedFr2.rsrq.push(rsrq);
                            } else {
                                output[op].nrSaConnectedFr2.rsrq.push(rsrq);
                            }
                        }
                    }
                } else {
                    if (!isNaN(rsrp)) {
                        output[op].nrFr1.rsrp.push(rsrp);
                        if (nrStatus === "connected") {
                            if (nrSubtype === "NR-NSA") {
                                output[op].nrNsaConnectedFr1.rsrp.push(rsrp);
                            } else {
                                output[op].nrSaConnectedFr1.rsrp.push(rsrp);
                            }
                        }
                    }
                    if (!isNaN(rsrq) && rsrq >= -20) {
                        output[op].nrFr1.rsrq.push(rsrq);
                        if (nrStatus === "connected") {
                            if (nrSubtype === "NR-NSA") {
                                output[op].nrNsaConnectedFr1.rsrq.push(rsrq);
                            } else {
                                output[op].nrSaConnectedFr1.rsrq.push(rsrq);
                            }
                        }
                    }
                }
            }
        }
    }
});

let rsrpFig = {};
let rsrqFig = {};
let maxRsrp = -999, maxRsrq = -999;

for (let op in output) {
    if (op === "Unknown") continue;

    for (let type in output[op]) {
        output[op][type].rsrp.sort(dataUtils.sortNumAsc);
        output[op][type].rsrq.sort(dataUtils.sortNumAsc);

        if (usePlotlib) {
            if (maxRsrp < output[op][type].rsrp[output[op][type].rsrp.length - 1]) maxRsrp = output[op][type].rsrp[output[op][type].rsrp.length - 1];
            if (maxRsrq < output[op][type].rsrq[output[op][type].rsrq.length - 1]) maxRsrq = output[op][type].rsrq[output[op][type].rsrq.length - 1];
            if (rsrpFig[type] === undefined) {
                rsrpFig[type] = [];
                rsrqFig[type] = [];
            }
            rsrpFig[type].push({ x: output[op][type].rsrp, y: dataUtils.createCdfY(output[op][type].rsrp.length), name: `${op}`, type: "scatter" });
            rsrqFig[type].push({ x: output[op][type].rsrq, y: dataUtils.createCdfY(output[op][type].rsrq.length), name: `${op}`, type: "scatter" });
        } else {
            let outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-rsrp-${type}.dat`);
            let string = `"Cumulative distributive function" "RSRP"\n`;

            for(let i = 0, length1 = output[op][type].rsrp.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][type].rsrp[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);

            outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-rsrq-${type}.dat`);
            string = `"Cumulative distributive function" "RSRQ"\n`;

            for(let i = 0, length1 = output[op][type].rsrq.length; i < length1; i++){
                string += `${i / (length1 - 1)} ${output[op][type].rsrq[i]}\n`;
            }

            fs.writeFileSync(outputFile, string);

            if (output[op][type].rssi) {
                output[op][type].rssi.sort(dataUtils.sortNumAsc);
                outputFile = path.join(outputPath, `${prefix}${prefix ? "-" : ""}${op}-cdf-rssi-${type}.dat`);
                string = `"Cumulative distributive function" "RSSI"\n`;

                for(let i = 0, length1 = output[op][type].rssi.length; i < length1; i++){
                    string += `${i / (length1 - 1)} ${output[op][type].rssi[i]}\n`;
                }

                fs.writeFileSync(outputFile, string);
            }
        }
    }
}

if (usePlotlib) {
    const plotlib = require('nodeplotlib');
    const typeString = {
        all: "LTE All",
        primary: "LTE Primary",
        other: "LTE Other",
        cbrs: "LTE CBRS",
        laa: "LTE LAA",
        nrFr1: "NR FR1",
        nrFr2: "NR FR2",
        nrNsaConnectedFr1: "NR-NSA FR1",
        nrNsaConnectedFr2: "NR-NSA FR2",
        nrSaConnectedFr1: "NR-SA FR1",
        nrSaConnectedFr2: "NR-SA FR2",
    }

    for (let type in rsrpFig) {
        let layout = {
            xaxis: {
                showgrid: true,
                title: 'RSRP (dBm)',
                range: [-140, maxRsrp]
            },
            yaxis: {
                showgrid: true,
                title: 'Cumulative distribution function'
            },
            title: `RSRP - ${typeString[type] ? typeString[type] : type}`,
            showlegend: true
        }
        plotlib.stack(rsrpFig[type], layout);
    }
    for (let type in rsrqFig) {
        let layout = {
            xaxis: {
                showgrid: true,
                title: 'RSRQ (dB)',
                range: [-20, maxRsrq]
            },
            yaxis: {
                showgrid: true,
                title: 'Cumulative distribution function'
            },
            title: `RSRQ - ${typeString[type] ? typeString[type] : type}`,
            showlegend: true
        }
        plotlib.stack(rsrqFig[type], layout);
    }
    plotlib.plot();
}