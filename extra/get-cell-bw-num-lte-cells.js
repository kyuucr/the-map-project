const agg = require('../lib/aggregator');
const csvUtils = require('../lib/csv-utils');
const dataUtils = require('../lib/data-utils');
const filter = require('../lib/filter');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [filter]`);
    process.exit(1);
}
let inputFolder = ARGS[0];
let filterString;

if (ARGS[2]) {
    let jsonString = ARGS[2];
    if (jsonString.match(/\.json$/)) jsonString = fs.readFileSync(jsonString);
    filterString = JSON.parse(jsonString);
}

let numData = 0;
let totalData = 0;
agg.callbackJsonRecursive(inputFolder, data => {
    let curPath = data.path;
    let currJson = data.json;
    currJson = filter.filterArray({cellBandwidths: "~undefined"}, currJson);
    if (filterString) {
        currJson = filter.filterArray(filterString, currJson);
    }
    console.log(`path: ${curPath}, # data: ${currJson.length}`);

    totalData += currJson.length;
    for (let i = 0, length1 = currJson.length; i < length1; i++){
        let cellBws = dataUtils.getCellBw(currJson[i]);
        if (cellBws.length > currJson[i].cell_info.length) {
            numData++;
        }
    }
});

console.log(`# of cell BWs > # LTE cells: ${numData}, total: ${totalData}, ratio: ${numData / totalData}`);