const agg = require('../lib/aggregator');
const filter = require('../lib/filter');
const geohelper = require('../lib/geohelper');
const fs = require('fs');
const path = require('path');

let allCells = agg.getJson(path.join("input", "capture_5g"));
let allFcc = agg.getJson(path.join("input", "fcc_5g"));

console.log("fcc.testId,fcc.download.local_datetime,fcc.download.environment.beginning.telephony.cell_signal.received_signal_power,sigcap.ss_rsrp,sigcap.rsrp,dist_meter,time_diff_milisec");

for (let i = 0, length1 = allFcc.length; i < length1; i++){
    if (allFcc[i].tests.testId
            && allFcc[i].version === "2.0.2439"
            && allFcc[i].tests.download.throughput > 0
            && allFcc[i].tests.upload.throughput > 0
            && allFcc[i].tests.download.environment.beginning.telephony.cellular_technology === "5g"
            && allFcc[i].tests.download.environment.end.telephony.cellular_technology === "5g"
            && allFcc[i].tests.upload.environment.beginning.telephony.cellular_technology === "5g"
            && allFcc[i].tests.upload.environment.end.telephony.cellular_technology === "5g"
            && allFcc[i].tests.latency.environment.beginning.telephony.cellular_technology === "5g"
            && allFcc[i].tests.latency.environment.end.telephony.cellular_technology === "5g") {
        let minDatetime = 999999;
        let fccDate = new Date(allFcc[i].tests.download.local_datetime);
        let ss_rsrp = 99;
        let rsrp = 99;
        let dist = -1;
        for(let j = 0, length2 = allCells.length; j < length2; j++){
            allCells[j]
            let datetimeString = `${allCells[j].datetime.date.substring(0,4)}-${allCells[j].datetime.date.substring(4,6)}-${allCells[j].datetime.date.substring(6,8)}T${allCells[j].datetime.time.substring(0,2)}:${allCells[j].datetime.time.substring(2,4)}:${allCells[j].datetime.time.substring(4,6)}.${allCells[j].datetime.time.substring(6,9)}${allCells[j].datetime.zone}`;
            let dataDate = new Date(datetimeString);
            if (dataDate < fccDate && allCells[j].nr_info.length > 0 && allCells[j].cell_info.length > 0) {
                ss_rsrp = allCells[j].nr_info[0].ssRsrp;
                rsrp = allCells[j].cell_info[0].rsrp;
                dist = geohelper.measure(allFcc[i].tests.download.environment.beginning.location.lat, allFcc[i].tests.download.environment.beginning.location.lon, allCells[j].location.latitude, allCells[j].location.longitude);
                minDatetime = fccDate.getTime() - dataDate.getTime();
            }
        }
        console.log(`${allFcc[i].tests.testId}, ${allFcc[i].tests.download.local_datetime}, ${allFcc[i].tests.download.environment.beginning.telephony.cell_signal.received_signal_power}, ${ss_rsrp}, ${rsrp}, ${dist}, ${minDatetime}`);
    }
}

