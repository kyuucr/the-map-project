const agg = require('./lib/aggregator');
const filter = require('./lib/filter');
const geohelper = require('./lib/geohelper');
const fs = require('fs');
const path = require('path');

////////////////////////////////
// Definitions
////////////////////////////////

const OPTIONS = {
    inputDir: null, // SigCap input dir
    tileSize: 12,   // array tile size
    accTh: -1,  // threshold of accuracy
    geoJson: false, // output as geoJSON?
    centroid: false, // output each latLng as centroid?
    filter: null,
    pciSelect: [],
    outputPath: path.join("html", "outputs", "nr-dots.geojson") // output path
};

////////////////////////////////
// Command line
////////////////////////////////

let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`
Usage: node ${path.basename(__filename)} <SigCap input dir>
            [--acc-th GPS accuracy threshold in meter (default: unset)]
            [--filter filter input (default: unset)]
            [--pci only consider this PCI when calculating LTE average RSRP (default: unset)]
            [--output output path with output filename (default: "html/outputs/nr-heatmaps.geojson")]`);
    process.exit(0);
}

while (args.length > 0) {
    switch (args[0]) {
        case "--tile-size":
            OPTIONS.tileSize = parseFloat(args[1]);
            args.splice(0, 2);
            break;
        case "--acc-th":
            OPTIONS.accTh = parseFloat(args[1]);
            args.splice(0, 2);
            break;
        case "--geo-json":
            OPTIONS.geoJson = true;
            args.splice(0, 1);
            break;
        case "--centroid":
            OPTIONS.centroid = true;
            args.splice(0, 1);
            break;
        case "--output":
            OPTIONS.outputPath = args[1];
            args.splice(0, 2);
            break;
        case "--filter":
            OPTIONS.filter = JSON.parse(args[1]);
            args.splice(0, 2);
            break;
        case "--pci":
            let pcis = args[1].split(",");
            for (pci of pcis) {
                OPTIONS.pciSelect.push(parseInt(pci));
            }
            console.log(`PCI select: ${OPTIONS.pciSelect.join(", ")}`)
            args.splice(0, 2);
            break;

        default:
            if (OPTIONS.inputDir !== null) {
                console.log("multiple input dir!");
                process.exit(0);
            }
            OPTIONS.inputDir = args[0];
            args.splice(0, 1);
            break;
    }
}

// Input
let allCells = agg.getJson(OPTIONS.inputDir);
if (OPTIONS.filter !== null) {
    allCells = filter.filterArray(OPTIONS.filter, allCells);
}

////////////////////////////////
// Preprocessing
////////////////////////////////

let minLat = 99.9;
let maxLat = -99.9;
let minLng = 99.9;
let maxLng = -99.9;
let minRsrp = 999.9;
let maxRsrp = -999.9;
let minSsrsrp = 999.9;
let maxSsrsrp = -999.9;
let pciList = [];
let earfcnList = [];

for (let i = allCells.length - 1; i >= 0; i--) {
    if (OPTIONS.pciSelect.length === 0 || allCells[i].cell_info.some((element) => {return OPTIONS.pciSelect.includes(element.pci)})) {
        minLat = (minLat > allCells[i].location.latitude) ? allCells[i].location.latitude : minLat;
        minLng = (minLng > allCells[i].location.longitude) ? allCells[i].location.longitude : minLng;
        maxLat = (maxLat < allCells[i].location.latitude) ? allCells[i].location.latitude : maxLat;
        maxLng = (maxLng < allCells[i].location.longitude) ? allCells[i].location.longitude : maxLng;

        for(let j = 0, length2 = allCells[i].cell_info.length; j < length2; j++){
            if (OPTIONS.pciSelect.length === 0 || OPTIONS.pciSelect.includes(allCells[i].cell_info[j].pci)) {
                minRsrp = (minRsrp > allCells[i].cell_info[j].rsrp) ? allCells[i].cell_info[j].rsrp : minRsrp;
                maxRsrp = (maxRsrp < allCells[i].cell_info[j].rsrp) ? allCells[i].cell_info[j].rsrp : maxRsrp;

                if (!pciList.includes(allCells[i].cell_info[j].pci)) {
                    pciList.push(allCells[i].cell_info[j].pci);
                }

                let found = earfcnList.some((element) => {return element.earfcn === allCells[i].cell_info[j].earfcn});
                // for (let earfcnBand of earfcnList) {
                //     if (earfcnBand.earfcn ) {
                //         found = true;
                //         break;
                //     }

                // }
                if (!found) {
                    earfcnList.push({earfcn: allCells[i].cell_info[j].earfcn, band: allCells[i].cell_info[j].band});
                }
            }
        }
        for(let j = 0, length2 = allCells[i].nr_info.length; j < length2; j++){
            minSsrsrp = (minSsrsrp > allCells[i].nr_info[j].ssRsrp) ? allCells[i].nr_info[j].ssRsrp : minSsrsrp;
            maxSsrsrp = (maxSsrsrp < allCells[i].nr_info[j].ssRsrp) ? allCells[i].nr_info[j].ssRsrp : maxSsrsrp;
        }
    }
}

pciList.sort((a,b) => {return a - b});
earfcnList.sort((a,b) => {return a.earfcn - b.earfcn});
console.log(`PCI list: ${pciList.join(", ")}`)
console.log(`EARFCN list: ${earfcnList.join(", ")}`)

let outputOptions = {
    minLat: minLat,
    maxLat: maxLat,
    minLng: minLng,
    maxLng: maxLng,
    minRsrp: minRsrp,
    maxRsrp: maxRsrp,
    rangeRsrp: maxRsrp - minRsrp,
    minSsrsrp: minSsrsrp,
    maxSsrsrp: maxSsrsrp,
    rangeSsrsrp: maxSsrsrp - minSsrsrp,
    pciList: pciList,
    earfcnList: earfcnList
};

console.log (`Lat-Lng area: ${minLat}, ${maxLat}, ${minLng}, ${maxLng}`);
console.log (`Center: ${(maxLat - minLat) / 2 + minLat}, ${(maxLng - minLng) / 2 + minLng}`);
console.log (`minRsrp: ${minRsrp}, maxRsrp: ${maxRsrp}`);
console.log (`minSsrsrp: ${minSsrsrp}, maxSsrsrp: ${maxSsrsrp}`);

if (OPTIONS.accTh > 0) console.log(`Using accuracy threshold: ${OPTIONS.accTh} m`);

////////////////////////////////
// Heatmap
////////////////////////////////

// Note: lat = y, lng = x
console.log("Calculating heatmap of average RSRP");
let output = {
    OPTIONS: outputOptions,
    type: "FeatureCollection",
    features: []
};
let skipped = 0;
let idx = 0;

for (let j = allCells.length - 1; j >= 0; j--) {
    if (allCells[j].location === undefined
        || (OPTIONS.accTh > 0 && allCells[j].location.hor_acc > OPTIONS.accTh)) {
        skipped++;
        continue;
    }

    let op = allCells[j].opName;
    if (op === "") {
        if (allCells[j].carrierName) {
            op = allCells[j].carrierName;
        } else {
            op = "Verizon";
        }
    };
    if (op === "Searching for Service") continue;
    op = op.replace(/ +$/, "");

    let lat = allCells[j].location.latitude;
    let lng = allCells[j].location.longitude;
    let timestamp = allCells[j].datetimeIso;

    for (let k = allCells[j].cell_info.length - 1; k >= 0; k--) {
        // Sanity checks
        if (allCells[j].cell_info[k].rsrp === 2147483647) continue;
        // PCI select
        if (OPTIONS.pciSelect.length > 0 && !OPTIONS.pciSelect.includes(allCells[j].cell_info[k].pci)) continue;
        // if (allCells[j].cell_info[k].pci !== 7 && allCells[j].cell_info[k].pci !== 311 && allCells[j].cell_info[k].pci !== 381) continue;

        let curRsrp = allCells[j].cell_info[k].rsrp;
        let curRsrq = allCells[j].cell_info[k].rsrq;
        let pci = allCells[j].cell_info[k].pci;
        let band = allCells[j].cell_info[k].band;
        let earfcn = allCells[j].cell_info[k].earfcn;

        let temp = {
            type: "Feature",
            properties: {
                isVisible: false,
                IDX: `${op}-lte-${pci}-${earfcn}-${idx++}`,
                OP: op,
                TYPE: "lte",
                PCI: pci,
                BAND: band,
                EARFCN: earfcn,
                RSRP: curRsrp,
                RSRQ: curRsrq,
                TIMESTAMP: timestamp,
                WEIGHT: (curRsrp - outputOptions.minRsrp) / outputOptions.rangeRsrp
            },
            geometry: {
                type: "Point",
                coordinates: [
                    lng,
                    lat
                ]
            }
        }
        output.features.push(temp);
    }

    for (let k = allCells[j].nr_info.length - 1; k >= 0; k--) {
        // Sanity checks
        if (allCells[j].nr_info[k].ssRsrp === 2147483647) continue;

        let curRsrp = allCells[j].nr_info[k].ssRsrp;
        let curRsrq = allCells[j].nr_info[k].ssRsrq;
        let pci = allCells[j].cell_info[k].pci;
        let band = allCells[j].cell_info[k].band;

        let temp = {
            type: "Feature",
            properties: {
                isVisible: false,
                IDX: `${op}-nr-${pci}-${idx++}`,
                OP: op,
                TYPE: "nr",
                PCI: pci,
                RSRP: curRsrp,
                RSRQ: curRsrq,
                TIMESTAMP: timestamp,
                WEIGHT: (curRsrp - outputOptions.minSsrsrp) / outputOptions.rangeSsrsrp
            },
            geometry: {
                type: "Point",
                coordinates: [
                    lng,
                    lat
                ]
            }
        }
        output.features.push(temp);
    }
}

////////////////////////////////
// Output
////////////////////////////////

console.log(`Number of skipped data: ${skipped}`);
console.log(`Writing output at ${OPTIONS.outputPath}`);
fs.writeFileSync(OPTIONS.outputPath, JSON.stringify(output));