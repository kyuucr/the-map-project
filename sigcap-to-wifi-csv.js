const agg = require('./lib/aggregator');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

const ARGS = process.argv.slice(2);
if (ARGS.length < 2) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> <output folder> [--label-location] [--multiple-outputs] [--skip-2.4ghz] [--skip-5ghz] [--skip-6ghz] [BSSID filters ...]`);
    process.exit(1);
}

let inputFolder = ARGS[0];
let outputPath = ARGS[1];
let bssidFilter = [];
let jsonFilter = [];
let isLocationLabel = false;
let useOneOutput = true;
let skip2_4ghz = false;
let skip5ghz = false;
let skip6ghz = false;
const MAX_LINE = 1e6;

if (ARGS.length > 2) {
    for(let i = 2, length1 = ARGS.length; i < length1; i++){
        if (ARGS[i] === "--label-location") {
            isLocationLabel = true;
        } else if (ARGS[i] === "--multiple-outputs") {
            useOneOutput = false;
        } else if (ARGS[i] === "--skip-2.4ghz") {
            skip2_4ghz = true;
        } else if (ARGS[i] === "--skip-5ghz") {
            skip5ghz = true;
        } else if (ARGS[i] === "--skip-6ghz") {
            skip6ghz = true;
        } else {
            // Try to parse to JSON
            try {
                let json = JSON.parse(ARGS[i]);
                jsonFilter.push(json);
            } catch (e) {
                bssidFilter.push(ARGS[i]);
            }
        }
    }
}

let header = `timestamp,latitude,longitude,altitude,hor_acc,ver_acc,${isLocationLabel ? ("location_category,") : ""}`
            + `uuid,device_model,ssid,bssid,primaryFreq,centerFreq0,centerFreq1,width,channelNum,primaryChNum,rssi,standard,`
            + `connected,linkSpeed,txLinkSpeed,rxLinkSpeed,maxSupportedTxLinkSpeed,maxSupportedRxLinkSpeed,capabilities,`
            + `timestampDeltaMs,staCount,chUtil,txPower,linkMargin,apName\n`;

let filenameIdx = 0;
let currLine = 0;
let outString = "";
let totalEntries = 0;

agg.callbackJsonRecursive(inputFolder, data => {
    if (data.type !== "sigcap") return;
    let curPath = data.path;

    if (!useOneOutput) {
        fs.mkdirSync(outputPath, { recursive: true }, (err) => {
            console.log("ERROR: cannot make output dir");
        });
        filePath = path.join(outputPath, `${curPath.replace(/\/|\\/g,"-")}.csv`);
    } else {
        filePath = outputPath;
    }
    let currLocationLabel = ""
    if (isLocationLabel) {
        if (curPath.includes("indoor")) {
            currLocationLabel = "indoor";
        } else if (curPath.includes("outdoor")) {
            currLocationLabel = "outdoor"
        } else {
            currLocationLabel = "undefined"
        }
    }

    let currJson = data.json;
    if (jsonFilter.length > 0) {
        currJson = filter.filterArray(jsonFilter, currJson);
    }

    if (isLocationLabel) console.log("Location label:", currLocationLabel);
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of SigCap entries: ${currJson.length}`);

    let timeData = {};
    for(let j = 0, length2 = currJson.length; j < length2; j++){
        for(let k = 0, length3 = currJson[j].wifi_info.length; k < length3; k++){
            let wifiEntry = currJson[j].wifi_info[k];

            if ((skip2_4ghz && wifiEntry.primaryFreq < 5000)
                    || (skip6ghz && wifiEntry.primaryFreq >= 5900)
                    || (skip5ghz && wifiEntry.primaryFreq >= 5000 && wifiEntry.primaryFreq < 5900)) {
                continue
            }
            let idx = wifiEntry.bssid;
            if (timeData[idx] === undefined) {
                timeData[idx] = [];
            }
            let timestampDeltaMs = dataUtils.cleanNumeric(wifiEntry.timestampDeltaMs);
            let actualTimestamp = new Date(currJson[j].datetimeIso).getTime() - timestampDeltaMs;
            if ((isNaN(timestampDeltaMs) || !timeData[idx].includes(actualTimestamp))
                && (bssidFilter.length === 0 || bssidFilter.includes(idx))) {
            // if (bssidFilter.length === 0 || bssidFilter.includes(idx)) {
                outString += `${currJson[j].datetimeIso},${currJson[j].location.latitude},${currJson[j].location.longitude},${currJson[j].location.altitude},`
                        + `${currJson[j].location.hor_acc},${currJson[j].location.ver_acc},${isLocationLabel ? (currLocationLabel + ",") : ""}`
                        + `${dataUtils.cleanString(currJson[j].uuid)},${dataUtils.cleanString(currJson[j].deviceName)},`
                        + `"${dataUtils.cleanString(wifiEntry.ssid)}",${dataUtils.cleanString(wifiEntry.bssid)},${dataUtils.cleanNumeric(wifiEntry.primaryFreq)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.centerFreq0)},${dataUtils.cleanNumeric(wifiEntry.centerFreq1)},${dataUtils.cleanNumeric(wifiEntry.width)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.channelNum)},${dataUtils.cleanNumeric(wifiEntry.primaryChNum)},${dataUtils.cleanSignal(wifiEntry.rssi)},`
                        + `${dataUtils.cleanString(wifiEntry.standard)},${dataUtils.cleanString(wifiEntry.connected)},${dataUtils.cleanNumeric(wifiEntry.linkSpeed)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.txLinkSpeed)},${dataUtils.cleanNumeric(wifiEntry.rxLinkSpeed)},${dataUtils.cleanNumeric(wifiEntry.maxSupportedTxLinkSpeed)},`
                        + `${dataUtils.cleanNumeric(wifiEntry.maxSupportedRxLinkSpeed)},${dataUtils.cleanString(wifiEntry.capabilities)},${dataUtils.cleanNumeric(wifiEntry.timestampDeltaMs)},`;
                let extras = dataUtils.cleanString(wifiEntry.extra);
                if (extras === "" || extras === "N/A") {
                    let staCount = dataUtils.cleanNumeric(wifiEntry.staCount);
                    let chUtil = dataUtils.cleanNumeric(wifiEntry.chUtil);
                    extras = `${staCount === -1 ? NaN : staCount},${chUtil === -1 ? NaN : chUtil},`;
                    extras += `${dataUtils.cleanSignal(wifiEntry.txPower)},`
                        + `${dataUtils.cleanSignal(wifiEntry.linkMargin)},`
                        + `${dataUtils.cleanString(wifiEntry.apName) ? dataUtils.cleanString(wifiEntry.apName) : "N/A"}`;
                } else {
                    // let extrasString = `"${extras.replace(/\"/g, "'")}"`
                    try {
                        let extraObj = JSON.parse(extras.replace(/\,\]$/, "]"));
                        extras = `${extraObj[0].bssLoad.staCount},${extraObj[0].bssLoad.chUtil},N/A,N/A,N/A`;
                    } catch (_) {
                        extras = "N/A,N/A,N/A,N/A,N/A";
                    }
                    // extras += extrasString;
                    // console.log(extras)
                }
                outString += `${extras}\n`;
                timeData[idx].push(actualTimestamp);
                currLine++;
                totalEntries++;
            }
        }
        if (currLine > MAX_LINE) {
            console.log(`Maximum Wi-Fi entries per file reached, writing to: ${filePath}, # of Wi-Fi entries: ${currLine}`);
            fs.writeFileSync(filePath, header + outString);
            currLine = 0;
            outString = "";
            filePath = filePath.replace(/-?\d*\.csv$/, `-${++filenameIdx}.csv`);
        }
    }

    if (!useOneOutput) {
        filenameIdx = 0;
    } else {
        outputPath = filePath;
    }
});

if (outString.length > 0) {
    console.log(`Writing to: ${filePath}, # of Wi-Fi entries: ${currLine}`);
    fs.writeFileSync(filePath, header + outString);
}
console.log(`Total # of Wi-Fi entries: ${totalEntries}\nDONE!`);