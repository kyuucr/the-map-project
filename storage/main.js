const dataUtils = require("../lib/data-utils");
const filter = require("../lib/filter");
const logger = require("../lib/logger");
const mlCsv = require("../lib/ml-csv");
const cliProgress = require("cli-progress");
const fs = require("fs");
const path = require("path");
const unzipper = require("unzipper");
// Load storage
const storage = require("./storage");

////////////////////////////////
// Definitions
////////////////////////////////

const OPTIONS = {
    command: null, // command
    metadataPath: null,   // path to metadata file for download command
    noFetch: false, // skip fetching for download command
    isHashCheck: true, // compare hash of cache and server file
    rebuild: false, // delete and rebuild the data folder for download command
    inputPath: null,
    outputPath: null,
    includeZipFilename: false,
    inputFolder: null,
    searchArgs: {
        filter: null,
        datasetType: ""
    }
};
const METADATA_TYPES = [ "dataset", "fcc_speedtest", "ml_csv" ];
const progressBar = new cliProgress.SingleBar({
    clearOnComplete: true,
    etaBuffer: 100,
    linewrap: true
}, cliProgress.Presets.shades_classic);
logger.progressBars.push(progressBar);

////////////////////////////////
// Command line
////////////////////////////////

let args = process.argv.slice(2);

if (args.length < 1) {
    logger.i(`Usage: node ${path.basename(__filename)} <command>
Commands are:
    download        Download files using metadata input, options:
        <metadata>       Path to metadata file
        [--input-folder] Use input folder containing zip archives instead of files from the server
        [--no-fetch]     Disable fetching and use cache files directly
        [--rebuild]      Delete and rebuild the dataset (use if the files in server was moved around)
        [--include-zip-filename] Add "zipFilename" field with original zip filename
    upload          Upload files to server, options:
        <input dir>     Path to input directory
        [output dir]    Path to output directory at server
    fetch-dataset   Fetch file from server to dataset cache directory
    fetch-speedtest Fetch file from server to speedtest cache directory
    clean-dataset   Delete dataset cache directory
    clean-speedtest Delete speedtest cache directory
    clean-all       Delete all cache directory
    find            Find according to search term
        <term>                  Can be filename or JSON object
        <output dir>            Output dir for the results
        [--dataset-type type]   Specify dataset type: "dataset" or "fcc_speedtest"
        [--no-fetch]            Disable fetching and use cache files directly
        [--input-folder]        Use input folder containing zip archives instead of files from the server
        [--include-zip-filename] Add "zipFilename" field with original zip filename`);
    process.exit(0);
}

while (args.length > 0) {
    switch (args[0]) {
        case "download":
            OPTIONS.command = "download";
            if (!args[1] || !fs.existsSync(args[1])) {
                logger.e(`Metadata file not found: ${args[1]}`);
                process.exit(0);
            }
            OPTIONS.metadataPath = args[1];
            args.splice(0, 2);
            break;
        case "upload":
            OPTIONS.command = "upload";
            if (!args[1] || !fs.existsSync(args[1])) {
                logger.e(`Input dir not found: ${args[1]}`);
                process.exit(0);
            }
            OPTIONS.inputPath = args[1];
            if (args[2]) {
                OPTIONS.outputPath = args[2];
                args.splice(0, 3);
            } else {
                OPTIONS.outputPath = "";
                args.splice(0, 2);
            }
            break;
        case "fetch":
            OPTIONS.command = "fetch";
            args.splice(0, 1);
            break;
        case "fetch-dataset":
            OPTIONS.command = "fetch-dataset";
            args.splice(0, 1);
            break;
        case "fetch-speedtest":
            OPTIONS.command = "fetch-speedtest";
            args.splice(0, 1);
            break;
        case "clean-dataset":
            OPTIONS.command = "clean-dataset";
            args.splice(0, 1);
            break;
        case "clean-speedtest":
            OPTIONS.command = "clean-speedtest";
            args.splice(0, 1);
            break;
        case "clean-all":
            OPTIONS.command = "clean-all";
            args.splice(0, 1);
            break;
        case "find":
            OPTIONS.command = "find";
            if (!args[1] || !args[2]) {
                logger.e(`Search input incomplete, please specify search term and output folder!`);
                process.exit(0);
            }
            OPTIONS.searchArgs.filter = args[1];
            OPTIONS.outputPath = args[2];
            args.splice(0, 3);
            break;
        case "--input-folder":
            if (OPTIONS.command !== "download") {
                logger.e(`Not using download command!`);
                process.exit(0);
            } else if (!args[1] || !fs.existsSync(args[1])) {
                logger.e(`Input dir not found: ${args[1]}`);
                process.exit(0);
            }
            OPTIONS.inputFolder = args[1];
            args.splice(0, 2);
            break;
        case "--no-fetch":
            OPTIONS.noFetch = true;
            args.splice(0, 1);
            break;
        case "--no-hash-check":
            OPTIONS.isHashCheck = false;
            args.splice(0, 1);
            break;
        case "--rebuild":
            OPTIONS.rebuild = true;
            args.splice(0, 1);
            break;
        case "--include-zip-filename":
            OPTIONS.includeZipFilename = true;
            args.splice(0, 1);
            break;
        case "--log-level":
            if (!args[1] || !logger.setLevel(args[1])) {
                logger.e(`Log level invalid: ${args[1]}`);
                process.exit(0);
            }
            logger.setLevel(args[1]);
            args.splice(0, 2);
            break;
        case "--dataset-type":
            if (OPTIONS.command !== "find") {
                logger.e(`Command is not find`);
            } else if (!args[1]) {
                logger.e(`Please specify datatype`);
                process.exit(0);
            } else if (!METADATA_TYPES.includes(args[1])) {
                logger.e(`Dataset type invalid: ${args[1]}`);
                process.exit(0);
            }
            OPTIONS.searchArgs.datasetType = args[1];
            args.splice(0, 2);
            break;


        default:
            logger.e(`Unknown command! ${args[0]}`);
            process.exit(0);
            break;
    }
}

let maxData = 5000;
let numChunk = 0;
let writeBuffer = [];

function flushBuffer (outputPath) {
    if (writeBuffer.length > 0) {
        let content = "[" + writeBuffer.join(",") + "]";
        writeBuffer = [];
        let filePath = path.join(outputPath, `chunk${numChunk++}.json`);
        fs.writeFile(filePath, content, (err) =>{
            if (err) {
                logger.w(`Write error ${filePath} `, err.message);
            }
        });
    }
}

function writeFile (filePath, buffer) {
    if (writeBuffer.length < maxData) {
        writeBuffer.push(buffer.toString());
    } else {
        flushBuffer(path.dirname(filePath));
        writeBuffer.push(buffer.toString());
    }
}

async function unzip(zipFilePath, cbContent, datetimeFilter = null, samplingRateMillisec = null) {
    const samplingTable = [];
    logger.d(`Sampling table === 0 ?`, samplingTable.length === 0);
    return fs.createReadStream(zipFilePath)
        .pipe(unzipper.Parse())
        .on("entry", entry => {
            let entryPath = entry.path;
            let entryType = entry.type;
            let pass = false;
            if (entryType === "File" && entryPath.match(/\.(json|txt)$/)) {
                pass = true;
                if (datetimeFilter) {
                    let fn = path.basename(entry.path).replace(/\.(json|txt)$/, "");
                    let datetimeObj = { datetimeIso: dataUtils.getCleanDatetime(fn) };
                    if (filter.compare(datetimeFilter, datetimeObj)) {
                        logger.d(datetimeFilter, datetimeObj, filter.compare(datetimeFilter, datetimeObj));
                    } else {
                        pass = false;
                    }
                }
                if (samplingRateMillisec) {
                    let fn = path.basename(entry.path).replace(/\.(json|txt)$/, "");
                    let sampledIdx = parseInt(new Date(dataUtils.getCleanDatetime(fn)).getTime() / samplingRateMillisec);
                    if (samplingTable.includes(sampledIdx)) {
                        pass = false;
                    } else {
                        logger.d(sampledIdx, samplingTable.includes(sampledIdx));
                        samplingTable.push(sampledIdx);
                    }
                }
            }
            if (pass) {
                entry.buffer().then(content => {
                    cbContent(content, entryPath, zipFilePath);
                });
            } else {
                entry.autodrain();
            }
        }).on("error", err => {
            logger.w(`zipfile error: ${zipFilePath} `, err.message);
        }).promise();
}

switch (OPTIONS.command) {
    case "download": {
        // Sanity checks
        let metadata;
        try {
            metadata = JSON.parse(fs.readFileSync(OPTIONS.metadataPath));
        } catch(e) {
            logger.e("Cannot JSON parse metadata file!");
            process.exit(0);
        }
        if (metadata.dataFolder === undefined) {
            logger.e("No data folder on metadata file!");
            process.exit(0);
        }
        if (metadata.numChunk === undefined) {
            logger.e("No numChunk information on metadata file!");
            process.exit(0);
        }
        if (metadata.type === undefined) {
            metadata.type = "dataset";
        } else if (!METADATA_TYPES.includes(metadata.type)) {
            logger.e(`Metadata type invalid: ${metadata.type}!`);
            process.exit(0);
        }

        // Setup
        numChunk = metadata.numChunk;
        const outputPath = path.join(path.dirname(OPTIONS.metadataPath), metadata.dataFolder);
        const datasetType = metadata.type === "ml_csv" ? "dataset" : metadata.type;
        let datetimeFilter = null;
        if (datasetType === "dataset" && metadata.filter && metadata.filter.datetimeIso) {
            datetimeFilter = { datetimeIso: metadata.filter.datetimeIso };
        }
        logger.i(`Output path: ${outputPath}`);
        if (OPTIONS.rebuild) {
            // Clear fileList
            metadata.fileList = [];
            numChunk = 0;
            // Delete outputPath and create it again
            storage.removeDir(outputPath);
            fs.mkdirSync(outputPath, { recursive: true });
        } else {
            // Check if outputPath and fileList exists, else create it
            if (!fs.existsSync(outputPath)) {
                fs.mkdirSync(outputPath, { recursive: true });
            }
            if (!metadata.fileList) {
                metadata.fileList = [];
            }
        }
        if (metadata.samplingRateMillisec) {
            logger.i(`Using sampling rate: ${metadata.samplingRateMillisec} ms`);
        }
        if (metadata.filter) {
            logger.i(`Using filter:`, metadata.filter);
            if (datetimeFilter) logger.d(`datetimeFilter:`, datetimeFilter);
        }
        if (metadata.type === "ml_csv") {
            // Setup mlCsv helper
            if (metadata.limit) mlCsv.setLimit(metadata.limit);
        }

        let numFiles = 0;
        const promises = [];
        let done = function () {
            // Write down metadata to update file list
            Promise.allSettled(promises)
                .then(data => {
                    // Do nothing since we don't care about the results
                }).catch(err => {
                    logger.e(err);
                }).finally(() => {
                    if (metadata.type === "ml_csv") {
                        // Do something else
                    } else if (writeBuffer.length > 0) {
                        flushBuffer(outputPath);
                    }
                    progressBar.stop();
                    metadata.numChunk = numChunk;
                    logger.i(`Writing down metadata...`);
                    fs.writeFileSync(OPTIONS.metadataPath, JSON.stringify(metadata, null, 4));
                    logger.i(`Finished!`);
                });
        }
        let cbContentNormal = function (content, entryPath, zipFilePath) {
            let outputFilePath = path.join(outputPath, path.basename(entryPath));
            let contentJson = null;
            try {
                contentJson = JSON.parse(content);
                if (OPTIONS.includeZipFilename) {
                    contentJson.zipFilename = path.basename(zipFilePath);
                    content = JSON.stringify(contentJson);
                }
                if (metadata.filter) {
                    if (filter.compare(metadata.filter, contentJson)) {
                        writeFile(outputFilePath, content);
                    }
                } else {
                    writeFile(outputFilePath, content);
                }
            } catch(err) {
                logger.w(`JSON parse error: ${zipFilePath}, ${entryPath} `, err.message);
            }
        }
        let cbContentMlCsv = function (content, entryPath, zipFilePath) {
            let contentJson = null;
            try {
                contentJson = JSON.parse(content);
                if (metadata.filter) {
                    if (filter.compare(metadata.filter, contentJson)) {
                        mlCsv.insert(outputPath, contentJson, zipFilePath);
                    }
                } else {
                    mlCsv.insert(outputPath, contentJson, zipFilePath);
                }
            } catch(err) {
                logger.w(`JSON parse error: ${zipFilePath}, ${entryPath} `, err.message);
            }
        }
        let processFiles = function (files, metadataType) {
            numFiles = 0;
            progressBar.start(files.length, 0);
            files.forEach(file => {
                logger.d(`Processing ${file}`);
                promises.push(
                    unzip(file,
                        (metadataType === "ml_csv") ? cbContentMlCsv : cbContentNormal,
                        (datasetType === "dataset" && datetimeFilter) ? datetimeFilter : null,
                        metadata.samplingRateMillisec)
                    .then(() => {
                        logger.v(`Done processing zipfile: ${file}`);
                        progressBar.increment();
                        metadata.fileList.push(file);
                        if (metadataType === "ml_csv") {
                            mlCsv.write(file);
                        }
                    })
                );
            });
        }

        if (OPTIONS.inputFolder) {
            // Use input folder
            logger.i(`Using input folder: ${OPTIONS.inputFolder}`);
            storage.listDir(OPTIONS.inputFolder).then(cacheFiles => {
                metadata.fileList.forEach(filePath => {
                    let idx = cacheFiles.indexOf(filePath);
                    if (idx >= 0) {
                        cacheFiles.splice(idx, 1);
                    }
                });
                processFiles(cacheFiles, metadata.type);
            }).catch(err => {
                logger.e(err);
            }).finally(done);
        } else if (OPTIONS.noFetch) {
            // Use cache files directly
            logger.i(`Using cache files directly...`);
            storage.listCache(datasetType).then(cacheFiles => {
                metadata.fileList.forEach(filePath => {
                    let idx = cacheFiles.indexOf(filePath);
                    if (idx >= 0) {
                        cacheFiles.splice(idx, 1);
                    }
                });
                processFiles(cacheFiles, metadata.type);
            }).catch(err => {
                logger.e(err);
            }).finally(done);
        } else {
            // Fetch cache files
            logger.i(`Fetching files from server...`);
            storage.fetchBucket(datasetType, OPTIONS.isHashCheck).then(data => {
                logger.i(`Files downloaded! Processing zipfiles...`);
                metadata.fileList.forEach(filePath => {
                    let idx = data.findIndex(elem => elem[0] == filePath);
                    if (idx >= 0 && data[idx][1]) {
                        data.splice(idx, 1);
                    }
                });
                processFiles(data.map(x => x[0]), metadata.type);
            }).catch(err => {
                logger.e(err);
            }).finally(done);
        }
        break;
    }
    case "upload": {
        storage.upload(OPTIONS.inputPath, OPTIONS.outputPath).then(data => {
            data.forEach(file => {
                if (!file[1]) {
                    logger.i(`File ${file[0]} was not in server`);
                }
            });
        }).catch(err => {
            logger.e(err);
        })
        break;
    }
    case "fetch": {
        storage.fetchBucket(null, OPTIONS.isHashCheck).then(data => {
            data.forEach((file) => {
                if (!file[1]) {
                    logger.i(`File ${file[0]} was not in cache`);
                }
            });
        }).catch(err => {
            logger.e(err);
        });
        break;
    }
    case "fetch-dataset": {
        storage.fetchBucket("dataset", OPTIONS.isHashCheck).then(data => {
            data.forEach((file) => {
                if (!file[1]) {
                    logger.i(`File ${file[0]} was not in cache`);
                }
            });
        }).catch(err => {
            logger.e(err);
        });
        break;
    }
    case "fetch-speedtest": {
        storage.fetchBucket("fcc_speedtest", OPTIONS.isHashCheck).then(data => {
            data.forEach((file) => {
                if (!file[1]) {
                    logger.i(`File ${file[0]} was not in cache`);
                }
            });
        }).catch(err => {
            logger.e(err);
        });
        break;
    }
    case "clean-dataset": {
        storage.cleanCache("dataset");
        break;
    }
    case "clean-speedtest": {
        storage.cleanCache("fcc_speedtest");
        break;
    }
    case "clean-all": {
        storage.cleanCache();
        break;
    }
    case "find": {
        let term = OPTIONS.searchArgs.filter;
        let datasetType = OPTIONS.searchArgs.datasetType;
        let mode = "filename";
        const outputPath = OPTIONS.outputPath;
        try {
            let jsonFilter = JSON.parse(term);
            if (typeof jsonFilter === "object") {
                mode = "filter";
                term = jsonFilter;
            }
        } catch (err) {
            logger.e(`JSON parse error on search term: ${term}`, err.message);
            process.exit(0);
        }
        logger.i(`Finding using ${mode}:`, term);
        let numFound = 0;

        const promises = [];
        let done = function () {
            // Write down metadata to update file list
            Promise.allSettled(promises)
                .then(data => {
                    // Do nothing since we don't care about the results
                }).catch(err => {
                    logger.e(err);
                }).finally(() => {
                    if (writeBuffer.length > 0) {
                        flushBuffer(outputPath);
                    }
                    progressBar.stop();
                    logger.i(`Finished! ${numFound} file(s) found!`);
                });
        }
        let cbContent = function (content, entryPath, zipFilePath) {
            let outputFilePath = path.join(outputPath, path.basename(entryPath));
            let contentJson = null;
            try {
                contentJson = JSON.parse(content);
                if (OPTIONS.includeZipFilename) {
                    contentJson.zipFilename = path.basename(zipFilePath);
                    content = JSON.stringify(contentJson);
                }
                if (mode === "filename") {
                    let filename = path.basename(entryPath);
                    if (filename.match(term)) {
                        writeFile(outputFilePath, content);
                        ++numFound;
                    }
                } else if (mode === "filter") {
                    if (filter.compare(term, contentJson)) {
                        writeFile(outputFilePath, content);
                        ++numFound;
                    }
                } else {
                    logger.e(`Find mode should not reached: ${mode}`);
                }
            } catch(err) {
                logger.w(`JSON parse error: ${zipFilePath}, ${entryPath} `, err.message);
            }
        }
        let processFiles = function (files) {
            progressBar.start(files.length, 0);
            files.forEach(file => {
                logger.d(`Processing ${file}`);
                promises.push(
                    unzip(file, cbContent).then(() => {
                        logger.v(`Done processing zipfile: ${file}`);
                        progressBar.increment();
                    })
                );
            });
        }

        if (OPTIONS.inputFolder) {
            // Use input folder
            logger.i(`Using input folder: ${OPTIONS.inputFolder}`);
            storage.listDir(OPTIONS.inputFolder).then(cacheFiles => {
                processFiles(cacheFiles);
            }).catch(err => {
                logger.e(err);
            }).finally(done);
        } else if (OPTIONS.noFetch) {
            // Use cache files directly
            logger.i(`Using cache files directly...`);
            storage.listCache(datasetType).then(cacheFiles => {
                processFiles(cacheFiles);
            }).catch(err => {
                logger.e(err);
            }).finally(done);
        } else {
            // Fetch cache files
            logger.i(`Fetching files from server...`);
            storage.fetchBucket(datasetType, OPTIONS.isHashCheck).then(data => {
                logger.i(`Files downloaded! Processing zipfiles...`);
                processFiles(data.map(x => x[0]));
            }).catch(err => {
                logger.e(err);
            }).finally(done);
        }
        break;
    }
    default: {
        logger.e("Should not be reached");
        break;
    }
}
