const logger = require("../lib/logger");
const { initializeApp, cert } = require('firebase-admin/app');
const { getStorage } = require('firebase-admin/storage');
const cliProgress = require("cli-progress");
const crypto = require("crypto");
const fs = require("fs");
const path = require("path");
const util = require("util");
// Firebase admin auth file
const serviceAccount = require("./auth/laa-cell-map-a3e01842e98c.json");

const cacheDir = path.join(__dirname, ".cache");
const exists = util.promisify(fs.exists);
const mkdir = util.promisify(fs.mkdir);

const progressBar = new cliProgress.SingleBar({
    clearOnComplete: true,
    etaBuffer: 100,
    linewrap: true
}, cliProgress.Presets.shades_classic);
logger.progressBars.push(progressBar);

let maxNumDl = 5;
let numDl = 0;
let numFiles = 0;

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function hashFile(path) {
    return new Promise((resolve, reject) => {
        const hash = crypto.createHash("md5");
        const stream = fs.createReadStream(path);
        stream.on('error', err => reject(err));
        stream.on('data', chunk => hash.update(chunk));
        stream.on('end', () => resolve(hash.digest("base64")));
    });
}

async function download (file, path, msg) {
    if (numDl < maxNumDl) {
        logger.d(msg);
        numDl++;
        let data = await file.download({
            destination: path
        });
        numDl--;
        return
    } else {
        let timeoutMs = Math.random() * 10000;
        await timeout(timeoutMs);
        return await download(file, path, msg);
    }
}

async function doUpload (file, bucketFile, msg) {
    if (numDl < maxNumDl) {
        if (msg) logger.d(msg);
        numDl++;
        let data = await bucketFile.save(fs.readFileSync(file));
        numDl--;
        return
    } else {
        let timeoutMs = Math.random() * 10000;
        await timeout(timeoutMs);
        return await doUpload(file, bucketFile, msg);
    }
}

async function uploadCheck (file, bucketFile) {
    logger.v(`upload check: ${bucketFile.name}, ${file}`);
    let temp = await bucketFile.exists();
    let isRemoteExists = temp[0];
    if (!isRemoteExists) {
        await doUpload(file, bucketFile, `${bucketFile.name} is not exist in server, uploading...`);
        logger.d(`${file.name} uploaded!`);
    } else {
        logger.v(`${bucketFile.name} exists, checking md5 hash`);
        let data = await bucketFile.getMetadata();
        let hash = data[0].md5Hash;
        let inputHash = await hashFile(file);
        if (hash !== inputHash) {
            isRemoteExists = false;
            await doUpload(file, bucketFile, `re-uploading ${bucketFile.name} since it does not match local file, source ${hash} cache ${inputHash}`);
            logger.d(`${file.name} uploaded!`);
        } else {
            logger.v(`hash match: ${bucketFile.name}`);
        }
    }
    progressBar.increment();
    return [bucketFile.name, isRemoteExists];
}

async function getCacheFile (file, isHashCheck = true) {
    let fileInCache = path.join(cacheDir, file.name);
    let dirOfFile = path.dirname(fileInCache);
    let isDirExist = await exists(dirOfFile);
    if (!isDirExist) {
        await mkdir(dirOfFile, { recursive: true });
    }
    let isCacheExists = await exists(fileInCache);

    if (!isCacheExists) {
        await download(file, fileInCache, `cache not exists! downloading file ${file.name}`);
        logger.d(`${file.name} downloaded!`);
    } else if (isHashCheck) {
        let data = await file.getMetadata();
        let hash = data[0].md5Hash;
        let cacheHash = await hashFile(fileInCache);
        if (hash !== cacheHash) {
            isCacheExists = false;
            await download(file, fileInCache, `cache does not match! re-downloading file ${file.name}, source ${hash} cache ${cacheHash}`);
            logger.d(`${file.name} downloaded!`);
        } else {
            logger.v(`cache match: ${fileInCache}`);
        }
    } else {
        logger.v(`cache exist but skipping hash check: ${fileInCache}`);
    }
    progressBar.update(++numFiles);
    return [fileInCache, isCacheExists];
}

function listDir (currPath) {
    let output = [];
    if (fs.existsSync(currPath)) {
        const files = fs.readdirSync(currPath);

        if (files.length > 0) {
            files.forEach(function(filename) {
                if (fs.statSync(path.join(currPath, filename)).isDirectory()) {
                    output = output.concat(listDir(path.join(currPath, filename)));
                } else {
                    output.push(path.join(currPath, filename));
                }
            });
        }
    }
    return output;
}

let storage = {
    init: false,

    setLogLevel: function(level) {
        logger.setLevel(level);
    },

    getBucket: function () {
        if (!this.init) {
            initializeApp({
                credential: cert(serviceAccount),
                storageBucket: "laa-cell-map.appspot.com"
            });
            this.init = true;
        }

        return getStorage().bucket();
    },

    listBucket: async function() {
        let storageBucket = this.getBucket();
        let data = await storageBucket.getFiles();
        return data[0];
    },

    fetchBucket: async function(subfolder, isHashCheck = true) {
        const files = await this.listBucket();
        const regex = new RegExp(`^${subfolder}\/.+\.zip$`);
        const promises = [];
        numFiles = 0;
        progressBar.start(files.length, 0);
        for (let file of files) {
            if (subfolder && !file.name.match(regex)){
                progressBar.update(++numFiles);
                continue;
            }
            promises.push(getCacheFile(file, isHashCheck));
        };
        return new Promise((resolve, reject) => {
            const filesInCache = [];
            Promise.allSettled(promises)
                .then(results => {
                    results.forEach(data => {
                        if (data.status === "fulfilled") {
                            filesInCache.push(data.value);
                        }
                    });
                }).finally(data => {
                    progressBar.stop();
                    resolve(filesInCache);
                });
        });
    },

    upload: async function(inputPath, outputPath) {
        let storageBucket = this.getBucket();
        let inputFiles = listDir(inputPath);
        const promises = [];
        progressBar.start(inputFiles.length, 0);
        inputFiles.forEach(file => {
            if (file.match(/\.zip$/)) {
                let destination = path.join(outputPath, path.relative(inputPath, file));
                let bucketFile = storageBucket.file(destination);
                promises.push(uploadCheck(file, bucketFile));
            } else {
                progressBar.increment();
            }
        });

        return new Promise((resolve, reject) => {
            const filesUploaded = [];
            Promise.allSettled(promises)
                .then(results => {
                    results.forEach(data => {
                        if (data.status === "fulfilled") {
                            filesUploaded.push(data.value);
                        }
                    });
                }).finally(data => {
                    progressBar.stop();
                    resolve(filesUploaded);
                });
        });
    },

    listCache: async function(subfolder = "") {
        return this.listDir(path.join(cacheDir, subfolder));
    },

    listDir: async function(inputDir) {
        return new Promise((resolve, reject) => {
            resolve(listDir(inputDir))
        });
    },

    cleanCache: function(subfolder = "") {
        this.removeDir(path.join(cacheDir, subfolder));
    },

    removeDir: function (currPath) {
        if (fs.existsSync(currPath)) {
            const files = fs.readdirSync(currPath);

            if (files.length > 0) {
                files.forEach(function(filename) {
                    if (fs.statSync(path.join(currPath, filename)).isDirectory()) {
                        this.removeDir(path.join(currPath, filename));
                    } else {
                        fs.unlinkSync(path.join(currPath, filename));
                    }
                });
                fs.rmdirSync(currPath);
            } else {
                fs.rmdirSync(currPath);
            }
        }
    },

    organize: function(isSandbox = true) {
    }
};

module.exports = storage;