const aggregator = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const logger = require('./lib/logger');
const filter = require('./lib/filter');
const commandParser = require('./lib/command-parser');
const fs = require('fs');

const TEST_LIST = [ "download", "upload", "latency" ];

commandParser.addArgument("input path", { isMandatory: true });
commandParser.addArgument("output path", { defaultValue: "output-fcc.csv", transform: (input) => {
    if (!input.endsWith(".csv")) {
        input += ".csv";
    }
    return input;
} });
commandParser.addArgument("filter", { keyword: "--filter", transform: (input) => {
    try {
        return JSON.parse(input);
    } catch (err) {
        console.log(`Filter parsing error!`, err);
        process.exit(1);
    }
} });
const options = commandParser.parse();

const checkSuccessAndProcess = function (obj, processFn = (input) => { input }) {
    if (obj.success_flag) {
        return processFn(obj);
    }
    return "FAILURE";
}

const countNetworkSubtype = function (arr) {
    const count = { NRSA: 0, NRNSA: 0, LTE: 0 };
    for (let entry of arr) {
        if (count[entry.network_subtype] !== undefined) {
            count[entry.network_subtype]++;
        }
    }
    return count;
}

const simpleCompareObj = function (a, b) {
    for (const key in a) {
        if (a[key] !== b[key]) {
            return false;
        }
    }
    return true;
}

const testIdList = [];
const output = [];
const allCells = [];
const headerItems = {
    download: null, upload: null, latency: null
};

aggregator.callbackJsonRecursive(options["input path"], data => {
    let fccst = data.json
    if (options["filter"]) {
        fccst = filter.filterArray(options["filter"], fccst);
    }
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${fccst.length}`);

    for (let entry of fccst) {
        for (let submission of entry.submissions) {
            if (submission.test_id === undefined || testIdList.includes(submission.test_id)) {
                continue;
            }
            let outEntry = {
                overview: {
                    start_time: "N/A",
                    "avg_location*": {
                        latitude: NaN,
                        longitude: NaN
                    },
                    operator: submission.provider_name,
                    phone_model: submission.model,
                    device_id: submission.device_id,
                    server: "",
                    download_Mbps: checkSuccessAndProcess(submission.tests.download, (obj) => obj.bytes_sec ? obj.bytes_sec * 8 / 1e6 : "N/A" ),
                    upload_Mbps: checkSuccessAndProcess(submission.tests.upload, (obj) => obj.bytes_sec ? obj.bytes_sec * 8 / 1e6 : "N/A" ),
                    latency_ms: checkSuccessAndProcess(submission.tests.latency, (obj) => obj.round_trip_time ? obj.round_trip_time / 1e3 : "N/A" ),
                    jitter_ms: checkSuccessAndProcess(submission.tests.latency, (obj) => obj.jitter ? obj.jitter / 1e3 : "N/A" ),
                    packet_loss: checkSuccessAndProcess(submission.tests.latency, (obj) => (obj.packets_sent - obj.packets_received) / obj.packets_sent ),
                    "connection*": "N/A",
                    test_id: submission.test_id,
                    "unique_cell_count*": 0,
                    "unique_cell_subtype_count*": {},
                    // zip_filename
                    // pci_at_start
                    // earfcn_at_start
                    // cell_identity_at_start
                }
            }

            const avgEntryLocation = { latitude: 0, longitude: 0, k: 0 };
            const uniqueEntryCells = [];
            const uniqueEntryServers = [];
            const entryCells = [];
            for (const testName of TEST_LIST) {
                if (submission.tests[testName]) {
                    outEntry[testName] = {};

                    // Time related
                    outEntry[testName].timestamp = submission.tests[testName].timestamp;
                    if (outEntry.overview.start_time === "N/A"
                            || new Date(outEntry.overview.start_time).getTime() > new Date(submission.tests[testName].timestamp).getTime()) {
                        outEntry.overview.start_time = submission.tests[testName].timestamp;
                    }

                    outEntry[testName].success_flag = submission.tests[testName].success_flag;

                    // Process locations
                    const avgTestLocation = { latitude: 0, longitude: 0, k: 0 };
                    for (const location of submission.tests[testName].locations) {
                        avgEntryLocation.k++;
                        avgEntryLocation.latitude += (location.latitude - avgEntryLocation.latitude) / avgEntryLocation.k;
                        avgEntryLocation.longitude += (location.longitude - avgEntryLocation.longitude) / avgEntryLocation.k;
                        avgTestLocation.k++;
                        avgTestLocation.latitude += (location.latitude - avgTestLocation.latitude) / avgTestLocation.k;
                        avgTestLocation.longitude += (location.longitude - avgTestLocation.longitude) / avgTestLocation.k;
                    }
                    outEntry[testName]["avg_location*"] = {
                        latitude: avgTestLocation.latitude,
                        longitude: avgTestLocation.longitude
                    };

                    // Aggregate and count cells
                    const uniqueTestCells = [];
                    for (let cell of submission.tests[testName].cells) {
                        entryCells.push({
                            operator_name: outEntry.overview.operator,
                            cell_timestamp: cell.timestamp,
                            test_id: outEntry.overview.test_id,
                            "download_timestamp*": null,
                            "download_location_lat*": null,
                            "download_location_long*": null,
                            download_Mbps: null,
                            spectrum_band: cell.spectrum_band,
                            carrier_aggregation_flag: submission.tests[testName].carrier_aggregation_flag,
                            network_subtype: cell.network_subtype,
                            network_generation: cell.network_generation,
                            physical_cell_id: cell.physical_cell_id,
                            cqi: cell.cqi,
                            rsrp: cell.rsrp,
                            rsrq: cell.rsrq,
                            rssi: cell.rssi,
                            sinr: cell.sinr,
                            arfcn: cell.arfcn,
                            cell_id: cell.cell_id,
                            csi_rsrp: cell.csi_rsrp,
                            csi_rsrq: cell.csi_rsrq,
                            csi_sinr: cell.csi_sinr,
                        });
                        let temp = {
                            physical_cell_id: cell.physical_cell_id,
                            network_subtype: cell.network_subtype,
                            arfcn: cell.arfcn
                        };
                        if (uniqueTestCells.findIndex(val => simpleCompareObj(val, temp)) === -1) {
                            uniqueTestCells.push(temp);
                        }
                        if (uniqueEntryCells.findIndex(val => simpleCompareObj(val, temp)) === -1) {
                            uniqueEntryCells.push(temp);
                        }
                    }
                    outEntry[testName]["connection*"] = submission.tests[testName].connection_type;
                    outEntry[testName]["unique_cell_count*"] = uniqueTestCells.length;
                    outEntry[testName]["unique_cell_subtype_count*"] = countNetworkSubtype(uniqueTestCells);
                    if (outEntry[testName]["connection*"] === "cell") {
                        for (const type in outEntry[testName]["unique_cell_subtype_count*"]) {
                            if (outEntry[testName]["unique_cell_subtype_count*"][type] > 0) {
                                outEntry[testName]["connection*"] = type;
                                break;
                            }
                        }
                    } else {
                        outEntry[testName]["connection*"] = outEntry.overview["connection*"].toUpperCase();
                    }
                    if (outEntry.overview["connection*"] === "N/A") {
                        outEntry.overview["connection*"] = outEntry[testName]["connection*"];
                    } else if (outEntry.overview["connection*"] !== outEntry[testName]["connection*"]) {
                        outEntry.overview["connection*"] = "MIXED";
                    }
                    outEntry[testName].carrier_aggregation_flag = submission.tests[testName].carrier_aggregation_flag;

                    // Aggregate servers
                    outEntry[testName].server = submission.tests[testName].targets.join(";");
                    for (const server of submission.tests[testName].targets) {
                        if (!uniqueEntryServers.includes(server)) {
                            uniqueEntryServers.push(server);
                        }
                    }

                    // copy params
                    outEntry[testName].duration = submission.tests[testName].duration;
                    if (testName === "latency") {
                        outEntry[testName].round_trip_time = submission.tests[testName].round_trip_time;
                        outEntry[testName].jitter = submission.tests[testName].jitter;
                        outEntry[testName].packets_sent = submission.tests[testName].packets_sent;
                        outEntry[testName].packets_received = submission.tests[testName].packets_received;
                    } else {
                        outEntry[testName].bytes_sec = submission.tests[testName].bytes_sec;
                        outEntry[testName].bytes_transferred = submission.tests[testName].bytes_transferred;
                    }
                    if (!headerItems[testName]) {
                        headerItems[testName] = outEntry[testName];
                    }
                }
            }
            outEntry.overview["avg_location*"].latitude = avgEntryLocation.latitude;
            outEntry.overview["avg_location*"].longitude = avgEntryLocation.longitude;
            outEntry.overview.server = uniqueEntryServers.join(";");
            outEntry.overview["unique_cell_count*"] = uniqueEntryCells.length;
            outEntry.overview["unique_cell_subtype_count*"] = countNetworkSubtype(uniqueEntryCells);

            for (var i = entryCells.length - 1; i >= 0; i--) {
                entryCells[i]["download_timestamp*"] = outEntry.download ? outEntry.download.timestamp : outEntry.overview.start_time ;
                entryCells[i]["download_location_lat*"] = outEntry.download ? outEntry.download["avg_location*"].latitude : outEntry.overview["avg_location*"].latitude ;
                entryCells[i]["download_location_long*"] = outEntry.download ? outEntry.download["avg_location*"].longitude : outEntry.overview["avg_location*"].longitude ;
                entryCells[i].download_Mbps = outEntry.overview.download_Mbps;
            }
            allCells.push(...entryCells);

            output.push(outEntry);
        }
    }
});

// Sort on time
output.sort((a,b) => (a.overview.start_time < b.overview.start_time) ? -1 : (a.overview.start_time > b.overview.start_time) ? 1 : 0);
allCells.sort((a,b) => (a.cell_timestamp < b.cell_timestamp) ? -1 : (a.cell_timestamp > b.cell_timestamp) ? 1 : 0);

// Write as CSV
logger.i(`# of data: ${output.length}`);
logger.i(`Writing to ${options["output path"]}`);
let os = fs.createWriteStream(options["output path"]);

os.write(csvUtils.unpackKeys(output[0].overview, "")
    + (headerItems.download ? csvUtils.unpackKeys(headerItems.download, "download") : "")
    + (headerItems.upload ? csvUtils.unpackKeys(headerItems.upload, "upload") : "")
    + (headerItems.latency ? csvUtils.unpackKeys(headerItems.latency, "latency") : "")
    + "\n");
for (let entry of output){
    os.write(csvUtils.unpackVals(entry.overview, false)
        + `${entry.download ? csvUtils.unpackVals(entry.download) : (headerItems.download ? csvUtils.getBlanks(headerItems.download) : "")}`
        + `${entry.upload ? csvUtils.unpackVals(entry.upload) : (headerItems.upload ? csvUtils.getBlanks(headerItems.upload) : "")}`
        + `${entry.latency ? csvUtils.unpackVals(entry.latency) : (headerItems.latency ? csvUtils.getBlanks(headerItems.latency) : "")}`
        + "\n");
}
os.close();

if (allCells.length > 0) {
    let cellOutputPath = options["output path"].replace(/\.csv$/, "-cells.csv");
    logger.i(`# of cell data: ${allCells.length}`);
    logger.i(`Writing cell data to ${cellOutputPath}`);
    os = fs.createWriteStream(cellOutputPath);

    os.write(csvUtils.unpackKeys(allCells[0]) + "\n");
    for (let entry of allCells) {
        os.write(csvUtils.unpackVals(entry) + "\n");
    }
}
os.close();

