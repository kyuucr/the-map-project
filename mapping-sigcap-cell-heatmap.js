const agg = require('./lib/aggregator');
const cellHelper = require('./lib/cell-helper');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const geohelper = require('./lib/geohelper');
const powerUtils = require('./lib/power-utils');
const fs = require('fs');
const path = require('path');

////////////////////////////////
// Definitions
////////////////////////////////

const OPTIONS = {
    inputDir: null, // SigCap input dir
    tileSize: 12.5,   // array tile size
    accTh: -1,  // threshold of accuracy
    centroid: false, // output each latLng as centroid?
    filter: null,
    pciSelect: -1,
    noNr: false,
    noLte: false,
    minZoomLevel: 10,
    maxZoomLevel: 15,
    multipleZoomOutput: true,
    outputPath: path.join("html", "outputs", "nr-heatmaps.geojson") // output path
};

let createGeoJson = function (heatmap, options, lengths, zoomFilter = -1) {
    console.log("Creating output geoJSON");
    let output = {
        OPTIONS: options,
        type: "FeatureCollection",
        features: []
    };
    for (let op in heatmap) {
        for (let type in heatmap[op]) {
            for (let bandName in heatmap[op][type]) {
                for (let zoomIdx in heatmap[op][type][bandName]) {
                    // If filter set, exclude zoomIdx !== zoomFilter
                    let zoomIdxInt = parseInt(zoomIdx);
                    if (zoomFilter !== -1 && zoomIdxInt !== zoomFilter) {
                        continue;
                    }
                    for (let idx in heatmap[op][type][bandName][zoomIdx]) {
                        let latLng = idx.split("-");
                        let latIdx = parseFloat(latLng[0]);
                        let lngIdx = parseFloat(latLng[1]);
                        let lat0 = options.minLat + (latIdx * lengths[zoomIdx].lat);
                        let lat1 = options.minLat + ((latIdx + 1) * lengths[zoomIdx].lat);
                        let lng0 = options.minLng + (lngIdx * lengths[zoomIdx].lng);
                        let lng1 = options.minLng + ((lngIdx + 1) * lengths[zoomIdx].lng);
                        let weight;
                        if (type === "nr") {
                            weight = (heatmap[op][type][bandName][zoomIdx][idx].rsrp - options.minSsrsrp) / options.rangeSsrsrp;
                        } else {
                            weight = (heatmap[op][type][bandName][zoomIdx][idx].rsrp - options.minRsrp) / options.rangeRsrp;
                        }
                        let zoomLevel = options.minZoomLevel + zoomIdxInt;

                        let temp = {
                            type: "Feature",
                            properties: {
                                isVisible: false,
                                IDX: `${op}-${type}-${bandName}-zoom${zoomLevel}-${idx}`,
                                OP: op,
                                TYPE: type,
                                BAND: bandName,
                                WEIGHT: weight,
                                ZOOM: zoomLevel,
                                RSRP: heatmap[op][type][bandName][zoomIdx][idx].rsrp,
                                NUM: heatmap[op][type][bandName][zoomIdx][idx].k
                            },
                            geometry: {
                                type: "Polygon",
                                coordinates: [
                                    [
                                        [ lng0, lat0 ],
                                        [ lng1, lat0 ],
                                        [ lng1, lat1 ],
                                        [ lng0, lat1 ],
                                        [ lng0, lat0 ]
                                    ]
                                ]
                            }
                        }
                        output.features.push(temp);
                    }
                }
            }
        }
    }
    return output;
};

////////////////////////////////
// Command line
////////////////////////////////

let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`
Usage: node ${path.basename(__filename)} <SigCap input dir>
            [output path with filename (default: "html/outputs/nr-heatmaps.geojson")]
            [--tile-size array tile size in meter at maximum zoom level (default: 12.5)]
            [--min-zoom minimum Google Map zoom level (default: 10)]
            [--max-zoom maximum Google Map zoom level (default: 15)]
            [--multiple-zoom-output]
            [--acc-th GPS accuracy threshold in meter (default: unset)]
            [--filter filter input string or path (default: unset)]
            [--pci only consider this PCI when calculating LTE average RSRP (default: unset)]
            [--centroid flag to enable centroid for lat-lng output (default: unset)]
            [--no-lte flag to ignore LTE cell data (default: unset)]
            [--no-nr flag to ignore NR cell data (default: unset)]`);
    process.exit(0);
}

while (args.length > 0) {
    switch (args[0]) {
        case "--tile-size":
            OPTIONS.tileSize = parseFloat(args[1]);
            args.splice(0, 2);
            break;
        case "--acc-th":
            OPTIONS.accTh = parseFloat(args[1]);
            args.splice(0, 2);
            break;
        case "--min-zoom":
            OPTIONS.minZoomLevel = parseInt(args[1]);
            args.splice(0, 2);
            break;
        case "--max-zoom":
            OPTIONS.maxZoomLevel = parseInt(args[1]);
            args.splice(0, 2);
            break;
        case "--multiple-zoom-output":
            OPTIONS.multipleZoomOutput = true;
            args.splice(0, 1);
            break;
        case "--centroid":
            OPTIONS.centroid = true;
            args.splice(0, 1);
            break;
        case "--no-lte":
            OPTIONS.noLte = true;
            args.splice(0, 1);
            break;
        case "--no-nr":
            OPTIONS.noNr = true;
            args.splice(0, 1);
            break;
        case "--output":
            OPTIONS.outputPath = args[1];
            args.splice(0, 2);
            break;
        case "--filter":
            let jsonString = args[1];
            if (jsonString.match(/\.json$/)) jsonString = fs.readFileSync(jsonString);
            OPTIONS.filter = JSON.parse(jsonString);
            args.splice(0, 2);
            break;
        case "--pci":
            OPTIONS.pciSelect = parseInt(args[1]);
            args.splice(0, 2);
            break;

        default:
            if (OPTIONS.inputDir !== null) {
                OPTIONS.outputPath = args[0];
            } else {
                OPTIONS.inputDir = args[0];
            }
            args.splice(0, 1);
            break;
    }
}

if (OPTIONS.noNr && OPTIONS.noLte) {
    console.log(`Please specify either --no-lte or --no-nr, but not both!`);
    process.exit(1);
}

if (OPTIONS.accTh > 0) console.log(`Using accuracy threshold: ${OPTIONS.accTh} m`);
console.log(`Using tile size: ${OPTIONS.tileSize} m`);

const zoomRange = OPTIONS.maxZoomLevel - OPTIONS.minZoomLevel;
// zoomLevel: 0 (biggest tile, zoomed out, at max zoom level) ----> zoomRange (smallest tile, zoomed in, at max zoom level)
const tileSizeAtZoom = function(zoomLevel) {
    // Invert zoom level: 2^(zoomRange - zoomLevel)
    return Math.pow(2, zoomRange - zoomLevel) * OPTIONS.tileSize;
}
console.log(`Google Map zoom level ${OPTIONS.minZoomLevel} to ${OPTIONS.maxZoomLevel}`);

////////////////////////////////
// Preprocessing
////////////////////////////////
let minLat = 999.9;
let maxLat = -999.9;
let minLng = 999.9;
let maxLng = -999.9;
console.log(`Preprocessing lat-lng...`);
agg.callbackJsonRecursive(OPTIONS.inputDir, data => {
    let allCells = data.json;

    if (OPTIONS.filter !== null) {
        allCells = filter.filterArray(OPTIONS.filter, allCells);
    }

    for (let i = allCells.length - 1; i >= 0; i--) {
        if (OPTIONS.noLte && allCells[i].nr_info.length === 0) {
            continue;
        } else if (OPTIONS.noNr && allCells[i].cell_info.length === 0) {
            continue;
        }
        minLat = (minLat > allCells[i].location.latitude) ? allCells[i].location.latitude : minLat;
        minLng = (minLng > allCells[i].location.longitude) ? allCells[i].location.longitude : minLng;
        maxLat = (maxLat < allCells[i].location.latitude) ? allCells[i].location.latitude : maxLat;
        maxLng = (maxLng < allCells[i].location.longitude) ? allCells[i].location.longitude : maxLng;
    }
});

console.log(`Lat-Lng area: ${minLat}, ${maxLat}, ${minLng}, ${maxLng}`);
console.log(`Center: ${(maxLat - minLat) / 2 + minLat}, ${(maxLng - minLng) / 2 + minLng}`);

// Convert tile size to lat lng degree, since it is more consistent (length of 1-degree longitude depends on the latitude)
// Now tile size is accurate to the midway between min and max latitude, and min longitude
const latLngLength = {};
for (let i = 0; i <= zoomRange; i++) {
    latLngLength[i] = {
        lat: geohelper.addLatitude(minLat, minLng, tileSizeAtZoom(i)) - minLat,
        lng: geohelper.addLongitude((maxLat - minLat) / 2 + minLat, minLng, tileSizeAtZoom(i)) - minLng,
    };
}

// This is now just informational
let latTotal = geohelper.measure(minLat, minLng, maxLat, minLng);
let lngTotalAtMinLat = geohelper.measure(minLat, minLng, minLat, maxLng);
let lngTotalAtMaxLat = geohelper.measure(maxLat, minLng, maxLat, maxLng);
console.log(`Total latitudinal length: ${latTotal}m`);
console.log(`Total longitudinal length at minimum latitude: ${lngTotalAtMinLat}m`);
console.log(`Total longitudinal length at maximum latitude: ${lngTotalAtMaxLat}m`);

////////////////////////////////
// Heatmap
////////////////////////////////

// This will work since obj is not reassigned
const updateRsrp = function(obj, type, band, idxList, rsrp) {
    if (obj[type] === undefined) obj[type] = {};
    if (obj[type][band] === undefined) obj[type][band] = {};

    // Update on all zoom levels
    for (let i = 0; i <= zoomRange; i++) {
        if (obj[type][band][i] === undefined) obj[type][band][i] = {};
        if (obj[type][band][i][idxList[i]] === undefined) {
            obj[type][band][i][idxList[i]] = {};
            obj[type][band][i][idxList[i]].k = 1;
            obj[type][band][i][idxList[i]].rsrp = rsrp;
        } else {
            ++obj[type][band][i][idxList[i]].k;
            obj[type][band][i][idxList[i]].rsrp += (rsrp - obj[type][band][i][idxList[i]].rsrp) / obj[type][band][i][idxList[i]].k;
        }
    }

};

// Note: lat = y, lng = x
console.log("Calculating heatmap of average RSRP");
let heatmap = {};
let skipped = 0;
let hashBin = [];

// TODO: implement region lookup
const currRegion = cellHelper.REGION.NAR;

agg.callbackJsonRecursive(OPTIONS.inputDir, data => {
    let allCells = data.json;

    if (OPTIONS.filter !== null) {
        allCells = filter.filterArray(OPTIONS.filter, allCells);
    }

    for (let j = allCells.length - 1; j >= 0; j--) {
        let hash = dataUtils.hashObj(allCells[j]);
        if (allCells[j].location === undefined
            || hashBin.includes(hash)
            || (OPTIONS.accTh > 0 && allCells[j].location.hor_acc > OPTIONS.accTh)) {
            skipped++;
            continue;
        }
        hashBin.push(hash);

        let idxList = {}
        for (let k = 0; k <= zoomRange; k++) {
            idxList[k] = Math.floor((allCells[j].location.latitude - minLat) / latLngLength[k].lat)
                    + "-"
                    + Math.floor((allCells[j].location.longitude - minLng) / latLngLength[k].lng);
        }
        let op = dataUtils.getCleanOp(allCells[j]);

        if (!OPTIONS.noLte && allCells[j].cell_info) {
            for (let k = allCells[j].cell_info.length - 1; k >= 0; k--) {
                // Sanity checks
                let rsrp = dataUtils.cleanSignal(allCells[j].cell_info[k].rsrp);
                if (allCells[j].cell_info[k].band === 0 || isNaN(rsrp) || rsrp === 0) continue;
                // PCI select
                if (OPTIONS.pciSelect >= 0 && allCells[j].cell_info[k].pci !== OPTIONS.pciSelect) continue;

                if (heatmap[op] === undefined) heatmap[op] = {};

                // Convert to Watts
                let curRsrp = powerUtils.dbmToW(rsrp);

                let band = cellHelper.getEarfcnBandCode(allCells[j].cell_info[k].earfcn);
                let freq = cellHelper.getEarfcnFreqCode(allCells[j].cell_info[k].earfcn);

                if (band !== "unknown") {
                    updateRsrp(heatmap[op], "lte", band, idxList, curRsrp);
                }
                updateRsrp(heatmap[op], "lte", freq, idxList, curRsrp);
                updateRsrp(heatmap[op], "lte", "all", idxList, curRsrp);

                // For primary
                if (allCells[j].cell_info[k].width > 0) {
                    updateRsrp(heatmap[op], "lte", "primary", idxList, curRsrp);
                }
            }
        }

        if (!OPTIONS.noNr && allCells[j].nr_info) {
            for (let k = allCells[j].nr_info.length - 1; k >= 0; k--) {
                // Sanity checks
                if (allCells[j].nr_info[k].ssRsrp === 2147483647) continue;

                // Convert to Watts
                let curRsrp = powerUtils.dbmToW(allCells[j].nr_info[k].ssRsrp);

                let band = "unknown";
                let freq = "unknown";
                if (allCells[j].nr_info[k].nrarfcn !== 2147483647) {
                    band = cellHelper.getNrarfcnBandCode(allCells[j].nr_info[k].nrarfcn, currRegion);
                    freq = cellHelper.getNrarfcnFreqCode(allCells[j].nr_info[k].nrarfcn);
                }
                if (band === "unknown" && dataUtils.getServiceState(allCells[j], "nrFrequencyRange") === "mmWave") {
                    freq = "mmwave";
                }

                if (heatmap[op] === undefined) heatmap[op] = {};

                if (band !== "unknown") {
                    updateRsrp(heatmap[op], "nr", band, idxList, curRsrp);
                }
                updateRsrp(heatmap[op], "nr", freq, idxList, curRsrp);
                updateRsrp(heatmap[op], "nr", "all", idxList, curRsrp);
            }
        }
    }
});


////////////////////////////////
// Output
////////////////////////////////

let minRsrp = 999.9;
let maxRsrp = -999.9;
let minSsrsrp = 999.9;
let maxSsrsrp = -999.9;
for (let op in heatmap) {
    for (let type in heatmap[op]) {
        for (let band in heatmap[op][type]) {
            for (let zoomIdx in heatmap[op][type][band]) {
                for (let idx in heatmap[op][type][band][zoomIdx]) {
                    // Convert back to dBm
                    heatmap[op][type][band][zoomIdx][idx].rsrp = powerUtils.wToDbm(heatmap[op][type][band][zoomIdx][idx].rsrp);
                    if (type === "nr") {
                        minSsrsrp = (minSsrsrp > heatmap[op][type][band][zoomIdx][idx].rsrp) ? heatmap[op][type][band][zoomIdx][idx].rsrp : minSsrsrp;
                        maxSsrsrp = (maxSsrsrp < heatmap[op][type][band][zoomIdx][idx].rsrp) ? heatmap[op][type][band][zoomIdx][idx].rsrp : maxSsrsrp;
                    } else if (type === "lte") {
                        minRsrp = (minRsrp > heatmap[op][type][band][zoomIdx][idx].rsrp) ? heatmap[op][type][band][zoomIdx][idx].rsrp : minRsrp;
                        maxRsrp = (maxRsrp < heatmap[op][type][band][zoomIdx][idx].rsrp) ? heatmap[op][type][band][zoomIdx][idx].rsrp : maxRsrp;
                    }
                }
            }
        }
    }
}
let rangeRsrp = maxRsrp - minRsrp;
let rangeSsrsrp = maxSsrsrp - minSsrsrp;
console.log (`minRsrp: ${minRsrp}, maxRsrp: ${maxRsrp}`);
console.log (`minSsrsrp: ${minSsrsrp}, maxSsrsrp: ${maxSsrsrp}`);

console.log(`Number of skipped data: ${skipped}`);

let options = {
    minLat: minLat,
    maxLat: maxLat,
    minLng: minLng,
    maxLng: maxLng,
    minRsrp: minRsrp,
    maxRsrp: maxRsrp,
    rangeRsrp: rangeRsrp,
    minSsrsrp: minSsrsrp,
    maxSsrsrp: maxSsrsrp,
    rangeSsrsrp: rangeSsrsrp,
    minZoomLevel: OPTIONS.minZoomLevel,
    maxZoomLevel: OPTIONS.maxZoomLevel,
};
if (OPTIONS.multipleZoomOutput) {
    for (let i = 0; i <= zoomRange; i++) {
        let newOutputPath = OPTIONS.outputPath.replace(/\.geojson$/, `-z${OPTIONS.minZoomLevel + i}.geojson`);
        console.log(`Writing output at ${newOutputPath}`);
        let output = createGeoJson(heatmap, options, latLngLength, i);
        fs.writeFileSync(newOutputPath, JSON.stringify(output));
    }
} else {
    console.log(`Writing output at ${OPTIONS.outputPath}`);
    let output = createGeoJson(heatmap, options, latLngLength);
    fs.writeFileSync(OPTIONS.outputPath, JSON.stringify(output));
}

console.log(`Done!`);