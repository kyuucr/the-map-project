const agg = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

const TEST_LIST = [ "download", "upload", "latency" ];
let args = process.argv.slice(2);

if (args.length < 1) {
    console.log(`Usage: node ${path.basename(__filename)} <FCC speed test dir> [output path] [--filter <filter JSON string>]`);
    process.exit(1);
}


let outputPath = "output-fcc.csv";
let inputFolder = args[0];
args.splice(0, 1);
let inputFilter;
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}


let getRsrp = function(telephony) {
    if (telephony && telephony.cell_signal && telephony.cell_signal.network_type) {
        if (telephony.cell_signal.network_type === "LTE") {
            return getKeyValue(telephony.cell_signal, "received_signal_power");
        } else if (telephony.cell_signal.network_type === "NR" || telephony.cell_signal.network_type === "NRNSA") {
            return getKeyValue(telephony.cell_signal, "received_signal_power_ss");
        } else {
            return getKeyValue(telephony.cell_signal, "signal_strength");
        }
    }
    return "N/A";
}

let getRsrq = function(telephony) {
    if (telephony && telephony.cell_signal && telephony.cell_signal.network_type) {
        if (telephony.cell_signal.network_type === "LTE") {
            return getKeyValue(telephony.cell_signal, "received_signal_quality");
        } else if (telephony.cell_signal.network_type === "NR" || telephony.cell_signal.network_type === "NRNSA") {
            return getKeyValue(telephony.cell_signal, "received_signal_quality_ss");
        }
    }
    return "N/A";
}

let getKeyValue = function(obj, key) {
    if (obj && obj[key]) return obj[key];
    return "N/A";
}

let getCellSignalKey = function(telephony, key) {
    if (telephony) return getKeyValue(telephony.cell_signal, key);
    return "N/A";
}

let isLaa = function(earfcn) {
    return earfcn >= 46790 && earfcn <= 54539;
}

let isTelephonyExists = function(recs) {
    let exists = true;
    for (let testName of TEST_LIST) {
        if (recs.tests[testName] === undefined) continue;
        for (let cond in recs.tests[testName].environment) {
            exists = exists && recs.tests[testName].environment[cond].telephony && recs.tests[testName].environment[cond].telephony.cell_signal;
        }
    }
    return exists;
}

let isNetworkExists = function(recs) {
    let exists = true;
    for (let testName of TEST_LIST) {
        if (recs.tests[testName] === undefined) continue;
        for (let cond in recs.tests[testName].environment) {
            exists = exists && recs.tests[testName].environment[cond].network;
        }
    }
    return exists;
}

let getConn = function(telephony, wifi) {
    if (telephony && telephony.connection_type) {
        if (telephony.connection_type === "cell") {
            if (telephony.cellular_technology
                && telephony.cellular_technology === "4g"
                && telephony.cell_signal
                && isLaa(telephony.cell_signal.absolute_rf_channel_number)) {
                return "4g-laa";
            }
            return telephony.cellular_technology;
        }
        return telephony.connection_type;
    } else if (wifi) {
        return "wi-fi";
    }
    return "N/A";
}

let getTarget = function(tests) {
    if (tests.download && tests.download.target) return tests.download.target;
    if (tests.upload && tests.upload.target) return tests.upload.target;
    if (tests.latency && tests.latency.target) return tests.latency.target;
    return "N/A";
}

let nullNetwork = {
    "is_available": "N/A",
    "is_connected": "N/A",
    "is_connected_or_connecting": "N/A",
    "is_failover": "N/A",
    "is_roaming": "N/A",
    "state": "N/A",
    "subtype": "N/A",
    "subtype_name": "N/A",
    "type": "N/A"
};
let nullWifi = {
    "frequency": "N/A",
    "hidden_ssid": "N/A",
    "link_speed": "N/A",
    "network_id": "N/A",
    "rssi": "N/A",
    "supplicant_state": "N/A"
};

let output = [];
let testIdList = [];
let headerItems = {
    download: null, upload: null, latency: null
};
let printZipfilename = false;
agg.callbackJsonRecursive(inputFolder, data => {
    let recs = data.json
    if (inputFilter) {
        recs = filter.filterArray(inputFilter, recs);
    }
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${recs.length}`);

    for (let i = 0, length1 = recs.length; i < length1; i++) {
        if (recs[i].tests.testId === undefined || testIdList.includes(recs[i].tests.testId)) continue;
        testIdList.push(recs[i].tests.testId);
        let aveLat, aveLon, conn, k, startTime;
        for (let test in recs[i].tests) {
            if (startTime === undefined) {
                startTime = recs[i].tests[test].local_datetime;
            }
            for (let cond in recs[i].tests[test].environment) {
                let curConn = getConn(recs[i].tests[test].environment[cond].telephony, recs[i].tests[test].environment[cond].wifi);

                if (recs[i].tests[test].environment[cond].location) {
                    if (aveLat === undefined) {
                        k = 1;
                        aveLat = recs[i].tests[test].environment[cond].location.lat;
                        aveLon = recs[i].tests[test].environment[cond].location.lon;
                    } else {
                        ++k;
                        aveLat += (recs[i].tests[test].environment[cond].location.lat - aveLat) / k;
                        aveLon += (recs[i].tests[test].environment[cond].location.lon - aveLon) / k;
                    }
                }
                if (conn === undefined) {
                    conn = curConn;
                } else if (conn !== "mixed" && conn !== curConn) {
                    conn = "mixed";
                }
            }
        }
        if (recs[i].zipFilename) printZipfilename = true;

        let temp = {
            overview: {
                operator: dataUtils.getCleanOp(recs[i]),
                avg_location: {
                    lat: aveLat,
                    lon: aveLon
                },
                time: (startTime) ? startTime : "FAILURE",
                server: getTarget(recs[i].tests),
                download_bytes_per_s: (recs[i].tests.download) ? recs[i].tests.download.throughput : "N/A",
                download_mbps: (recs[i].tests.download) ? (recs[i].tests.download.throughput * 8 / 1e6) : "N/A",
                upload_bytes_per_s: (recs[i].tests.upload) ? recs[i].tests.upload.throughput : "N/A",
                upload_mbps: (recs[i].tests.upload) ? (recs[i].tests.upload.throughput * 8 / 1e6) : "N/A",
                latency_microsec: (recs[i].tests.latency) ? recs[i].tests.latency.round_trip_time : "N/A",
                jitter_microsec: (recs[i].tests.latency) ? recs[i].tests.latency.jitter : "N/A",
                packet_loss: (recs[i].tests.latency && recs[i].tests.latency.packets_sent !== 0) ?
                                (recs[i].tests.latency.packets_sent - recs[i].tests.latency.packets_received) / recs[i].tests.latency.packets_sent :
                                "FAILURE",
                connection: conn,
                test_ID: recs[i].tests.testId,
                phone_model: recs[i].model,
                zip_filename: (recs[i].zipFilename) ? recs[i].zipFilename : "N/A",
                pci_at_start: recs[i].tests.download ? getCellSignalKey(recs[i].tests.download.environment.beginning.telephony, "physical_cell_identity") : "N/A",
                earfcn_at_start: recs[i].tests.download ? getCellSignalKey(recs[i].tests.download.environment.beginning.telephony, "absolute_rf_channel_number") : "N/A",
                cell_identity_at_start: recs[i].tests.download ? getCellSignalKey(recs[i].tests.download.environment.beginning.telephony, "cell_identity") : "N/A",
            }
        };
        // Check before further process
        // if (temp.overview.download_bytes_per_s === 0
        //     // || temp.overview.upload_bytes_per_s === 0
        //     // || temp.overview.packet_loss === "FAILURE"
        //     // || temp.overview.packet_loss < 0
        //     || !isTelephonyExists(recs[i])
        //     || !isNetworkExists(recs[i])) {
        //     continue;
        // }

        for (let testName of TEST_LIST) {
            if (recs[i].tests[testName]) {
                temp[testName] = {
                    location: {
                        beginning: recs[i].tests[testName].environment.beginning.location,
                        end: recs[i].tests[testName].environment.end.location
                    },
                    connection: {
                        beginning: getConn(recs[i].tests[testName].environment.beginning.telephony, recs[i].tests[testName].environment.beginning.wifi),
                        end: getConn(recs[i].tests[testName].environment.end.telephony, recs[i].tests[testName].environment.end.wifi)
                    },
                    network: {
                        beginning: (recs[i].tests[testName].environment.beginning.network) ? recs[i].tests[testName].environment.beginning.network : nullNetwork,
                        end: (recs[i].tests[testName].environment.end.network) ? recs[i].tests[testName].environment.end.network : nullNetwork
                    },
                    network_type: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "network_type"),
                        end:getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "network_type"),
                    },
                    pci: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "physical_cell_identity"),
                        end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "physical_cell_identity")
                    },
                    earfcn: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "absolute_rf_channel_number"),
                        end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "absolute_rf_channel_number")
                    },
                    cell_identity: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "cell_identity"),
                        end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "cell_identity")
                    },
                    cell_operator: {
                        beginning: getKeyValue(recs[i].tests[testName].environment.beginning.telephony, "cellular_operator_name"),
                        end: getKeyValue(recs[i].tests[testName].environment.end.telephony, "cellular_operator_name")
                    },
                    sim_operator: {
                        beginning: getKeyValue(recs[i].tests[testName].environment.beginning.telephony, "sim_operator_name"),
                        end: getKeyValue(recs[i].tests[testName].environment.end.telephony, "sim_operator_name")
                    },
                    rsrp: {
                        beginning: getRsrp(recs[i].tests[testName].environment.beginning.telephony),
                        end: getRsrp(recs[i].tests[testName].environment.end.telephony)
                    },
                    rsrq: {
                        beginning: getRsrq(recs[i].tests[testName].environment.beginning.telephony),
                        end: getRsrq(recs[i].tests[testName].environment.end.telephony)
                    },
                    rssnr: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "received_signal_noise_ratio"),
                        end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "received_signal_noise_ratio")
                    },
                    cqi: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "channel_quality_indicator"),
                        end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "channel_quality_indicator")
                    },
                    timing_advance: {
                        beginning: getCellSignalKey(recs[i].tests[testName].environment.beginning.telephony, "timing_advance"),
                        end: getCellSignalKey(recs[i].tests[testName].environment.end.telephony, "timing_advance")
                    },
                    wifi: {
                        beginning: recs[i].tests[testName].environment.beginning.wifi ? recs[i].tests[testName].environment.beginning.wifi: nullWifi,
                        end: recs[i].tests[testName].environment.end.wifi ? recs[i].tests[testName].environment.end.wifi: nullWifi
                    },
                    server: recs[i].tests[testName].target,
                    duration: recs[i].tests[testName].duration,
                    time: recs[i].tests[testName].local_datetime
                };
                if (testName === "latency") {
                    temp[testName].round_trip_time = recs[i].tests[testName].round_trip_time;
                    temp[testName].packets_sent = recs[i].tests[testName].packets_sent;
                    temp[testName].packets_received = recs[i].tests[testName].packets_received;
                    temp[testName].jitter = recs[i].tests[testName].jitter;
                    temp[testName].packet_size = recs[i].tests[testName].packet_size;
                    temp[testName].stream_bits_per_sec = recs[i].tests[testName].stream_bits_per_sec;
                } else {
                    temp[testName].throughput = recs[i].tests[testName].throughput;
                    temp[testName].bytes_transferred = recs[i].tests[testName].bytes_transferred;
                }
                if (!headerItems[testName]) {
                    headerItems[testName] = temp[testName];
                }
            }
        }

        output.push(temp);
    }
});

// Sort on time
output.sort((a,b) => {return (a.overview.time < b.overview.time) ? -1 : (a.overview.time > b.overview.time) ? 1 : 0});

// Write as CSV
console.log("FCC data count:", output.length);
console.log("Writing FCC speed test data as CSV");
const os = fs.createWriteStream(outputPath);

os.write(csvUtils.unpackKeys(output[0].overview, "", !printZipfilename ? [ "zip_filename" ] : [])
    + (headerItems.download ? csvUtils.unpackKeys(headerItems.download, "download") : "")
    + (headerItems.upload ? csvUtils.unpackKeys(headerItems.upload, "upload") : "")
    + (headerItems.latency ? csvUtils.unpackKeys(headerItems.latency, "latency") : "")
    + "\n");

for (let i = 0, length1 = output.length; i < length1; i++){
    os.write(csvUtils.unpackVals(output[i].overview, false, !printZipfilename ? [ "zip_filename" ] : [])
        + `${output[i].download ? csvUtils.unpackVals(output[i].download) : (headerItems.download ? csvUtils.getBlanks(headerItems.download) : "")}`
        + `${output[i].upload ? csvUtils.unpackVals(output[i].upload) : (headerItems.upload ? csvUtils.getBlanks(headerItems.upload) : "")}`
        + `${output[i].latency ? csvUtils.unpackVals(output[i].latency) : (headerItems.latency ? csvUtils.getBlanks(headerItems.latency) : "")}`
        + "\n");
}
os.close();
