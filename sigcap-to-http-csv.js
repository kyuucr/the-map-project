const agg = require('./lib/aggregator');
const csvUtils = require('./lib/csv-utils');
const cellHelper = require('./lib/cell-helper');
const dataUtils = require('./lib/data-utils');
const filter = require('./lib/filter');
const fs = require('fs');
const path = require('path');

let args = process.argv.slice(2);
if (args.length < 1) {
    console.log("Not enough argument!");
    console.log(`Usage: nodejs ${path.basename(__filename)} <input folder> [output path] [--filter <filter JSON string>]`);
    process.exit(1);
}

let outputPath = "output-sigcap-http.csv";
let inputFolder = args[0];
let inputFilter;
args.splice(0, 1);
while (args.length > 0) {
    switch (args[0]) {
        case "--filter": {
            try {
                inputFilter = JSON.parse(args[1]);
            } catch (err) {
                console.log(`Filter parsing error!`, err);
                process.exit(1);
            }
            args.splice(0, 2);
            break;
        }
        default: {
            outputPath = args[0];
            args.splice(0, 1);
            break;
        }
    }
}
console.log(`Input folder: ${inputFolder}`);

console.log(`Writing to: ${outputPath}`);
let os = fs.createWriteStream(outputPath);

// Write header
const header = `operator,sim_operator,carrier,device_id,device_model,android_ver,sigcap_ver,`
             + `timestamp,latitude,longitude,altitude,hor_acc,ver_acc,`
             + `target_url,bytes_downloaded,start_time_ns,duration_ns,tput_mbps,\n`;
os.write(header);

hashBin = [];

const getOverviewString = function(sigcap) {
    return `${dataUtils.getCleanOp(sigcap)},`
        + `${sigcap.simName ? sigcap.simName : "N/A"},`
        + `${sigcap.carrierName ? sigcap.carrierName : "N/A"},`
        + `${sigcap.uuid ? sigcap.uuid : "N/A"},`
        + `${sigcap.deviceName ? sigcap.deviceName : "N/A"},`
        + `${dataUtils.getAndroidVersion(sigcap.androidVersion)},`
        + `${sigcap.version}${sigcap.isDebug === true ? "-debug" : ""},`
        + `${dataUtils.getCleanDatetime(sigcap)},`
        + `${sigcap.location.latitude},`
        + `${sigcap.location.longitude},`
        + `${sigcap.location.altitude},`
        + `${sigcap.location.hor_acc},`
        + `${sigcap.location.ver_acc},`;
}

agg.callbackJsonRecursive(inputFolder, data => {
    let sigcap = data.json;
    if (inputFilter) {
        sigcap = filter.filterArray(inputFilter, sigcap);
    }
    console.log(`processing... path: ${data.path}, # of files: ${data.filePath.length}, # of data: ${sigcap.length}`);

    // Write csv
    for(let i = 0, length1 = sigcap.length; i < length1; i++){
        // Sanity checks
        let hash = dataUtils.hashObj(sigcap[i]);
        if (hashBin.includes(hash)) continue;
        hashBin.push(hash);
        if (sigcap[i].opName === undefined && sigcap[i].simName === undefined && sigcap[i].carrierName === undefined) continue;
        let op = dataUtils.getCleanOp(sigcap[i]);
        if (op === "Unknown") continue;
        // Skip if there is no ping data
        if (sigcap[i].http_info === undefined || sigcap[i].http_info.targetUrl === "") continue

        let tputMbps = sigcap[i].http_info.bytesDownloaded * 8e3 / sigcap[i].http_info.durationNano
        let entry = getOverviewString(sigcap[i])
            + `${sigcap[i].http_info.targetUrl},`
            + `${sigcap[i].http_info.bytesDownloaded},`
            + `${sigcap[i].http_info.startTimeNano},`
            + `${sigcap[i].http_info.durationNano},`
            + `${tputMbps}\n`
        os.write(entry);
    }
});

os.close();
